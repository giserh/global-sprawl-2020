
# This is to be used as follows:
# In you aliases, define:
# pylabnoX() {
# unset DISPLAY
# ipython --pdb -i set_backend_agg.py
# }

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

#!/usr/bin/python
"""
General file of various aspects of raw OSM data.
To be modified as needed (e.g. as OSM data is updated.
"""

"""
SEGS_TO_DELETE is a dictionary of the IDs of raw OSM data that throw fatal errors during the osm_master run.
When the number of data points is small enough, we consider these anomalies and add them to this file, to be skipped when necessary.
The number of the issue(s) describing the problem is commented at the top of the dictionaries.
This file can and should be updated and new OSM data is downloaded.
"""
SEGS_TO_DELETE={
'europe0': #Issue #120; for information on where these values come from, see dev.issue120.getSegsToDelete()
['23624020', '26598527', '32150389', '34655827', '35469571', '19544879', '23193831', '24106649', '25887626', '27260652', '27451270', '30824373', '34878726', '35149566', '35207936', '22445976', '23631875', '27830031', '28239877', '30106400', '33354217', '19872569', '22829708', '25103261', '28331150', '30967769', '34990363', '36107322']
}

"""
COORDINATE_LOOKUPS is a dictionary of latlon coordinates of the boundaries of various rectangularregions in the world.
It's accessed to make test continents.
"""
COORDINATE_LOOKUPS={
'small':{
    'continent':'centralamerica',
    'west':-71.754522,
    'east':-69.754522,
    'south':19,
    'north':19.5
    },
'montreal':{
    'continent':'northamerica',
    'west':-74.5710539,
    'east':-72.5710539,
    'south':45.25,
    'north':45.75
    },
'santiago':{
    'continent':'southamerica',
    'west':-71.6406969,
    'east':-69.6406969,
    'south':-33.75,
    'north':-33.25
    },
'paris':{
    'continent':'europe',
    'west':1.341370,
    'east':3.341370,
    'south':48.65,
    'north':49.15
    },
'cairo':{
    'continent':'africa',
    'west':30.234560,
    'east':32.234560,
    'south':29.75,
    'north':30.25
    },
'sydney':{
    'continent':'australiaoceania',
    'west':149.0033749,
    'east':151.0033749,
    'south':-34.15,
    'north':-33.65
    }
}

available_test_continents = list(COORDINATE_LOOKUPS.keys())


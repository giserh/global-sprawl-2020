#!/usr/bin/python

"""CONNECTIVITY METRICS TESTS"""

"""
Testing that (non-aggregated) connectivity metrics are calculated properly.
"""

import sys
import os
import re
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
#from wpca import WPCA,EMPCA
from osm_config import defaults,paths
from aggregate_connectivity_metrics import principal_component_analysis, PCA_VARS_LOGFORM, disconnectivity_analysis
import postgres_tools as psqlt
db=psqlt.dbConnection()
from tests.utils import test_all

def _make_data(columns, length = 10):
    index=list(range(length))
    data=[]
    for _ in range(length):
        data.append(np.random.randint(length,size=len(columns)))
    return pd.DataFrame(data,index=index,columns=columns)


#def garba_test_pca():

#    columns=disconnectivity_analysis().DISCONNECTIVITY_METRICS.keys()+['ls_pop']
#    df=_make_data([c.lower() for c in columns])
#
#    from pca import pca_result, estimatePCA
#    estimatePCA(df, weight=None, tmpname=None, scratch_path=None, method=None, package='stata', verbose=True, dropna=False, **argv)

def test_pca():
    """ What do we want to test, exactly? approx coefficients? , ...
    In old version, we tested weighting, but we are no longer using that.
    """

    #Jan2020: skip this because PCA results are now hardcoded
    #          @cpbl: take a look at making this more useful?
    return
         

    columns = PCA_VARS_LOGFORM+['ls_pop']
    df=_make_data( columns, 100)
    weights_col= None


    # Ensure we do not overwrite real estimates, in case defaults are set for production:
    _continents=defaults['osm']['continents']
    _cr=defaults['osm']['clusterRadii']
    _landscan,_ghsl=defaults['osm']['landscan'],defaults['osm']['ghsl']
    defaults['osm']['continents']=['testpca']
    defaults['osm']['clusterRadii']=['cr']
    defaults['osm']['landscan'],defaults['osm']['ghsl'] = False, False

    pcAnalyzer=principal_component_analysis(#algorithm=algorithm,
                                                n_components=1,
                                                weights=None)
                                                # weights_col,)
                                                #path=paths['scratch'])
        
    assert pcAnalyzer.weights==weights_col
    assert all([c in df.columns for c in pcAnalyzer.datavars])
    pcaresult = pcAnalyzer.estimate_and_save_master_pca(df[columns[:-1] if weights_col is None else columns], 'test_pca', weightmethod = 'allunweighted')
    pca=pcAnalyzer.load_master_pca_results()
    defaults['osm']['continents']=_continents
    defaults['osm']['clusterRadii']=_cr
    defaults['osm']['landscan'],defaults['osm']['ghsl']=_landscan,_ghsl

    # We're not really doing any tests yet.  Maybe add some after the pcaresult object is also rewritten/modified
    return


def old_test_pca(algorithm='WPCA'):
    columns=list(disconnectivity_analysis().DISCONNECTIVITY_METRICS.keys())+['ls_pop']
    df=_make_data(columns)

    # Test df of weights, for the weighted and unweighted case
    for independent_variable_cols,weights_col in [ (None,None),
                                                   (columns[:-1],columns[-1]) ]:
        pcAnalyzer=principal_component_analysis(algorithm=algorithm,
                                                n_components=1,
                                                weights=weights_col,
                                                path=paths['scratch'])
        assert pcAnalyzer.weights==weights_col
        assert all([c in df.columns for c in pcAnalyzer.datavars])
        
        pcAnalyzer.estimate_and_save_master_pca(df[columns[:-1] if weights_col is None else columns], 'test_pca', weightmethod = 'allunweighted')
        pcAnalyzer.load_master_pca_results()

        
        df_,weights=pcAnalyzer.get_df_of_weights(df,
                            independent_variable_cols=independent_variable_cols)
        if not independent_variable_cols:
            independent_variable_cols=columns
            
        # Check format of weights df
        assert df[independent_variable_cols].shape==weights.shape==df_.shape
        assert weights.shape==(10,len(independent_variable_cols))
        for ix in range(10):
            assert len(set(np.array(weights.loc[ix])))==1
            _df=df.replace({weights_col:{0:1}})
            if weights_col:
                assert ( abs(weights.loc[ix][0]*df[weights_col].mean()
                            - _df.loc[ix][weights_col])
                            < 1.0/float(1e10)
                     )
        # Check all weights of 0 have been removed
        assert 0 not in np.asarray(weights)
        # Check the unweighted version gives the same output as an unweighted 
        # PCA algorithm
        # (within a certain margin of error)
        if not weights_col:
            pca=PCA(n_components=1)
            wpca_=pcAnalyzer.pca_
            pca.fit(df)
            wpca_.fit(df)
            if pca.components_[0][0]*wpca_.components_[0][0]<0:
                wpca_.components_[0]=[-1*w for w in wpca_.components_[0]]
            for dimension in zip(pca.components_[0],wpca_.components_[0]):
                assert abs(dimension[0]-dimension[1])<1.0/float(1e4)

        # Test get_df_for_PCA
        df_,independent_variable_columns=pcAnalyzer.get_df_for_PCA(df)
        if weights_col:
            assert len(df_.columns)==len(independent_variable_columns)+1
            assert ( weights_col in df_.columns and weights_col not in
                     independent_variable_columns )
        else:
            assert len(df_.columns)==len(independent_variable_columns)
        assert all([ col in df_.columns for col in independent_variable_columns 
                   ])

    # Test explained_variance_ratio output
    pcAnalyzer=principal_component_analysis(algorithm=algorithm,n_components=2,
                                            path=paths['scratch'])
    pca_df=pcAnalyzer.get_df_of_PCA(df)
    pca_dict=pca_df.to_dict()
    assert ( pca_dict['v1']['v1_explained_variance_ratio']==
             pcAnalyzer.pca_.explained_variance_ratio_[0] )
    assert ( pca_dict['v2']['v2_explained_variance_ratio']==
             pcAnalyzer.pca_.explained_variance_ratio_[1] )


def test_check_connectivity_results():
    from aggregate_connectivity_metrics import disconnectivity_analysis

    schema=defaults['osm']['analysisSchema']
    db.execute("""
                drop table if exists schema.disconnectivity_id_1_fakedata_10;
                drop table if exists schema.disconnectivity_iso_fakedata_10;

                create table schema.disconnectivity_id_1_fakedata_10(
                PCA1 double precision,
                PCA2 double precision
                );

                create table schema.disconnectivity_iso_fakedata_10(
                PCA1 double precision,
                PCA2 double precision
                );

                insert into schema.disconnectivity_id_1_fakedata_10 values(2,3);
                insert into schema.disconnectivity_id_1_fakedata_10 values(3,5);

                insert into schema.disconnectivity_iso_fakedata_10 values(1,3);
                insert into schema.disconnectivity_iso_fakedata_10 values(4,4);
                """.replace('schema',schema))
    disconnectivity_analysis().check_connectivity_results(continent='fakedata',
                                                          clusterradius='10',
                                                          resolution=1)
    log=paths['working']+'logs/connectivity_fakedata_10_id_1.log'
    output=os.popen('tail %s'%log).read()
    assert 'Mismatched ranges' in output
    assert len(re.findall('2.0* -- 3.0*',output))==1
    assert len(re.findall('1.0* -- 4.0*',output))==1
    assert len(re.findall('3.0* -- 5.0*',output))==0
    assert len(re.findall('3.0* -- 4.0*',output))==0 
    db.execute("""drop table schema.disconnectivity_id_1_fakedata_10;
                  drop table schema.disconnectivity_iso_fakedata_10;
               """.replace('schema',schema))
    os.system('rm %s'%log)

def _main():
    current_module=sys.modules[__name__]
    test_all(current_module)

if __name__=='__main__':
    test_pca()
    _main()

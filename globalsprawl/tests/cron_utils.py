#!/usr/bin/python
import numpy as np
import pandas as pd
import postgres_tools as psqlt
from osm_config import defaults,paths,update_paths_to_be_specific_to_database
from aggregate_connectivity_metrics import disconnectivity_analysis
from gadm import GADM_ID_LEVELS
import osmTools as osmt
DATABASE='sabinas'
LOGGER=None

def get_logger(database=DATABASE):
    global defaults

    defaults['server']['postgres_db']=database
    defaults=update_paths_to_be_specific_to_database(defaults)
    logger=osmt.osm_logger('cron-osm-unittest')
    print('\n\n'+logger.fout.name)

def get_cur(database=DATABASE):
    global defaults

    defaults['server']['postgres_db']=database
    return psqlt.dbConnection(schema='osmanalysis',logger=LOGGER)

def get_tablenames(continent='small',database=DATABASE):
    cur=get_cur(database=database)
    res=cur.find_tables([ 'disconnectivity',continent ])
    tables=[ r[1] for r in res if 'original' not in r[1] ]
    schemas=[ r[0] for r in res if 'original' not in r[1] ]
    return tables,schemas

def rename_old_disconnectivity_data(continent='small',database=DATABASE,
                                    logger=None):
    global LOGGER

    if not isinstance(logger,type(None)):
        assert isinstance(logger,str)
        LOGGER=open(logger,'a')
    tables,schemas=get_tablenames(continent,database=database)
    cur=get_cur(database=database)
    for table in tables:
        cur.execute("""drop table if exists osmanalysis.original%s;

                       create table osmanalysis.original%s as ( select * from %s ) 
                       ;"""%(table,table,table))
    if not isinstance(LOGGER,type(None)):
        LOGGER.close()

def compare_old_tables_to_new_tables(continent='small',database=DATABASE,
                                     logger=None):
    global LOGGER

    if not isinstance(logger,type(None)):
        assert isinstance(logger,str)
        LOGGER=open(logger,'a')
    tables,schemas=get_tablenames(continent,database=database)
    cur=get_cur(database=database)
    da=disconnectivity_analysis()
    node_metric_cols=list(da.node_metrics.keys())+da.pcas+da.logNodalSparsity
 
    # Initialize dictionary of correlation coefficients of node metric data
    # between yesterday's and today's data
    ccdata=dict()

    for table,schema in zip(tables,schemas):
        assert edge_data_equal(table,'original%s'%table,schema,'osmanalysis',
                               database=database)
        corcoefs=get_correlation_coefficient(table,'original%s'%table,schema,
                                             'osmanalysis',
                                             columns=node_metric_cols,
                                             continent=continent,
                                             database=database
                                            )
        ccdata['table']=ccdata.get('table',[])
        ccdata['table'].append(table)
        for col in list(corcoefs.keys()):
            ccdata[col]=ccdata.get(col,[])
            ccdata[col].append(corcoefs[col])

    # Put the dictionary into a table in the database
    df=pd.DataFrame(ccdata)
    for i in range(len(df)):
        for col in [ c for c in df.columns if c!='table' ]:
            ix=ccdata['table'].index(df.loc[i]['table'])
            assert df.loc[i][col]==ccdata[col][ix]
    cur.df2db(df,'node_metric_correlations')

    if not isinstance(LOGGER,type(None)):
        LOGGER.close()

def get_id_cols(t,s,database=DATABASE):
    cur=get_cur(database=database)
    cols=GADM_ID_LEVELS+['density_decile','ghsl_yr']
    # The cur's class function list_columns_in_tables ignores views, so use
    # the following command instead
    cur.execute("""select column_name from information_schema.columns
                   where table_schema='%s' and table_name='%s'
                   ;"""%(s,t))
    res=cur.fetchall()
    columns=[ r[0] for r in res ]
    return [ c for c in cols if c in columns ]

def edge_data_equal(t1,t2,schema1,schema2,database=DATABASE):
    cur=get_cur(database=database)
    id_cols=get_id_cols(t1,schema1,database=database)
    columns=id_cols+list(disconnectivity_analysis().edge_metrics.keys())
    return cur.are_equal(t1,t2,columns=columns,schema1=schema1,schema2=schema2)

def get_correlation_coefficient(t1,t2,schema1,schema2,columns,continent='small',
                                database=DATABASE):
    cur=get_cur(database=database)
    da=disconnectivity_analysis()
    id_cols=get_id_cols(t1,schema1,database=database)
    df1=cur.db2df(t1,columns=id_cols+columns,schema=schema1,drop_geom=False)
    df2=cur.db2df(t2,columns=id_cols+columns,schema=schema2,drop_geom=False)

    # Assign GADM_ID_LEVELS and density/ghsl data as index. Sort by the index
    # to make sure the data in the two dfs is in the same order
    df1=df1.set_index(id_cols)
    df1['ix']=df1.index
    df1.sort_values(by='ix',inplace=True)
    df1=df1[[ c for c in df1.columns if c!='ix' ]]
    df2=df2.set_index(id_cols)
    df2['ix']=df2.index
    df2.sort_values(by='ix',inplace=True)
    df2=df2[[ c for c in df2.columns if c!='ix' ]]
    assert all(df1.index==df2.index)

    # Initialize dictionary of correlation coefficients of node metric data
    # between yesterday's and today's data for this table
    tdata=dict()    

    for col in columns:
        a1,a2=np.asarray(df1[col]),np.asarray(df2[col])
        a1[np.isnan(a1)]=0
        a2[np.isnan(a2)]=0
        tdata[col]=np.corrcoef(a1,a2)[0][1]
    return tdata

def clean_up(continent='small',database=DATABASE):
    tables,schemas=get_tablenames(continent)
    cur=get_cur(database=database)
    for table in tables:
        cur.execute('drop table if exists original%s;'%table)

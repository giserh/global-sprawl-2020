#!/usr/bin/python

"""POSTGRES_TOOLS TEST"""

"""
Tests of various functions in postgres_tools.py,
osm's helper module for interacting with PostgreSQL database.
"""

import os,sys
from osm_config import paths,defaults
SP=paths['scratch']
WP=paths['working']
import pandas as pd
import postgres_tools as pgt 
import osmTools as osmt 
from io import StringIO
import psycopg2
from tests.utils import VERBOSE,print_,test_all,fakedata
 
sampleDFdata1="""edge_id class_ification_1
5601191 C
5603732 C
5601218 C
1651715 C
5603768 C
5601204 C
5603682 C
5601188 C
1651716 C
"""
sampleDFdata2="""edge_id col_b
5601191 C
5603732 C
5601218 C
1651715 C
5603768 C
5601204 C
5603682 C
5601188 C
1651716 C
"""
sampleDFdata3="""edge_id col_b class_ification_1
5601191 C B
5603732 C B
5601218 C B
1651715 C B
5603768 C B
5601204 C B
5603682 C B
5601188 C B
1651716 C B
"""
def test_update_table_from_array():
    cur=pgt.dbConnection(verbose=VERBOSE)  
    df=pd.read_table(StringIO(sampleDFdata1), sep=' ')

    print_('Create table...')
    cur.df2db(df,'tmptest1')
    check1=cur.execfetch('SELECT * FROM tmptest1;')
    assert len(check1)==9

    df2=pd.read_table(StringIO(sampleDFdata2), sep=' ')
    print_('Create table...')
    cur.df2db(df2,'tmptest2')

    df3=pd.read_table(StringIO(sampleDFdata3), sep=' ')
    print_('Create table...')
    cur.df2db(df3,'tmptest3')

    print_('Update with no overlapping columns')
    cur.update_table_from_array(df, 'tmptest2')
    check2=cur.execfetch('SELECT edge_id,col_b,class_ification_1 FROM tmptest2;')
    assert len(check2)==9

    cur.df2db(df,'tmptest1')
    cur.update_table_from_array(df, 'tmptest3', forceUpdate=True) # force update allows overlapping columns
        
    cur.delete_all_tables(startswith='tmptest',force_do_not_ask=True)

def test_write_a_table_from_pandas_and_change_its_permissions():
    engine=pgt.getPgEngine()
    df=pd.read_table(StringIO(sampleDFdata1))
    df.to_sql('tmptable1', engine, schema='public', if_exists='replace', index=False, chunksize=100000)
    cur=pgt.dbConnection(verbose=VERBOSE)  

    cur.fix_permissions_of_new_table('tmptable1')

    cur.execute("""

     ALTER TABLE tmptable1 OWNER to osmusers
      """)
    # N.B. Above works but following fails:
    cur.execute("""
     ALTER TABLE tmptable1 OWNER to osmusers
      """)


    cur.execute('DROP TABLE tmptable1')

def test_merge_table_into_table():
    schema=defaults['osm']['processingSchema']
    cur=osmt.pgisConnection(schema=schema,verbose=True) # subclass of pgt.dbConnection
    datamaker=fakedata()
    strdict={ 'cont': datamaker.continent, 'cr': datamaker.clusterradius,
              'gt': datamaker.GADM_TABLE, 'schema': schema
            }

    datamaker.build_all()
    nodestn='nodes_%(cont)s_%(cr)s'%strdict
    nodesdf=cur.db2df(nodestn)
    lookuptn='lookup_%(cont)s_cluster%(cr)s_%(gt)s'%strdict
    cur.create_indices(lookuptn,gadm=True)
    lookupdf=cur.db2df(lookuptn)
    cur.merge_table_into_table(from_table=nodestn,targettablename=lookuptn,
                               commoncolumns='cluster_id')
    
    # Check that the merge worked
    mergeddf=cur.db2df(lookuptn)
    assert set(mergeddf.columns)==set(nodesdf.columns).union(set(lookupdf.columns))
    assert len(mergeddf)==len(lookupdf)

    # Check that the indices were properly recreated
    indices=cur.execfetch("""select indexname from pg_indexes where
                          tablename='%s' and schemaname='%s'
                          ;"""%(lookuptn,schema)
                         )
    indices=[ ix[0] for ix in indices ]

    for res in [ 'iso12','id3','id4','id5' ]:
        idx=lookuptn+'_idx_'+res
        assert idx in indices

    datamaker.clean_up()

def test_are_equal():
    cur=pgt.dbConnection(verbose=True)
    datamaker1=fakedata()
    datamaker1.make_fake_node_and_edge_data(include_geom=False)
    nodetable1='nodes_fakedata_10'
    edgetable1='edges_fakedata_10'

    datamaker2=fakedata(continent='morefakedata')
    datamaker2.make_fake_node_and_edge_data(include_geom=False)
    nodetable2='nodes_morefakedata_10'
    edgetable2='edges_morefakedata_10'

    metric_columns=[ 'onemetric','anothermetric','yetanothermetric' ]

    assert cur.are_equal(nodetable1,nodetable2)
    assert cur.are_equal(edgetable1,edgetable2)
    assert cur.are_equal(nodetable1,edgetable1,columns=metric_columns)

    cur.execute('update %s set cluster_id = cluster_id*2;'%nodetable2)
    assert not cur.are_equal(nodetable1,nodetable2)
    assert cur.are_equal(nodetable1,nodetable2,columns=metric_columns)

    cur.execute('update %s set onemetric = onemetric*2;'%nodetable2)
    assert not cur.are_equal(nodetable1,nodetable2,columns=metric_columns)

    # Now make sure the order of rows and having duplicate rows doesn't make a
    # difference
    cur.execute("""create table unordered_fakedata as ( select * from
                   %s order by (power(onemetric,2))::integer%%3 )
                   ;"""%nodetable1)
    orderedd=[ tuple(row) for row in 
               cur.execfetch('select * from %s;'%nodetable1) ]
    unorderedd=[ tuple(row) for row in 
                 cur.execfetch('select * from unordered_fakedata;') ]
    assert orderedd!=unorderedd
    assert set(orderedd)==set(unorderedd)
    assert len(orderedd)==len(unorderedd)
    assert cur.are_equal(nodetable1,'unordered_fakedata')
    cur.execute('select * from unordered_fakedata limit 1;')
    onerow=cur.fetchone()
    cur.execute('insert into unordered_fakedata values (%s);'%(','.join(str(r)
                for r in onerow)))
    unorderedd=[ tuple(row) for row in
                 cur.execfetch('select * from unordered_fakedata;') ]
    assert len(unorderedd)==len(orderedd)+1
    assert cur.are_equal(nodetable1,'unordered_fakedata')

    datamaker1.clean_up()
    datamaker2.clean_up()

def _main():
    current_module=sys.modules[__name__]
    test_all(current_module)

if __name__ == "__main__":
    _main()

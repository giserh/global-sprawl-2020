#!/usr/bin/python

"""CLUSTER ANALYSIS TESTS"""

import sys
from osm_config import defaults,paths
import postgres_tools as psqlt
from tests.utils import test_all, fakedata
import kmeans_analysis as km
from osmTools import CONTINENT_IDS

def test_cluster():
    datamaker=fakedata()
    datamaker.make_fake_disconnectivity_data()
    ca = km.kmeans_analyzer(nClusters=3,continents=['fakedata'],gadmLevel=0,clusterradius='10',sortCol='onemetric', columns=['onemetric','anothermetric','yetanothermetric'],urban=False )
    
    # test that clusters are in order of 'onemetric'
    ca.defineClusters(sortOrder='asc')
    clustMeans = ca.df.groupby('cluster').onemetric.mean()
    assert all([clustMeans.loc[i]<clustMeans.loc[i+1] for i in range(len(clustMeans)-1)])
    ca.defineClusters(sortOrder='desc')
    clustMeans = ca.df.groupby('cluster').onemetric.mean()
    assert all([clustMeans.loc[i]>clustMeans.loc[i+1] for i in range(len(clustMeans)-1)])
    
    # test that rows are assigned to right cluster 
    ca = km.kmeans_analyzer(nClusters=3,continents=['fakedata'],gadmLevel=0,clusterradius='10',sortCol='anothermetric', columns=['onemetric','anothermetric','yetanothermetric'],urban=False )
    ca.defineClusters()
    assert ca.clusterLabels==[1, 0, 2, 2, 1, 1, 2, 1, 1, 2, 2, 1]
    assert [ca.getClosestMember()[i][0][0] for i in range(3)]==['j','h','g']
    
    datamaker.clean_up()
    
def _main():
    current_module=sys.modules[__name__]
    test_all(current_module)

if __name__=='__main__':
    _main()

#!/usr/bin/python
"""

These functions aggregate the node- and edge-level metrics to GADM regions (e.g. countries), 
World Bank regions, grid cells, and other geographic units.

It creates two series of tables:

  - aggregated_* tables contain only sums (length) and counts

  - disconnectivity_* tables contain ratios and means calculated from those.

Aggregate connectivity data recorded in edge and cluster tables at the edge and cluster level to GADM (or grid) (or non-GADM aggregation_IDs) scales.
This creates the "aggregated_" tables and from them the "disconnectivity_" tables, which are more or less our final result.

In general, we are creating several levels of analysis (ie values derived from the aggregated values, ie everything in the "disconnectivity" tables) 
in the database (PostGIS) rather than doing it in Python, simply because it makes for easier visualization
of all data layers at once in QGIS.

The classes below were set up assuming that geographies were given by the GADM regions. 
In order to allow for other geographies (for special/national-level analysis), you can specify non-GADM table names

"""
from osm_config import paths,defaults
from copy import deepcopy
import os
import time
import numpy as np
import pylab as plt
plt.yticks.labelsize=12
#from sklearn.decomposition import PCA
from cpblUtilities.parallel import runFunctionsInParallel
from cpblUtilities import dgetget,dsetset, orderListByRule
from cpblUtilities.mathgraph import savefigall, transbg, figureFontSetup, remove_underscores_from_figure  # Wrapper for making legends transparent
from cpblUtilities.color import linearColormapLookup, assignSplitColormapEvenly, addColorbarNonImage

import osmTools as osmt
import postgres_tools as psqlt
import gadm
from gadm import  GADM_schema
from osm_networkx import Graph as osmnGraph
from metric_settings import metricNames
import networkx as nx
import pandas as pd
import osm_lookups
from pointmetrics_calculations import get_analysis_radii,get_analysis_radii_suffixes
import svgTools as svgt
from metric_settings import metricSettings,metricNames, sensible_metric_order,order_variables #Defines display names, axis limits, axis direction, outlier dropping limits for each variable
from pca import pca_result, estimatePCA

def str_reps(dosubs,ss):
    for a,b in list(dosubs.items()):
        ss=ss.replace(a,b)
    return(ss)


PCA1_BOOST = '3.0+' # For convenience, shift PCA1 up a bit to avoid negative values
metricNames['fraction_length_included'] = 'Frc length included' # not really a metric name, but we do a map of it
min_nodes=15 # minimum for mapping

def log(string,logger):
    if logger:
        logger.write(string)
    else:
        print (string)

def get_all(x,default):
    assert isinstance(default,list)
    if x is None:
        return default 
    if isinstance(x,list):
        return x
    return [x]

def iter_through_tables(clusterradius,continent,resolution,
                        density_ghsl_urban= None,reverse_resolution=False
                       ):
    density_ghsl_urban = [ (False,False,False) ] if density_ghsl_urban is None else density_ghsl_urban
    assert all([ isinstance(_arg,list) for _arg in [ clusterradius,continent,
                 resolution,density_ghsl_urban ]
               ])
    for cr in clusterradius:
        for cont in continent:
            for res in ( reversed(resolution) if reverse_resolution
                         else resolution ):
                for densityItem,ghslItem,urbanItem in density_ghsl_urban:
                        yield (cr,cont,res,densityItem,ghslItem,urbanItem)

"""
A wrapper for core and helper functions for the aggregation step. To run the
aggregation step in full, call aggregate_node_and_edge_results().run_all().

run_all() iterates over distinct combinations of continents, clusterradii, GADM
id levels, density (a boolean), and ghsl (a boolean). These variables can be
specified at run time. Otherwise, their values default to the settings in the
user's configuration file. run_all() outputs a table prefixed by "aggregated"
for each distinct combination of variables it is run on. All tables provide full
coverage for the continent.

density
-------
Setting density to True includes the value for the relevant region from the
density_decile column in the nodes and edges tables.

ghsl
----
Setting ghsl to True includes the value for the relevant region from the ghsl_yr
column in the nodes and edges tables.


Node and edge data are aggregated separately.

AGGREGATE EDGE METRICS
-----------------------
aggregate_node_and_edge_results().aggregate_edge_metrics()

aggregate_edge_metrics() iterates through GADM regions at the relevant level, 
using the lookup tables to find edges that fall within that region and summing 
the metrics (defined in the class's __init__ function as class attributes 
edge_metrics). Because the edge tables contain data for every edge, there's no 
reason to scale the sums.


AGGREGATE NODE METRICS
-----------------------
Step 1: Add estimated region-level node metrics (to the finest-resolution table)
--------------------------------------------------------------------------------
aggregate_node_and_edge_results().add_estimated_region_level_node_metrics()

Because the node tables DON'T contain data for every node (i.e. they only
contain data for a sample of nodes), it's necessary to estimate region-level
sums. add_estimated_region_level_node_metrics() is called only for the finest
level of resolution (metrics for coarser regions are estimated as summed
aggregations of those calculated in this step; see Step 4 for further detail).
add_estimated_region_level_node_metrics() uses the lookup tables to find nodes 
that fall within each region and estimates region-level node metrics (defined in
the class's __init__ function as the class attribute node_metrics) by scaling
results from the sampled data (see the class function aggregate_node_metricss() 
for more details on the scaling factor). The scaled sums are then added to the 
aggregated table. After this stage completes, the aggregated table at the finest 
level of resolution is complete.

Step 2: Sum node metrics from smaller regions
--------------------------------------------- 
aggregate_node_and_edge_results().sum_node_metrics_from_smaller_regions()

sum_node_metrics_from_smaller_regions() is called at all resolutions EXCEPT the
finest resolution. Node metrics for each region are calculated as sums of the
metrics at one finer level of resolution associated with overlapping regions.
After this stage completes, the aggregated tables at all levels of resolution
are complete.


ADD INDICES
------------
aggregate_node_and_edge_results().add_indices()

Adds indices to the final aggregated_* tables.


UNIT TEST
----------
To test, run tests.aggregation_test.
"""
class aggregate_node_and_edge_results(object):
    
    def __init__(self,continent=None,clusterradius=None,resolution=None,
                 density=False,ghsl=False,urban=False,
                 forceUpdate=False,
                 schema=defaults['osm']['processingSchema'],verbose=True,
                 logger=None,
                 # Following are only used in non-GADM agg region case, and all must be specified if any are:
                 # You must also specify the lookup tables for nodes and edges to those aggregation regions.
                 # For GADM regions, these things are all known or generated automatically
                 aggregation_IDs=None,
                 edge_lookup_table = None,
                 node_lookup_table = None,
                 aggregation_geometry_table = None):

        self.continent=get_all(continent,default=defaults['osm']['continents'])
        self.clusterradius=get_all(clusterradius,default=defaults['osm']['clusterRadii'])
        # Provide all or none:
        gotall = aggregation_geometry_table is None and aggregation_IDs is None and   edge_lookup_table is  None and   node_lookup_table is  None
        gotnone =  aggregation_geometry_table is not None and aggregation_IDs is not  None and   edge_lookup_table  is not   None and   node_lookup_table  is not   None
        assert gotall or gotnone
        self.nonGADM = aggregation_IDs is not None
        self.agg_IDs = defaults['gadm']['ID_LEVELS'] if aggregation_IDs is None else aggregation_IDs
        self.aggTableName = defaults['gadm']['TABLE'] if aggregation_geometry_table is None else aggregation_geometry_table
        # The folllowing should remain None when aggregation is over GADM:
        self.edge_lookup_table = edge_lookup_table 
        self.node_lookup_table = node_lookup_table
        
        self.resolution=get_all(resolution,default= list(range(len(self.agg_IDs))))
        self.density_ghsl_urban = define_density_ghsl_urban_combinations(density, ghsl, urban)
        self.stock = True if ghsl and urban else False
        self.forceUpdate=forceUpdate
        self.schema=schema
        self.verbose=verbose
     
        # Nodal connectivity metrics
        # Keys correspond to regionmetrics, and values are the corresponding
        # pointmetric
        # Values are inputted into a formula to estimate a region-level sum
        # from the available nodal data (see class function 
        # aggregate_node_metrics
        # The value for 'nnodes' is a kludge to get a sum of all the nodes in
        # a region
        self.node_metrics={
            'deadends': 'CAST(degree=1 AS INT)',
            'threeways': 'CAST(degree=3 AS INT)',
            'fourplus': 'CAST(degree>=4 AS INT)',
            'nnodes': 'CAST(degree IS NOT NULL AS INT)',
            'nnodes_sampled': 'CAST(n_destinations_0_500 is not Null AS INT)'
        }
        for RRsuffix in get_analysis_radii_suffixes():
            self.node_metrics['N_'+RRsuffix]='n_destinations_'+RRsuffix
            self.node_metrics['SUM_CD_'+RRsuffix]='sumcartesian_'+RRsuffix
            self.node_metrics['SUM_ND_'+RRsuffix]='sumnetworkd_'+RRsuffix
        self.scale_nodal_sums=True

        # Edge connectivity metrics
        # Keys correspond to regionmetrics, and values correspond to the
        # aggregation formula derived from the relevant pointmetrics
        self.edge_metrics={
            'n_cycle': 'SUM(CAST(classification=\'C\' AS INT))',
            'n_bridge': 'SUM(CAST(classification=\'B\' AS INT))',
            'n_culdesac': 'SUM(CAST(classification=\'D\' AS INT))',
            'n_selfloop': 'SUM(CAST(classification=\'S\' AS INT))',
            'cycle_m': 'SUM(CAST(classification=\'C\' AS INT)*length_m)',
            'bridge_m': 'SUM(CAST(classification=\'B\' AS INT)*length_m)',
            'culdesac_m': 'SUM(CAST(classification=\'D\' AS INT)*length_m)',
            'selfloop_m': 'SUM(CAST(classification=\'S\' AS INT)*length_m)',
            'length_m': 'SUM(length_m)',
            'sum_startend_length_m': 'NULLIF(CAST(SUM(startend_length_m) AS REAL), 0.)'
            }

        self.logger= logger

    def open_logger(self):
        # Initialize logger
        _logger_params=['aggregation']+self.continent+self.clusterradius
        self.logger=osmt.osm_logger('_'.join(_logger_params),
                                    verbose=self.verbose,timing=True,
                                    overwrite=True)

        self.logger.write('Continents: '+', '.join(self.continent)+'\n'+ \
                          'Cluster radii: '+' & '.join(self.clusterradius)+'\n'+ \
                          'Resolutions: '+', '.join([ str(res) for res in 
                                                      self.resolution])+'\n'+ \
                          'Density: '+str(len(set([ i[0] for i in 
                                        self.density_ghsl_urban ]))>1).upper()+'\n'+ \
                          'GHSL: '+str(len(set([ i[1] for i in
                                        self.density_ghsl_urban]))>1).upper()+'\n'+ \
                          'Urban: '+str(len(set([ i[2] for i in
                                        self.density_ghsl_urban]))>1).upper()+'\n'+ \
                          'Force update: '+str(self.forceUpdate).upper()+'\n'+ \
                          'Using schema: '+self.schema+'\n'+ \
                          'Node metrics: '+str(self.node_metrics)+'\n'+ \
                          'Scale nodal sums: '+str(self.scale_nodal_sums)+'\n'+ \
                          'Edge metrics: '+str(self.edge_metrics)
                          )

    def close_logger(self):
        if not isinstance(self.logger,osmt.osm_logger):
            self.logger.close()

    # To avoid confusing the cursor when running in parallel, create new cursors on the fly
    def get_cur(self):
        return osmt.pgisConnection(schema=self.schema,verbose=self.verbose,
                                   logger=self.logger)

    def make_strdict(self,cont,cr,res,densityItem,ghslItem,urbanItem):
        return dict(CONTINENT=cont,NNCL=cr, AGGIDLEVEL=self.agg_IDs[res], 
                    AGG_ID_LIST=','.join(self.agg_IDs[:res+1]),
                    _DENSITY='_density'*densityItem, _GHSL='_ghsl'*ghslItem, _URBAN='_urban'*urbanItem,
                    WHEREURBAN=(' AND urban_'+str(defaults['osm']['urban_pctile']))*urbanItem,
                    AGGLEVELNONZERO= ' TRUE ', 
                    MYSCHEMA=defaults['osm']['processingSchema'],
                    NODEGADMLOOKUPTABLE= self.node_lookup_table  if self.node_lookup_table is not None else osm_lookups.GADM_roadgraph_lookups('nodes', cr, continent=cont).get_lookup_tablename(),
                    EDGEGADMLOOKUPTABLE= self.edge_lookup_table  if self.edge_lookup_table is not None else osm_lookups.GADM_roadgraph_lookups('edges', cr, continent=cont).get_lookup_tablename(),
                    GRAPHNODETABLE=defaults['osm']['cluster_tables'].replace('REGION',cont).replace('CLUSTERRADIUS',cr),
                    GRAPHEDGETABLE=defaults['osm']['edge_tables'].replace('REGION',cont).replace('CLUSTERRADIUS',cr),
                    TABLENAME='_'.join(['aggregated',self.agg_IDs[res],cont,cr])+'_density'*densityItem+'_ghsl'*ghslItem+'_urban'*urbanItem
                   )
    
    # Return False if the aggregated table exists and if forceUpdate is False
    # Otherwise, return True
    def check_for_forceupdate(self,continent,clusterradius,resolution,density,
                              ghsl,urban):
        cur=self.get_cur()
        strdict=self.make_strdict(continent,clusterradius,resolution,density,
                                  ghsl,urban)
        if ( not self.forceUpdate and strdict['TABLENAME'] in cur.list_tables()            ): 
            log('Quitting aggregation for '+' '.join(
                [continent,clusterradius,str(resolution),strdict['_DENSITY'],
                strdict['_GHSL'],strdict['_URBAN']])+'; use forceUpdate to overwrite.',
                self.logger)
            return False
        return True
    
    # Checks if edge and node tables needed for aggregation exist
    # Returns False if not
    # Otherwise, returns True
    def check_for_existence_of_edge_and_node_tables(self,continent,
                                                    clusterradius,resolution,
                                                    density,ghsl,urban):
         cur=self.get_cur()
         strdict=self.make_strdict(continent,clusterradius,resolution,density,
                                  ghsl,urban)
         if not ( 
                 strdict['GRAPHNODETABLE'] in cur.list_tables()
                 and strdict['GRAPHEDGETABLE'] in cur.list_tables()
                ):
            log('Quitting aggregation for '+' '.join(
                [continent,clusterradius,str(resolution),strdict['_DENSITY'],
                strdict['_GHSL'],strdict['_URBAN']])+'; edge and node tables do not exist.',
                self.logger)
            return False
         return True

    # Transform a nodal pointmetric into the formula needed to calculate a
    # regionmetric
    # Since it is not guaranteed that there is data for every node, there's an
    # option to estimate a region-level sum from the available data. In the
    # runall flow, this option is included only at the level of aggregation with
    # the highest level of resolution
    ## ONLY WORKS FOR NODAL POINTMETRICS
    def aggregate_node_metrics(self,regionmetric):
        assert regionmetric in self.node_metrics
        pointmetric=self.node_metrics[regionmetric]
        agg_formula='SUM(COALESCE(%s, 0))'%pointmetric
        if not self.scale_nodal_sums:
            return agg_formula
        num_sampled='SUM(CAST(COALESCE(%s, 0) IS NOT NULL AS INT))'%pointmetric
        scaled_metric="""
            CASE WHEN %s=0 THEN NULL
            ELSE %s*( COUNT(*)::float/%s )
            END"""%(num_sampled,agg_formula,num_sampled)
        return scaled_metric

    # Use the lookup tables to find edges that fall within each region in the
    # continent and at the GADM resolution specified.
    # Sum the edges metrics (defined as class attribute edge_metrics in
    # __init__).
    #
    # Outputs a table of aggregated data
    # aggregated_<resolution>_<continent>_<clusterradius>_<density>_<ghsl>.
    def aggregate_edge_metrics(self,continent=None,clusterradius=None,
                               resolution=None,density=False,ghsl=False,urban=False):
        cur=self.get_cur()
        strdict=self.make_strdict(continent,clusterradius,resolution,density,
                                  ghsl,urban)

        # Aggregate edge metrics
        cmd=str_reps(strdict,"""
            DROP TABLE IF EXISTS
            MYSCHEMA.TABLENAME
            ;"""*self.forceUpdate+"""

            select * INTO
            MYSCHEMA.TABLENAME
            FROM (
                WITH edge_id_list AS 
                    ( SELECT edge_id, AGG_ID_LIST FROM EDGEGADMLOOKUPTABLE
                      WHERE AGGLEVELNONZERO
                    ) 
                    SELECT
                    """+',\n'.join([v+' as '+k for k,v in
                                    self.edge_metrics.items()
                                   ])+""",\n
                    AGG_ID_LIST"""+""",density_decile"""*density+"""
                    ,ghsl_yr"""*ghsl+"""
                     FROM GRAPHEDGETABLE AS edges, edge_id_list as lookup WHERE
                    edges.edge_id = lookup.edge_id AND nocars=False
                              WHEREURBAN
                     GROUP BY AGG_ID_LIST"""+""",density_decile"""*density+"""
                    ,ghsl_yr"""*ghsl+""" ) AS data
            ;""")
        print(cmd)
        cur.execute(cmd)
        return

    # Add node metrics: fill in estimated and exact  node metrics with the region-level estimate (or exact value) from scaling
    # the sampled data (in the case of exact values, this scaling does nothing).
    def add_estimated_region_level_node_metrics(self,continent=None,
                                                clusterradius=None,
                                                resolution=None,density=False,
                                                ghsl=False,urban=False):
        assert resolution==len(self.agg_IDs)-1
        cur=self.get_cur()
        strdict=self.make_strdict(continent,clusterradius,resolution,density,
                                  ghsl,urban)

        # Calculate region-level estimates from scaling sampled data
        cmd="""
            DROP TABLE IF EXISTS %(MYSCHEMA)s.tmptomergeinto_%(TABLENAME)s;

            CREATE TABLE %(MYSCHEMA)s.tmptomergeinto_%(TABLENAME)s AS
            (
                WITH cluster_id_list AS ( SELECT cluster_id, 
                                          %(AGG_ID_LIST)s FROM
                                          %(NODEGADMLOOKUPTABLE)s WHERE 
                                          %(AGGLEVELNONZERO)s
                                        )
                SELECT %(AGG_ID_LIST)s,
                """%strdict+',\n'.join([self.aggregate_node_metrics(k)+' as '+k 
                                        for k,v in self.node_metrics.items()
                                ])+",density_decile"*density+",ghsl_yr"*ghsl+"""
                FROM %(GRAPHNODETABLE)s AS clusters, cluster_id_list as lookup 
                WHERE clusters.cluster_id = lookup.cluster_id  
                      AND clusters.nocars = FALSE
                      %(WHEREURBAN)s
                GROUP BY %(AGG_ID_LIST)s"""%strdict+"""
                ,density_decile"""*density+""",ghsl_yr"""*ghsl+"""
            );"""
        cur.execute(cmd)

        # Merge to create final aggregated table
        commonColumns=['%(AGG_ID_LIST)s'%strdict]
        if density:
            commonColumns.append('density_decile')
        if ghsl:
            commonColumns.append('ghsl_yr')
        cur.merge_table_into_table(
            'tmptomergeinto_'+strdict['TABLENAME'],strdict['TABLENAME'],
            commonColumns,fromschema=strdict['MYSCHEMA'], 
            targetschema=strdict['MYSCHEMA'],join_type='FULL OUTER',forceUpdate=self.forceUpdate
            )
        cur.execute("""
                    DROP TABLE IF EXISTS
                    %(MYSCHEMA)s.tmptomergeinto_%(TABLENAME)s
                    ;"""%strdict)
        return

    # Fill in tables at coarser resolutions with sums from regions at finer
    # resolutions
    def sum_node_metrics_from_smaller_regions(self,continent=None,
                                              clusterradius=None,
                                              resolution=None,density=False,
                                              ghsl=False,urban=False):
        assert resolution < len(self.agg_IDs)-1
        cur=self.get_cur()
        strdict=self.make_strdict(continent,clusterradius,resolution,density,
                                  ghsl,urban)

        strdict['AGGIDLEVEL_PLUSONE']=self.agg_IDs[resolution+1]
        
        cmd="""
            CREATE TABLE
            %(MYSCHEMA)s.tmptomergeinto_%(TABLENAME)s
            AS (
                SELECT 
                """%strdict+',\n'.join(['SUM('+k+') as '+k for
                                        k,v in self.node_metrics.items()
                                        ])+""",
                %(AGG_ID_LIST)s
                """%strdict+', density_decile '*density+', ghsl_yr '*ghsl+"""
                FROM aggregated_%(AGGIDLEVEL_PLUSONE)s_%(CONTINENT)s_%(NNCL)s%(_DENSITY)s%(_GHSL)s%(_URBAN)s
                GROUP BY %(AGG_ID_LIST)s
                """%strdict+', density_decile '*density+', ghsl_yr '*ghsl+"""
            );"""%strdict
        cur.execute(cmd)

        # Merge to create final aggregated table
        commonColumns=['%(AGG_ID_LIST)s'%strdict]
        if density:
            commonColumns.append('density_decile')
        if ghsl:
            commonColumns.append('ghsl_yr')
        cur.merge_table_into_table(
            'tmptomergeinto_'+strdict['TABLENAME'],strdict['TABLENAME'],
            commonColumns,fromschema=strdict['MYSCHEMA'],join_type='FULL OUTER',
            targetschema=strdict['MYSCHEMA'],forceUpdate=self.forceUpdate
            )
        
        cur.execute("""
                    DROP TABLE IF EXISTS
                    %(MYSCHEMA)s.tmptomergeinto_%(TABLENAME)s
                    ;"""%strdict)
        return

    def add_indices(self,continent=None,clusterradius=None,resolution=None,
                    density=False,ghsl=False,urban=False):
        cur=self.get_cur()
        strdict=self.make_strdict(continent,clusterradius,resolution,density,
                                  ghsl,urban)
 
        # Add indices to aggregated table
        non_unique_columns=strdict['AGG_ID_LIST'].split(',')[:3]
        cur.create_indices(strdict['TABLENAME'],schema=strdict['MYSCHEMA'],
                           non_unique_columns=non_unique_columns
                          )
        return
 
    def aggregateToStock(self,cont,cr,res,densityItem,ghslItem,urbanItem):
        """Sums previously created tables to stocks for each ghsl year"""
        strdict=self.make_strdict(cont,cr,res,densityItem,
                                  ghslItem,urbanItem)
        strdict['STOCKTN'] = strdict['TABLENAME'] + '_stock'
        cur=self.get_cur()

        cmd=str_reps(strdict,"""
            DROP TABLE IF EXISTS MYSCHEMA.STOCKTN;
            
            CREATE TABLE MYSCHEMA.STOCKTN (
               LIKE MYSCHEMA.TABLENAME including all);""")
        print(cmd)
        cur.execute(cmd)

        strdict['SUMMEDMETRICS'] = ','.join(['SUM('+kk+') AS '+kk for kk in list(self.edge_metrics.keys())+list(self.node_metrics.keys())])

        for ghsl_yr in [1975,1990,2000,2014]:
            cmd = str_reps(strdict,"""
                  INSERT INTO MYSCHEMA.STOCKTN
                  SELECT AGG_ID_LIST,
                  """+str(ghsl_yr)+""" AS ghsl_yr,
                  SUMMEDMETRICS
                  FROM MYSCHEMA.TABLENAME
                  WHERE ghsl_yr>1 AND ghsl_yr<="""+str(ghsl_yr)+"""
                  GROUP BY AGG_ID_LIST;""")
            print(cmd)
            cur.execute(cmd)

    def aggregation_test(self):
        """Test that aggregation not impacted by Null values
        See issue #494"""

        if any([ii[1] is True for ii in self.density_ghsl_urban]): # if ghsl is True
            cur=self.get_cur()
            for continent in self.continent:
                for cr in self.clusterradius:
                    strdict=self.make_strdict(continent,cr,self.resolution[-1],False, False, False)
                    strdict['NODEMETRICSUMS'] = ','.join(['SUM('+nm+') AS '+nm for nm in self.node_metrics])
                    strdict['EQTEST'] = ' OR '.join(['cr.'+nm+'!=t.'+nm for nm in self.node_metrics])
                    cmd = str_reps(strdict, '''WITH checky_regions as (
                                SELECT * FROM (SELECT AGG_ID_LIST, min(ghsl_yr) AS ghslmin, NODEMETRICSUMS
                                FROM TABLENAME_ghsl GROUP BY AGG_ID_LIST) t1 
                            WHERE ghslmin>0)
                            SELECT COUNT(*) FROM checky_regions cr LEFT JOIN TABLENAME t
                                USING (AGG_ID_LIST)
                                WHERE EQTEST;''')
                    assert cur.execfetch(cmd)[0][0]==0

    def run_all(self, skipCellsandRegions=False):
        self.open_logger()

        # Only run on tables that pass check_for_forceupdate and
        # check_for_existence_of_edge_and_node_tables
        self.exclude=[]
        for cr,cont,res,densityItem,ghslItem,urbanItem in iter_through_tables(
                                                self.clusterradius,
                                                self.continent,self.resolution,
                                                self.density_ghsl_urban):
            if not (
                    self.check_for_forceupdate(cont,cr,res,densityItem,ghslItem,urbanItem)
                    and
                    self.check_for_existence_of_edge_and_node_tables(cont,cr,
                        res,densityItem,ghslItem,urbanItem)
                   ):
                #assert urbanItem or not ghslItem # Probably need to run using forceUpdate?
                self.exclude.append((cr,cont,res,densityItem,ghslItem,urbanItem))
        if len(self.exclude)>0:
            self.logger.write('Excluding:\n')
        for item in self.exclude:
            self.logger.write('- '+', '.join([ str(i) for i in item]))

        # Aggregate edge metrics
        self.logger.write('Aggregating edge metrics')

        funcs,names=[],[]
        for cr,cont,res,densityItem,ghslItem,urbanItem in iter_through_tables(
                                                self.clusterradius,
                                                self.continent,self.resolution,
                                                self.density_ghsl_urban):
            if (cr,cont,res,densityItem,ghslItem,urbanItem) in self.exclude:
                continue
            funcs+=[[self.aggregate_edge_metrics,
                [cont,cr,res,densityItem,ghslItem,urbanItem]
            ]]
            names+=['-'.join([
                    defaults['server']['postgres_db'],str(defaults['osm']['continents']),'aggregate_node_and_edge_results_aggregate-edge-metrics',
                    cont,cr,str(res),'density'*densityItem,'ghsl'*ghslItem,'urban'*urbanItem])
            ]
        runFunctionsInParallel(funcs,names=names,maxAtOnce=None,
            parallel=defaults['server']['parallel'])
        
        # Add estimated region-level node metrics to the id_5 table
        # Sum node metrics from smaller regions to calculate node metrics for
        # all other tables
        self.logger.write('Calculate nodal metrics')

        funcs,names=[],[]
        for cr,cont,res,densityItem,ghslItem,urbanItem  in iter_through_tables(
                                                self.clusterradius,
                                                self.continent,self.resolution,
                                                self.density_ghsl_urban,
                                                reverse_resolution=True):
            if (cr,cont,res,densityItem,ghslItem,urbanItem) in self.exclude:
                continue
            if res==len(self.agg_IDs)-1:
                self.add_estimated_region_level_node_metrics(cont,cr,res,
                                                             densityItem,
                                                             ghslItem,urbanItem )
    
            else:
                self.sum_node_metrics_from_smaller_regions(cont,cr,res,
                                                           densityItem,ghslItem,urbanItem )

        # Add indices to new aggregated tables
        self.logger.write('Clean up')

        funcs,names=[],[]
        for cr,cont,res,densityItem,ghslItem,urbanItem in iter_through_tables(
                                                self.clusterradius,
                                                self.continent,self.resolution,
                                                self.density_ghsl_urban):
            if (cr,cont,res,densityItem,ghslItem,urbanItem) in self.exclude:
                continue
            funcs+=[[self.add_indices,
                [cont,cr,res,densityItem,ghslItem,urbanItem]
            ]]
            names+=['-'.join([
                    defaults['server']['postgres_db'],'aggregate_node_and_edge_results-add_indices',
                    cont,cr,str(res),'density'*densityItem,'ghsl'*ghslItem,'urban'*urbanItem])
            ]
        runFunctionsInParallel(funcs,names=names,maxAtOnce=None,
            parallel=defaults['server']['parallel'])
        self.aggregation_test()
        
        # sum the stock to create stocks for intermediate years
        # only for density=False and urban=True
        if self.stock:
            self.logger.write('Create stocks for intermediate years')
            for burban in [True,False]:
                stock_density_ghsl_urban = [(False, True, burban)] 
                funcs,names=[],[]
                for cr,cont,res,densityItem,ghslItem,urbanItem in iter_through_tables(
                                                        self.clusterradius,
                                                        self.continent,self.resolution,
                                                        stock_density_ghsl_urban):
                    funcs+=[[self.aggregateToStock,
                        [cont,cr,res,densityItem,ghslItem,urbanItem]
                    ]]
                    names+=['-'.join([
                            defaults['server']['postgres_db'],str(defaults['osm']['continents']),'aggregate_node_and_edge_results_aggregateToStock',
                            cont,cr,str(res),'density'*densityItem,'ghsl'*ghslItem,'urban'*urbanItem])
                    ]
                runFunctionsInParallel(funcs,names=names,maxAtOnce=None,
                    parallel=defaults['server']['parallel'])


        if not skipCellsandRegions: # skipping is normally for fake geographies or individual continents used for testing
            if sorted(self.continent) in [['fakeplanet'],['planet'], ['africa','asia','australiaoceania','centralamerica','europe','northamerica','southamerica']]:
                self.logger.write('Aggregating to cells')
                funcs,names=[],[]
                for cr in self.clusterradius:
                    funcs+=[[self.aggregateToCells,[cr,self.continent]]]
                    names+=[defaults['server']['postgres_db']+':aggregateToCells_'+cr]
                runFunctionsInParallel(funcs,names=names,maxAtOnce=None,parallel=defaults['server']['parallel'])
            else:
                self.logger.write('Skipping aggregating to cells because continent %s is a subset of planet.' % self.continent)
     
            if self.continent in [['fakeplanet'],['planet']]:
                self.logger.write('Aggregating to WB regions')
                self.aggregateToWBregions() # Fast, so do in serial
            else:
                self.logger.write('Skipping aggregating to WB regions because continent %s is not planet or fakeplanet.' % self.continent)
     
            
        self.close_logger()
              
    def aggregateToCells(self, clusterradius, continent=None):
        """For each raster pixel, density and ghsl will be constant. 
        So the idea here is to create a single table (with raster row and col id), which can be masked by the ghsl and/or density raster
        continent=None does the whole world (aggregated). The name of the raster will be the same - individual continents are just for testing
        Note: creates 30-arc second raster (21600x43200) at present
        
        Note that "urban" cells are defined as cells where frc urban nodes within that cell >0.5. Normally, frc urban nodes will be 0 or 1"""            
        
        if continent is None: continent=defaults['osm']['continents']

        if sorted(continent) not in [['fakeplanet'],['planet'], ['africa','asia','australiaoceania','centralamerica','europe','northamerica','southamerica']]:
            print(('\n\n\You are calling aggregate_node_and_edge_results().aggregateToCells() with continent %s.' % continent))
            print(('\n\n\This will overwrite aggregated_by_cell_'+clusterradius))            
            assert input('  Override and continue? (no/yes) ').lower() in ['y','yes']

        if isinstance(continent, list):
            edgeTableNames='('+' UNION ALL '.join(['SELECT * FROM '+defaults['osm']['edge_tables'].replace('REGION',cont).replace('CLUSTERRADIUS',clusterradius) for cont in continent])+')' 
            nodeTableNames='('+' UNION ALL '.join(['SELECT * FROM '+defaults['osm']['cluster_tables'].replace('REGION',cont).replace('CLUSTERRADIUS',clusterradius) for cont in continent])+')' 
            lookupTableNames='('+' UNION ALL '.join(['SELECT * FROM '+'lookup_REGION_clusterCLUSTERRADIUS_GADM_TABLE'.replace('REGION',cont).replace('CLUSTERRADIUS',clusterradius).replace('GADM_TABLE',self.aggTableName) for cont in continent])+')'
        else:
            edgeTableNames=defaults['osm']['edge_tables'].replace('REGION',continent).replace('CLUSTERRADIUS',clusterradius)
            nodeTableNames=defaults['osm']['cluster_tables'].replace('REGION',continent).replace('CLUSTERRADIUS',clusterradius)
            lookupTableNames='lookup_REGION_clusterCLUSTERRADIUS_GADM_TABLE'.replace('REGION',continent).replace('CLUSTERRADIUS',clusterradius).replace('GADM_TABLE',self.aggTableName)
            
        cur=self.get_cur()

        tableName = 'aggregated_by_cell_'+clusterradius
        if self.forceUpdate:
            cur.execute('DROP TABLE IF EXISTS %s;' % (self.schema+'.'+tableName))

        if tableName in osmt.pgisConnection(schema=self.schema).list_tables():
            return # already done the table creation
        
        strdict = {'EDGEMETRICS': ',\n'.join([v+' as '+k for k,v in self.edge_metrics.items()]),
                   'NODEMETRICS': ',\n'.join([self.aggregate_node_metrics(k)+' as '+k for k,v in self.node_metrics.items()]),
                   'URBAN1': ('AVG(urban_'+str(defaults['osm']['urban_pctile'])+'::int)>0.5 AS urban,')*defaults['osm']['landscan']*defaults['osm']['ghsl'],
                   'URBAN2': 'urban,'*defaults['osm']['landscan']*defaults['osm']['ghsl'],
                   'GHSL1': 'mode() WITHIN GROUP (ORDER BY ghsl_yr) AS ghsl_yr,'*defaults['osm']['ghsl'],
                   'GHSL2': 'ghsl_yr,'*defaults['osm']['ghsl'],                   
                   'EDGECOLS':','.join(list(self.edge_metrics.keys())), 'NODECOLS':','.join(list(self.node_metrics.keys())),
                   'NEWTABLE': self.schema+'.'+tableName, 'GADM_TABLE':self.aggTableName,
                   'EDGETABLE': edgeTableNames, 'NODETABLE': nodeTableNames,
                   'LOOKUPTABLE': lookupTableNames}
                   
        # Aggregate edge metrics based on start and end points. 
        # Note: TRUNC rounds down to nearest integer, meaning the geometry of the cell is the CENTER
        # WARNING: this double counts all edges (both at start point and end point), but there should not be any bias
        cmd = '''DROP TABLE IF EXISTS %(NEWTABLE)s_edgestmp;
               CREATE TABLE %(NEWTABLE)s_edgestmp AS 
               SELECT %(EDGEMETRICS)s, row, col\n
               FROM 
                  (SELECT *, TRUNC((ST_Y(ST_StartPoint(geom))-90)/-180*21600)::int AS row,
                             TRUNC((ST_X(ST_StartPoint(geom))+180)/360*43200)::int AS col
                   FROM %(EDGETABLE)s t1 WHERE nocars=False
                          UNION ALL
                   SELECT *, TRUNC((ST_Y(ST_EndPoint(geom))-90)/-180*21600)::int AS row,
                             TRUNC((ST_X(ST_EndPoint(geom))+180)/360*43200)::int AS col
                   FROM %(EDGETABLE)s t2 WHERE nocars=False
                  ) AS edges 
               GROUP BY row, col;''' % strdict
        cur.execute(cmd) 
               
        # Aggregate node metrics
        cmd = '''DROP TABLE IF EXISTS %(NEWTABLE)s_nodestmp;
               CREATE TABLE %(NEWTABLE)s_nodestmp AS 
               SELECT %(NODEMETRICS)s, mode()  WITHIN GROUP (ORDER BY iso) AS iso, %(URBAN1)s %(GHSL1)s
                 TRUNC((ST_Y(geog::geometry)-90)/-180*21600)::int AS row,\n
                 TRUNC((ST_X(geog::geometry)+180)/360*43200)::int AS col\n
               FROM %(NODETABLE)s AS nodes, %(LOOKUPTABLE)s AS lookup
               WHERE nodes.cluster_id=lookup.cluster_id AND nocars=False
               GROUP BY row, col;''' % strdict
        cur.execute(cmd)
        
        cmd = '''CREATE TABLE %(NEWTABLE)s AS
                SELECT COALESCE(edges.row,nodes.row) AS row, COALESCE(edges.col,nodes.col) AS col,
                       %(EDGECOLS)s, %(NODECOLS)s, iso, %(URBAN2)s %(GHSL2)s
                       ST_SetSrid(ST_MakePoint(COALESCE(edges.col,nodes.col)/43200.*360-180+1./240, COALESCE(edges.row,nodes.row)/21600.*-180+90-1./240),4326) AS geom
                FROM %(NEWTABLE)s_edgestmp AS edges 
                     FULL OUTER JOIN 
                     %(NEWTABLE)s_nodestmp AS nodes
                ON edges.row=nodes.row AND edges.col=nodes.col;''' % strdict
        cur.execute(cmd)
    
        if defaults['osm']['landscan']:
            cur.create_indices(tableName, geom=True)
            cmd = '''DROP TABLE IF EXISTS %(NEWTABLE)s_tmpmerge;
                     CREATE TABLE %(NEWTABLE)s_tmpmerge AS
                        SELECT row, col, ST_Value(rast, 1, geom) AS ls_pop, ST_Value(rast, 3, geom) AS ls_density
                        FROM landscan, %(NEWTABLE)s
                        WHERE ST_Intersects(rast, geom);''' % strdict
            cur.execute(cmd)    
            cur.merge_table_into_table(tableName+'_tmpmerge', tableName, ['row', 'col'],fromschema=self.schema, targetschema=self.schema, drop_old=True, recreate_indices=True)   
            cur.execute('DROP TABLE %s.%s_tmpmerge;' % (self.schema, tableName))
        
        cur.execute('DROP TABLE %(NEWTABLE)s_edgestmp;' % strdict)
        cur.execute('DROP TABLE %(NEWTABLE)s_nodestmp;' % strdict)
        
        return
        
    def aggregateToWBregions(self):

        assert self.continent in [['fakeplanet'],['planet']]
        cur=self.get_cur()
        
        # create temp lookup table
        c2wb=osmt.country2WBregionLookup().reset_index().rename(columns={'ISOalpha3':'iso'})
        c2wb.columns = [cc.lower() for cc in c2wb.columns]
        cur.df2db(c2wb[['groupcode','groupname','iso']],'wbr_lookup')
        
        for clusterradius in self.clusterradius:
            for density, ghsl, urban in self.density_ghsl_urban:
                strdict=self.make_strdict(self.continent[0],clusterradius,0,density,
                                  ghsl,urban)
                strdict['WBTNAME'] = strdict['TABLENAME'].replace('iso','wbr')
                
                colNames = [cc for cc in cur.list_columns_in_table(strdict['TABLENAME']) if cc not in ['iso','density_decile','ghsl_yr']]
                
                cmd=str_reps(strdict,"""
                    DROP TABLE IF EXISTS MYSCHEMA.WBTNAME;
                    SELECT * INTO MYSCHEMA.WBTNAME
                    FROM (
                            SELECT groupcode AS wbregion,"""+"""density_decile,"""*density+"""ghsl_yr,"""*ghsl+"""\n
                            """+', '.join(['SUM('+cc+') AS '+cc for cc in colNames])+"""\n
                            FROM TABLENAME a, wbr_lookup b WHERE a.iso=b.iso
                             GROUP BY groupcode """+""",density_decile"""*density+""",ghsl_yr"""*ghsl+""" ) AS data;""")
                cur.execute(cmd)        

def define_density_ghsl_urban_combinations(density, ghsl, urban):
    """ This is used in two classes, so define it in one place """
    if density and ghsl:
        density_ghsl_urban=[ (True,True,False),(False,False,False) ]
    elif density:
        density_ghsl_urban=[ (True,False,False),(False,False,False) ]
    elif ghsl:
        density_ghsl_urban=[ (False,True,False),(False,False,False) ]
    elif not density and not ghsl:
        density_ghsl_urban=[ (False,False,False) ]
    if urban:
        assert density and ghsl
        density_ghsl_urban= [  (False,True,False), (False,False,True,), (False,True,True) ]+density_ghsl_urban
    return (density_ghsl_urban)

                
class disconnectivity_analysis():
    """ This creates the final connectivity tables, including the PCA measure, from the aggregated versions of pointmetrics."""
    def __init__(self,density=False,ghsl=False,urban=False,
                 # Following are only used in non-GADM agg region case, and all must be specified if any are:
                 aggregation_IDs=None,
                 aggregation_geometry_table = None):

        self.edge_metrics={
        'frc_length_noncycle':"""CAST(bridge_m+culdesac_m+selfloop_m AS REAL)
        / NULLIF(CAST(bridge_m+culdesac_m+selfloop_m+cycle_m AS REAL),0)
        """,
        'frc_length_bridge':"""CAST(bridge_m AS REAL)
        / NULLIF(CAST(bridge_m+culdesac_m+selfloop_m+cycle_m AS REAL),0)
        """,
        'frc_edges_noncycle':"""CAST(n_bridge+n_culdesac+n_selfloop AS REAL)
        / NULLIF(CAST(n_bridge+n_culdesac+n_selfloop+n_cycle AS REAL),0)
        """,
        'frc_edges_bridge':"""CAST(n_bridge AS REAL) 
        / NULLIF(CAST(n_bridge+n_culdesac+n_selfloop+n_cycle AS REAL),0)
        """,
        'fraction_length_included':"""CAST(bridge_m+culdesac_m+selfloop_m+cycle_m AS REAL)
        / NULLIF(CAST(length_m AS REAL),0)
        """,
        'length_m':'length_m ',
        'log10length_m':'log(length_m)',
        #u'length_m_per_pop':'length_m/ls_pop ',
        'n_edges':'n_bridge+n_culdesac+n_selfloop+n_cycle',
            'curviness':'length_m/sum_startend_length_m',
            'logcurviness':'log(length_m/sum_startend_length_m)',
        }
        self.node_metrics={
        'negdegree':"""CAST(-4*fourplus-deadends-3*threeways AS REAL)
        / NULLIF(CAST(deadends+threeways+fourplus AS REAL),0)
        """,
        'fraction_1_3':"""CAST(deadends+threeways AS REAL)
        / NULLIF(CAST(deadends+threeways+fourplus AS REAL),0)
        """,
        'fraction_deadend':"""CAST(deadends AS REAL) 
        / NULLIF(CAST(deadends+threeways+fourplus AS REAL),0)
        """,
        'n_nodes':'deadends+threeways+fourplus'
        }

        # check sortedness, which is assumed by lognodaldensity formula
        analysis_radii = [RR.split('_') for RR in  get_analysis_radii_suffixes()]
        assert all([int(RR1[0])<int(RR2[0]) for RR1, RR2 in zip(analysis_radii[:-1], analysis_radii[1:])]) 
        assert all([int(RR1[1])==int(RR2[0]) for RR1, RR2 in zip(analysis_radii[:-1], analysis_radii[1:])]) 

        for ii, RRsuffix in enumerate(get_analysis_radii_suffixes()):
            self.node_metrics['n_%s'%RRsuffix]='n_%s'%RRsuffix
            self.node_metrics['distanceRatio_%s'%RRsuffix]="""
                CAST(sum_nd_%s AS REAL) / NULLIF(CAST(sum_cd_%s AS REAL),0)
                """%(RRsuffix,RRsuffix)
            self.node_metrics['logCircuity_%s'%RRsuffix]="""
                LOG(CAST(sum_nd_%s AS REAL) / NULLIF(CAST(sum_cd_%s AS REAL),0))
                """%(RRsuffix,RRsuffix)
            RR2=RRsuffix.split('_')[1]
            self.node_metrics['lognodalsparsity_%s'%RR2] = 'LN(PI()*('+RR2+'/1000.)^2*nnodes/NULLIF(CAST('+'+'.join(
                                         ['N_'+RR for RR in get_analysis_radii_suffixes()[:ii+1]])+' AS real),0))'
        
        self.DISCONNECTIVITY_METRICS=dict(self.edge_metrics,**self.node_metrics)

        self.density_ghsl_urban = define_density_ghsl_urban_combinations(density, ghsl, urban)
        self.stock = True if ghsl and urban else False

        # Provide all or none:
        gotall = aggregation_geometry_table is None and aggregation_IDs is None 
        gotnone =  aggregation_geometry_table is not None and aggregation_IDs is not  None 
        assert gotall or gotnone
        self.agg_IDs = defaults['gadm']['ID_LEVELS'] if aggregation_IDs is None else aggregation_IDs
        self.aggTableName = defaults['gadm']['TABLE'] if aggregation_geometry_table is None else aggregation_geometry_table


    def create_final_connectivity_tables(self,continent=None,clusterradius=None,
                                         resolution=None,
                                         density=None,ghsl=None,urban=None,stock=None,
                                         include_geom=False,
                                         forceUpdate=False
                                        ):
        """
    Create our final (dis)connectivity metrics, using the aggregated_* tables.
    These disconnectivity_* tables are  essentially our final output from all
    the osm processing procedures.

    In priciple, we should be able to do this by making a "view" to the "aggregated_*" tables, which
    generates the metrics we use on the fly. However, VIEWs are hard to
    load in QGIS, so we just make new tables. Also, we put the tables in a
    separate SCHEMA, since we're done with the processing.

    Unit test: tests/connectivity_metrics_test.py


                 To ensure the PCA metrics are consistent and comparable across 
                 geographic resolutions, use a single set of coefficients at all 
                 resolutions. These coefficients are estimated using data at the 
                 finest geographic resolution available.

        Thus, when this method is called with most arguments as "None", we run our "preferred" master version to estimate the PCA for it after creating the other columns.
        We then run everything (including rerunning that master flavour/table) in parallel, and these will use the result from that master estimate to create the PCA columns.
        We therefore only need to call this function ONCE, because it will itself run the necessary table twice.

        """

        # Special case: If this is called with no arguments    [continent,clusterradius,resolution,density,ghsl,urban], and the class was instantiated without urban=False, then we should
        # (1) Run the "master" id_5 case once (with include_geom=False), which will estimate the master PCA, then
        # (2) Run everything, in parallel (which is done already,  below)

        if all([vv is None for vv in [continent,clusterradius,resolution,density,ghsl,urban] ]) and (False,False,True) in self.density_ghsl_urban:
            assert len(defaults['osm']['continents'])==1
            assert len(defaults['osm']['clusterRadii'])==1
            self.create_final_connectivity_tables(continent = defaults['osm']['continents'][0], clusterradius = defaults['osm']['clusterRadii'][0], resolution = 5,
                density=False, ghsl=False, urban=True, stock=False, include_geom=False, forceUpdate = forceUpdate)

        # If this run will invoke a parallelization of multiple calls to this
        # function:
        if any([vv is None or isinstance(vv, list) for vv in
                          [continent,clusterradius,resolution,density,ghsl,urban]
                         ]) :

            continent = get_all(continent,default=defaults['osm']['continents'])
            clusterradius = get_all(clusterradius,default=defaults['osm']['clusterRadii'])
            resolution = get_all(resolution,default=[5,4,3,2,1,0])
            stocks = [True, False] if self.stock else [False]
            funcs,names=[],[]
            for cr in clusterradius:
                for cont in continent: 
                    for res in resolution:
                        for densityItem,ghslItem,urbanItem in self.density_ghsl_urban:
                            for stockItem in stocks:
                                if stockItem and (densityItem is True or ghslItem is False): continue
                                funcs+=[[self.create_final_connectivity_tables,[cont,cr,res,densityItem,ghslItem,urbanItem,stockItem,include_geom,forceUpdate]]]
                                names+=['-'.join([defaults['server']['postgres_db'],'create_final_connectivity_tables',cont,cr,str(res),'density'*densityItem,'ghsl'*ghslItem,'urban'*urbanItem,'stock'*stockItem])]
            runFunctionsInParallel(funcs,names=names,maxAtOnce=None, parallel=defaults['server']['parallel'])
            return

        
        # Initialize logger. Each call to this function has its own logger.
        _logger_params=['connectivity',continent,clusterradius,
                        self.agg_IDs[resolution]]
        if density:
            _logger_params.append('density')
        if ghsl:
            _logger_params.append('ghsl')
        if urban:
            _logger_params.append('urban')
        if stock:
            _logger_params.append('stock')
        logger=osmt.osm_logger('_'.join(_logger_params),verbose=True,
                               timing=True,overwrite=forceUpdate
                              )
        logger.write('Including geometry: '+str(include_geom).upper()+'\n'+ \
                     'Force update: '+str(forceUpdate).upper()
                    )
        logger.write('   Beginning a create_final_connectivity_tables for '+str([continent,clusterradius,
                                         resolution,
                                         density,ghsl,urban,
                                                                         include_geom,
                                                                                 forceUpdate]))
            
        cur =osmt.pgisConnection(verbose=True, logger=logger)
            
        strdict=dict(
            CONTINENT=continent,
            NNCL=clusterradius,
            GADMIDLEVEL=self.agg_IDs[resolution], 
            ID_COL_LIST=','.join(self.agg_IDs[:resolution+1]),
            DENSITYSTR='density_decile,' if density else '',
            GHSLSTR='ghsl_yr,' if ghsl else '',
            _DENSITY='_density'*density, _GHSL='_ghsl'*ghsl, _URBAN='_urban'*urban, _STOCK='_stock'*stock,
            GADMLEVELNONZERO=self.agg_IDs[resolution]+' != 0' if resolution>0 else "iso != ''" ,
            MYSCHEMA=defaults['osm']['analysisSchema'],
            GADMTABLE=self.aggTableName
            )
        strdict['ATABLENAME']='aggregated_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_GHSL}{_URBAN}{_STOCK}'.format(**strdict)
        strdict['DTABLENAME']='disconnectivity_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_GHSL}{_URBAN}{_STOCK}'.format(**strdict)

        # Initialize the PCA interface:
        pcAnalyzer=principal_component_analysis(weights= 'ls_pop' if defaults['osm']['landscan'] else None,
                                                )
        
        cmdstr='DROP TABLE IF EXISTS {MYSCHEMA}.{DTABLENAME} CASCADE;'

        # Still need nodal density, population density, total distance, and counts of    # nodes, edges, etc, other measures we can produce at GADM level, here:
        #  The PCA vectors can only be calculated after all of the above are created 
        # so we must add them in as extra columns:
        # Method below: create all the non-PCA columns in a temporary table 
        # "most_columns" and then put them, and the PCA columns made from then, into 
        # the permanent "disconnectivity" table.
        cmdstr+="""
        CREATE  TABLE {MYSCHEMA}.{DTABLENAME} AS (
            WITH most_columns AS ( SELECT
                {ID_COL_LIST},
                {DENSITYSTR}
                {GHSLSTR}
                """+',\n'.join([
                    self.DISCONNECTIVITY_METRICS[metric] + ' as ' + metric for 
                    metric in list(self.DISCONNECTIVITY_METRICS.keys())
                    ])+"""
            from {ATABLENAME} )
            SELECT *,
            -- And now add some columns built from those above, according to the coefficients from PCA analysis:
            """+ pcAnalyzer.get_psql_code_to_apply_coefficients_to_data(format='as', use_tablename='most_columns')+"""
            from most_columns
        );"""
        cur.execute(cmdstr.format(**strdict))

        # Add in population and density data, the geometry column and geometry
        # centroids
        strdict['GEOM_COL']='geom' if include_geom else 'Null::geometry'
        strdict['LANDSCAN_COLS']='ls_pop,ls_density,'*defaults['osm']['landscan']

        strdict['TMPTN']= strdict['DTABLENAME'].replace('disconnectivity','tmp_geometrydata')
        

        # Make temporary table
        cmd="""DROP TABLE IF EXISTS {TMPTN};

               CREATE TABLE {TMPTN}
               AS
               (
                SELECT {ID_COL_LIST}, {GEOM_COL} AS geom,{LANDSCAN_COLS}
                TRIM(both ' )' from SUBSTRING(ST_AsText(ST_Transform(ST_Centroid({GEOM_COL}),4326)) from ' .*\)$' ))::DOUBLE PRECISION as geom_centroid_lat,
                TRIM(both 'POINT (' from SUBSTRING(ST_AsText(ST_Transform(ST_Centroid({GEOM_COL}),4326)) from 'POINT\(.* ' ))::DOUBLE PRECISION as geom_centroid_lon
                FROM {GADMTABLE}_{GADMIDLEVEL}
               );"""                    
        cur.execute(cmd.format(**strdict))

        # Merge into disconnectivity table
        cur.merge_table_into_table(from_table=strdict['TMPTN'],
                                   targettablename=strdict['DTABLENAME'],
                                   targetschema=strdict['MYSCHEMA'],
                                   commoncolumns=strdict['ID_COL_LIST'].split(',')
                                  )

        # Delete temporary table
        cur.execute('DROP TABLE {TMPTN};'.format(**strdict))

        # Add in logNodalSparsity
        cur.addColumnToTable(table=strdict['DTABLENAME'],columnname='lognodalsparsity_unweighted',
                             columntype='double precision',schema=strdict['MYSCHEMA']
                            )
        if include_geom and not density and not ghsl: # nullif is to avoid taking log of zero
            # only valid for whole geometries, i.e. not subdivided by density or ghsl year
            cmd="""UPDATE {MYSCHEMA}.{DTABLENAME} SET
                   lognodalsparsity_unweighted = LN(ST_AREA(geom::geography)/1000000)-LN(NULLIF(n_nodes,0)) 
                   ;"""
            cur.execute(cmd.format(**strdict))
        else:
            log('Skipping unweighted nodal sparsity because geometry column does not exist or because ghsl or density is True.',logger)

        # Add indices to the disconnectivity table 
        non_unique_columns=strdict['ID_COL_LIST'].split(',')[:3]

        cur.create_indices('disconnectivity_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_GHSL}{_URBAN}{_STOCK}'.format(**strdict), non_unique_columns=non_unique_columns,geom=True)
        if include_geom:
            cur.ensure_consistent_geom('disconnectivity_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_GHSL}{_URBAN}{_STOCK}'.format(**strdict),
            column=strdict['GEOM_COL'], schema=strdict['MYSCHEMA'])
    
        # Now create a view of the deltas for the disconnectivity tables
        if ghsl and not stock: # can only do deltas if the ghsl data exist
            cmd = 'DROP VIEW IF EXISTS disconnectivity_delta_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_URBAN}'
            cur.execute(cmd.format(**strdict))
            # also drop older views (left over from prior code  - issue #366) that have 'ghsl' in the view name
            cmd = 'DROP VIEW IF EXISTS disconnectivity_delta_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_GHSL}{_URBAN}'
            cur.execute(cmd.format(**strdict))
            strdict['METRICDELTAS'] = ',\n'.join(['t2.'+kk+'-t1.'+kk+' AS '+kk for kk in list(self.DISCONNECTIVITY_METRICS.keys())+['PCA1','PCA2']])
            strdict['ID_COL_NOGHSL_LIST'] = ','.join(['t1.'+kk for kk in strdict['ID_COL_LIST'].split(',')+[strdict['DENSITYSTR'].rstrip(',')]*density])
            strdict['WHERE_CLAUSE'] = ' AND '.join(['t1.'+kk+'=t2.'+kk for kk in strdict['ID_COL_LIST'].split(',')+[strdict['DENSITYSTR'].rstrip(',')]*density])
                            
            unionCmds = []
            # build up a series of UNION ALLs that will create the view
            for yr1 in [1975,1990,2000,2014]:
                for yr2 in [1990,2000,2014]:
                    if yr2<=yr1: continue
                    cmd = '''SELECT t1.geom, {ID_COL_NOGHSL_LIST}, 'YR1-YR2' AS ghsl_yr, {METRICDELTAS}
                             FROM  (SELECT * FROM disconnectivity_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_GHSL}{_URBAN} WHERE ghsl_yr=YR1) t1,
                                   (SELECT * FROM disconnectivity_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_GHSL}{_URBAN} WHERE ghsl_yr=YR2) t2
                             WHERE {WHERE_CLAUSE}'''
                    cmd = cmd.replace('YR1',str(yr1)).replace('YR2',str(yr2))
                    unionCmds+=[cmd]  
            
            # issue #366 - don't include 'ghsl' in view name, because all deltas are ghsl
            cmd = 'CREATE VIEW disconnectivity_delta_{GADMIDLEVEL}_{CONTINENT}_{NNCL}{_DENSITY}{_URBAN} AS\n'+'\nUNION ALL\n'.join(unionCmds)
            cur.execute(cmd.format(**strdict))

            if isinstance(logger,osmt.osm_logger):
                logger.close()
               
        # Even though setting defaults['osm']['PCA_coefficients_from'] to "calculate" is supposed to get us to estimate and write PCA coefficients, we actually refuse to write them unless we are using our base case, which is ID_5 level and not density and not ghsl and urban and not stock:
        if defaults['osm']['PCA_coefficients_from'] == 'calculate' and not density and not ghsl and urban and not stock and resolution==5:
            logger.write("  *** Calculating MASTER PCA coefficients for {}... (defaults['osm']['PCA_coefficients_from'] == 'calculate') *** ".format(strdict['DTABLENAME']))
            df = cur.db2df(strdict['DTABLENAME'], schema = strdict['MYSCHEMA'])[pcAnalyzer.datavars+ ([] if pcAnalyzer.weights is None else [pcAnalyzer.weights])]
            pcAnalyzer.estimate_and_save_master_pca(df, '{MYSCHEMA}-{DTABLENAME}'.format(**strdict))


                
    def create_final_disconnectivity_wbregions(self,continent=None, aggTables=None, forceUpdate=False,logger=None):
        """Similar to create_final_connectivity_tables, but relies on a different PCA.
        So it's hard to integrate into that workflow
        If aggTables is None, creates disconnectivity tables for all the aggregated_wbr_* tables that it finds"""

        if isinstance(logger,osmt.osm_logger):
            logger=logger
        elif logger==True:
            logger=osmt.osm_logger('disconnectivity_bywbregion',verbose=True, timing=True,overwrite=forceUpdate)
        
        if continent is None: continent=defaults['osm']['continents']
        if continent not in [['fakeplanet'],['planet']]:
            if logger: logger.write('Skipping disconnectivity to WB regions because continent %s is not planet or fakeplanet.' % continent)
            return
            
        cur =osmt.pgisConnection(schema=defaults['osm']['analysisSchema'],verbose=True, logger=logger)
        
        if aggTables is None:
            aggTables = [tn for schema in cur.execfetch('SHOW search_path;')[0][0].split(', ')
                            for tn in cur.list_tables(schema) 
                            if tn.startswith('aggregated_wbr_'+continent[0])]
        elif not(isinstance(aggTables,list)):
            aggTables=[aggTables]
            
        for aggTable in aggTables:
            disconn_tn=aggTable.replace('aggregated_','disconnectivity_')
            if disconn_tn in cur.list_tables() and not forceUpdate:
                if logger: logger.write('Skipping creating %s. Already exists' % disconn_tn)
                return
            if logger: logger.write('Creating table %s' % disconn_tn)      
            
            strdict = {'MYSCHEMA':defaults['osm']['analysisSchema'], 
                       'DTABLENAME': disconn_tn, 'AGGTABLENAME': aggTable,
                       'DENSITYSTR': 'density_decile'*('density' in aggTable),
                       'GHSLSTR':'ghsl_yr'*('ghsl' in aggTable),
                       'GADM_TABLE':self.aggTableName}
                        
            cmdstr='DROP TABLE IF EXISTS MYSCHEMA.DTABLENAME CASCADE;'
            cmdstr+="""
            CREATE TABLE MYSCHEMA.DTABLENAME AS (
                SELECT wbregion, """+"density_decile,"*('density' in aggTable)+"ghsl_yr,"*('ghsl' in aggTable)+"""\n
                        """+',\n'.join([
                        self.DISCONNECTIVITY_METRICS[metric] + ' as ' + metric for 
                        metric in list(self.DISCONNECTIVITY_METRICS.keys())
                        ])+""",\n"""+',\n'.join(['Null::real AS '+mm for mm in ['ls_pop','pca1','pca2','v1_explained_variance_ratio','v2_explained_variance_ratio']])+"""
                FROM AGGTABLENAME a);"""
            cur.execute(str_reps(strdict,cmdstr))
            
            cmdstr="""UPDATE MYSCHEMA.DTABLENAME
                      SET ls_pop = popsum FROM 
                         (SELECT groupcode, sum(ls_pop) AS popsum FROM wbr_lookup l, GADM_TABLE_iso g WHERE l.iso=g.iso
                              GROUP BY l.groupcode) t WHERE groupcode=wbregion;"""
            cur.execute(str_reps(strdict,cmdstr))
            
            #no, let's use the same as for gadm-level analysis (i.e. use id_5 level PCA)
            # Initialize the PCA interface:
            pcAnalyzer=principal_component_analysis(weights= 'ls_pop' if defaults['osm']['landscan'] else None,  )#   PCA_from_continent = self.PCA_from_continent )
            
            #pdf = principal_component_analysis().get_saved_PCA_coefficients(GADM_ID_LEVELS[-1])
            cr =  aggTable.strip('aggregated_wbr_'+continent[0]).split('_')[0]
            #pdf = pdf[pdf.clusterradius==cr]
            #assert not pdf.empty      
            
            cmdstr="""UPDATE MYSCHEMA.DTABLENAME SET
                  """+ pcAnalyzer.get_psql_code_to_apply_coefficients_to_data(format='equals')+"""  ; """
            cur.execute(str_reps(strdict,cmdstr))

    def create_final_disconnectivity_cells(self,clusterradius=None, override=False, logger=None):
        """Similar to create_final_connectivity_tables, but does this for the grid cell version
        Grid cell aggregates are created with aggregate_node_and_edge_results.aggregateToCells()
	If override is False, this will be skipped for a partial planet (e.g. just a single continent)"""


        if isinstance(logger,osmt.osm_logger):
            logger=logger
        elif logger==True:
            logger=osmt.osm_logger('disconnectivity_bycell_'+'_'.join(get_all(clusterradius,default=defaults['osm']['clusterRadii'])),verbose=True,
                                       timing=True)

        if sorted(defaults['osm']['continents']) not in [['fakeplanet'],['planet'], ['africa','asia','australiaoceania','centralamerica','europe','northamerica','southamerica']]:
                if override is False:
                    if logger: logger.write('Skipping create_final_disconnectivity_cells() because continents %s are a subset of planet.' % defaults['osm']['continents'])
                    return
                else:
                    print(('\n\n\You are calling aggregate_node_and_edge_results().create_final_disconnectivity_cells() with continents %s.' % defaults['osm']['continents']))
                    print(('\n\n\This will overwrite disconnectivity_by_cell_'+clusterradius))            
                    assert input('  Override and continue? (no/yes) ').lower() in ['y','yes']
                    if logger: logger.write('Overriding skip of create_final_disconnectivity_cells().')

        schema = defaults['osm']['analysisSchema']
        cur = osmt.pgisConnection(schema=schema, verbose=True, logger=logger)
        clusterradius=defaults['osm']['clusterRadii'] if clusterradius is None \
                               else clusterradius if isinstance(clusterradius, list) \
                               else [clusterradius]
                               
        for cr in clusterradius:

            aggTableName = 'aggregated_by_cell_'+cr
            disconnTableName = 'disconnectivity_by_cell_'+cr
   
            if aggTableName not in cur.list_tables(schema=defaults['osm']['processingSchema']):
                print('Need to aggregate cells before creating disconnectivity measures')
                return

            cur.execute('DROP TABLE IF EXISTS %s.%s;' % (schema, disconnTableName))

            strdict=dict(
                AGG_TABLE_NAME=aggTableName,
                DISCONN_TABLE_NAME=disconnTableName,
                NNCL=cr,
                MYSCHEMA=defaults['osm']['analysisSchema'],
                GADM_TABLE=self.aggTableName
                )
                
            dmetrics = self.DISCONNECTIVITY_METRICS.copy()
            dmetrics['lognodalsparsity_unweighted'] = 'logarea-LN(NNODES)'
            
            extraCols = ','.join([cc for cc in cur.list_columns_in_table(aggTableName) if cc in ['ls_pop','ls_density','urban','ghsl_yr']])
            if extraCols!='': extraCols+=',\n'
            cmdstr="""
            CREATE  TABLE MYSCHEMA.DISCONN_TABLE_NAME AS (
                WITH areas AS (SELECT row AS arow, LN(ST_Area(ST_GeomFromText('POLYGON((0 '||lat||', 0.008333 '||lat||', 0.008333 '||lat-0.008333||', 
                                                    0 '||lat-0.008333||', 0 '||lat||'))',4326)::geography)/1000000) AS logarea
                        FROM (SELECT DISTINCT row, (row / 21600::real * -180 ) + 90 AS lat  FROM AGG_TABLE_NAME) t1),
                     most_columns AS ( SELECT
                    row, col, iso, 
                    """+extraCols+',\n'.join([
                        dmetrics[metric] + ' as ' + metric for metric in list(dmetrics.keys())
                        ])+',\n'+','.join(['Null::real AS '+mm for mm in ['pca1','pca2','v1_explained_variance_ratio','v2_explained_variance_ratio']])+"""          
                from AGG_TABLE_NAME, areas WHERE areas.arow = AGG_TABLE_NAME.row )
                SELECT *
                from most_columns
            );"""
            cur.execute(str_reps(strdict, cmdstr))

 
            pcAnalyzer=principal_component_analysis(weights= 'ls_pop' if defaults['osm']['landscan'] else None,)
#                                                                             PCA_from_continent = self.PCA_from_continent )

            # Here's a strange hard-code kludge: Add +2.15 to the pca1 vector, so that its values are positive.
            cmdstr="""UPDATE MYSCHEMA.DISCONN_TABLE_NAME SET
            """+ pcAnalyzer.get_psql_code_to_apply_coefficients_to_data(format='equals')+"""  ;   """
            cur.execute(str_reps(strdict,cmdstr)) 



    # As a sanity check, check that, for distinct combinations of continents,
    # cluster radii, density and ghsl items, the ranges of the PCA metrics are both
    # more restricted at coarser resolutions, and are nested within the ranges of
    # the same metric at finer resolutions.  Possibly needs updating since a commit or two after 9195846de22aa0a0169e3d9dd0a81ab0875a4672
    def check_connectivity_results(self,continent=None,clusterradius=None,
                                   resolution=None,set_logger=True):
        continent=get_all(continent,default=defaults['osm']['continents'])
        clusterradius=get_all(clusterradius,default=defaults['osm']['clusterRadii'])
        resolution=get_all(resolution,default=[5,4,3,2,1,0])
        
        for cr,cont,res,densityItem,ghslItem,urbanItem in iter_through_tables(clusterradius,
                continent,resolution,
                self.density_ghsl_urban): 
            if res==0: 
                continue

            # Append onto the connectivity logger initialized in
            # create_final_connectivity_tables()
            if set_logger:
               _logger_params=['connectivity',cont,cr,self.agg_IDs[res]]
               if densityItem:
                   _logger_params.append('density')
               if ghslItem:
                   _logger_params.append('ghsl')
               if urbanItem:
                   _logger_params.append('urban') 
               logger=osmt.osm_logger('_'.join(_logger_params),
                                      verbose=True,timing=True,
                                      overwrite=False
                                     )
            else:
                logger=None

            cur=osmt.pgisConnection(verbose=True,logger=logger,
            schema=defaults['osm']['analysisSchema'])

            strdict={ 'cont':cont, 'cr':cr,
                      'density': '_density' if densityItem else '',
                      'ghsl': '_ghsl' if ghslItem else '',
                      'urban': '_urban' if urbanItem else '',
                      'res': self.agg_IDs[res],
                      'enclosing_res': self.agg_IDs[res-1]
                    }
            tablename='disconnectivity_%(res)s_%(cont)s_%(cr)s%(density)s%(ghsl)s%(urban)s'%strdict
            coarser_tablename='disconnectivity_%(enclosing_res)s_%(cont)s_%(cr)s%(density)s%(ghsl)s%(urban)s'%strdict
            strdict['tn']=tablename
            strdict['ctn']=coarser_tablename
            pcas=[ col for col in cur.list_columns_in_table(tablename) 
                   if 'pca' in col ]
            for pca in pcas:
                strdict['pca']=pca
                min_,max_=cur.execfetch("""
                          SELECT MIN(%(pca)s),MAX(%(pca)s)
                          FROM %(tn)s;"""%strdict)[0]
                coarse_min,coarse_max=cur.execfetch("""
                          SELECT MIN(%(pca)s),MAX(%(pca)s)
                          FROM %(ctn)s;"""%strdict)[0]
                if not ( coarse_min >= min_ and 
                         coarse_max <= max_ ):
                    log("""Mismatched ranges for {}:
                           {}: {} -- {}
                           {}: {} -- {}
                        """.format(pca,tablename,min_,max_,
                             coarser_tablename,coarse_min,
                             coarse_max),logger)
                    #assert min_ # to do: What does it mean when this comes back as None?
            if isinstance(logger,osmt.osm_logger):
                logger.close()

# Can classes have @static data members? following should be one of the following class, if such things exist in python
PCA_VARS_LOGFORM = ['negdegree', 'fraction_deadend', 'frc_length_bridge', 'frc_length_noncycle', 'frc_edges_noncycle', 'frc_edges_bridge', 'logcurviness', 'logcircuity_0_500', 'logcircuity_500_1000', 'logcircuity_1000_1500', 'logcircuity_1500_2000', 'logcircuity_2000_2500', 'logcircuity_2500_3000']
class principal_component_analysis(object):
    def __init__(self,n_components=2, weights=None):#, compute_PCA_anew= False):
        
        """ 
        By default, we use coefficients given in the open source release from 2019/2020 papers. 
Rather than weight the results, we simply take everything at the id5 level.
        See "weightmethod" below. We use the weight variable only for sorting, but sorting is now irrelvant since we take everything.

        """
        self.package='stata'
        self.method = 'cor'
        self.n_components=n_components
        # Get data for PCA analysis
        #nologs = ['negdegree','fraction_deadend','frc_length_bridge','frc_length_noncycle','frc_edges_noncycle','frc_edges_bridge','curviness']+[dd for dd in df2.columns if 'distanceratio' in dd]
        self.datavars = PCA_VARS_LOGFORM
        kdm= [dm.lower() for dm in list(disconnectivity_analysis().DISCONNECTIVITY_METRICS.keys())]
        assert all([vv.lower() in kdm for vv in self.datavars])
        self.datavars = order_variables(self.datavars, include_everything=True)
        self.pcacols = ['PCA{}'.format(ii+1) for ii in range(n_components)]
        self.weights=weights
        assert len(defaults['osm']['clusterRadii'])==1
        self.clusterRadius= defaults['osm']['clusterRadii'][0]



        """
        # Major revision here in approach to safety [20190303] wrt to pca coefficients:
        #  But always read PCA estimates based on the setting in defaults['osm']['PCA_from_continent']
        # OLD Version notes:

        # The following distinction is to make perfectly sure we never write coefficients to an external file that we are using for its PCA coefficients.
        # In fact, below we also simply abort estimate_and_save_master_pca() when independent coefficients are to be used (ie from another continent)
        # And we don't even define  self.master_write_file  when PCA_from_continent is specified  (even if it's specified to be the same as the current continent, right?)

        assert 'PCA_from_continent' in defaults['osm']
        assert PCA_from_continent is None or PCA_from_continent == defaults['osm']['PCA_from_continent']  # issue 495
        PCA_from_continent =  defaults['osm']['PCA_from_continent']
        assert len(defaults['osm']['continents'])==1 or PCA_from_continent is not None
        #  Always write PCA estimates to the standard file target.
        self.write_continent = defaults['osm']['continents'][0]
        self.master_write_file = paths['working']+'master_id5_PCA_coefficients_{continent}_{radius}_urban.pyshelf'.format(continent=self.write_continent, radius=self.clusterRadius)
        self.read_continent = defaults['osm']['continents'][0] if PCA_from_continent is None else  PCA_from_continent
        self.master_read_file = paths['working']+'master_id5_PCA_coefficients_{continent}_{radius}_urban.pyshelf'.format(continent=self.read_continent, radius=self.clusterRadius)
        if PCA_from_continent is not None and not os.path.exists(self.master_read_file):
            raw_input('  You have specified a custom/external master PCA coefficients file, but it does not exist. \n  Please copy the file to {} and press enter.'.format(self.master_read_file))
            assert os.path.exists(self.master_read_file) # Must already exist
        """
        if defaults['osm']['PCA_coefficients_from'] == 'hardcoded':
            self.read_PCA_file = None
        elif defaults['osm']['PCA_coefficients_from'] == 'estimate':
            assert len(defaults['osm']['continents'])==1
            # Note that the following file is the same for all variants of resolution, urban, stock, ghsl. However, we will later actually refuse to write to it unless not density and not ghsl and urban and not stock and resolution==5
            self.read_PCA_file = paths['working']+'master_id5_PCA_coefficients_{continent}_{radius}_urban.pyshelf'.format(continent=defaults['osm']['continents'][0], radius=self.clusterRadius)
        else:
            assert os.path.exists(defaults['osm']['PCA_coefficients_from'])
            self.read_PCA_file = os.path.exists(defaults['osm']['PCA_coefficients_from'])
            
        print((' Aggregation object initialized with PCA to come from {}{}.'.format(self.read_PCA_file,
              (defaults['osm']['PCA_coefficients_from'] == 'estimate')*" *after* estimating (overwriting) them based on current data\n   When region_metrics is finished, you can copy this file to a permanent location, and change your config defaults['osm']['PCA_coefficients_from'] to use it."
        )))
        
    def dummy_pca_results(self):
        df= pd.DataFrame(index=self.datavars, columns = self.pcacols).fillna(0)
        return pca_result(df, data_means =[0] *len(self.datavars),  data_stds = [1]*len(self.datavars), method_comments='Fake/null data: dummy values')
    def estimate_and_save_master_pca(self,df, tablename, weightmethod = 'allunweighted'):#'top5000'):
        assert defaults['osm']['PCA_coefficients_from'] == 'estimate'
        assert weightmethod in ['allunweighted']# 'top5000','weighted','all' ...
        if self.weights:
            if weightmethod in ['top5000']:
                dfB = df.sort_values(self.weights,ascending=False).dropna()
                df = dfB[:min(5000,len(dfB))]
            df = df[[cc for cc in df.columns if cc not in [self.weights]]]
        pcar= estimatePCA(df, weight=None , tmpname=tablename, scratch_path=paths['scratch'], method=self.method, package=self.package, verbose=True, dropna=True,
                          method_comments = ' '.join([weightmethod, self.method, self.package, tablename]))
        # N.B. pcar contains much more than the DataFrame it appears to be. It has other member objects like pca.explained, etc.
        pcar.save(self.read_PCA_file)
    def load_master_pca_results(self):
        """ This retrieves the *coefficients* to use """
        if defaults['osm']['PCA_coefficients_from'] == 'hardcoded':
            return pca_result()
        if os.path.exists(self.read_PCA_file):
            return pca_result(self.read_PCA_file)
            # It's possible that when running in parallel, this needs a try/catch/sleep/repeat for reading the file.
        else:
            return self.dummy_pca_results()
        
    def get_psql_code_to_apply_coefficients_to_data(self, format='equals', use_tablename=''):
        """  Use the existing "master" coefficients, give postgresql code to generate PCA1 and PCA2.        """
        pca =  self.load_master_pca_results()
        if use_tablename: use_tablename+='.'
        if format == "equals":
            return """  pca1 = """+PCA1_BOOST+""" 
                      """+ '+'.join(['({datavar}-({vmean}))/{vstd}*{coef}'.format(datavar=datavar, vmean=pca.data_means.loc[datavar,'data_mean'], vstd= pca.data_stds.loc[datavar,'data_std'], coef = pca.df.loc[datavar,'PCA1'])
                                     for datavar in self.datavars])+""",
                        pca2 = 
                      """+ '+'.join(['({datavar}-({vmean}))/{vstd}*{coef}'.format(datavar=datavar, vmean=pca.data_means.loc[datavar, 'data_mean'], vstd= pca.data_stds.loc[datavar,'data_std'], coef = pca.df.loc[datavar,'PCA2'])
                                     for datavar in self.datavars])+"""           """
        elif format =='as':
            return     '  '+PCA1_BOOST+ '+'.join(['({tab}{datavar}-({vmean}))/{vstd}*{coef}'.format(datavar=datavar, vmean=pca.data_means.loc[datavar, 'data_mean'], vstd= pca.data_stds.loc[datavar, 'data_std'], coef = pca.df.loc[datavar,'PCA1'], tab=use_tablename)
                                     for datavar in self.datavars])+"""  as pca1,
                      """+ '+'.join(['({tab}{datavar}-({vmean}))/{vstd}*{coef}'.format(datavar=datavar, vmean=pca.data_means.loc[datavar,'data_mean'], vstd= pca.data_stds.loc[datavar,'data_std'], coef = pca.df.loc[datavar,'PCA2'], tab=use_tablename)
                                     for datavar in self.datavars])+""" as pca2      """
        else:
            raise Exception('Do not know that format')
    def format_LaTeX_table_of_PCA_coefficients(self):
        pcar=self.load_master_pca_results()
        df = pcar#.correlations

        # Reorder variables:
        df = df.reindex(order_variables(df.index, include_everything=True)) 
        # Rename variables
        df.index =df.index.map(lambda ss: metricNames.get(ss.lower(),  metricNames.get(ss,ss.replace('_','-'))))

        # Choose columns
        df=df[['PCA1','PCA2','PCA3']].fillna('')

        # Add rows for estimate diagnostics. The hline is a kludge to add a hline in the LateX table
        df.loc[r'\hline Variance explained',:] = pcar.explained[:(len(df.columns))].map(lambda ff: '%d\\%%'%(ff*100))
        df.loc[r'Eigenvalue',:] = pcar.eigenvalues[:(len(df.columns))].map(lambda ff: '%.1f'%ff)
        
        from cpblUtilities.textables import formatDFforLaTeX, dataframeWithLaTeXToTable

        # In-place formatting (sig digs, negative signs)
        formatDFforLaTeX(df,row=None,sigdigs=3,colour=None,leadingZeros=False)
        df.index.name=''
        df.rename(columns={'PCA1':'PCA$_1$','PCA2':'PCA$_2$', 'PCA3':'PCA$_3$',}, inplace=True)
        fn = paths['output']+'tables/PCA-coefficients.tex'
        dataframeWithLaTeXToTable(df.fillna('').reset_index(), fn)
        # Also make a figure of the eigenvalues?
        pcar.diagnostic_plot(paths['output']+'graphics/PCA-diagnostics.pdf')
        return

def create_dataframes_microdata(continent=None,clusterradius=None,  edge_columns=None, node_columns=None, forceUpdate=False,):
    """
    We may want Pandas data at the edge or node level.

    """

    strdict=dict(
        CONTINENT=continent,
        NNCL=clusterradius,
        MYSCHEMA=defaults['osm']['processingSchema']
        )
    cur =osmt.pgisConnection(verbose=True)
    if edge_columns is None:
        edge_columns=['classification','edge_id',]
    if node_columns is None:
        node_columns=['node_id',]
    cmd="""SELECT  """+','.join(edge_columns)+"""
                       FROM MYSCHEMA.edges_CONTINENT_NNCL;"""
    outl=cur.execfetch(str_reps(strdict, cmd))

def mapAggregatedResults(metrics=None,continent=None,clusterradius=None,resolution=None,density=False,ghsl=False,urban=False,logger=None,forceUpdate=False):
    """Colorizes the svgs with the aggregated results
    If None, we do all
    Specifying a continent does a svg of just that continent, or a list of continents. World does the whole world. None does all
    
    density can specify a density decile, a list of deciles, or None (or True) to do all the deciles. False does all densities combined
    Same with ghsl, which can be a year or a delta (e.g. '1975-1990')
    
    forceUpdate just updates the blank svgs
    """
    if not(os.path.exists(paths['scratch']+'worldmaps')): os.mkdir(paths['scratch']+'worldmaps')

    resolution = list(range(6)) if resolution is None else resolution if isinstance(resolution, list) else [resolution]
    continent = defaults['osm']['continents'] if continent is None else continent if isinstance(continent, list) else [continent]
    # world is different from 'planet' as it assumes that data is in individual continent tables
    if sorted(continent)==['africa','asia','australiaoceania','centralamerica','europe','northamerica','southamerica']: continent+=['world']

    if metrics is None: metrics = ['length_m_per_pop', 'length_m',]+[mm for mm in list(disconnectivity_analysis().DISCONNECTIVITY_METRICS.keys()) if not(mm.startswith('n_'))]+['PCA1','PCA2']
    if not(isinstance(metrics, list)): metrics = [metrics]    
    clusterradius = defaults['osm']['clusterRadii'] if clusterradius is None else clusterradius if isinstance(clusterradius, list) else [clusterradius]
    density = [False]+list(range(1,11)) if density in [None,True] else density if isinstance(density, list) else [density]
    urban = [False,True] if urban in [None,True] else urban if isinstance(urban, list) else [urban]
    ghsl = [False,1975,1990,2000,2014,"'1975-1990'","'1975-2000'","'1975-2014'","'1990-2000'","'1990-2014'","'2000-2014'"] if ghsl in [None,True] else ghsl if isinstance(ghsl, list) else [ghsl]

    # If iterating over multiple values of both density and ghsl, skip iterations where density==False xor ghsl==False
    skip_xors=True if all([ False in l and len(l)>1 for l in [ density,ghsl ] ]) else False

    # First, create the blank svgs (by continent and resolution) in parallel 
    def _makeBlankSVG(cont, res):
        if cont in ['planet','world']:
            whereClauseISO = ''
        else: 
            isos = np.unique(list(osm_lookups.gadm2continentLookup(0,clusterradius[0],continent=cont).keys()))
            isos = '(\''+isos[0]+'\')' if len(isos)==1 else str(tuple(isos)) 
            whereClauseISO = 'WHERE iso in %s' % isos
        svg = svgt.pg2SVG(paths['scratch']+'blankSVG-'+cont+'-'+str(res)+'.svg')
        if res>0: svg.prepareOutline(defaults['gadm']['TABLE']+'_iso',where=whereClauseISO)
        outlines = True if res>0 else False
        svg.createBlankSVG(defaults['gadm']['TABLE']+'_'+defaults['gadm']['ID_LEVELS'][res], defaults['gadm']['ID_LEVELS'][:res+1],where=whereClauseISO,outlines=outlines,simplify=1000,dropSmall=10000**2,forceUpdate=forceUpdate)
       
    funcs,names=[],[]
    for cont in continent:
        for res in resolution:
            if cont in ['world','planet'] and res>3:
                if logger: logger.write('Skipping %s resolution %s because svg files will be too large.' % (continent, resolution))
                continue
            funcs+=[[_makeBlankSVG,[cont,res]]]
            names+=['-'.join([defaults['server']['postgres_db']+':_makeBlankSVG',cont,str(res)])]
    runFunctionsInParallel(funcs,names=names,maxAtOnce=None, parallel=defaults['server']['parallel'])
    
    # Now we can parallelize over everything
    
    funcs,names=[],[]
    for cont in continent:
        for res in resolution:
            if cont in ['world','planet'] and res>3:
                if logger: logger.write('Skipping %s resolution %s because svg files will be too large.' % (continent, resolution))
                continue
            for cr in clusterradius:
                for metric in metrics:
                    for dens in density:
                        for ghslYr in ghsl:
                            for urb in urban:
                                if skip_xors and not urb and (( dens!=False and ghslYr==False ) or (dens==False and ghslYr!=False )): continue
                                if urb and dens!=False: continue # don't break down density for urban edges
                                funcs+=[[fillInSvg,[cont,res,cr,metric,dens,ghslYr,urb]]]
                                names+=['-'.join([defaults['server']['postgres_db']+':fillInSvg',cont,str(res),cr,metric,str(dens),str(ghslYr),str(urb)])]
    if 0: 
        funcs = funcs[:1]
        names= names[:1]
    runFunctionsInParallel(funcs,names=names,maxAtOnce=30, parallel=defaults['server']['parallel'])

def fillInSvg(cont, res, cr, metric, dens, ghslYr, urb, ticks=None, colorRangeDict=None):
    """for use with mapping function, but also called externally for 'nice' plots
    colorRange uses the 5/5/95 percentiles as default, or can take a dict of min/max/median"""

    assert colorRangeDict is None or isinstance(colorRangeDict,dict)
    db = osmt.pgisConnection(verbose=True, schema = defaults['osm']['analysisSchema'],curType='default',logger=None)
    idLists = [ ",'-',".join(defaults['gadm']['ID_LEVELS'][:ares+1])  # create single id for the svg, joined by '-'
                for ares in [0,1,2,3]]
    idList= idLists[res]
    if dens is False and ghslYr is False:
        whereClause=''
    elif dens is False:  
        whereClause='WHERE ghsl_yr='+str(ghslYr)
    elif ghslYr is False:
        whereClause='WHERE density_decile='+str(dens)
    else:
        assert bool(ghslYr) and bool(dens)
        whereClause='WHERE density_decile='+str(dens)+' AND ghsl_yr='+str(ghslYr)
    outFn = paths['scratch']+'worldmaps/'+'-'.join([cont,metric.replace('_','').replace(' ','').replace('/','_'), defaults['gadm']['ID_LEVELS'][res],'radius'+cr]+['density'+str(dens)]*bool(dens)+['ghsl'+str(ghslYr)]*bool(ghslYr)+['urban']*bool(urb)).replace("'","")
    print(('   fillInSvg planning to write to {}'.format(outFn)))
    contList = defaults['osm']['continents'] if cont=='world' else [cont]
    # get data and colorize
    assert 'density' not in whereClause


    
    df = pd.DataFrame()
    for cc in contList:  # loop over all for whole world, otherwise just one continent
        tableNames = ['_'.join(['disconnectivity'+'_delta'*isinstance(ghslYr,str), defaults['gadm']['ID_LEVELS'][ares],cc,cr]+['density']*bool(dens)+['ghsl']*( isinstance(ghslYr,int) and not isinstance(ghslYr,bool) )+['urban']*bool(urb))               for ares in [0,1,2,3]]
        tableName=tableNames[res]
        cmdDicts = [ {'idList':idLists[ares], 'eachid': ','.join(defaults['gadm']['ID_LEVELS'][:ares+1]), 'metric':metric,'tableName':tableNames[ares],'whereClause':whereClause}                  for ares in [0,1,2,3]]
        cmdDict = cmdDicts[res]
        # The database does not contain some empty regions. So we need a complete list in order to infill data from coarser levels:
        all_regions = pd.DataFrame(db.execfetch(  '''SELECT distinct concat(%(idList)s) AS id  FROM %(tableName)s ;''' % cmdDict), columns=['id']).set_index('id')
        
        if metric in ['log10length_m','length_m_per_pop']: # Allow for special metrics here, which we need to construct outside the db:
            if isinstance(ghslYr,str): return # Differences make less sense (and aren't available) for population measures.
            cmd = '''SELECT concat({idList}) AS id, n_nodes, length_m,ls_pop FROM {tableName} {whereClause};'''.format(**cmdDict)
            df = pd.concat([df, pd.DataFrame(db.execfetch(cmd), columns=['id','n_nodes','length_m', 'ls_pop'])])
            # Caution: the following are coded twice. See also metric_settings.add_standard_derived_metrics
            df['log10length_m'] = np.log10(df['length_m'])
            df['length_m_per_pop'] = (df['length_m']/df['ls_pop']).replace(np.inf,np.nan)
            df=df[['id','n_nodes',metric]]
        else:
            cmd = '''SELECT concat({idList})   AS id, n_nodes, {metric}   FROM {tableName} {whereClause};'''.format(**cmdDict)
            df = pd.concat([df, pd.DataFrame(db.execfetch(cmd), columns=['id','n_nodes',metric])])
            df = df.set_index('id').reindex(all_regions.index).reset_index()
            df['orig_metric'] = df[metric]
            for coarser in range(0,res):
                c_cmd  = '''SELECT concat(%(idList)s) AS id, n_nodes, %(metric)s FROM %(tableName)s %(whereClause)s;''' % cmdDicts[coarser]
                c_df = pd.DataFrame(db.execfetch(c_cmd), columns=['id','n_nodes',metric]).set_index('id')
                vc='{}_{}'.format(metric,coarser)
                df[vc] = c_df.loc[  df['id'].map(lambda s: '-'.join(s.split('-')[:(coarser+1)])),   metric].values
                df[metric] = df[metric].mask(df[metric].isnull(),  df[vc])
            """
            assert res==0
            if res==2:
                cmd0 = '''SELECT concat(%(idList)s) AS id, n_nodes, %(metric)s FROM %(tableName)s %(whereClause)s;''' % cmdDicts[0]
                cmd1 = '''SELECT concat(%(idList)s) AS id, n_nodes, %(metric)s FROM %(tableName)s %(whereClause)s;''' % cmdDicts[1]
                df0 = pd.DataFrame(db.execfetch(cmd0), columns=['id','n_nodes',metric]).set_index('id')
                df1 = pd.DataFrame(db.execfetch(cmd1), columns=['id','n_nodes',metric]).set_index('id')

                df['id1-val'] = df1.loc[  df['id'].map(lambda s: '-'.join(s.split('-')[:2])),   metric].values
                df['id0-val'] = df0.loc[  df['id'].map(lambda s: s.split('-')[0]),              metric].values
                
                df[metric] = df[metric].mask(df[metric].isnull(),  df['id1-val'])
                df[metric] = df[metric].mask(df[metric].isnull(),  df['id0-val'])
              """
    if 0:  # Below is misguided now; we in-fill
        df.sort_values('n_nodes',ascending=False,inplace=True)
        df = df[df.n_nodes>=15]
    df.drop_duplicates(subset='id', inplace=True)  # in case we have more than one continent
    #df.dropna(inplace=True)
    if df[metric].dropna().count()<2 or df[metric].std()==0:
        print(('Not enough data found. Skipping %s.' %outFn))
        return
    lw = 0.5 if res==0 else 0  # don't have separate outlines for ISO level

    # To standardize the colour map, lets allow for the saving of a standard color distribution.  It should be the id_1 2014 distribution.
    # Actually, it looks below like I use nothing except the 5,50,95 %iles for colormapevenly!?
    standard_distribution_file = paths['scratch'] + 'standard_distribution_id1_2014_'+metric+'.pandas'
    if os.path.exists( standard_distribution_file):
        data_dist = pd.read_pickle(standard_distribution_file)[[metric]].dropna()
        print(('    Using fixed colormapping from 2014 id_1 version of {}'.format(metric)))
    elif  str(ghslYr)== '2014' and res==1:
        df[[metric,'id']].dropna(subset=[metric]).to_pickle( standard_distribution_file)
        data_dist = df.copy()[[metric]].dropna()
    else:
        data_dist = df.copy()[[metric]].dropna()
        
    if metric=='degree':
        data2color = assignSplitColormapEvenly([2,2.5,3,3.5,4], splitdataat = 3, RGBpoints=list(reversed(osmt.threeSprawlyColors)))
    else:
        colorRange = np.percentile(data_dist[metric],[5,50,95])
        if len(np.unique(colorRange))<3:  # assignSplitColormapEvenly will not work if 50th percentile is same as min or max
            colorRange[1] = np.mean([colorRange[0],colorRange[2]])
        if colorRangeDict is not None:
            if 'min' in colorRangeDict: colorRange[0] = colorRangeDict['min']
            if 'median' in colorRangeDict: colorRange[1] = colorRangeDict['median']
            if 'max' in colorRangeDict: colorRange[2] = colorRangeDict['max']
        try:
            data2color = assignSplitColormapEvenly([colorRange], splitdataat = colorRange[1], RGBpoints=osmt.threeSprawlyColors)
        except:
            print(('Cannot assign color range. Probably range of values is insufficient. Skipping %s' % outFn))
            This_should_not_fail_anymore
            return
    svg = svgt.pg2SVG(paths['scratch']+'blankSVG-'+cont+'-'+str(res)+'.svg')
    #assert res!=1 or str(ghslYr) != '1990' or urb # For testing the in-filling in the craziest case: ECU and VEN have nothing for 1990 !
    #assert res!=2 or str(ghslYr)!='1975' or urb # For testing the in-filling in the craziest case: ECU and VEN have nothing for 1990 !
    colorbarlimits= [colorRange[0], colorRange[1] ]
    if metric=='pca1':
        colorbarlimits= [0, 8]
        if ticks is None:
            ticks=[0,2,4,6,8]
    for i in range(0,2): # catch I/O errors by trying again: only if runnin gin paralle, which doesn't work well anyway.
        if 1:#try:  
            svg.colorizeSVG(df.set_index('id')[metric],outFn+'.svg',data2color=data2color,cbylabel=metricNames[metric.lower()],lw=lw,outlineLw=0.5,
                            fontsize=32, ticks=ticks,
                            makePNG=True,
                            cbarpar = dict(expandx=.9, movebartox=725,movebartoy=12,scalebar=.4, aspect_ratio=6) , #fontsize=fontsize,

                            colorbarlimits= colorbarlimits )
            print((' Wrote {}.svg.  The colorbar limits should have been {}'.format(outFn, colorbarlimits)))
            return
        else: #except Exception as e:
            print(('   Failed to create SVG for reason {}'.format(e)))
            continue
        print(('fillInSvg with %s failed with exception %e' % (outFn, e)))
        
        
    
################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
################################################################################################
    import inquirer
    runmode = inquirer.prompt([inquirer.List('A', message='Run which mode?', choices=[
        'export_PCA_coefficients',
        'dev',
            ])])['A']
    if runmode in ['dev']:
        import sys
        pcAnalyzer=principal_component_analysis()
        pcAnalyzer.format_LaTeX_table_of_PCA_coefficients()
        stophere
    if runmode in ['export_PCA_coefficients']:
        # Simply load the pyshelf file in Working paths (which contains more than just the coefficients) and save the coefficients to a tsv in output:
        pcac = principal_component_analysis()
        pcar=pcac.load_master_pca_results()
        ofn = ofn.replace(paths['working'], paths['output']).replace('.pyshelf','.tsv')
        pcar.to_csv(ofn, sep='\t')
        print((' Wrote the following to {}:\n{}'.format(ofn,pcar)))


#!/usr/bin/python
# coding=utf-8

import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from cpblUtilities.mathgraph import weightedPearsonCoefficient
from cpblUtilities import shelfSave, shelfLoad


def _normalize(s):
    return (s-s.mean())/s.std()


def _demean(s):
    return (s-s.mean())

def test_pca_result():
    def assert_checks(pr):
        print((' Checking {}'.format(pr)))
        assert 'PCA1' in pr.df.columns
        assert 'PCA1' in pr.correlations
        assert 'PCA1' in pr.eigenvalues.index
        assert 'PCA1' in  pr.explained.index
        assert 'negdegree' in  pr.data_means.index
        assert 'negdegree' in  pr.data_stds.index
        assert pr.method_comments

    pr = pca_result()
    assert_checks(pr)
    assert len( pr.get_diagnostics().columns )==3
    pr.diagnostic_plot('tmp_pca_diagnostic_plot.pdf')
    pr.save('tmp_pca_result.pyshelf')
    pr.load('tmp_pca_result.pyshelf')
    
    pr = pca_result('tmp_pca_result.pyshelf') #'/home/projects/osm/workingData/osm10/master_id5_PCA_coefficients_planet_10_urban.pyshelf')
    assert_checks(pr)
    

class pca_result(object):#pd.DataFrame):
    """
    Stores PCA coefficients (ie, loadings) for use on other data than those on which they were estimated.
    In order to apply the coefficients, it also requires storing the standard deviations and means for each column of raw data.

    Also provides a method for applying the estimated coefficients to a new dataset.

    By default (fix_signs=True), this flips the sign on all components so that each is aligned with the variable it is most correlated with.
    """

    def __init__(self, df = None, data_means=None, data_stds=None, original_data=None, weight=None, explained=None, eigenvalues=None,  fix_signs=True,  filename=None,   verbose=True, other_output=None,  method_comments=None):

        if df is None: # Use hardcoded defults from 2019/2020 published papers
            from PCAhardcodedValues2020 import PCA_result_2019_2020
            self.df = PCA_result_2019_2020['pca_coefs']
            self.eigenvalues = PCA_result_2019_2020['eigenvalues']
            self.data_means =PCA_result_2019_2020['data_means']
            self.data_stds = PCA_result_2019_2020['data_stds']
            self.correlations = PCA_result_2019_2020['correlations']
            self.method_comments = PCA_result_2019_2020['method_comments']
            self.explained = PCA_result_2019_2020['explained']
            if verbose: print('PCA result initialized using hardcoded values')
        elif isinstance(df, str): 
            pyshelf_file = df
            self.load(pyshelf_file) # This sets self.df to have pca coefficients, as well as the following
            assert data_means is None
            assert data_stds is None
            if verbose: print(('PCA result initialized using file '+df))
        else:#    return
            # Columns are the data variables.  Rows (index) are PCA1, PCA2, ...
            self.df = df.copy() #pd.DataFrame.__init__(self, df)
            self.data_means = None
            self.data_stds = None
            self.correlations = None
            if verbose: print('PCA result initialized from passed values')
            
        self.verbose = verbose
        if not self.df.columns[0] == "PCA1": # Is this obselete in by 2019?
            if self.verbose:
                print('  Overwriting columns to PCA1, PCA2, ...')
            assert self.df.columns[0] in [0, 1, '0', '1']
            self.df.columns = ['PCA{}'.format(i+1)
                            for i in range(len(self.df.columns))]
        if self.verbose:
            print((' pca_coefficients object initialized with data variables {}..{} and rows {}..{}'.format(
                self.df.index[0], self.df.index[-1], self.df.columns[0], self.df.columns[-1],)))
        if self.data_means is not None:
            assert data_means is None
        elif original_data is None:
            self.data_means = pd.Series(data_means, index=self.df.index)
            self.data_stds = pd.Series(data_stds, index=self.df.index)
            self.correlations = None
        else:
            self.data_means = original_data.mean()
            self.data_stds = original_data.std()
            self.correlations = self.calculate_correlations(
                original_data, weight=weight)
            if fix_signs:
                self.fix_signs()

        if self.explained is not None:
            assert explained is None
        elif isinstance(explained, np.ndarray) or isinstance(explained, list):
            self.explained = pd.DataFrame(
                dict(fraction_explained=explained), index=self.df.columns)
        else:
            self.explained = explained
            
        if self.eigenvalues is not None:
            assert eigenvalues is None
        elif isinstance(eigenvalues, np.ndarray) or isinstance(eigenvalues, list):
            self.eigenvalues = pd.DataFrame(
                dict(fraction_eigenvalues=eigenvalues), index=self.df.columns)
        else:
            self.eigenvalues = eigenvalues

        if other_output is not None:
            assert         self.other_output is None
            self.other_output = other_output
        if method_comments is not None:
            assert         self.method_comments is None
            self.method_comments = method_comments

        if filename is not None:
            self.save(filename)

    def apply_coefficients_to_data(self, X, n_components=None):
        """ X should be a dataframe with row-observations and X columns which match the known PCA coefficients. However, these are not necessarily the data which generated the PCA estimate of those coefficients.
        It uses known variances and means and coefficients from a PCA estimation, and applies them to a new dataset, X.
        It returns the first n_components PCA vectors (ie new variables), called PCA1, PCA2, etc.
        """
        assert set(X.columns) == set(self.df.index)
        assert not pd.isnull(X).any().any()
        cmat = self.df if n_components is None else self.df.loc[:, :n_components]
        # Use the ORIGINAL DATA normalization parameters to normalize these data:
        Xnorm = (X-self.data_means)/self.data_stds
        scores = Xnorm.dot(self.df)  # Nice: name-checking matrix multiplication
        assert not pd.isnull(scores).any().any()
        return scores

    def calculate_correlations(self, original_data, weight=None):
        # Calc correlations of original variables with the principal components (scores):
        scores = self.apply_coefficients_to_data(original_data)
        pccorr = self.df*np.nan  # Just use same columns, index as the coefficients matrix
        for pcv in pccorr.columns:
            for ov in pccorr.index:
                pccorr.loc[ov, pcv] = weightedPearsonCoefficient(
                    original_data[ov].values, scores[pcv].values, weight)
        return pccorr

    def flip_sign(self, component):
        self.df[component] *= -1
        self.correlations[component] *= -1

    def fix_signs(self):
        """ Flip any PCA vector which is pointing away from its strongest correlate """
        for pc in self.df.columns:
            if np.sign(self.df.loc[self.correlations[pc].abs().idxmax(), pc]) == -1:
                self.flip_sign(pc)
        return

    def save(self, fn):  # This sould be updated to use proper Python self pickling methods!
        shelfSave(fn, dict(df=self.df, data_means=self.data_means, data_stds=self.data_stds, correlations=self.correlations,
                           explained=self.explained, eigenvalues=self.eigenvalues, method_comments=self.method_comments))
        if self.verbose:
            print(('   Saved '+fn))

    def load(self, fn):  # This sould be updated to use proper Python methods!
        dd = shelfLoad(fn)
        self.df = pd.DataFrame(dd['df']) #pd.DataFrame.__init__(self, dd['df'])
        self.data_means = dd['data_means']
        self.data_stds = dd['data_stds']
        self.correlations = dd['correlations']
        self.eigenvalues = dd['eigenvalues']
        self.explained = dd['explained']
        self.method_comments = dd['method_comments']
        return(self)

    def get_diagnostics(self):
        """ Return eigenvalues and fraction explained. """
        dfd= pd.DataFrame(self.eigenvalues).join(pd.DataFrame(self.explained))
        dfd['PCAcomponent'] = dfd.index.str.replace('PCA','').map(int)
        return dfd

    def diagnostic_plot(self, filename=None):
        # Diagnostic plot
        # mpl's figsize and 'tight' export both fail / appear to be nearly meaningless.
        fig, ax1 = plt.subplots(figsize=(4, 2.5))
        dfd = self.get_diagnostics()
        xPCAticks = np.arange(len(dfd.index))+1
        ax1.plot(xPCAticks, dfd.explained.values, 'b.-',
                 label='Fraction explained variance')
        ax1.set_xlabel('PCA component')
        ax1.set_xticks(xPCAticks)
        plt.axis('tight')
        # Make the y-axis label, ticks and tick labels match the line color.
        ax1.set_ylabel('Explained variance', color='b')
        ax1.tick_params('y', colors='b')
        for cn,adf in dfd.iterrows():
            if adf['eigenvalue']>1:
                ax1.text(adf['PCAcomponent'], adf['explained'], cn, va='center')

        ax2 = ax1.twinx()
        ax2.plot(xPCAticks, self.eigenvalues, 'b', label='Eigenvalue')
        ax2.set_ylabel('Eigenvalue', color='b')
        ax2.tick_params('y', colors='b')
        ax2.plot(xPCAticks[[0, -1]], [1, 1], 'm:')
        fig.tight_layout()
        ax1.set_xlim(xPCAticks[[0, -1]]+[-.2, .2])
        if filename is not None:
            print(('  Writing '+filename))
            plt.savefig(filename, bbox_inches='tight')
    # To make a LaTeX table of coefficients, see e.g. format_LaTeX_table_of_PCA_coefficients(self) in aggregate_connectivity_metrics.py


def estimatePCA(df, weight=None, tmpname=None, scratch_path=None, method=None, package='stata', verbose=True, dropna=False, **argv):
    """ Pandas interface to multiple  PCA functions, including Stata's 
    df is a dataframe with desired data vectors as columns, possibly with one extra column given by weight.

    Returns dict including: coefficients, eigenvalues, cumulative fraction variance explained, the PCA vectors, correlation matrix
    """
    
    assert package in ['stata', 'scipy', 'jakevdp']
    if package in ['jakevdp'] and method is None:
        method = 'WPCA'
    if package in ['stata'] and method is None:
        method = 'cor'
    assert (method in ['cor', 'cov'] and package in ['stata']) or (package in [
        'jakevdp'] and method in ['WPCA', 'EMPCA']) or (package in ['scipy'] and weight is None)

    dfnn = df.dropna()
    if not len(dfnn) == len(df):
        msg = ' WARNING: estimatePCA dropped {} NaN observations (out of {}).'.format(
            -len(dfnn)+len(df), len(df))
        if dropna:
            df = dfnn
        else:
            raise Exception(
                ' WARNING: estimatePCA dropped {} NaN observations (out of {}).'.format(-len(dfnn)+len(df), len(df)))
        df = dfnn
    df = df.copy()
    if 0 and weight is None:
        assert 'wuns' not in df.columns
        df.loc[:, 'wuns'] = 1
        weight = 'wuns'
    pcvars = [cc for cc in df.columns if cc not in [weight]]
    if package == 'stata':
        df.to_stata(scratch_path+tmpname+'.dta')
        statado = """
        capture ssc inst pcacoefsave

        use {fn},clear
        pca {pcvars} {ww} , {method}
        * Not using explicit PCAvlist in following because it's possible (e.g. archetypes) for there to be fewer components than input variables
        predict PCA*, score
        outsheet PCA* using {SP}{fn}_score.tsv, replace  noquote        

        use {fn},clear
        pca {pcvars} {ww} , {method}
        pcacoefsave using {SP}{fn}_pca_coefs, replace
        mat eigenvalues = e(Ev)
        gen eigenvalues = eigenvalues[1,_n]
        egen varexpl=total(eigenvalues) if !mi(eigenvalues)
        replace varexpl=sum((eigenvalues/varexpl)) if !mi(eigenvalues)
        gen component=_n if !mi(eigenvalues)
        keep varexpl eigenvalues component 
        keep if ~missing(component)
        list
        outsheet using {SP}{fn}_varexpl.tsv, replace  noquote
        use {SP}{fn}_pca_coefs, clear
        outsheet using {SP}{fn}_pca_coefs.tsv, replace noquote
        """.format(PCAvlist=' '.join(['PCA{}'.format(ii+1) for ii in range(len(pcvars))]), method=method, fn=tmpname, SP=scratch_path, pcvars=' '.join(pcvars), ww='' if weight is None else '[w='+weight+']')
        with open(scratch_path+tmpname+'.do', 'wt') as fout:
            fout.write(statado)
        os.system(
            ' cd {SP} && stata -b {SP}{fn}.do'.format(SP=scratch_path, fn=tmpname))

        coefsfn=scratch_path+tmpname+'_pca_coefs.tsv'
        varexplfn=scratch_path+tmpname+'_varexpl.tsv'
        scorefn = scratch_path+tmpname+'_score.tsv'
        if not os.path.exists(coefsfn):
            print(("""

            Cannot find {cfn}.  Maybe copy the PCA coefficients you want to use here, or make use of 
            the "PCA_from_continent = planet" option in the config file.
            In this case, you still need to ensure the appropriate (two) pca tsv files are copied to the local db folder.
            {cfn}
            {vfn}
            {sfn}
            and an associated pyshelf file. Seems to be workingData/ rather than scratch/

            """.format(cfn=coefsfn, vfn=varexplfn, sfn = scorefn)))
            input(" Press enter to continue when you've fixed this problem, or to acknowledge failure.")


        df_coefs = pd.read_table(coefsfn)
        df_varexpl = pd.read_table(varexplfn)
        df_varexpl['cumvarexpl'] = df_varexpl['varexpl'].values
        df_varexpl['varexpl'] = df_varexpl['cumvarexpl'].diff()
        df_varexpl.loc[0, 'varexpl'] = df_varexpl['cumvarexpl'][0]
        df_score = pd.read_table(scorefn)

        df_cmat = df_coefs.pivot(
            index='PC', columns='varname', values='loading').dropna()
        df_cmat.index = df_cmat.index.map(lambda nn: 'PCA'+str(nn))
        df_varexpl.index = df_cmat.index
        assert not pd.isnull(df_cmat).any().any()

        # Stata messes up variable order. Reinforce it here:
        pcaresult = pca_result(df_cmat[pcvars].T, original_data=df[pcvars],
                               explained=df_varexpl['varexpl'],
                               other_output={'vectors_stata': df_score},
                               eigenvalues=df_varexpl['eigenvalues'], weight=None if weight is None else df[weight], **argv)
        # The proportion of the variance that each eigenvector represents can be calculated by dividing the eigenvalue corresponding to that eigenvector by the sum of all eigenvalues.

    elif package == 'scipy':
        assert not 'not written yet'
        #from sklearn.decomposition import PCA as spPCA
        from statsmodels.multivariate.pca import PCA as smPCA
        # No sample weights available; the weights argument weights variables!
        ss = smPCA(df[pcvars], standardize=True)
        return ss
        foo
    elif package == 'jakevdp':
        from wpca import PCA, WPCA, EMPCA
        #pca = PCA(n_components=10).fit(dfnorm)
        fPCA = {'PCA': PCA, 'WPCA': WPCA, 'EMPCA': EMPCA}[method]
        # Compute the PCA vectors & variance
        if weight is None:
            ww = None
            dfdata = df[pcvars]
        # Construct weight matrix of identical shape to df ( :( ). Also, avoid singular matrix by dropping zero rows.
        else:
            iuse = df[weight] > 0
            if verbose and sum(-iuse):
                print((' Warning: estimatePCA is dropping {} rows due to zero weight'.format(
                    sum(-iuse))))
            dfdata = df.loc[iuse, pcvars]
            mw = df.loc[iuse, weight]/(df.loc[iuse, weight].mean())
            ww = dfdata.copy()
            for cc in ww:
                ww[cc] = mw

        pca = fPCA().fit(dfdata.apply(_normalize, axis=0),    weights=ww)
        df_cmat = pd.DataFrame(pca.components_, columns=pcvars, index=[
                               'PCA{}'.format(i+1) for i in range(len(pca.components_))])

        pcaresult = pca_result(df_cmat.T, original_data=df[pcvars],  # Send entire data, even where weight is zero
                               other_output={'vectors_jakevdp': pca.transform(dfdata),
                                             'vectors_jakevdp_w': pca.transform(dfdata, weights=ww)},
                               explained=pca.explained_variance_ratio_,
                               eigenvalues=pca.explained_variance_, weight=None if weight is None else df[weight], **argv)


    # Diagnostic plot
    plt.figure(456789876)
    fig, ax1 = plt.subplots()
    xPCAticks = np.arange(len(pcaresult.explained.index))+1
    xPCAticklabels = [ll.replace('_','-') for ll in pcaresult.columns]#index.values]  # latex friendly - not _

    ax1.plot(xPCAticks, pcaresult.explained, 'b',
             label='Fraction explained variance')
    ax1.set_xlabel('PCA component')
    ax1.set_xticks(xPCAticks)
    ax1.set_xticklabels(xPCAticklabels)
    plt.axis('tight')
    # Make the y-axis label, ticks and tick labels match the line color.
    ax1.set_ylabel('Explained variance', color='b')
    ax1.tick_params('y', colors='b')
    ax1.grid()
    ax2 = ax1.twinx()
    ax2.plot(xPCAticks, pcaresult.eigenvalues, 'b', label='Eigenvalue')
    ax2.set_ylabel('Eigenvalue', color='b')
    ax2.tick_params('y', colors='b')
    fig.tight_layout()
    plotfn = scratch_path+tmpname+'diagnostic-plot.pdf'
    plt.savefig(plotfn)
    pcaresult.fig = fig
    pcaresult.vectors = pcaresult.apply_coefficients_to_data(df[pcvars])
    return pcaresult

    # pcs is a df with the new vectors, along with the original index of df (so use df.join if you wish to merge)
    results = {'coefs': df_cmat, 'loadingStata': df_coefs, 'explained': df_varexpl,
               'corr': pccorr, 'plot': plotfn, 'vectors': pcs,  'vectorsStata': df_score, 'fig': fig}
    # 'vectorsN':pcsN,
    return results



if __name__ == '__main__':

    test_pca_result()

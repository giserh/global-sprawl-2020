#!/usr/bin/python

"""
Provide a few instances of tileable unit graphs (three "paradigms" from urban form of local road network) and tile them into large continuous networks.
"""

import pylab as plt
import networkx as nx
import numpy as np
import sys,os
sys.path.append(os.path.dirname("../"))
from osm_networkx import Graph as osmnxGraph

__author__="CPBL"

class stylized_unit_graphs(osmnxGraph):
    """
    Generate a few small graphs which can be replicated and connected (tiled), or simply serve as standard paradigms of urban form. 
    We make use of networkx.Graph-based class.
    """
    def __init__(self, name=None): # N.B. Don't have a default here, because ns.Graph.__init__ doesn't require a parameter [subtle bug otherwise!]
        if name is None:
            super(stylized_unit_graphs, self).__init__()
        else:
            edges, latlon, gluerules={'grid':self.create_grid, 'hell':self.create_hell, 'medieval':self.create_medieval}[name]()
            super(stylized_unit_graphs, self).__init__(edges,latlon=latlon)
            #G=osmnxGraph(edges,latlon=latlon)
            #self.__dict__.update(G.__dict__)
            self.gluerules=gluerules
            self.name=name

    def tile(self,n,spacing_factor=0):
        import tiled_graphs as TG # This has to be here, to avoid circular imports.
        sys.path.append(os.path.dirname("speed_scaling/"))
        sys.path.append(os.path.dirname("../"))

        gt=TG.graph_tiler(self,self.get_glue_rules(),n,spacing_factor=spacing_factor)
        return gt.as_osm_networkx()

    def move_to_db(self,size={'grid':85,'hell':25,'medieval':50},schemaname='osmdata',tablename=None):
        import postgres_tools as pgt

        if isinstance(size,dict): size = size[self.name]
        if tablename is None: tablename = 'osm_%s_segs' % self.name
        tiled_unit_graph = self.tile(size)
        cmd = tiled_unit_graph.create_psql_table(schemaname,tablename)
        dbc = pgt.dbConnection(command=cmd)

    def get_boundary_points(self, nw, se):
        n,w=nw[0],nw[1]
        s,e=se[0],se[1]
        walk_west=.5*(n-s)
        walk_north=.5*(w-e)
        xm=np.mean((n,s))
        ym=np.mean((w,e))
        xw,xe=xm+walk_west, xm-walk_west
        yn,ys=ym+walk_north, ym-walk_north
        return xw,xm,xe,yn,ym,ys

    def build_3x3_outline(self,xw,xm,xe,yn,ym,ys):
        """ This adds nodes to G for the 3x3 outline. It also returns the lat/lon coordinate and the gluing rules for those nodes.
        """
        
        latlon=dict([(nn, (yn if 'n' in nn else ys if 's' in nn else ym,   xw if 'w' in nn else xe if 'e' in nn else xm))  for nn in ['nw','nn','ne','ee','se','ss','sw','ww']])

        grules={'N':dict(nw='sw', nn='ss',ne='se'),
                    'E':dict(ne='nw', ee='ww', se='sw'), }
        grules=self.add_SW_rules_from_NE(grules)

        return(latlon,grules)

    def add_SW_rules_from_NE(self, grules):
        # The other rules are defined by symmetry:
        grules['S']=dict([[b,a] for a,b in   list(grules['N'].items())])
        grules['W']=dict([[b,a] for a,b in   list(grules['E'].items())])
        return(grules)

    def moving_to_france(self,latlon): # This function anchors all NW nodes for the unit graphs at
        # coordinates 48.110649, .613102
        new_lat = 48.110649
        new_lon = .613102
        lat_offset = new_lat - latlon['nw'][0]
        lon_offset = new_lon - latlon['nw'][1]
        for nn in list(latlon.keys()):
                latlon[nn] = (latlon[nn][0]+lat_offset,latlon[nn][1]+lon_offset)
        return latlon

    def create_grid(self): # See https://www.google.ca/maps/@49.2383299,-123.0842616,17z
        # Let's make grid in France-ish, with 100m square.
        lat,lon=47,0
        d=100.0/6375e3
        #Latitude is 111km per degree:
        lat1=lat- 100.0/111e3
        # The number of kilometers per degree of longitude is approximately
        lon1=lon+ 100.0/   (np.pi/180 * 6378e3 * np.cos(47*np.pi/180) )
        #xw,xm,xe,yn,ym,ys=self.get_boundary_points((-123.085156, 49.239834), \
        # (-123.084155,49.236475))        
        xw,xm,xe,yn,ym,ys=self.get_boundary_points((lon,lat), (lon1,lat1))
        latlon=dict([(nn, (yn if 'n' in nn else ys if 's' in nn else ym,   xw if 'w' in nn else xe if 'e' in nn else xm))  for nn in ['nw','ne','se','sw']])
        grules={'N':dict(nw='sw',ne='se'),
                    'E':dict(ne='nw',  se='sw'), }
        grules=self.add_SW_rules_from_NE(grules)
        
        #latlon,gluerules=self.build_3x3_outline(xw=xw,xm=xm,xe=xe,yn=yn,ym=ym,ys=ys)

        #G=nx.Graph()
        #latlon,gluerules=self.build_3x3_outline()
        edges= [('nw','ne'),('ne','se'), ('se','sw'),('sw','nw') ]        
        #edges+=[ ('A',nn)  for nn in ('nn','ww','ee','ss')]
        ys,xs=list(zip(*(list(latlon.values()))))
        #latlon['A']=(np.mean(ys),np.mean(xs))
        latlon = self.moving_to_france(latlon)
        return(edges, latlon, grules)
                
    def create_hell(self):
        """ Modeled after the block centred at:
https://www.google.ca/maps/@37.3563112,-122.0393387,17z
        """
        #G=nx.Graph()
        xw,xm,xe,yn,ym,ys=self.get_boundary_points((-122.041485, 37.359420), \
         (-122.032429, 37.352223))
        latlon,gluerules=self.build_3x3_outline(xw=xw,xm=xm,xe=xe,yn=yn,ym=ym,ys=ys)

        #Boundary edges
        edges=[ ('nn','ne'), ('se','ss'), ('ss', 'B'), ('B', 'sw'), ('sw', 'D'), \
         ('D', 'E'), ('E', 'F'), ('F', 'ww'), ('ww', 'G'), ('G', 'nw') ]

        edges+=[ ('nn', 'N'), ('N', 'L'), ('L', 'M'), ('L', 'J'), ('J', 'K'), \
        ('J', 'T'), ('J', 'H'), ('H', 'I'), ('H', 'G'), ('H', 'V'), \
        ('V', 'W'), ('V', 'U'), ('U', 'S'), ('S', 'Q'), ('Q','QR'),('QR','QR1'), \
        ('R','R1'),('Q', 'R'), ('Q', 'O'), ('O', 'P'), ('O', 'N'), ('U', 'X'), \
        ('X', 'Y'), ('X', 'Z'), ('Z', 'AA'), ('BB', 'FF'), \
        ('FF', 'CC'), ('FF', 'KK'), ('KK', 'LL'), ('LL', 'O'), \
        ('LL', 'MM'), ('KK', 'OO'), ('OO', 'PP'), ('OO', 'TT'), ('TT', 'RR'), \
        ('TT', 'UU'), ('UU', 'HH'), ('HH', 'II'), ('II', 'JJ'), ('HH', 'DD'), \
        ('DD', 'GG'), ('DD', 'BB'), ('ee', 'XX'), ('B', 'III'), ('D', 'HHH'), \
        ('E', 'GGG'), ('F', 'FFF'), ('FFF', 'EEE'), \
        ('FFF', 'CCC'), ('CCC', 'DDD'), ('CCC', 'YY'), ('YY', 'S'), \
        ('YY', 'ZZ'), ('ZZ', 'BBB'), ('ZZ', 'AAA') ]

        # See https://www.google.ca/maps/@37.355924,-122.036887,16z
        latlon.update({
         #'A': (yn, -122.033625), \
                'B': (ys, -122.040406), \
                #'C': (-122.041479, 37.352564),
                'D': (37.353058, xw),'E': (37.353604, xw), \
                'F': (37.354304, xw),'G': (37.357357, xw), \
                'H': (37.357544, -122.040363), 'I': (37.358670, -122.040578), \
                'J': (37.357851, -122.039483), 'K': (37.358653, -122.039655), \
                'L': (37.358158, -122.038647), 'M': (37.358414, -122.03871), \
                'N': (37.358261, -122.036844), 'O': (37.357544, -122.037123), \
                'P': (37.357442, -122.038303), 'Q': (37.356845, -122.037402), \
            'R1': (37.354, -122.037), 'QR': (37.3555, -122.038), 'QR1': (37.3540, -122.038), \
                'R': (37.356180, -122.037381), 'S': (37.356470, -122.038797), \
                'T': (37.357169, -122.039119), 'U': (37.356180, -122.039376), \
                'V': (37.356845, -122.039913), 'W': (37.356657, -122.040240), \
                'X': (37.355907, -122.039805), 'Y': (37.355310, -122.039644), \
                'Z': (37.355770, -122.040556), 'AA': (37.355327, -122.040556), \
                'BB': (37.358278, -122.033583), 'CC': (37.358269, -122.035970), \
                'DD': (37.357613, -122.033604), 'FF': (37.358244, -122.035562), \
                'GG': (37.357595, -122.034575), 'HH': (37.356913, -122.033631), \
                'II': (37.356896, -122.034468), 'JJ': (37.356333, -122.034532), \
                'KK': (37.357527, -122.035455), 'LL': (37.357561, -122.036356), \
                'MM': (37.357135, -122.036334), 'OO': (37.356436, -122.035476), \
                'PP': (37.356402, -122.036442), 'RR': (37.355702, -122.036442), \
                'TT': (37.355753, -122.035455), 'UU': (37.355822, -122.033652), \
                #'VV': (37.355276, xe), \
                'XX': (37.355259, -122.035541), \
                'YY': (37.354372, -122.038502), 'ZZ': (37.353502, -122.038523), \
                'AAA': (37.352871, -122.038523), 'BBB': (37.353553, -122.039827), \
                'CCC': (37.354346, -122.039596), 'DDD': (37.354687, -122.039553), \
                'EEE': (37.354704, -122.040562), 'FFF': (37.354295, -122.040562), \
                'GGG': (37.353562, -122.040283), 'HHH': (37.353041, -122.040787), \
                'III': (37.352808, -122.040371)})

        latlon = self.moving_to_france(latlon)

        return(edges, latlon, gluerules)        

    def create_medieval(self, use_original_location=False):
        """ Modeled on:
        https://www.google.ca/maps/@41.000748,28.9238743,17z
        Use use_original_
        """

        xw,xm,xe,yn,ym,ys=self.get_boundary_points((28.924209, 41.002019), \
         (28.927835, 40.999934))
        latlon,gluerules=self.build_3x3_outline(xw=xw,xm=xm,xe=xe,yn=yn,ym=ym,ys=ys)

        edges = [ ('nw', 'S'), ('S', 'ww'), ('E', 'ne'), ('V', 'W'), 
         ('W', 'R'), ('R', 'S'), ('R', 'Q'), ('V', 'sw'), ('Q', 'B'), 
         ('B', 'C'), ('C', 'P'), ('P', 'Q'), ('P', 'O'), ('O', 'W'), 
         ('O', 'M'), ('M', 'N'), ('N', 'X'), ('X', 'F'), ('F', 'C'), 
         ('ss','K'), ('E', 'ee'), ('E', 'X'), ('N', 'G'), 
         ('G', 'H'), ('H', 'K'), ('K', 'L'),
                  #('L', 'M'), # This is the original. I'm tweaking to:
                  ('M', 'A'), ('A', 'L'),
                  ('L', 'V'), 
         ('H', 'A'), ('nn', 'B'), ('G','ee'), ('se', 'ss') ]

        # See https://www.google.ca/maps/@41.0007272,28.9261294,18.04z  SRQP is Alper Street.      
        latlon.update({
            #'A':(41.000434, 28.926395), # This is reality; I'm bending it:
            'A':(41.00034, 28.92632), 
            'B':(41.001683, 28.925971), 
         'C':(41.001533, 28.926477), #'D':(yn, 28.926815), 
         'E':(41.001185, 28.927458), 'F':(41.001424, 28.926789), 
         'G':(41.000432, 28.927507), 'H':(41.000371, 28.927220), 
         #'I':(41.001604, 28.926320), 'J':(41.000156, 28.926734), 
         'K':(41.000171, 28.926744), 'L':(41.000124, 28.926187), 
            # This following line is reality. I'm going to tweak it to what follows:
            #'M':(41.000598, 28.926487),
            'M':(41.00055, 28.9265),
            'N':(41.000780, 28.926504),   
         'O':(41.000626, 28.926031), 'P':(41.001023, 28.926123), 
         'Q': (41.001088, 28.925656), 'R':(41.001120, 28.925313), 
         'S': (41.001156, 28.925034), 
         #'T': (41.001201, xw), 'U': (41.000541, xw), 
         'V': (41.000417, 28.925157), 'W': (41.000758, 28.925339), 
         'X': (41.001330, 28.926764)})

        if not use_original_location:
            latlon = self.moving_to_france(latlon)

        return(edges, latlon, gluerules)        

    def create_awkwardland(): #Create archetype with the same geometry as Monaco,
        #but all coordinates translated to France

        #Create new latlon dictionary that translates all clusters in Monaco:
        #dbc.execute("""SELECT cluster_id,ST_AsGeoJSON(geom) from osm_monaco_juncs""")
        #node_info=dbc.fetchall()
        #latlon=( json.loads(ii[1])['coordinates']+OFFSET for ii in node_info )
        #latlon=dict(zip([ ii[0] for ii in node_info ],latlon))

        pass

    def get_glue_rules(self):
        """
        External getter for gluerules
        """
        return(self.gluerules)
    
    def plot(self):
        self.plot_geographic()
        plt.show()

    def available_graphs(self):
        return(['grid','hell','medieval'])#['colonial_urbanism' ,'culdesac_hell' ,'old_world','grid'][-1:])

if __name__ == "__main__":
    # Do testing here
    sys.path.append(os.path.dirname("tests/"))
    import unitgraphs_test
 
    unspecified = len(sys.argv) == 1

    if unspecified or "hell" in sys.argv:
        a=stylized_unit_graphs('hell')
        a.plot()
    if unspecified or "grid" in sys.argv:
        a=stylized_unit_graphs('grid')
        a.plot()
    if unspecified or "medieval" in sys.argv:
        a=stylized_unit_graphs('medieval')
        a.plot()

    plt.show()

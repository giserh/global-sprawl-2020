#!/usr/bin/python

"""
Create lookup tables between GADM geometries (e.g. countries)
and nodes and edges
This speeds up aggregation as the spatial query only 
needs to be executed once
"""

import os, sys, platform, time
import pandas as pd
from osm_config import paths,defaults
import postgres_tools as psqlt
import cpblUtilities as utils
import osmTools as osmt
from cpblUtilities.parallel import runFunctionsInParallel

def listit2(x):
    """ Subtle conversion to make tuples but not strings lists, nestedly. 
    Python ignores parentheses around a single object. Convert tuples to lists so as not to confuse lone string with a alist, in compareGADM_string first argument. 

    N.B.! This function is duplicated in gadm.py.
    """
    assert isinstance(x,(list,tuple,int,str))
    if isinstance(x,(list,tuple)):
        if len(x)==1 or isinstance(x[1],int): return list(x)
        return([[k] if not isinstance(k,(list,tuple)) else list(k) for k in x])
    return [x]

def pGADMformat(pGADM):
    """ Returns a string and a tuple, no matter what is given.
        Possible formats for pGADM: (1) ISO string: 'DEU'; (2) tuple: ('DEU', '33', '1'), (3) pGADM string: 'DEU_33_1'
        For some methods like compareGADM_string it is vital that ids are integers, not strings.
    """
    if isinstance(pGADM, tuple) or isinstance(pGADM, list):
        t = [pGADM[0].strip("'")]+[int(i) for i in pGADM[1:]]
        return '_'.join([str(ii) for ii in t]), t
    assert isinstance(pGADM, str)
    if '_' in pGADM:
        return pGADMformat(pGADM.split('_'))
    return pGADM.strip("'"), (pGADM.strip("'"),)  # It's just the ISO
    

def compareGADM_string(list_of_ISOandIDs, tablename=None):
    """ Return the postgres string expression comparing database columns to values for a particular region, or to any of a list of regions. 

    Usage is tricky. Non-ISO ids must be integers, not strings, to differentiate them from a list of ISOs. See also pGADMformat.

    tablename is a prefix for the columns names (typically, "lookup").
 """
    import numpy as np
    columnNames = [cn for cn in defaults['gadm']['ID_LEVELS']]
    if tablename:
        columnNames = [tablename+'.'+cc for cc in columnNames]
    list_of_ISOandIDs = listit2(list_of_ISOandIDs)
    if isinstance(list_of_ISOandIDs[0], str):  # a single tuple was passed
        O1 = columnNames[0]+" = '%s'" % list_of_ISOandIDs[0]
        if len(list_of_ISOandIDs) > 1:
            O2 = ' and '.join(['%s = %s' % (columnNames[1:][ii], RR) for ii, RR in enumerate(
                list_of_ISOandIDs[1:]) if RR not in [0]])
            if O2:
                O1 += ' and '+O2
        return(' WHERE '+O1)
    else:  # a list of tuples was passed. In this case, we should be smarter, by recursively sorting the conditions to combine similar comparisons.
        # return(' or '.join(['(%s)'%compareGADM_string(LL) for LL in  list_of_ISOandIDs])  )  # old way
        # for why this is better, see http://stackoverflow.com/questions/6672665/doing-a-where-in-on-multiple-columns-in-postgresql
        idLevels = np.unique([len(cc) for cc in list_of_ISOandIDs])
        maxIdLevel = max(idLevels)
        if maxIdLevel == 1:  # iso only
            paddedList = [cc[0] for cc in list_of_ISOandIDs]  # unnest
            GADMstr = ' WHERE iso IN %s' % str(tuple(paddedList))
        else:
            gadmColNames = '('+', '.join(defaults['gadm']['ID_LEVELS'][:maxIdLevel])+')'
            paddedList = [str(tuple(cc)+(0,)*(maxIdLevel-len(cc)))
                          for cc in list_of_ISOandIDs]         # pad to max gadm level
            colAliases = '('+', '.join(['v'+str(ii)
                                        for ii in range(maxIdLevel)])+')'
            GADMstr = ', (VALUES %s) v%s \n WHERE %s = %s' % (
                ', '.join(paddedList), colAliases, gadmColNames, colAliases)
        # need to add an OR where we want to have a higher-level GADM
        # loop over all other idLevels (already done maxIdLevel)
        for idLevel in idLevels[:-1]:
            gadmColNames = '('+', '.join(defaults['gadm']['ID_LEVELS'][:idLevel])+')'
            colAliases = '('+', '.join(['v'+str(ii)
                                        for ii in range(idLevel)])+')'
            colAliases2 = '('+', '.join(['v'+str(ii)
                                         for ii in range(idLevel, maxIdLevel)])+')'
            zeroList = '(' + \
                ', '.join(['0' for ii in range(idLevel, maxIdLevel)])+')'
            GADMstr += ' OR (%s = %s AND %s = %s)' % (gadmColNames,
                                                      colAliases, colAliases2, zeroList)
        assert '()' not in GADMstr  # issue 156
        return(GADMstr)

def compareGADM_string2(list_of_ISOandIDs, tablename=None):
    """ Return the postgres string expression comparing database columns to values for a particular region, or to any of a list of regions. 

    tablename is a prefix for the columns names (typically, "lookup").

    N.B.! This function is intentionally duplicated in gadm.py. But the version there is more efficient. Copy over ??
    """
    columnNames= defaults['gadm']['ID_LEVELS'] #['ISO','id_1','id_2','id_3','id_4','id_5']
    if tablename: 
        columnNames=[tablename+'.'+cc for cc in columnNames]
    list_of_ISOandIDs=listit2(list_of_ISOandIDs)
    if isinstance(list_of_ISOandIDs[0], str)  :  # a single tuple was passed
        O1=columnNames[0]+" = '%s'"%(list_of_ISOandIDs[0].strip("'"))
        if len(list_of_ISOandIDs)>1:
            O2=' and '.join(['%s = %s'%(columnNames[1:][ii],RR) for ii,RR in enumerate(list_of_ISOandIDs[1:]) if RR not in [0] ])
            if O2:
                O1+=' and '+O2
        return(O1)
    else:  # a list of tuples was passed. In this case, we should be smarter, by recursively sorting the conditions to combine similar comparisons.
        return(' or '.join(['(%s)'%compareGADM_string2(LL) for LL in  list_of_ISOandIDs])  )


def create_all_lookups(forceUpdate=False):
    osmt.getUrbanEdgeThresholds() # creates density/ghsl urban thresholds table
    funcs,names=[],[]
    for mode in ['edges','nodes']:
        for continent in defaults['osm']['continents']:
            for cluster_radius in defaults['osm']['clusterRadii']:
                funcs+=[[GADM_roadgraph_lookups,[mode,cluster_radius,continent,forceUpdate,None,True]]]
                names+=[  '-'.join(['GADM_roadgraph_lookups',mode,cluster_radius,continent])  ]
    runFunctionsInParallel(funcs,names=names, expectNonzeroExit=True, parallel=defaults['server']['parallel'])
    addMissingLookups()
    
class GADM_roadgraph_lookups():
    """
    This generates lookups for the location of nodes and edges in GADM regions.
    It does not create global lookups, by default, but rather does it by continent.
    Available modes are now simplified, 
    since we should not have reason to look up anything other than the roadgraph (reduced) 
    features of clusterRadius-specific nodes and edges.
    """
    def __init__(self, mode=None, cluster_radius=None, continent=None, forceUpdate=False, logger=None, create=False):
        self.db=None
        self.mode =mode
        self.continent=continent
        self.cluster_radius=cluster_radius
        assert cluster_radius is not None
        self.logger=logger
        self.forceUpdate=forceUpdate
        if create and    mode is not None and cluster_radius is not None and continent is not None:
            self.logger=osmt.osm_logger('GADM_roadgraph_lookups_'+'-'.join([mode,continent,cluster_radius]), verbose=True, timing=True, prefix=None)
            self.create_one_lookup()
        else:
            assert mode in ['edges','nodes']

    def ensure_db_connection(self):
        if self.db is None:
            self.db = osmt.pgisConnection(verbose=True, schema = defaults['osm']['processingSchema'], logger=self.logger)

    def report(self,ss):
        """ A wrapper for "print". In verbose mode, it prints comments and SQL commands.  If a logger has been provided, it calls logger.write() instead of print().    """
        if self.logger:
            self.logger.write(ss)
        print(ss)

    def get_lookup_tablename(self):
        return(
        'lookup_'+self.continent+'_'+{'nodes':'cluster','edges':'edge'}[self.mode]+self.cluster_radius+'_'+ defaults['gadm']['TABLE']
            )
    def create_one_lookup(self):
        self.ensure_db_connection()
        strlu={
            'mode': self.mode,
            'key': {'nodes':'cluster_id','edges':'edge_id'}[self.mode],
            'keyname': {'nodes':'cluster','edges':'edge'}[self.mode]+self.cluster_radius,
            'geoCol': {'nodes':'geog','edges':'geom'}[self.mode],
            'gidList': ', '.join(['gadm.'+ii for ii in  defaults['gadm']['ID_LEVELS']]),
            'gadmtable':defaults['gadm']['TABLE'],
            'schema' : defaults['osm']['processingSchema'],
            'fromtable' : dict(
                nodes = defaults['osm']['cluster_tables'].replace('REGION',self.continent).replace('CLUSTERRADIUS',self.cluster_radius),
                edges = defaults['osm']['edge_tables'].replace('REGION',self.continent).replace('CLUSTERRADIUS',self.cluster_radius)
                )[self.mode],
            'restrictByDegree': (' AND nodes.degree != 2 ') * (self.mode=='node'),
            'newtable' : self.get_lookup_tablename(),
            'tmptable' : self.get_lookup_tablename()+'_tmpmerge',
            'srid': defaults['osm']['srid_compromise'],
            }

        if self.mode=='nodes': # create 'fake' index on cast geometry
            self.db.execute('CREATE INDEX IF NOT EXISTS %(fromtable)s_geogcast_idx  ON %(fromtable)s  USING gist  (CAST(geog AS geometry));'%strlu)

        if not self.forceUpdate and strlu['newtable'] in self.db.list_tables(schema=defaults['osm']['processingSchema']):
            self.report('Table '+strlu['newtable']+' already exists. Skipping lookup...')
            return
        if self.forceUpdate:
            self.db.execute('DROP TABLE IF EXISTS  %(schema)s.%(newtable)s '%strlu)
        self.report( 'Creating lookup for continent '+self.continent+' v. r='+self.cluster_radius+'m')
        cmd = '''
             CREATE TABLE  %(schema)s.%(newtable)s   AS
                     SELECT %(mode)s.%(key)s  as %(key)s, %(gidList)s
                     FROM %(fromtable)s AS %(mode)s, %(gadmtable)s as gadm
                     WHERE ST_Intersects(%(mode)s.%(geoCol)s::geometry, gadm.the_geom)   %(restrictByDegree)s  ;'''%strlu
        self.db.execute(cmd)
        
        # Now catch edges/nodes that do not intersect any GADM polygon - take the nearest within 1km
        # See http://www.bostongis.com/PrinterFriendly.aspx?content_name=postgis_nearest_neighbor
        # This could be part of the query above, but is easier to understand as a separate one
        cmd = '''INSERT INTO %(schema)s.%(newtable)s
                     SELECT DISTINCT ON (%(mode)s.%(key)s)
                          %(mode)s.%(key)s AS %(key)s, %(gidList)s                     
                     FROM %(fromtable)s AS %(mode)s LEFT JOIN %(schema)s.%(newtable)s AS lookup USING(%(key)s), 
                          %(gadmtable)s as gadm
                     WHERE lookup.%(key)s is NULL AND ST_DWithin(ST_Transform(%(mode)s.%(geoCol)s::geometry,%(srid)s), gadm.geom_compromise, 1000)
                     ORDER BY %(mode)s.%(key)s, ST_Distance(ST_Transform(%(mode)s.%(geoCol)s::geometry,%(srid)s), gadm.geom_compromise);''' % strlu
        self.db.execute(cmd)  
        
        # Now add urban flags to the edges and nodes (not the lookup) tables
        # We do this in lookups because we need the country information
        # GROUP BY is because there are duplicates that cross gadm boundaries
        if defaults['osm']['landscan'] and defaults['osm']['ghsl']:
            pctileCols = [cc for cc in self.db.list_columns_in_table('urban_thresholds') if 'pctile' in cc]
            pctiles = [cc.strip('pctile') for cc in pctileCols]
            ghsl_pctiles = self.db.execfetch("SELECT %s FROM urban_thresholds WHERE iso='ghsl';" % (','.join(pctileCols)))[0]
            cmd = '''DROP TABLE IF EXISTS %(schema)s.%(tmptable)s;
                     CREATE TABLE %(schema)s.%(tmptable)s AS
                        SELECT l.%(key)s''' % strlu
            for pct,pctCol,ghslPct in zip(pctiles,pctileCols,ghsl_pctiles):
                cmd+=(',\nCOALESCE(AVG(density)>=MIN('+pctCol+') OR AVG(ghsl_frc_developed)>='+str(ghslPct)+',False) AS urban_'+pct)
            cmd+='''\nFROM %(schema)s.%(newtable)s l, %(fromtable)s f, urban_thresholds t
               WHERE l.%(key)s=f.%(key)s AND l.iso=t.iso GROUP BY l.%(key)s;''' % strlu
            self.db.execute(cmd)
            
            # Drop existing urban_ cols in edges and nodes tables
            colsToDrop = [cc for cc in self.db.list_columns_in_table(strlu['fromtable']) if cc.startswith('urban_')]
            if colsToDrop: self.db.execute('ALTER TABLE '+strlu['fromtable']+','.join([' DROP COLUMN '+cc for cc in colsToDrop])+';')
            
            self.db.merge_table_into_table(from_table=strlu['tmptable'], 
                              targettablename=strlu['fromtable'], commoncolumns=strlu['key']) 
            self.db.execute('DROP TABLE %(schema)s.%(tmptable)s;' % strlu)
              
        self.db.create_indices(strlu['newtable'], gadm=True, forceUpdate=True) # No unique key, key = strlu['key'])

    def get_nodes_by_GADM(self,gadm_ids, extraCols=None):
        """ 
        Return a list of nodes (clusterIDs) corresponding to nodes in gadm_ids=(ISO,id1,... ).

        If extraCols is not None, then we need to look in the nodes table as well as the lookup, in order to find those values (e.g./i.e. "nocars").

        Returns a list if extraCols=None
        Returns a dict if len(extraCols) ==1
        Returns a DataFrame if len(extraCols) >1
        """
        whereStr= compareGADM_string2(gadm_ids, tablename='lookup')
        self.ensure_db_connection()
        if extraCols is None:
            nodes=self.db.execfetch("""
            SELECT cluster_id FROM {table} as lookup
               WHERE {wheres} ;""".format(
                 table = self.get_lookup_tablename(),
                 wheres = whereStr))
            return([nn[0] for nn in nodes]) # List of cluster ids
        
        else:
            
            nodes_and_nocars=self.db.execfetch("""
            SELECT lookup.cluster_id,  {cols} 
                FROM {ltable} as lookup, {ntable} as nodes
            WHERE {wheres}
                   AND nodes.cluster_id = lookup.cluster_id   ;
            """.format(
                 ltable = self.get_lookup_tablename(),
                cols = ','.join(['nodes.'+ec for ec in extraCols]),
                ntable = defaults['osm']['cluster_tables'].replace('REGION',self.continent).replace('CLUSTERRADIUS',self.cluster_radius),
                 wheres = whereStr))
            if len(extraCols)==1:
                return( dict( nodes_and_nocars) ) # Different format from extraCols=None case
            else:
                return ( pd.DataFrame([list(x) for x in nodes_and_nocars], columns = ['cluster_id']+extraCols).set_index('cluster_id'))

    def count_nodes_by_GADM_ISO(self):
        """ 
        Return a count of nodes (clusterIDs) corresponding to nodes in gadm_ids=(ISO,id1,... )
        """
        cmd="""
        SELECT  COUNT(cluster_id),ISO FROM """+self.get_lookup_tablename()+"""  group by iso ; """
        self.ensure_db_connection()
        nodes=self.db.execfetch(cmd)
        return pd.DataFrame([list(a) for a in nodes],columns=['nodes','iso']).sort_values('nodes',ascending=False) #.iso.values:


    def make_GADM_subset_edge_lookup(self, pGADM):
        """Since we use the edge lookup many times for each ISO, it would be worthwhile for non-tiny ISOs to create a subset of the lookup having source ("core") edges only in that ISO.
        We can also make indices ignoring the ISO column.

        """
        gstr, gadm_ids = pGADMformat(pGADM)
        self.delete_GADM_subset_edge_lookup(gadm_ids)
        fmts= dict(edgetable = self.get_lookup_tablename(),
                   schema = defaults['osm']['processingSchema'],
                   whereStr= compareGADM_string(gadm_ids),
                   gstr = gstr,
                   )
        cmd = """SELECT * INTO {schema}.{edgetable}{gstr} from {edgetable} {whereStr}""".format(**fmts)
        self.ensure_db_connection()
        self.db.execute(cmd)        
        #self.db.create_indices(edgetable, gadm=True, forceUpdate=True) # No unique key, key = strlu['key'])
        self.db.execute('CREATE INDEX  {edgetable}{gstr}idx_123 ON {schema}.{edgetable}{gstr} (id_1,id_2,id_3);'.format(**fmts))
        self.db.execute('CREATE INDEX  {edgetable}{gstr}idx_4 ON {schema}.{edgetable}{gstr} (id_4);'.format(**fmts))
        self.db.execute('CREATE INDEX  {edgetable}{gstr}idx_5 ON {schema}.{edgetable}{gstr} (id_5);'.format(**fmts))
        
     
    def delete_GADM_subset_edge_lookup(self,pGADM):
        gstr, gadm_ids = pGADMformat(pGADM)
        edgetable = self.get_lookup_tablename()        
        cmd = """DROP TABLE IF EXISTS {}{} CASCADE """.format(edgetable,gstr)
        self.ensure_db_connection()
        self.db.execute(cmd)        

def addMissingLookups():
    """When a node is outside the GADM polygon, we want to make sure that it's also captured in the edge lookup table
        to maintain consistency between edge and node lookups. See issue #19
        Note: can't be parallelized with the lookup creation above (because it relies on edge and node tables already created)"""
    
    logger=osmt.osm_logger('GADM_roadgraph_lookups_addMissing', verbose=True, timing=True, prefix=None)
    db = osmt.pgisConnection(verbose=True, schema = defaults['osm']['processingSchema'], logger=logger)
    
    for continent in defaults['osm']['continents']:
        for cluster_radius in defaults['osm']['clusterRadii']:
            cmdDict={
                'schema' : defaults['osm']['processingSchema'],
                'edgeLookup': 'lookup_'+continent+'_edge'+cluster_radius+'_'+defaults['gadm']['TABLE'],
                'nodeLookup': 'lookup_'+continent+'_cluster'+cluster_radius+'_'+defaults['gadm']['TABLE'],
                'edgeTable': defaults['osm']['edge_tables'].replace('REGION',continent).replace('CLUSTERRADIUS',cluster_radius),
                'idList': ','.join(defaults['gadm']['ID_LEVELS']),
                'nidList': ','.join(['n.'+ii for ii in defaults['gadm']['ID_LEVELS']]),
                }
        
            cmd = '''INSERT INTO %(schema)s.%(edgeLookup)s
                     SELECT DISTINCT edge_id, %(nidList)s 
                        FROM      %(nodeLookup)s n 
                        LEFT JOIN %(edgeTable)s e ON (cluster_id=ept_cluster_id OR cluster_id=spt_cluster_id)
                        LEFT JOIN %(edgeLookup)s l USING (edge_id, %(idList)s)
                        WHERE  l.edge_id is Null;''' % cmdDict
            db.execute(cmd)
            logger.write('Added %s missing edge lookups' % str(db.cursor.rowcount))
        
def gadm2continentLookup(gadmLevel,cluster_radius,continent=None,logger=None):
    """Returns a dictionary of gadm id to continent
    For development only
    Some countries are within more than one continent, in which case it returns the one with the largest N
    The gadm lookups need to have been created first"""
    assert gadmLevel in range(6)
    continents = defaults['osm']['continents'] if continent is None else continent if isinstance(continent,list) else [continent]
    db = osmt.pgisConnection(verbose=True, schema = defaults['osm']['processingSchema'],curType='default',logger=logger)
    gadmLevels = defaults['gadm']['ID_LEVELS'][:gadmLevel+1]
    df = pd.DataFrame()
    for continent in continents:
        l = GADM_roadgraph_lookups(mode='edges',continent=continent,cluster_radius=cluster_radius)
        tableName = l.get_lookup_tablename()
        if tableName not in db.list_tables(schema=defaults['osm']['processingSchema']):
            print(('Lookups for continent %s radius %s do not exist. Create them first' % (continent,cluster_radius)))
            return
        results = pd.DataFrame(db.execfetch('SELECT %s, COUNT(*) FROM %s GROUP BY %s' % (', '.join(gadmLevels), tableName, ', '.join(gadmLevels))),columns = gadmLevels+['N'])
        results['continent'] = continent
        df = pd.concat([df, results])
        
    # sort and then take first to return the continent with the largest N given a duplicate
    df.sort_values('N',ascending=False,inplace=True)
    df.drop_duplicates(subset=gadmLevels, inplace=True)
    
    # convert Decimal column to int
    for col in gadmLevels[1:]:
        df[col] = df[col].astype(int)
    
    return df.set_index(gadmLevels)['continent'].to_dict()
            
    
    
            
################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
################################################################################################
    create_all_lookups()



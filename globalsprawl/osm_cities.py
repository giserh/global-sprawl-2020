#!/usr/bin/python
"""
Usage:
  atlas_data.py purge  [options]
  atlas_data.py load  [options]
  atlas_data.py deltas  [options]
  atlas_data.py makeworld [options]
  atlas_data.py aggregate  [options]
  atlas_data.py disconnectivity  [options]
  atlas_data.py export  [options]
  atlas_data.py copy_to_local  [options]
  atlas_data.py plot  [options]
  atlas_data.py plotr  [options]
  atlas_data.py plotc  [options]
  atlas_data.py plotscatter  [options]
  atlas_data.py build_template    [options]
  atlas_data.py AUEmetrics  [options]
  atlas_data.py run_all_from_scratch    [options]
  atlas_data.py copy_template    [options]
  atlas_data.py -h | --help
  atlas_data.py cleanup  [options]

Options:
  -h --help        Show this screen.
  --db=<database>        Specify database to use (e.g. osm, osmdev, etc)
  -f --force-update     Overwrite/recalculate database tables and outputs
  --continent=<continent>   Process only a single continent
  -s --serial         Turn off parallel computation; useful for debugging 
  --cluster-radius=<radius>   Specify a single cluster radius to override config file setting
  -l --local     Use local Pandas files rather than the database or a tunnel to the database.


Here's the prep you'll need to do on the database:

-- DROP SCHEMA cities CASCADE;

CREATE SCHEMA cities
  AUTHORIZATION postgres;

GRANT ALL ON SCHEMA cities TO postgres;
GRANT USAGE ON SCHEMA cities TO public;
GRANT ALL ON SCHEMA cities TO osmusers;

ALTER DEFAULT PRIVILEGES IN SCHEMA cities
     GRANT INSERT, SELECT, UPDATE, DELETE ON TABLES
     TO osmusers;



The overall plan proceeds as follows:

 (1) Load in the city boundary data, merge the multigeometries into single geometries for each city/year, and calculate difference-geometries in order to identify the new regions for each year.
The result is two tables, cityedges and citychanges, that cover all cities.  All other tables made to this point (per city/year) are deleted.

 (2) These two tables can be saved in the template database, and can also be stored as SQL files. If they exist, the prefabricated versions can be loaded straight into a new database with node/edge data. 
More simply, they may already be present because the database started out as a copy of the template database.

 (3) Parallelization is used to aggregate the edge and node sums ("aggregated" data) to the city-year level, exploding the whole-world table citychanges out into one per city/year  (e.g. cities.aggregated_10_Singrauli_3_planet for the final period ("3") in Singrauli.)

 (4) Those files are rejoined to a global one, and the disconnectivity measures are calculated from it.  Two PCA vectors are calculated after the other disconnectivity measures are done.

This plot city data as defined by the Atlas of Urban Expansion

N.B. osm_raster_template now includes the cities data: cityedges and citychanges. So run_all_from_scratch could be modified to skip those steps, or make a new run_from_template

Usage examples:
 run osm_cities.py run_all_from_scratch --db=osm  --continent=planet  --cluster-radius=10 -f

"""


import docopt
from osm_config import paths,defaults,  update_paths_to_be_specific_to_database
import numpy as np
import textwrap
import osmTools as osmt
import postgres_tools as psqlt
import glob
from collections import OrderedDict

import sys,os,time
import pandas as pd
import matplotlib.pyplot as plt
import cpblUtilities
from cpblUtilities import str2pathname
from cpblUtilities.mathgraph import multipage_plot_iterator,cpblScatter, dfOverplotLinFit
from cpblUtilities.matplotlib_utils import plot_diagonal

from aggregate_connectivity_metrics import aggregate_node_and_edge_results,disconnectivity_analysis, principal_component_analysis,PCA1_BOOST

import metric_settings as smetrics
from metric_settings import add_standard_derived_metrics


def extract_some_files_from_zip_to_folder(zipfile,filelist,destination, verbose = False):
    assert zipfile.lower().endswith('.zip')
    cmd = """  unzip -j "{}"  """.format(zipfile) + ' '.join(['"{}"'.format(aff) for aff in filelist])+ """ -d "{}" """.format(destination)
    if verbose:
          print((" Looking for {} files in {} to put in {}".format(len(filelist),zipfile,destination)))
          print(cmd)
    os.system(cmd)

class city_atlas():
    """
    This goes from downloaded zip files for city boundaries (over time)  to postGIS "disconnectivity" tables for each city and time period. 

    That involves taking the GIS difference between boundaries over time, to find the "new" developments (ie disjoint regions which sum to the latest full extent), and joining with node and edge tables from the main osm analysis.
 Note that these regions cannot be re-summed to regain the originals, since the creation of many islands and corner-adjacent squares by differencing causes NOT ST_ISVALID geometries in the differenced regions.
 
   """
    def __init__(self, forceUpdate=False):
        self.forceUpdate=forceUpdate
        self.inpath = paths['input']+'AtlasUrbanExpansion/'
        self.logger = osmt.osm_logger('city_atlas_logger_'
                                 +'-'.join(defaults['osm']['continents'])
                                 +'_'
                                      +'-'.join(defaults['osm']['clusterRadii']),
                                 verbose=True,timing=True,overwrite=False
                                )
        
        self.db = None
        self.atlascities=self.get_list_of_cities().set_index('name') # Set the index to the name we'll use for everything (though convert to lower case for sql)
        assert len(self.atlascities.index.unique()) == len(self.atlascities) # Our short names are unique
        assert len(defaults['osm']['clusterRadii']) == 1  
        self.cradius = defaults['osm']['clusterRadii'][0]  # Allow for only one radius for the moment. (c.f. Issue #275)
        self.atlasyears_xticks = np.array([1990,1999,2013])
        self.atlasyears_labels_short = [r'$<$1990','90-99','2000-13']
        self.atlasyears_labels_shortest = [r'$<$1990','90-99',"'00-13"]
        self.atlasyears_labels = [r'$<$1990','1990-99','2000-13']
        self.atlasyears_deltalabels = ['1990-99','2000-13']
        self.atlasyears_ranges = dict( byIndex = OrderedDict([(1,[1900,1989]), (2,[1990,1999]), (3,[2000,2013])]))
        
        self.plotvars = smetrics.sensible_metric_order(include_density=False, include_variance=False, includeNegatives=False, long_names=False)
        self.varnames = dict([[pp,smetrics.metricNames.get(pp,pp)] for pp in self.plotvars])
        self.template_dumpfile =  paths['scratch']+'cities_edges_changes.dmp'
        self.export_path = paths['working']+'cities_disconnectivity_'+self.cradius+'.pandas'

    def get_dbconn(self):
        if self.db is None:
            self.db = osmt.pgisConnection(verbose=True, logger=self.logger)
        return(self.db)
    def get_list_of_cities(self, include_only = None):
        """ Use the source zip files to generate a list of city path names, and also cleaned-up city names for use in the database.
        include_only can be a list of city compact names, in which case only matches to those will be included.
        """
        #include_only = ['Guangzhou', 'Rawang', 'NewYork', 'Budapest', 'Singapore','Chicago']

        cities = pd.DataFrame(dict(zipfile =     glob.glob(self.inpath+'*.zip'))) # Find all zip files    
        cities['foldername'] = cities.zipfile.str[:-4].str.replace(self.inpath,'').str.replace('/','')
        cities['longname'] = cities.zipfile.str[:-4].str.replace(self.inpath,'').str.replace('/','').str.replace(',','').str.replace('_','')
        cities['name'] = cities.zipfile.str[:-4].str.replace(self.inpath,'').map(lambda ss: ss.split(',')[0]).str.replace('_','')

        # Add in ISO and GADM continent (a shame; we should split up GADM "asia" continent into two or three)
        isolu = pd.read_table(paths['bin']+'atlas-iso-lookup-tabsep.txt')
        isolu['name'] = isolu.City.str.replace(' ','')
        cid2cont = {17: 'southamerica', 16:'europe', 15:'centralamerica', 14: 'asia', 13:'africa',12:'northamerica', 11:'australiaoceania', 1:'planet'} # But then Cochabamba is wrong! BOL is in southamerica :(
        isolu['continent'] = isolu.ContinentID.map(lambda nn: cid2cont[nn])

        def _findname(nn,cities):
            matches = cities[cities.name.str.lower().str.startswith(nn.lower())]
            assert len(matches) ==1
            return matches.name.values[0]
        isolu['name'] = isolu.name.map(lambda nn,cities=cities:_findname(nn,cities))

        cities =      isolu.set_index('name').join(cities.set_index('name')).reset_index()
        
        if len(defaults['osm']['continents'])==1 and 'planet' in defaults['osm']['continents'][0]:
            cities['ContinentID']=1
        cities['continent'] = cities.ContinentID.map(lambda cid: cid2cont[cid])

        # Reorder them to put problems first:
        toplist = ['NewYork', 'Budapest', 'Singapore','Chicago','Guangzhou', 'Rawang', ]
        cities.set_index('name',inplace=True)
        cities = cities.reindex(toplist+list(set(cities.index)-set(toplist))).reset_index()
        
        # Subset of cities
        if include_only is not None:
            cities = cities[cities.name.isin(include_only)]
        return( cities)

    # Following three methods are high-level interfaces to city atlas functionality called by osm_master, or by modes here.
    def aggregatePointMetrics(self, runmode = 'run_all_from_scratch'):
         self.aggregate_by_city_year() # Generates whole-world all-years cities_aggregated_10
    def disconnectivity(self):
        self.create_disconnectivity_table()
    def cleanup_tables(self):
        # And cleanup:
        self.get_dbconn().delete_all_tables(contains='_edge_t2', schema= 'cities',  force_do_not_ask= True)
        self.get_dbconn().delete_all_tables(contains='_edge_t3', schema= 'cities',  force_do_not_ask= True)

        self.get_dbconn().delete_all_tables(contains='_edge_t1', schema= 'cities',  force_do_not_ask= True)
        self.get_dbconn().delete_all_tables(startswith='aggregated', schema= 'cities',  force_do_not_ask= True)
        self.get_dbconn().delete_all_tables(contains='_t2_t1', schema= 'cities',  force_do_not_ask= False)
        self.get_dbconn().delete_all_tables(contains='_t3_t2', schema= 'cities',  force_do_not_ask= False)
        return

    
    def esriprj2standards(self,shapeprj_path, verbose=False):
        """ This  finds the right projection from a shp file's .prj """
        return osmt.get_EPSG_SRS_from_esri_prj_file(shapeprj_path, verbose)

    def load_saved_citychanges_and_cityedges(self, forceUpdate=False):
        """  Look for saved .sql files to skip the first loading step. Return False if they're not available. Return True if they're loaded.
        """
        alltables = self.get_dbconn().list_tables(schema='cities')
        target_db=defaults['server']['postgres_db']
        
        if 'citychanges' in alltables and 'cityedges' in alltables and not forceUpdate:
            print('cities.citychanges and cities.cityedges already present')
            return True
        if not os.path.exists(self.template_dumpfile):
            self.save_citychanges_and_cityedges()
        if os.path.exists(self.template_dumpfile):
            if forceUpdate and alltables:
                print(' Dropping entire cities schema! ')
                os.system('cd ~postgres && sudo su postgres  -c "psql {target_db} -c \'DROP SCHEMA cities CASCADE;\' " '.format(target_db=target_db))
            
            print(('  Restoring **cities** schema from  {} to {}.'.format(self.template_dumpfile,target_db )))
            os.system('cp -a {dfn} /tmp/{dfnu} && chmod o+r /tmp/{dfnu}'.format(dfn=self.template_dumpfile, dfnu=self.template_dumpfile.replace('/','_')))
            os.system('cd ~postgres && sudo su postgres  -c "psql {target_db} < {dumpfile}" '.format(target_db=target_db,  dumpfile= '/tmp/'+self.template_dumpfile.replace('/','_')))
            return True
        return False

    def save_citychanges_and_cityedges(self, from_db='osm_rasters_template'): 
        """ 
        Save the entire cities schema to a pg_dump file. This is only to copy the cityedges and citychanges tables, to save time in setup.
        """
        fcur = osmt.pgisConnection(verbose=True, db=from_db)
        fromdb_tables = fcur.list_tables(schema='cities')
        if 'citychanges' in fromdb_tables and 'cityedges' in fromdb_tables:
            print(('  Dumping **cities** schema from template database {}  to {}.'.format(from_db, self.template_dumpfile )))
            os.system('pg_dump -n cities {} > {} '.format(from_db, self.template_dumpfile ))
            return True
        return False
                      
        
    def load_all_city_borders_into_postGIS(self):
        """ For each city, extract the needed files into a new folder.  Then upload them to postGIS.

        Then each city consists of several rows, if the shape is disjoint. So in a separate step, we convert these to single geometries with ST_UNION
        """
        
        tdir = paths['scratch']+'AtlasUrbanExpansion/'
        os.system("""
        rm -rf {tdir}
        """.format(tdir = tdir))

        self.get_dbconn().delete_all_tables(startswith='tmp', schema= 'cities',  force_do_not_ask= True)
        for cityname in self.atlascities.index:
            citynamelower=cityname.lower()
            os.system('mkdir -p '+tdir+cityname)
            cityf=tdir+self.atlascities.ix[cityname]['longname']+'/'
            for iedge in [1,2,3]:
                extract_some_files_from_zip_to_folder(self.atlascities.ix[cityname].zipfile, 
                                                                      [self.atlascities.ix[cityname].foldername+'/urban_edge_t{}{}'.format(iedge,suffix) for suffix in ['.dbf', '.prj', '.sbn', '.sbx', '.shp', '.shp.xml', '.shx']],
                                                                      cityf, verbose = True)
                 
                EPSG_SRID =  self.esriprj2standards(cityf+'urban_edge_t{}.prj'.format(iedge))
                sql =  'shp2pgsql -s {}:4326 "{}urban_edge_t{}.shp" cities.{}_edge_t{} |psql {}'.format(EPSG_SRID, cityf,iedge, citynamelower,iedge, defaults['server']['postgres_db'])
                print (sql)
                os.system(sql)
                self.get_dbconn().fix_permissions_of_new_table('{}_edge_t{}'.format(citynamelower,iedge), 'cities')
                # Fix one problem with geometry: ( Ring Self-intersection at or near point 2271382.1897918563 5587843.6820252305 at 2271382.1897918563 5587843.6820252305 )
                if cityname == "Belgrade" and iedge == 3: #for invalids in ['Belgrade_edge_t3']:
                    self.get_dbconn().execute( """
                    UPDATE cities.Belgrade_edge_t3
                    SET geom = ST_MULTI(ST_COLLECTIONEXTRACT(ST_MAKEVALID(geom),3))
                    ;
                    """)
                # Now make a second version of this table, with aggregated geometries. Also incorporate the city name and year:
                cmd = """
                ALTER TABLE cities.{cityname}_edge_t{iedge}
                RENAME TO tmp_{cityname}_edge_t{iedge} ;

                CREATE TABLE cities.{cityname}_edge_t{iedge} AS 
                  ( SELECT ST_UNION(city.geom) as geom  , labels.* 
                     FROM cities.tmp_{cityname}_edge_t{iedge} as city ,
                (VALUES('{cityname}' , {iedge})) AS labels (city,iyear)
                GROUP BY city,iyear
                  ); 

                DROP TABLE cities.tmp_{cityname}_edge_t{iedge} CASCADE;
                """.format(cityname=citynamelower,iedge=iedge)
                self.get_dbconn().execute(cmd)
                self.get_dbconn().fix_permissions_of_new_table( '{cityname}_edge_t{iedge}'.format(cityname=citynamelower,iedge=iedge),'cities')
                self.get_dbconn().create_primary_key_constraint('{cityname}_edge_t{iedge}'.format(cityname=citynamelower,iedge=iedge) ,'city,iyear',schema='cities')
                self.get_dbconn().create_indices('{cityname}_edge_t{iedge}'.format(cityname=citynamelower,iedge=iedge), key=None, non_unique_columns=None, 
                              gadm=False, geom='geom', skip_vacuum=False, forceUpdate=True, schema='cities')

    def createLookupTable(self):
        """For convenience, add a postgres table with the city-iso lookup"""
        self.get_dbconn()
        
        lookupDf = self.atlascities[['ISO']]
        lookupDf.columns = [cc.lower() for cc in lookupDf.columns]
        lookupDf.index.name = 'city'
        self.db.df2db(lookupDf, tablename='citylookup', index=True, schema='cities')
        self.db.create_indices(table='citylookup', key='city', schema='cities')
                
    def generate_one_city_delta(self, cityname, forceUpdate = None):
        if forceUpdate is None:        forceUpdate = self.forceUpdate
        db = osmt.pgisConnection(verbose=True) # Do not use self.get_dbconn() if running in parallel
        
        cmd = forceUpdate * """

        DROP TABLE IF EXISTS cities.{cityname}_t2_t1 CASCADE; 
        DROP TABLE IF EXISTS cities.{cityname}_t3_t2 CASCADE; 
        """.format(cityname=cityname)
        db.execute(cmd)

        cmd = """
        CREATE TABLE cities.{cityname}_t2_t1 AS (
        select c2.city,c2.iyear,ST_DIFFERENCE(c2.geom,c1.geom) geom
             from cities.{cityname}_edge_t1 c1, cities.{cityname}_edge_t2 c2
        ) ;

        CREATE TABLE cities.{cityname}_t3_t2 AS  (
        select c3.city,c3.iyear,ST_DIFFERENCE(c3.geom,c2.geom) geom
             from cities.{cityname}_edge_t2 c2, cities.{cityname}_edge_t3 c3
        ) ;

        """.format(cityname=cityname)
        db.execute(cmd)
        db.fix_permissions_of_new_table(cityname+'_t2_t1','cities')
        db.fix_permissions_of_new_table(cityname+'_t3_t2','cities')

        db.create_primary_key_constraint(cityname+'_t2_t1','city,iyear',schema='cities')
        db.create_primary_key_constraint(cityname+'_t3_t2','city,iyear',schema='cities')
        
        db.create_indices(cityname+'_t2_t1', key=None, non_unique_columns=None, 
                              gadm=False, geom='geom', skip_vacuum=False, 
                              forceUpdate=True, schema='cities')

        db.create_indices(cityname+'_t3_t2', key=None, non_unique_columns=None, 
                              gadm=False, geom='geom', skip_vacuum=False, 
                              forceUpdate=True, schema='cities')

        
    def generate_city_deltas(self, forceUpdate = None):
        """ This  takes half a day using 25 processors, split into 4000 jobs.
        Clearly it's best to separate the operations by city, since they don't overlap, so let's create new tables for each city for each delta.

        Either do this in parallel, or include an sql comment saying how far along it is.
        """
        if forceUpdate is None:        forceUpdate = self.forceUpdate
        
        cpblUtilities.parallel.runFunctionsInParallel([[self.generate_one_city_delta, [cityname, forceUpdate]] for cityname in self.atlascities.index],
                                                                     names =self.atlascities.index,
                                                                     maxAtOnce = 10, parallel = defaults['server']['parallel'])
        return

    def generate_world_cities_tables(self):
        """
        Create tables that contain all our cities, in which city name and year are just columns.

        May still need to incorporate the city and year values? I don't think so, but if so do it using VALUES, like in
                  ( SELECT ST_UNION(city.geom) as geom  , labels.* 
                     FROM cities.tmp_{cityname}_edge_t{iedge} as city ,
                (VALUES('{cityname}' , {iedge})) AS labels (city,iyear)
                GROUP BY city,iyear
                  ); 

        """
        db = osmt.pgisConnection(verbose=True) 
        for suffix in ['edge_t3','edge_t1','t2_t1','t3_t2','edge_t2']:
            cmd = ("""  
            DROP TABLE IF EXISTS cities.cities_{suffix};
             CREATE TABLE cities.cities_{suffix} AS (
            """+ ' UNION '.join([' SELECT * FROM cities.{city}_{suffix}'.format(city=tt.lower(),suffix=suffix)  for tt in self.atlascities.index])+"""
            ); """).format(suffix = suffix)
            db.execute(cmd)
            
        cmd = """ DROP TABLE IF EXISTS cities.cityedges ;
        CREATE TABLE cities.cityedges AS (
             SELECT * FROM cities.cities_edge_t1
        UNION
             SELECT * FROM cities.cities_edge_t2
        UNION
             SELECT * FROM cities.cities_edge_t3
        ); """
        db.execute(cmd) 
        cmd = """  DROP TABLE IF EXISTS cities.citychanges ;

CREATE TABLE cities.citychanges AS (
SELECT geom, city,iyear FROM cities.cities_t2_t1 
 UNION 
  SELECT geom, city,iyear FROM cities.cities_t3_t2 
  UNION 
  SELECT geom, city,iyear FROM cities.cities_edge_t1
        ); """
        db.execute(cmd)


        db.fix_permissions_of_new_table('cityedges','cities')
        db.fix_permissions_of_new_table('citychanges','cities')

        db.create_primary_key_constraint('cityedges','city,iyear',schema='cities')
        db.create_primary_key_constraint('citychanges','city,iyear',schema='cities')
        
        db.create_indices('cityedges', key=None, non_unique_columns=None, 
                              gadm=False, geom='geom', skip_vacuum=False, 
                              forceUpdate=True, schema='cities')

        db.create_indices('citychanges', key=None, non_unique_columns=None, 
                              gadm=False, geom='geom', skip_vacuum=False, 
                              forceUpdate=True, schema='cities')

        
        print(' Now you can delete all individual city files')
        
    def aggregate_one_city_year_continent(self, citiestable, cityname, iyear, continent):
        """
        Use the syntax from aggregate_connectivity_metrics.py to aggregate node, edge measures to time-resolved city boundaries
        We exclude all nocars edges and nodes, regardless of mode, in the aggregation (they may have been used for connectivity calculations, but we don't want them as destinations).

        Pass the table name as well as what city name and year (1,2,3) label it should get. The table could be a cumulative or delta region.

        The spatial index for the result still needs to be made.
        """
        db = osmt.pgisConnection(verbose=True) # Do not use self.get_dbconn() if running in parallel
        agg =  aggregate_node_and_edge_results() # This contains node_metrics and edge_metrics
        formats=dict(cradius = self.cradius, citiestable=citiestable, cityname=cityname, iyear=iyear, continent=continent )
        
        cmd = self.forceUpdate*"""
            DROP TABLE IF EXISTS cities.aggregated_{cradius}_{cityname}_{iyear}_{continent}_{citiestable} CASCADE;""".format(**formats)
        db.execute(cmd)
        cmd = ("""
        CREATE TABLE cities.aggregated_{cradius}_{cityname}_{iyear}_{continent}_{citiestable}    AS (

        -- This first "WITH" Clause is available in the next with clauses
        WITH 
        onecity as (  SELECT '{citiestable}'::text as stockOrChange, * FROM cities.{citiestable}  WHERE city='{cityname}' and iyear={iyear}),
        aggedge AS (
                SELECT
                  """+',\n'.join([v.replace('geom','edges.geom')+' as '+k for k,v in
                                                    agg.edge_metrics.items()
                                                  ])+""",
                city.*
                FROM edges_{continent}_{cradius} AS edges, 
                onecity as city
                WHERE
                     ST_INTERSECTS(edges.geom, city.geom) AND nocars=False
                                                  GROUP BY city.geom, city.city, city.iyear, city.stockOrChange
                                                  ),
        aggnode AS (
            SELECT
                  """+',\n'.join(['SUM('+v+') as '+k for k,v in
                                                    agg.node_metrics.items()
                                                  ])+""",
                                                  city.city,city.iyear
                FROM nodes_{continent}_{cradius} AS nodes, 
                onecity as city
                WHERE
                     ST_WITHIN(nodes.geog::geometry, city.geom) AND nocars=False
                                                  GROUP BY city.geom, city.city, city.iyear, city.stockOrChange
                  )
                                                          SELECT * FROM aggedge
                                                  LEFT JOIN aggnode USING (city,iyear)
                                                  );

        """).format(**formats)
        db.execute(cmd)
        
        return

        
    def aggregate_by_city_year(self, only_make_global = False):
        """
        Create a table such as cities_aggregated_10  from the node and edge tables and the citychanges table.
        This is carried out by making many aggregated_10_CITYNAME_IYEAR_* tables, and then union'ing them.

        For development, the flag  only_make_global = True only does the second step, ie union'ing the many single city-year tables of sums.
        """
    
        funs, names = [],[]
        unions = []
        for cityname in self.atlascities.index:
            for iyear, suffix, tablename in [[1,'edge_t1','cityedges'],[2,'edge_t2','cityedges'],[3,'edge_t3','cityedges'],[2,'t2_t1','citychanges'],[3,'t3_t2','citychanges']]:
                continent = self.atlascities.loc[cityname].continent
                funs += [[self.aggregate_one_city_year_continent, [tablename,cityname, iyear, continent]]]
                names+= [defaults['server']['postgres_db']+str(defaults['osm']['continents'])+'_'.join([cityname,suffix,continent])]
                unions += ['aggregated_{cradius}_{city}_{iyear}_{continent}_{tablename}'.format(cradius=self.cradius,city=cityname, suffix=suffix, iyear=iyear,continent=continent, tablename=tablename)]

        if not only_make_global:
            cpblUtilities.parallel.runFunctionsInParallel(funs, names=names, parallel = defaults['server']['parallel']) # maxAtOnce = 25,

        db = osmt.pgisConnection(verbose=True)
        alltables = db.list_tables(schema='cities')
        for uut in unions:
            if uut.lower() not in alltables:
                print((' WARNING!!! DID NOT FIND TABLE '+uut))
        unionsFound = [tt for tt in unions if tt.lower() in alltables]
        TN= 'cities_aggregated_{}'.format(self.cradius)
        db = osmt.pgisConnection(verbose=True)
        if self.forceUpdate and unionsFound:
            db.execute(' DROP TABLE IF EXISTS cities.'+TN+';')
        cmd = """  
             CREATE TABLE cities."""+TN+""" AS (
        """+ ' UNION '.join([' SELECT * FROM cities.'+tt  for tt in unionsFound])+"""
        ); """
        
        db.execute(cmd)
        db.fix_permissions_of_new_table(TN,'cities')
        db.create_primary_key_constraint(TN, 'city,iyear,stockorchange',schema='cities')
        db.create_indices(TN, key=None, non_unique_columns=None, 
                              gadm=False, geom='geom', skip_vacuum=False, 
                              forceUpdate=True, schema='cities')

    def create_disconnectivity_table(self):
        """ Compare to  create_final_disconnectivity_wbregions(self,continent=None, aggTables=None, forceUpdate=False,logger=None) ?
        """
        db=self.get_dbconn()
        _disconnectivity_metrics = disconnectivity_analysis().DISCONNECTIVITY_METRICS
        _discon_table= 'cities_disconnectivity_{cradius}'.format(cradius=self.cradius)
        _agg_table =  'cities_aggregated_{cradius}'.format(cradius=self.cradius)
        
        metrics = _disconnectivity_metrics.copy()  # Below, add Area to the final metrics.
        metrics.update({'areakm2':'ST_AREA(geom::geography)/1000000',
                        'logNodalSparsity':'LN(ST_AREA(geom::geography)/1000000)-LN(NULLIF(deadends+threeways+fourplus,0))',
                        })

        db.execute('DROP TABLE IF EXISTS cities.'+_discon_table+';')
        cmd = ("""
        CREATE  TABLE cities.{DTABLENAME} AS (
          SELECT geom,city,iyear,stockOrChange,
                """+',\n'.join([
                     metrics[metric] + ' as ' + metric for metric in list(metrics.keys()) 
                     ])+"""
                     FROM {ATABLENAME}
                     );
                     """).format(DTABLENAME= _discon_table, ATABLENAME = _agg_table)

        db.execute(cmd)

        pcAnalyzer=principal_component_analysis(weights= 'ls_pop' if defaults['osm']['landscan'] else None)
        cddf= db.db2df('cities_disconnectivity_'+self.cradius, schema='cities')


        
        for addcolumn in ['pca1','pca2',]:#'v1_explained_variance_ratio','v2_explained_variance_ratio']:
            if addcolumn not in db.list_columns_in_table(_discon_table, schema='cities'):
                db.execute("""ALTER TABLE {DTABLENAME} ADD COLUMN {NEWVAR} real; """.format(DTABLENAME=_discon_table, NEWVAR=addcolumn))

        cmdstr="""UPDATE cities.{DTABLENAME} SET
            """+ pcAnalyzer.get_psql_code_to_apply_coefficients_to_data(format='equals')+"""    ; """
        db.execute(cmdstr.format(DTABLENAME= _discon_table))


        db.fix_permissions_of_new_table(_discon_table,'cities')
        db.create_primary_key_constraint(_discon_table, 'city,iyear,stockorchange',schema='cities')
        db.create_indices(_discon_table, key=None, non_unique_columns=None, 
                              gadm=False, geom='geom', skip_vacuum=False, 
                              forceUpdate=True, schema='cities')

        return
        
    def export_to_pandas(self):
        """ Save cities connectivity and aggregated tables (ie two total) from postgres to Pandas """
        self.get_dbconn().db2df('cities_disconnectivity_'+self.cradius, self.export_path, schema = 'cities',  drop_geom = True, )
        self.get_dbconn().db2df('cities_aggregated_'+self.cradius, self.export_path.replace('disconnectivity','aggregated'), schema = 'cities',  drop_geom = True, )


    def load_saved_connectivity(self, separate=False, duplicate_for_world_regions=False):
        return  self.load_connectivity(separate=separate, duplicate_for_world_regions=duplicate_for_world_regions, from_db=False)
    def load_connectivity(self, separate=False, duplicate_for_world_regions=False, from_db=True):
        """ Using the saved pandas files (see self.export_to_pandas), optionally provide two (separate=True) dataframes, one for stocks over time and for stock the first year and new construction for subsequent periods.

        Set duplicate_for_world_regions=False to get one record (row) for each observation, rather than one for each World Region for each observation.
        """
        from analysis import osm_analysis as osman # Circular import
        
        if from_db:
            dfc = self.get_dbconn().db2df('cities_disconnectivity_'+self.cradius,  schema = 'cities',  drop_geom = True, )
            dfo = dfc.set_index('city').join(self.atlascities)
        else:
            if 'USE_DB' in defaults and defaults['USE_DB']:
                input(' This should not happen: Confirm use saved file?')
            dfo = pd.read_pickle(self.export_path).set_index('city').join(self.atlascities)

        
        n_check = len(dfo)
        df = add_standard_derived_metrics(dfo.copy()) 
        df['iso'] = df.ISO
        df =  osman.add_country_data_to_df_by_iso(df.reset_index(),   duplicate_for_world_regions=duplicate_for_world_regions)
        assert len(df) <= n_check or duplicate_for_world_regions
        allCities = df.groupby('City').negdegree.count()
        # If we're running this before all the aggregation is done (because we're testing/impatient), some cities might be missing:
        # There should be five entries for each city:
        if allCities[allCities<5].index.tolist():
            print(('    ******   Losing the following cities!!! {}'.format(allCities[allCities<3].index.tolist())))
        df = df[df.City.isin(  allCities[allCities>4].index.tolist())]
        assert len(df)==len(df.index.unique())
        if separate:
            stockdf = df[(df.stockorchange == 'cityedges') & (df.iyear ==3)].set_index('City', drop=False)
            absdf = df[(df.stockorchange == 'citychanges') | (df.iyear==1)].set_index('City', drop=False)
            return stockdf,absdf
        return df
    def load_saved_aggregated(self):
        return self.load_aggregated(from_db=False)
    def load_aggregated(self, from_db=None):
        from analysis import osm_analysis as osman # Circular import

        if from_db is None: from_db = True if 'USE_DB' not in defaults else defaults['USE_DB']
        if from_db:
            dfo = self.get_dbconn().db2df('cities_aggregated_'+self.cradius, schema = 'cities',  drop_geom = True, )
        else:
            if 'USE_DB' in defaults and defaults['USE_DB']:
                input(' This should not happen: Confirm use saved file?')
            dfo = pd.read_pickle(self.export_path.replace('disconnectivity','aggregated')).set_index('city').join(self.atlascities)
        df=dfo.set_index('city').join(self.atlascities)
        df['iso'] = df.ISO.values
        df =  osman.add_country_data_to_df_by_iso(df.reset_index())
        return df
    
    def prep_plot_data(self, forceUpdate=False):
        """
        Among other things, this creates weighted means of connectivity variables at the level of ISO, GADM continent, and WB region.
        # N.B. 201709: These aggregates are much less interesting now that we have "urban" aggregates for WB regions.

        """

        fa,fd=paths['scratch']+'cities_prep_plot_data_abs.pandas',  paths['scratch']+'cities_prep_plot_data_delta.pandas'
        if not forceUpdate and os.path.exists(fa) and os.path.exists(fd) and (time.time() - os.path.getmtime(fa) < ( 24 * 60 * 60)) and  (time.time() - os.path.getmtime(fd) < ( 24 * 60 * 60)):
            print(('   prep_plot_data: Loading from saved file... (forceUpdate={}, USE_DB={})'.format(forceUpdate, None if 'USE_DB' not in defaults else defaults['USE_DB'])))
            return(pd.read_pickle(paths['scratch']+'cities_prep_plot_data_abs.pandas') , pd.read_pickle(paths['scratch']+'cities_prep_plot_data_delta.pandas'))

        df = self.load_connectivity()

        """
         Below, we generate dataframes containing summed n_nodes and n_node-weighted means of disconnectivity measures.
         These are generated both for ISOs and for continents, and both are eventually appended to the City-level data
         The following works by making use of the unique numeric index of df (after rest_index()). That way, we can use it inside groupby()s over different columns.
        See:         http://stackoverflow.com/questions/31521027/groupby-weighted-average-and-sum-in-pandas-dataframe   
         First, make sure that's how things are set up:
        """
        assert len(df)==len(df.index.unique())
        weighted_sum =lambda adf, odf=df: np.average(adf, weights = df.loc[adf.index,'n_nodes'])
        
        print(' Building ISO sums...')
        isomeans = df.groupby(['ISO','iyear','stockorchange'])[self.plotvars].agg(weighted_sum).reset_index()# .add_prefix('meansiso_') # Weighted mean dataframe
        isomeans['City'] = 'meanISO'+isomeans.ISO
        isosums = df.groupby(['ISO','iyear','stockorchange'])[['n_nodes']].agg(sum).reset_index()
        isosums['City'] = 'meanISO'+isosums.ISO
        clu = df.reset_index()[['ISO','continent']].drop_duplicates().set_index('ISO').dropna().to_dict()['continent']
        isomeans = isomeans.set_index(['ISO','iyear','stockorchange','City']).join(isosums.set_index(['ISO','iyear','stockorchange','City'])).reset_index()
        isomeans['continent'] = isomeans.ISO.map(lambda iso:clu[iso])
        isomeans['iso_mean']=True

        print(' Building continent sums...')
        continentmeans = df.groupby(['continent','iyear','stockorchange'])[self.plotvars].agg(weighted_sum).reset_index()
        continentmeans = continentmeans.assign(City = 'mean'+continentmeans.continent) #.add_prefix('meansiso_')  # Weighted mean dataframe
        continentsums = df.groupby(['continent','iyear','stockorchange'])[['n_nodes']].agg(sum).reset_index()
        continentsums = continentsums.assign(City = 'mean'+continentsums.continent)

        continentmeans = continentmeans.set_index(['iyear','stockorchange','City','continent']).join(continentsums.set_index(['iyear','stockorchange','City','continent'])).reset_index().assign(continent_mean=True)
 

        # Making means for World Bank groups is not so easy (many-to-many matches)
        print(' Building WB region sums...')
        c2wb = osmt.country2WBregionLookup() # GroupCode is the WBgroup. WBcode is the ISO code.
        WBmeans = pd.concat( [df[df.WBcode.isin(rdf)].groupby(['iyear','stockorchange'])[self.plotvars].agg(weighted_sum).assign(City='meanWB'+regionCode) for regionCode,rdf in c2wb.reset_index().groupby('GroupCode')['WBcode']] ).reset_index()
        WBsums = pd.concat( [df[df.WBcode.isin(rdf)].groupby(['iyear','stockorchange'])[['n_nodes']].agg(sum).assign(City='meanWB'+regionCode) for regionCode,rdf in c2wb.reset_index().groupby('GroupCode')['WBcode']] ).reset_index()
        WBmeans = WBmeans.set_index(['iyear','stockorchange','City']).join(WBsums.set_index(['iyear','stockorchange','City'])).reset_index().assign(WB_mean=True)
        
        df = pd.concat([df,isomeans,continentmeans, WBmeans])
        # Grab latest n_nodes for each city:
        latest_new_nodes = df.sort_values(['iyear','stockorchange']).groupby('City').tail(1).set_index('City')[['n_nodes']]
        latest_new_nodes.columns=['latest_nodes']
        df = df.set_index('City').join(latest_new_nodes).reset_index()

        df.set_index(['City','iyear','stockorchange'],inplace=True)
        numvars = [cc for cc in df.select_dtypes(include=[np.number]).columns  if cc not in ['ContinentID']]
        ddf = df.copy()
        for city in [ccc for ccc in df.reset_index().City.unique() if not cc.startswith('mean')]:
            ddf.loc[(city,3),numvars] = ddf.loc[(city,3)][numvars] -ddf.loc[(city,2)][numvars]             
            ddf.loc[(city,2),numvars] = ddf.loc[(city,2)][numvars] -ddf.loc[(city,1)][numvars]
        ddf = ddf.reset_index()
        ddf = ddf[ddf.iyear.isin([2,3])].set_index(['City','iyear','stockorchange'])
        # Let's make sure plotting values are all normal numeric types here:
        outs = [df,ddf]
        for idf,adf in enumerate(outs):
            for var in self.plotvars:
                outs[idf][var] = outs[idf][var].astype(float)

        df.reset_index().to_pickle(paths['scratch']+'cities_prep_plot_data_abs.pandas')
        ddf.reset_index().to_pickle(paths['scratch']+'cities_prep_plot_data_delta.pandas')
        print(('   prep_plot_data: Created saved files... (forceUpdate={}, USE_DB={})'.format(forceUpdate, None if 'USE_DB' not in defaults else defaults['USE_DB'])))

        return(df.reset_index(), ddf.reset_index())
    
    def set_iyear_xticklabels(self, ax, version='abs', data_are_real_years=False, fontsize=8):
        """ This simply labels the years of the Atlas of Urban Expansions time ranges.
        version is 'abs' (three xaxis points, one for each band) or 'delta' (two xaxis points, for changes)
        """
        if version in ['abs']:
            if data_are_real_years:
                ax.set_xticks(self.atlasyears_xticks)
            else:
                ax.set_xticks([1,2,3])
            labs = ax.set_xticklabels(self.atlasyears_labels_short, fontsize= fontsize)
            labs[0].set_horizontalalignment('left')
            labs[2].set_horizontalalignment('right')
        elif version in ['delta']:
            if data_are_real_years:
                ax.set_xticks(self.atlasyears_xticks[1:])
            else:
                ax.set_xticks([2,3])
            labs = ax.set_xticklabels(self.atlasyears_deltalabels)
            labs[0].set_horizontalalignment('left')
            labs[1].set_horizontalalignment('right')
            ax.plot(ax.get_xlim(),[0,0],'k:', alpha=.4, zorder=-10)
        else:
            unknown_version
        return labs

    def createCellLookup(self):
        """creates lookup from city to gridcell"""
        tableName = 'city_cell_lookup_'+self.cradius
        cmd = 'DROP TABLE IF EXISTS cities.%s;\n' % tableName
        cmd +=  '''CREATE TABLE cities.%s AS
                  SELECT row, col, city
                  FROM aggregated_by_cell_%s AS edges,  cityedges c
                WHERE
                     ST_INTERSECTS(edges.geom, c.geom) and iyear=3;''' % (tableName, self.cradius)
        
        db = self.get_dbconn()
        db.execute(cmd)

    def plot_cities_by_world_region(self, yvar = None, world_regions = 'WB', forceUpdate=False ):
        from analysis import osm_analysis as osman  # Circular import
        
        params = { 'text.usetex': True,
        }
        assert world_regions in ['WB', 'GADM']
        c2wb = osmt.country2WBregionLookup() # GroupCode is the WBgroup. WBcode is the ISO code.
        region_prettyname_dict = osmt.GADM_CONTINENT_PRETTYNAMES if world_regions=='GADM' else c2wb[['GroupName']].to_dict()['GroupName']
        os.system('mkdir -p {}citytrends'.format( paths['scratch'])  )

        maxCityLabels = 32
        if yvar is None:
            cpblUtilities.parallel.runFunctionsInParallel([[self.plot_cities_by_world_region,[var]] for var in self.plotvars], names=[defaults['server']['postgres_db']+str(defaults['osm']['continents'])+pv for pv in self.plotvars], parallel=defaults['server']['parallel'], offsetsSeconds=0.5)
            return
        absdf,deltadf = self.prep_plot_data()
        absdf = absdf.query('stockorchange=="citychanges" or iyear==1')
        
        assert 'pca1' in absdf
        for df,version in [[absdf,'abs'],[deltadf,'delta']]:
            if world_regions == 'GADM':
                regions = df.continent.unique()  # N.B. This isn't written/updated yet. Fix this to allow world_regions to be 'GADM'
            elif world_regions == 'WB':
                regions = [[regionCode, df[df.WBcode.isin(rdf)]] for regionCode,rdf in c2wb.reset_index().groupby('GroupCode', sort=False)['WBcode']]
                regions = [[a,b] for a,b in regions if not b.empty]
            ylims = smetrics.get_plotlim(yvar, [df[yvar].min(), df[yvar].max()])
            for adict in multipage_plot_iterator(regions, filename = paths['scratch']+'citytrends/cities-by-world_region_trends_-{}-{}-{}'.format(version,str2pathname(yvar),  defaults['server']['postgres_db'])):
                regiont,ax,fig = adict['data'], adict['ax'], adict['fig']
                region,rdf = regiont 
                adf = rdf[-rdf.City.str.startswith('mean')].sort_values(['iyear'],ascending=False)
                ISOs = adf.ISO.unique()
                nISOs = len(ISOs)
                ISOcolours = cpblUtilities.color.getIndexedColormap('jet',ISOs)
                lcontinent = region_prettyname_dict.get(region,region) # likely this won't help, since we are using WB regions, not GADM continents

                nCities = len(adf.City.unique())
                print((' {} countries ({} cities) in {}'.format(nISOs, nCities, region)))
                icounter=0
                for iso in ISOs:
                    ccounter=0
                    for city,cadf in adf[(adf.ISO ==iso) & -(adf.City.str.startswith('mean'))].groupby('City'):
                        hh = ax.plot(cadf.iyear,cadf[yvar],'-'+'-'*(icounter%2 and nISOs>7),color = ISOcolours[iso], label = "_nolegend_" if ccounter or nCities>maxCityLabels else iso , alpha =.8)
                        ccounter += 1
                    icounter += 1
                # Now look for the means:
                mdf = df.set_index('City').loc['meanWB'+region].reset_index()
                ax.plot(mdf.iyear,mdf[yvar],'k', label = "_nolegend_", lw=10, alpha=.4, zorder = +15)
                ax.set_ylim(ylims)
                if smetrics.metricSettings[yvar]['direction'] == -1:
                    ax.invert_yaxis()

                ax.yaxis.grid(alpha=.15)
                self.set_iyear_xticklabels(ax, version)

                titletext = cpblUtilities.cpblunicode.str2latex('\n'.join(textwrap.wrap(lcontinent.replace('_',' ').replace('(','\n('),18))) +adict['first']*('\n'+(version=='delta')*r'$\Delta$'+self.varnames[yvar])
                if nCities > maxCityLabels:
                    osman.bottomlefttext(ax,titletext,  alpha = 0.5, color='k')
                cpblUtilities.mathgraph.transbg(ax.legend(loc='best', ncol = 1+(nISOs>7), title = titletext, 
fontsize = 4 if nISOs>10 else 'xx-small' if nISOs>7 else 'x-small' if nISOs>3 else 'small' ))

                if 0: ax.set_title(lcontinent)

    
    def plot_cities_by_continent_older(self, yvar = None, forceUpdate=False ):
        if yvar is None:
            allVars= self.plotvars
            cpblUtilities.parallel.runFunctionsInParallel([[self.plot_cities_by_world_region,[var]] for var in allVars], names=[defaults['server']['postgres_db']+str(defaults['osm']['continents'])+pv for pv in allVars], parallel=defaults['server']['parallel'], offsetsSeconds=0.5)
            return
        absdf,deltadf = self.prep_plot_data()
        ### Plot by continent 
        for df,version in [[absdf,'abs'],[deltadf,'delta']]:
            plt.close('all')
            f, axarr = plt.subplots(2, 4)
            axAsia = plt.subplot2grid((2,4), (0,3), rowspan=2)
            axL = dict( list(zip(['australiaoceania', 'centralamerica', 'africa', 'southamerica', 'northamerica', 'europe', 'asia'], np.concatenate(axarr[:,:-1]).tolist()+[axAsia])))
            fig=plt.gcf()
            regions = df.continent.unique()
            for icontinent,continent in enumerate(regions):
                ax = axL[continent] 
                adf = df[df.continent==continent]
                ISOs = adf.ISO.unique()
                nISOs = len(ISOs)
                ISOcolours = cpblUtilities.color.getIndexedColormap('jet',ISOs)
                lcontinent = osmt.GADM_CONTINENT_PRETTYNAMES[continent]
                print((' {} countries ({} cities) in {}'.format(nISOs, len(adf.City.unique()), continent)))
                icounter=0
                for iso in ISOs:
                    ccounter=0
                    for city,cadf in adf[(adf.ISO ==iso) & -(adf.City.str.startswith('mean'))].groupby('City'):
                        hh = ax.plot(cadf.iyear,cadf[yvar],'-'+'-'*(icounter%2 and nISOs>7),color = ISOcolours[iso], label = "_nolegend_" if ccounter else iso , alpha =.8)
                        ccounter += 1
                mdf = df.loc['mean'+continent].reset_index()
                ax.plot(mdf.iyear,mdf[yvar],'k', label = "_nolegend_", lw=10, alpha=.2)
                ax.yaxis.grid(alpha=.15)
                self.set_iyear_xticklabels(ax, version)

                cpblUtilities.mathgraph.transbg(ax.legend(loc='best', ncol = 1+(nISOs>7), title = lcontinent +(icontinent==0)*('\n'+(version=='delta')*r'$\Delta$'+self.varnames[yvar]), fontsize = 'x-small' if nISOs>7 else 'medium' ))

                if 0: ax.set_title(lcontinent)
            cpblUtilities.mathgraph.savefigall(paths['scratch']+'cities_by_continent_trends_'+version+'_'+str2pathname(self.varnames[yvar]), rv=False, wh_inches=[8.75,7])
                 
        
    def plot_cities_by_country(self, yvar = None,  nrows=3, ncols=4, wh_inches='landscape', include_GHSL_means = True, forceUpdate=False ):
        """ Make a multi-page set of plots of all Atlas cities in each country, ordered by size (?) of country.
        """
        from analysis import osm_analysis as osman # Circular import
        
        os.system('mkdir -p {}citytrends'.format( paths['scratch']))
        params = { 'text.usetex': True,
                   #'text.latex.unicode': True,
                   #'text.latex.preamble':r'\usepackage{amsmath},\usepackage{amssymb}',
        }
        plt.rcParams.update(params)
        if yvar is None:
            absdf,deltadf = self.prep_plot_data()
            allVars= [ vv for vv in self.plotvars if vv in absdf]
            cpblUtilities.parallel.runFunctionsInParallel([[self.plot_cities_by_country,[var, nrows, ncols, wh_inches]] for var in allVars], names=[defaults['server']['postgres_db']+str(defaults['osm']['continents'])+pv for pv in allVars], parallel=defaults['server']['parallel'], offsetsSeconds=0.5)
            return
        absdf,deltadf = self.prep_plot_data()
        absdf = absdf.query('stockorchange=="citychanges" or iyear==1')
        if include_GHSL_means:
            analyzer=osman.osm_ghsl_analysis()
            if 0: 
                df0 = analyzer.has_sufficient_counts(analyzer.integrate_country_data('iso')).sort_values('ghsl_yr')
                if 0: #Not used?!
                    df1 = analyzer.has_sufficient_counts(analyzer.integrate_country_data('id_1')).sort_values('ghsl_yr')
                for dd in [df0,df1]:
                    add_standard_derived_metrics(dd)

                ghsl_df = df0[df0.ghsl_yr>0]
            # 2019 update: get data in useful form using new methods:
            ghsl_df = osman.get_long_TS_dataset_countries_and_cities_and_regions(urban=True,
                                                                                 density=False,
                                                                                 forceUpdate=False,
                                                                                 from_db=None,
                                                                                 formatted_year_names=True)

            ghsl_df = ghsl_df.query('city==False').query('ghsl_yr>0').sort_values('ghsl_yr')
            ghsl_ylims = [ghsl_df[yvar].min(), ghsl_df[yvar].max()]


        
        for tdf,version in [[absdf,'abs'],[deltadf,'delta']]:
            df = tdf.reset_index().sort_values(['iyear','latest_nodes'],ascending=False).set_index('City')
            ISOs = df[df.iso_mean==True].sort_values('n_nodes',ascending=False).ISO.unique()
            ylims = smetrics.get_plotlim(yvar, [df[yvar].min(), df[yvar].max()])
            for adict in multipage_plot_iterator([df[(df.ISO==iso)].reset_index() for iso in ISOs], filename = paths['scratch']+'citytrends/cities-by-country-trends-{}-{}-{}'.format(version,str2pathname(yvar), defaults['server']['postgres_db']), nrows=nrows, ncols=ncols, wh_inches=wh_inches): 
                plotDf,ax,fig = adict['data'], adict['ax'], adict['fig']
                plotDf = plotDf[-plotDf.City.str.startswith('mean')].sort_values(['iyear'],ascending=False)
                iso,countryName = plotDf['ISO'].unique()[0],  plotDf.iloc[0].CountryName 
                cityColours = cpblUtilities.color.getIndexedColormap('jet',plotDf.City.unique())
                ncities = len(cityColours)
                ccounter=0
                for city,adf in plotDf.groupby('City',sort=False): # The sort=False means leave the order (sorted by latest_nodes) rather than resorting by City
                    hh = ax.plot(self.atlasyears_xticks[adf.iyear-1],adf[yvar],'-'+'-'*(ccounter%2 and ncities>7),color = cityColours[city], label = city , alpha=.8)
                    ccounter += 1
                if ncities>1:
                    mdf = df.loc['meanISO'+iso].reset_index()
                    ax.plot(self.atlasyears_xticks[mdf.iyear-1],mdf[yvar],'k', label = "_nolegend_", lw=10, alpha=.2, zorder = -20)
                if include_GHSL_means:
                     ax.plot(ghsl_df[ghsl_df.iso==iso].ghsl_yr.values, ghsl_df[ghsl_df.iso==iso][yvar], 'k',label='_nolegend_', lw=20,alpha=.2, zorder =-21)

                    
                ax.set_ylim(ylims)
                if smetrics.metricSettings[yvar]['direction'] == -1:
                    ax.invert_yaxis()
                xlabs = self.set_iyear_xticklabels(ax, version,  data_are_real_years=True)
                if version =='abs' and include_GHSL_means:
                    ax.set_xlim(self.atlasyears_xticks[[0,-1]] + np.array([-5, 2]))
                    xlabs[0].set_horizontalalignment('center')
                if not adict['bottom']:
                    ax.set_xticklabels([])
                if not adict['left']:
                    ax.set_yticklabels([])
                ax.yaxis.grid(alpha=.15)

                handles, labels = ax.get_legend_handles_labels()
                maxLlength = max([len(s) for s in labels])
                ncol, fontsize = 1+(ncities>3), 'small'
                fontsize = 4 if ncities>10  or (ncol>1 and maxLlength>8) else 'xx-small' if ncities>7 else 'xx-small' if ncities>3 else 'small' 
                cpblUtilities.mathgraph.transbg(ax.legend(loc='best', ncol = ncol, title =countryName+adict['first']*('\n'++(version=='delta')*r'$\Delta$ '+smetrics.metricNames[yvar]), fontsize = fontsize))

                fig.subplots_adjust(wspace=.06,hspace=0.06)
                
    def scatterplot_changes_by_size(self, yvar = None, ncols = None, nrows = None, forceUpdate=False):
        """
        look at recent changes, with symbol size representing number of new nodes.
        """
        if yvar is None and ncols in [1, None] and nrows in [1,None]:
            allVars=self.plotvars
            cpblUtilities.parallel.runFunctionsInParallel([[self.scatterplot_changes_by_size,[var]] for var in allVars], names=[defaults['server']['postgres_db']+str(defaults['osm']['continents'])+pv for pv in allVars], parallel=defaults['server']['parallel'], offsetsSeconds=0.5)
            return

        assert (ncols is None and  nrows is None) or (ncols is not None and  nrows is not None)
        ncols = 1 if ncols is None else ncols
        nrows = 1 if nrows is None else nrows
        
        absdf,deltadf = self.prep_plot_data(forceUpdate=forceUpdate)
        absdf = absdf[-absdf.City.str.startswith('mean')]
        ISOs = absdf.ISO.unique()  # 76 of them
        nISOs = len(ISOs)
        countries = ISOs
        yvar = 'degree'
        from cpblUtilities.color import assignSegmentedColormapEvenly # Actually, I wanted to create the colour lookup before the loop below, but it's tricky


        generator = multipage_plot_iterator(
            [x for pair in zip(self.plotvars,self.plotvars) for x in pair],
            filename = paths['scratch']+'cities-scatterplots-persistence', ncols=ncols, nrows=nrows, wh_inches='landscape')
        for adict in generator:
            yvar,ax,fig = adict['data'], adict['ax'], adict['fig']

            dfv=absdf[['City','iyear', yvar]].pivot(columns = 'iyear', values = yvar, index = 'City')
            xynames = [self.varnames[yvar]+' ('+yy+')' for yy in self.atlasyears_labels]
            dfv.columns = xynames
            dfn=absdf[['City','iyear', 'logn_nodes']].pivot(columns = 'iyear', values = 'logn_nodes', index = 'City')
            n_names = [r'$\log(N_{\rm nodes})$ '+yy for yy in self.atlasyears_labels]
            dfn.columns = n_names
            df = dfv.join(dfn)
            df['logn_nodes'] = df[n_names].apply(np.exp).sum(axis=1).map(np.log)
            cmapper = assignSegmentedColormapEvenly([[0,0,0],[1,0,0]], df.logn_nodes.values)
            df['logn_nodes_color'] = df.logn_nodes.map(cmapper)
            cpblScatter(df, xynames[1],xynames[2], 'logn_nodes_color', markersize = 10*df[n_names[2]], labels = df.index, labelfontsize=3, ax=ax)
            ax.set_xlabel(xynames[1]) # Why does cpblScatter do this on page one but not >1?~??
            ax.set_ylabel(xynames[2])
            diag = df[xynames[1:3]].min().max(),df[xynames[1:3]].max().min()
            ax.plot( diag,diag,'k:', zorder=-10)
            #toplefttext(ax, '{}: {} vs {}'.format(cpblUtilities.cpblunicode.str2latex(self.varnames[yvar]),xynames[1],xynames[2]))
            # Now move on to the next axis, and plot a related thing:
            try:
                adict = next(generator)   # This line causes an error in/aftercp the final loop. Why?!
            except StopIteration:
                continue
            yvardummy,ax,fig =  adict['data'], adict['ax'], adict['fig']
            cpblScatter(df, xynames[0],xynames[1], 'logn_nodes_color', markersize = 10*df[n_names[1]], labels = df.index, labelfontsize=3,  ax = ax)
            ax.set_xlabel(xynames[0]) 
            ax.set_ylabel(xynames[1])
            diag = df[xynames[0:2]].min().max(),df[xynames[0:2]].max().min()
            ax.plot( diag,diag,'k:', zorder=-10)



    def load_AUEmetrics(self):
        """ Load three .xlsx files provided by AUE. 
        Re-separate them based on the 'filename' column
        TO DO : Some problem remains with concatenating them. Just return the Blocks _1 for the moment
        """
        fns = [                                       'Blocks_and_Roads_Table_1.xlsx',
                                                      'Blocks_and_Roads_Table_2.xlsx',
                                                      'Areas_and_Densities_Table_1.xlsx',
        ][:1]
        rawdfs = [pd.read_excel(ca.inpath+'AUEanalysis/'+infile, skiprows=None,) for infile in fns] # Following fails for multi-col headers: pd.read_excel(ca.inpath+'AUEanalysis/'+infile, skiprows=None, header=[0,1]) for infile in [

        for idf,df in enumerate(rawdfs): # Do a fix, since read_excel fails with this multiindex header with some multicol entries:
            lev0 = df.columns.tolist()
            lev1 = [v if pd.notnull(v) else '' for v in df.iloc[0].values]
            lt = None
            for i,t in enumerate(lev0):
                if t.startswith('Unnamed:'):
                    lev0[i] = lt
                else:
                    lt = lev0[i].strip()
            chck = list(zip(lev0, lev1))
            assert len(set(chck)) == len(chck)
            df.columns = pd.MultiIndex.from_tuples(list(zip(lev0, lev1)) , names=['H1', 'H2'])
            # One option here is to collapse the multiindex:
            df.columns = df.columns.map('|'.join)
            df['filename'] = fns[idf]
        return rawdfs[0].iloc[1:] 

        

def cityDensities_exploratory():
    """Plots and temp tables of city densities. See #297"""
    db = osmt.pgisConnection(verbose=True)
    
    # Density deciles of grid cells
    percentileStr = str([ii/100. for ii in range(101)])
    cmd = '''SELECT city, percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY density) FROM (
                 SELECT city, unnest(ST_DumpValues(ST_Union(safe_ST_Clip(rast, ST_Transform(c.geom, 4326))), 3) ) AS density
                 FROM cities.cityedges AS c, rasterdata.landscan as r
                 WHERE ST_Intersects(r.rast, ST_Transform(c.geom, 4326)) AND iyear=3 -- iyear=3 is the max extent (cities don't get smaller)
                 GROUP BY city) t1 WHERE density>0 GROUP BY city ;''' % percentileStr
    result = db.execfetch(cmd)
    cityCellsDf = pd.DataFrame([[ii[0]]+ii[1] for ii in result],columns=['city']+['pctile'+str(ii) for ii in range(101)]).set_index('city')
    cityCellsDf.to_pickle(paths['scratch']+'cityCellsDf.pandas')
    
    # Density deciles and frc developed of edges
    cmd = '''SELECT city, percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY density),
                          percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY ghsl_frc_developed)  FROM (
                 SELECT city, density, ghsl_frc_developed
                 FROM cities.cityedges AS c, edges_planet_%s AS e
            WHERE ST_Intersects(e.geom, c.geom) AND iyear=3) t1
            GROUP BY city;''' % (percentileStr, percentileStr, defaults['osm']['clusterRadii'][0])
    result = db.execfetch(cmd)
    cityEdgesDf = pd.DataFrame([[ii[0]]+ii[1] for ii in result],columns=['city']+['pctile'+str(ii) for ii in range(101)]).set_index('city')
    cityEdgesDf.to_pickle(paths['scratch']+'cityEdgesDf.pandas')


    # Density deciles of grid cells, by country
    cmd = '''SELECT iso, percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY density) FROM (
                 SELECT iso, unnest(ST_DumpValues(ST_Union(safe_ST_Clip(rast, ST_Transform(c.geom, 4326))), 3) ) AS density
                 FROM cities.cityedges AS c, rasterdata.landscan as r, %s AS g
                 WHERE ST_Intersects(r.rast, ST_Transform(c.geom, 4326)) AND iyear=3
                   AND ST_Intersects(ST_Transform(c.geom, 4326), ST_Transform(g.the_geom, 4326))
                 GROUP BY iso) t1 WHERE density>0 GROUP BY iso ;''' % (percentileStr, defaults['gadm']['TABLE'])
    result = db.execfetch(cmd)
    isoCellsDf = pd.DataFrame([[ii[0]]+ii[1] for ii in result],columns=['iso']+['pctile'+str(ii) for ii in range(101)]).set_index('iso')
    isoCellsDf.to_pickle(paths['scratch']+'isoCellsDf.pandas')

    # Density deciles of edges, by country
    cmd = '''DROP TABLE IF EXISTS city_densities_tmp;
             CREATE TABLE city_densities_tmp AS
             SELECT iso, percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY density) AS density_pctiles, 
                         percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY ghsl_frc_developed) AS ghsl_devel_pctiles FROM (
                 SELECT iso, density, ghsl_frc_developed
                 FROM cities.cityedges AS c, edges_planet_%s AS e, %s_iso AS g
            WHERE ST_Intersects(e.geom, c.geom) AND iyear=3
              AND ST_Intersects(ST_Transform(c.geom, 4326), ST_Transform(g.geom, 4326))) t1
            GROUP BY iso;''' % (percentileStr, percentileStr, defaults['osm']['clusterRadii'][0], defaults['gadm']['TABLE'])
    db.execute(cmd)
    cmd = '''ALTER TABLE city_densities_tmp ADD COLUMN density_pctile5 real,    ADD COLUMN density_pctile10 real, 
                                            ADD COLUMN ghsl_devel_pctile5 real, ADD COLUMN ghsl_devel_pctile10 real;
             UPDATE city_densities_tmp SET density_pctile5 = density_pctiles[6],    density_pctile10 = density_pctiles[11],
                                           ghsl_devel_pctile5 = ghsl_devel_pctiles[6], ghsl_devel_pctile10 = ghsl_devel_pctiles[11];'''
    db.execute(cmd)
    
    result = db.execfetch('SELECT iso, density_pctiles FROM city_densities_tmp;')
    isoEdgesDf = pd.DataFrame([[ii[0]]+ii[1] for ii in result],columns=['iso']+['density_pctile'+str(ii) for ii in range(101)]).set_index('iso')
    isoEdgesDf.to_pickle(paths['scratch']+'isoEdgesDf.pandas')
    result = db.execfetch('SELECT iso, ghsl_devel_pctiles FROM city_densities_tmp;')
    isoGhslEdgesDf= pd.DataFrame([[ii[0]]+ii[1] for ii in result],columns=['iso']+['ghsl_devel_pctile'+str(ii) for ii in range(101)]).set_index('iso')
    
    # add world
    result = db.execfetch('''SELECT percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY ghsl_frc_developed) 
                             FROM edges_planet_%s''' % (percentileStr, defaults['osm']['clusterRadii'][0]))
    isoGhslEdgesDf.loc['WLD'] = result[0][0]
    isoGhslEdgesDf.to_pickle(paths['scratch']+'isoGhslEdgesDf.pandas')

    # now plot deciles by city and country

    outList = []
    for df,dfname in zip([cityCellsDf,cityEdgesDf,isoCellsDf,isoEdgesDf,isoGhslEdgesDf],
                         ['density cell by city','density edge by city','density cell by iso','density edge by iso','ghsl frc edge by iso']):
        fig,axes=plt.subplots(1,2,figsize=(6.5,9)) 
        for city, row, in df.iterrows():
            lw, c = (4, 'k') if city=='WLD' else (None, None)
            axes[0].plot(list(range(101)), row.values,lw=lw,color=c)
            axes[1].plot(list(range(101)), row.values,lw=lw,color=c)
        if 'ghsl' in dfname:
            axes[0].set_xlabel('Percentile (by edge and iso)')
            axes[1].set_xlabel('Percentile (by edge and iso)')
            axes[0].set_ylabel('Frc of GHSL pixels developed')
            axes[1].set_ylim(bottom=0, top=0.2)  
            for iso, val in df[df.ghsl_devel_pctile50<.15].iterrows():
                axes[0].text(50, val['ghsl_devel_pctile50'],iso,fontsize=8)
            axes[0].text(50, df.loc['WLD','ghsl_devel_pctile50'],'WLD',fontsize=12)
            axes[1].text(50, df.loc['WLD','ghsl_devel_pctile50'],'WLD',fontsize=12)
        else:
            axes[0].set_xlabel('Density percentile (%s)' % dfname)
            axes[1].set_xlabel('Density percentile (%s)' % dfname)
            axes[0].set_ylabel('Density (persons km sq)')
            axes[1].set_ylim(bottom=0, top=10000)
        fig.tight_layout()
        figname = paths['scratch']+dfname.replace(' ','_')+'.pdf'
        fig.savefig(figname)
        outList.append(figname)
    
    cpblUtilities.mergePDFs(outList, paths['scratch']+'densityandGhslPctile_cities.pdf')  

    # Calculate 5% and 10% percentiles of densities for cells and edges for each country
    # If we use this as a threshold for the entire country, what fraction of edges do we capture?
    cmd = '''DROP TABLE IF EXISTS city_edges_urban_tmp;
             CREATE TABLE city_edges_urban_tmp AS
             SELECT iso, COUNT(*) AS n_edges, SUM(density_above5) AS n_above5_density, SUM(density_above10) AS n_above10_density,
                        SUM(density_above5)::real/COUNT(*) AS frc_above5_density, SUM(density_above10)::real/COUNT(*) AS frc_above10_density,
                        SUM(ghsl_above5) AS n_above5_ghsl, SUM(ghsl_above10) AS n_above10_ghsl,
                        SUM(ghsl_above5)::real/COUNT(*) AS frc_above5_ghsl, SUM(ghsl_above10)::real/COUNT(*) AS frc_above10_ghsl
                FROM
                 (SELECT l.iso, (density>density_pctile5)::int AS density_above5,(density>density_pctile10)::int AS density_above10,
                                (ghsl_frc_developed>ghsl_devel_pctile5)::int AS ghsl_above5,(ghsl_frc_developed>ghsl_devel_pctile10)::int AS ghsl_above10
                 FROM
                     city_densities_tmp c, edges_planet_%s e, lookup_planet_edge%s_%s l
                        WHERE e.edge_id=l.edge_id AND c.iso=l.iso) t1 GROUP BY iso;''' % (defaults['osm']['clusterRadii'][0], defaults['osm']['clusterRadii'][0], defaults['gadm']['TABLE'])
    db.execute(cmd)
    countryDf = osmt.loadOtherCountryData().set_index('ISOalpha3')
    result = db.execfetch('SELECT iso,frc_above5_density, frc_above10_density,frc_above5_ghsl, frc_above10_ghsl FROM city_edges_urban_tmp;')
    frcDf = pd.DataFrame([list(ii) for ii in result],columns=['ISOalpha3','frc_above5_density','frc_above10_density','frc_above5_ghsl','frc_above10_ghsl']).set_index('ISOalpha3')

    db.execute('DROP TABLE city_edges_urban_tmp; DROP TABLE city_densities_tmp;')
    
    countryDf=countryDf.join(frcDf)
    countryDf['frc_pop_urban']=countryDf.urbanPop2012*1./countryDf.pop2012
    outList=[]
    for yvar in ['density','ghsl']:
        fig,axes=plt.subplots(2,2,figsize=(6.5,9)) 
        axes[0,0].scatter(countryDf.popDensity_sqkm2012.apply(np.log), countryDf['frc_above5_'+yvar])
        axes[0,1].scatter(countryDf.popDensity_sqkm2012.apply(np.log), countryDf['frc_above10_'+yvar])
        axes[1,0].scatter(countryDf.frc_pop_urban, countryDf['frc_above5_'+yvar])
        axes[1,1].scatter(countryDf.frc_pop_urban, countryDf['frc_above10_'+yvar])
        axes[0,0].set_xlabel('Log population density (national)')
        axes[0,1].set_xlabel('Log population density (national)')
        axes[1,0].set_xlabel('Frc pop urban')
        axes[1,1].set_xlabel('Frc pop urban')
        axes[0,0].set_ylabel('frc_edges_above5_'+yvar)
        axes[0,1].set_ylabel('frc_edges_above10_'+yvar)
        axes[1,0].set_ylabel('frc_edges_above5_'+yvar)
        axes[1,1].set_ylabel('frc_edges_above10_'+yvar)
        for col in [0,1]:
            axes[1,col].plot([(0,0),(1,1)])
        fig.tight_layout()
        fig.savefig(paths['scratch']+yvar+'tmp.pdf')
        outList.append(paths['scratch']+yvar+'tmp.pdf')
    cpblUtilities.mergePDFs(outList, paths['scratch']+'density_and_ghslPctile_vs_nationaldensity.pdf')  

def compare_AUEmetrics():
    ca = city_atlas(forceUpdate=True)
    osm =ca.load_connectivity(separate=False)
    aue = ca.load_AUEmetrics()


    iyear=3
    osm3 = osm.query('stockorchange=="citychanges" and iyear=={}'.format(iyear))
    df = aue.set_index('City Name|').join( osm3.set_index('index'))
    xv, yv = 'Walkability Ratio|1990 - 2015', 'distanceratio_500_1000'
    fig,ax= plt.subplots(1)
    ax.plot(df[xv], df[yv], '.', label="_none_")
    ax.set_xlabel('Walkability ratio (AUE sample, 1990--2015)')
    ax.set_ylabel('Distance ratio (0.5--1km), {})'.format(ca.atlasyears_labels[iyear-1]))
    h = dfOverplotLinFit(df,xv,yv,aweights=None, ax=ax, ci=True, zorder = -10,
                         label='R$^2$$_a$={r2a:.2f}')#$^2_a$=')
    plt.legend()
    fig.savefig(paths['graphics']+'AUE-OSM-distRatio.pdf')



    for iyear in [2,3]:
        osm3 = osm.query('stockorchange=="citychanges" and iyear=={}'.format(iyear))
        df = aue.set_index('City Name|').join( osm3.set_index('index'))
        df['fraction4way'] = 1-df.fraction_1_3
        xv,yv =  'Share of Intersections that are 4-Way|1990 - 2015', 'fraction4way'
        #df.plot(x=xv, y=yv, kind='scatter', label="_none_")  #ax =
        fig,ax= plt.subplots(1)
        ax.plot(df[xv], df[yv], '.', label="_none_")
        ll = min(min(df[xv]), min(df[yv])), max(max(df[xv]), max(df[yv]))
        plot_diagonal(df[[xv,yv]], color= [.3,.3,.3], linestyle='--', ax=ax, lw=.2, label='identity')
        ax.set_xlabel('Fraction 4-way (AUE sample, 1990--2015)')
        ax.set_ylabel('Fraction 4-way (OSM census, {})'.format(ca.atlasyears_labels[iyear-1]))
        plt.axis('tight')
        h = dfOverplotLinFit(df,xv,yv,aweights=None, ax=ax, ci=True, zorder = -10,
                             label='R$^2$$_a$={r2a:.2f}')#$^2_a$=')
        plt.legend()
        fig.savefig(paths['graphics']+'AUE-OSM-frc4way-yr{}.pdf'.format(iyear))
    
    plt.show()

################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
################################################################################################
    from osm_config import parse_common_docopt_options
    from cpblUtilities.configtools import get_docopt_mode
    try:
        # Parse arguments, use file docstring as a parameter definition
        arguments = docopt.docopt(__doc__)
        FORCEUPDATE = parse_common_docopt_options(defaults, arguments)
        runmode = get_docopt_mode(arguments)
        defaults['USE_DB'] = not arguments['--local']

        ca = city_atlas(forceUpdate=True)
        if runmode in ['purge']:
                ca.db.delete_all_tables(contains='_t3', schema= 'cities',  force_do_not_ask= False)
                ca.db.delete_all_tables(contains='_t2', schema= 'cities',  force_do_not_ask= True)
                ca.db.delete_all_tables(startswith='aggregated', schema= 'cities',  force_do_not_ask= True)

        preloaded_success=False                
        if runmode in ['run_all_from_scratch','copy_template']:
            # First, try to load from pre-existing template or sql file:
            if not forceUpdate and ca.load_saved_citychanges_and_cityedges():
                print('  Successfully loaded pre-fab tables')
                preloaded_success=True # Skip several steps below

        if runmode in ['load','run_all_from_scratch', 'build_template'] and not preloaded_success:
                ca.db.delete_all_tables(contains='_edge_t', schema= 'cities',  force_do_not_ask= False)
                ca.load_all_city_borders_into_postGIS()
        if runmode in ['deltas','run_all_from_scratch', 'build_template']  and not preloaded_success:
                ca.generate_city_deltas()
        if runmode in ['makeworld','run_all_from_scratch', 'build_template'] and not preloaded_success:
                ca.generate_world_cities_tables()
        if runmode in ['aggregate','run_all_from_scratch']:
            ca.aggregatePointMetrics()

        if runmode in ['disconnectivity','run_all_from_scratch']:
            ca.disconnectivity()

        if runmode in ['export','run_all_from_scratch']:
            ca.export_to_pandas()
            ca.prep_plot_data(forceUpdate=True)

        if runmode in ['export','run_all_from_scratch', 'cleanup', 'build_template']:
            ca.cleanup_tables()
                
        if runmode in ['plot','plotr','plotc']:
                ca.prep_plot_data(forceUpdate=FORCEUPDATE)
        if runmode in ['plot','plotr','run_all_from_scratch']:
                ca.plot_cities_by_world_region(forceUpdate=False) # This runs in parallel, using result of forceUpdate-dependent call above.
        if runmode in ['plot','plotc','run_all_from_scratch']:
                ca.plot_cities_by_country(wh_inches = 'landscape', forceUpdate=FORCEUPDATE)
                #ca.plot_cities_by_country(ncols=1, nrows=1, wh_inches = 'landscape')

        if runmode in ['plot','plotscatter']:
            ca.scatterplot_changes_by_size(ncols=2,nrows=2, forceUpdate=FORCEUPDATE)#'degree')            

        if runmode in ['copy_to_local']:
            ca.copy_plot_data_from_server()

        if runmode in ['AUEmetrics']:
            compare_AUEmetrics()
            
    # Handle invalid options
    except docopt.DocoptExit as e:
          print((e.message))

    
          
















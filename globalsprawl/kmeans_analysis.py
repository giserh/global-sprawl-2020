#!/usr/bin/python

"""
k-means cluster analysis to identify clusters (empirical archetypes)
of street connectivity patterns
They can be defined at the grid cell level (as in published analysis)
or for GADM regions (e.g. countries, sub-national geographies)

This code provides numerous options to explore different cluster definitions
based on different values of k and component metrics
The production (published) versions are specific in defaultK, kmDefaultCols, and kmDefaultDrop
"""

__author__ = 'cpbl,amb'
from osm_config import paths,defaults
import postgres_tools as psqlt
from scipy.cluster.vq import kmeans2,vq, whiten
from scipy.spatial.distance import cdist

import pandas as pd
import numpy as np
from collections import OrderedDict
import osm_lookups
import osm_cities
import svgTools as svgt
import math, time, string
import osmTools as osmt
from rasterTools import getDensityDeciles, latlon_to_urldict, mapCell
import rasterTools
from scipy.stats import gaussian_kde, percentileofscore
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.gridspec as gridspec
from matplotlib.path import Path
from matplotlib.spines import Spine
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection
from cpblUtilities.color import linearColormapLookup,assignSplitColormapEvenly
from cpblUtilities import shelfLoad, mergePDFs
from cpblUtilities.parallel import runFunctionsInParallel

from aggregate_connectivity_metrics import metricNames
metricNames={kk:vv.replace(' (','\n(') for kk, vv in metricNames.items()}
metricNames['fraction_1_3'] = metricNames['fraction_1_3'].replace('and ','and\n')


# This is a list of potential components used for development
colsToUseDict = {1: ['negdegree','fraction_1_3','fraction_deadend','distanceratio_500_1000','frc_edges_noncycle','curviness',],
                 2: ['negdegree','fraction_1_3','fraction_deadend','distanceratio_0_500','distanceratio_500_1000','distanceratio_1000_1500','distanceratio_1500_2000', 'frc_length_bridge','frc_length_noncycle','frc_edges_noncycle','frc_edges_bridge','curviness'],
                 3: ['negdegree','distanceratio_500_1000','frc_edges_noncycle',],
                 4: ['negdegree','fraction_1_3', 'fraction_deadend','distanceratio_0_500','distanceratio_500_1000','frc_length_noncycle','frc_length_bridge','curviness'],
                 5: ['negdegree','fraction_1_3', 'fraction_deadend','distanceratio_0_500','distanceratio_500_1000','frc_edges_noncycle','frc_edges_bridge','curviness'],
                 6: ['negdegree','fraction_deadend', 'fraction_1_3','distanceratio_0_500','distanceratio_500_1000','frc_edges_noncycle','curviness',],
                 7: ['negdegree','fraction_deadend', 'fraction_1_3','distanceratio_500_1000','distanceratio_1000_1500','frc_edges_noncycle','curviness',],
                 8: ['negdegree','fraction_1_3', 'fraction_deadend','distanceratio_0_500','distanceratio_500_1000','frc_edges_bridge','curviness'],}

# Specify the columns, k, names, etc. used in the published paper
defaultK = 10
kmDefaultCols = colsToUseDict[5]
kmDefaultDrop = [6,9] # clusters to drop
clusterNames = {'H':'Disconnected','G':'Dendritic','F':'Dead ends','E':'Circuitous','D':'Broken grid',
                'C':'Irregular grid','B':'Degree-3','A':'Grid','I':'Cluster I','J':'Cluster J'}
defaultExamples = {8:(4251, 20877), # UK / N Ireland   H
                             7:(6314, 36864), # Korea   G
                             5:(7033, 25809), # Palestine F
                             4:(9068, 36120), # Philippines  E 
                             3:(9231, 31225), # India D
                             2:(4406, 25729), # Russia C
                             1:(14194, 25093), # South Africa B
                             0:(14804, 14816)   } # Uruguay A


class kmeans_analyzer():
    """
    K-means cluster: 
    Can also return the closest member of each cluster from each continent
    Can also return a radar plot of the centroids of each cluster
    """
    
    def __init__(self, nClusters=10, gadmLevel=5, byCell=False, clusterradius=None, continents=None, columns=None, urban=True, minNodes=25, dropClusters=None, sortCol=None,minDensityDecile=None):
        """By cell clusters on grid cells. If True, gadmLevel is ignored
        minDensityDecile restricts cells to those above a certain decile (warning: this is 1-based, i.e. 1-10"""
        self.nClusters=nClusters
        self.continents=defaults['osm']['continents'] if continents is None else continents
        self.schema = defaults['osm']['analysisSchema']
        self.gadmLevel = gadmLevel
        self.ids = ['row','col'] if byCell else defaults['gadm']['ID_LEVELS'][:gadmLevel+1]
        self.idName = 'cell' if byCell else defaults['gadm']['ID_LEVELS'][gadmLevel]
        self.byCell = byCell
        self.clusterradius =  clusterradius if clusterradius is not None else defaults['osm']['clusterRadius']
        assert self.clusterradius is not None #Issue#275
        assert isinstance(self.clusterradius, str) and isinstance(self.gadmLevel, int)

        self.logger=osmt.osm_logger('kmeans_analysis_cr'+self.clusterradius+'_'+self.idName+'_k'+str(self.nClusters), verbose=True, timing=True)        
        self.db = psqlt.dbConnection(self.schema, logger=self.logger)
        self.engine=psqlt.getPgEngine()

        if columns is None:
            tableName = 'disconnectivity_by_cell_'+self.clusterradius if byCell else 'disconnectivity_'+self.idName+'_'+self.continents[0]+'_'+self.clusterradius
            allColumns = self.db.list_columns_in_table(tableName)
            self.idColumns = [cc for cc in allColumns if cc in self.ids]
            self.columns  = [cc for cc in sorted(allColumns) if cc in list(metricNames.keys())] 
        else:
            if any([cc in self.ids for cc in columns]):
                raise Exception('Do not pass gadm id in columns argument. Use gadmLevel instead')
            self.columns=columns
            self.idColumns = self.ids
        self.colsToUseId = [kk for kk in colsToUseDict if colsToUseDict[kk]==self.columns]
        self.colsToUseId = '' if self.colsToUseId==[] else '_ctu'+str(self.colsToUseId[0]) 
        
        self.centroids = None
        self.clustermeans = None
        self.df = None
        self.metricNames = metricNames.copy()
        self.closestMembers = None
        self.minNodes = minNodes  # just for plotting - does not affect the clusters
        self.minNodes_definition = 0 # 20
        self.fraction_deadend_definition = 1.0 # frc deadends must be less than this
        self.sortCol=sortCol
        self.sortColMeans = None
        self.ghsl_yr_df = None
        
        self.urban=urban
        self.minDensityDecile=minDensityDecile        
        if minDensityDecile: # restrict to cells over a certain density decile
            if minDensityDecile==0: raise Exception('density deciles are 1-based. You specified %d' % minDensityDecile)
            if not byCell: raise Exception('minDensityDecile only applies when byCell is True')
            deciles = getDensityDeciles()
            self.minDensity = deciles[minDensityDecile-1]
            self.densityDecileSuffix= '_densityDecile'+str(minDensityDecile)+'plus'
        else:
            self.minDensity=0
            self.densityDecileSuffix= ''
        
        # which clusters don't we show?
        self.dropClusters=[] if dropClusters is None else dropClusters
        self.clusterLabelMap = OrderedDict([(jj,string.ascii_uppercase[ii]) for ii, jj in enumerate([kk for kk in range(self.nClusters) if kk not in self.dropClusters])])

        # colors for clusters
        cmap=cm.jet
        cmapNorm = cm.ScalarMappable(norm=mpl.colors.Normalize(vmin=0, vmax=self.nClusters-1), cmap=cmap)
        self.clustColors = [cmapNorm.to_rgba(ii) for ii in range(self.nClusters)]
            
    def getArray(self):
        """Gets a dataframe of the disconnectivity tables for all the continents"""
        self.logger.write('Getting dataframe of disconnectivity tables')
        if self.byCell:
            colsToGet = self.columns+['n_nodes', 'ghsl_yr','iso']+['ls_density']*int(self.minDensity>0)+['urban']*self.urban
            if self.sortCol is not None and self.sortCol not in colsToGet: colsToGet.append(self.sortCol)
            dfAll = pd.read_sql_table('disconnectivity_by_cell_'+self.clusterradius, self.engine, schema=self.schema, index_col=self.idColumns, columns=colsToGet)
            if self.urban:
                self.logger.write('Dropping {} of cells for urban==True'.format(1-(dfAll.urban==True).mean()))
                dfAll = dfAll[dfAll.urban==True]
            if self.minDensity>0:
                self.logger.write('Dropping {} of cells for ls_density>={}'.format(1-(dfAll.ls_density>=self.minDensity).mean(),self.minDensity))
                dfAll = dfAll[dfAll.ls_density>=self.minDensity]
            if self.minNodes_definition>0:
                self.logger.write('Dropping {} of cells for n_nodes>={}'.format(1-(dfAll.n_nodes>=self.minNodes_definition).mean(), self.minNodes_definition))
                dfAll = dfAll[dfAll.n_nodes>=self.minNodes_definition]
            if self.fraction_deadend_definition<1:
                self.logger.write('Dropping {} of cells for fraction_deadend <= {}'.format((1-(dfAll.fraction_deadend <= self.fraction_deadend_definition).mean()), self.fraction_deadend_definition))
                dfAll = dfAll[dfAll.fraction_deadend <= self.fraction_deadend_definition] 
            if 'ls_density' in dfAll.columns: dfAll.drop('ls_density', axis=1, inplace=True)
            if 'urban' in dfAll.columns: dfAll.drop('urban', axis=1, inplace=True)                
        else:
            dfAll = pd.DataFrame()
            colsToGet = self.columns+['n_nodes']
            if self.sortCol is not None and self.sortCol not in colsToGet: colsToGet.append(self.sortCol)
            for cont in self.continents:
                table = 'disconnectivity_'+self.idName+'_'+cont+'_'+self.clusterradius+'_urban'*self.urban
                df = pd.read_sql_table(table, self.engine, schema=self.schema, index_col=self.idColumns, columns=colsToGet)
                df['continent'] = cont
                dfAll = pd.concat([dfAll, df])
            del df  # for safety

        # drop rows with missing data, and coerce to float
        dfAll.dropna(subset=self.columns,inplace=True)
        for col in dfAll.columns:
            if col in ['ghsl_yr','continent','iso']: continue
            if not(dfAll[col].dtype==np.float): dfAll[col] = dfAll[col].astype(np.float)
        
        # drop duplicates (e.g. if a gadm id is spread across multiple continents)
        dfAll.sort_values('n_nodes',ascending=False,inplace=True)
        dfAll = dfAll[~dfAll.index.duplicated()] # http://stackoverflow.com/questions/38662285/how-to-perform-pandas-drop-duplicates-based-on-index-column

        # convert nodal density to neg log form
        for col in dfAll.columns:
            if col.startswith('nodedensity_'):
               dfAll[col] = dfAll[col].apply(np.log)*-1
               self.metricNames[col] = self.metricNames[col].replace('Nodal density','Nodal density (-log)')
        
        if not self.byCell: dfAll.set_index('continent', append=True, inplace=True)
        self.df = dfAll[self.columns] # use this for the PCA estimates
        if self.sortCol not in self.columns: 
            self.sortColValues = pd.DataFrame(dfAll[self.sortCol])    

        # normalize array - recommended here: https://docs.scipy.org/doc/scipy-0.18.1/reference/generated/scipy.cluster.vq.whiten.html#scipy.cluster.vq.whiten
        # do this manually, because the scipy whiten routine gives slightly different values, and won't allow us to unnormalize
        # we can also demean
        self.stddevs = self.df.std()
        self.means = self.df.mean()
        self.normArray = np.array((self.df-self.means)/self.stddevs)
        self.n_nodes = dfAll.n_nodes
        self.ghsl_yr_df = dfAll['ghsl_yr'] if self.byCell else None
        self.iso_df =  dfAll['iso'] if self.byCell else None
        
    def defineClusters(self,sortOrder='asc', saveToDisk=None):
        """Runs the k-means algorithm
        Optional argument sortCol will order the clusters by the specified column"""
        if self.df is None: self.getArray()
        assert sortOrder in ['asc','desc']
        self.logger.write('Defining kmeans clusters')
        
        # From scipy docs: df should be a M by N array where the rows are the observation vectors       
        # Use kmeans2, not kmeans, as it reaches at least a local optimum. See http://stackoverflow.com/questions/20661142/whats-the-difference-between-kmeans-and-kmeans2-in-scipy 
        #self.centroids, self.distortion = kmeans2(self.normArray, self.nClusters, iter=1000)
        # we need to initialize at zero for all clusters, so that we get the same clusters each time
        # Note that centroids are the mean of each cluster
        print('AMB/CPBL say: If you see next a "UserWarning: One of the clusters is empty", ignore the warning. This is a scipy bug.')
        self.centroids, self.clusterLabels = kmeans2(self.normArray, np.zeros((self.nClusters,len(self.columns))), iter=1000)
        
        if self.sortCol is not None:
            if self.sortCol in self.columns:
                sortOrderParam = 1 if sortOrder=='asc' else -1
                sortColNum = self.columns.index(self.sortCol)
                sortOrder = (sortOrderParam*self.centroids[:,sortColNum]).argsort()   # multiply array by -1 to reverse the sort order
            else: # sort by a column not used to define the clusters. See #317
                sortAsc = True if sortOrder=='asc' else False
                self.sortColValues['cluster'] = self.clusterLabels
                sortOrder = (self.sortColValues.groupby('cluster')[self.sortCol].mean()).sort_values(ascending=sortAsc).index.values 
            self.clusterLabels = [sortOrder.tolist().index(ii) for ii in self.clusterLabels]
            self.centroids = self.centroids[sortOrder]  # http://stackoverflow.com/questions/2828059/sorting-arrays-in-numpy-by-column
        
        # assign each gadm id to a cluster
        assert all(self.clusterLabels == vq(self.normArray,self.centroids)[0])  # double check that the mapping worked
        self.df['cluster'] = self.clusterLabels
        
        # what are means of sortCol?
        if self.sortCol is not None:
            if self.sortCol in self.columns:
                self.sortColMeans = self.df.groupby('cluster')[self.sortCol].mean()
            else:
                self.sortColMeans = self.df.join(self.sortColValues[self.sortCol]).groupby('cluster')[self.sortCol].mean()                
            
        # Exception handler if analysis area is too small. See #271.
        if len(self.df) < self.nClusters:
            raise Exception('Analysis area is too small. Cannot proceed with \
                cluster analysis.')
        elif len(self.df.groupby('cluster').size()) < self.nClusters:
            raise Exception ('Some of the clusters are empty.')
    
        # add distances from each cluster to centroid
        # cdist returns a matrix of sum of distances to each cluster centroid
        self.sses = cdist(self.normArray, self.centroids, 'sqeuclidean') # should match vq, not clear if this is squared distances
        for clustId in range(self.nClusters):
            mask = (self.df.cluster==clustId)
            self.df.loc[self.df.cluster==clustId,'dist2centroid'] = self.sses[mask.values, clustId]

        if saveToDisk or (self.nClusters==defaultK and self.columns==kmDefaultCols and self.dropClusters==kmDefaultDrop):
            if saveToDisk is not False:
                # Construct table of normalized centroids, plus means and standard deviations
                odf1 = pd.DataFrame(self.centroids, columns=self.columns)
                odf2 = pd.DataFrame([self.means,self.stddevs], index=['mean','stddev'])
                odf = pd.concat([odf1,odf2])
                # And write it out:
                ofn =paths['output']+'kmeans_centroids.tsv'
                odf.to_csv(ofn , sep='\t')
                print(('Wrote following to {}:\n{}'.format(ofn,odf)))
            
    def getClosestMember(self, n=1, byContinent=False, forceUpdate=False):
        """Returns closest n representatives of each cluster, optionally from each continent
        Defined as the closest representative""" 
        if self.df is None or 'dist2centroid' not in self.df.columns: raise Exception('Must run defineClusters() or postgres2clusters() first')
        if self.closestMembers is not None and not forceUpdate:
            return self.closestMembers
        self.logger.write('Getting closest member to each cluster centroid')
        closestMembers = {}
        
        # sort data first
        df_sorted=self.df.join(self.n_nodes).sort_values(by='dist2centroid')
        
        for clustId in range(self.nClusters):
            if byContinent:
                closestMembers[clustId] = {}
                for cont in self.continents:
                    mask = (df_sorted.index.get_level_values(-1)==cont) & (df_sorted.cluster==clustId) & (df_sorted.n_nodes>self.minNodes)
                    closestMembers[clustId][cont]= df_sorted.loc[mask]
            else:
                mask = (df_sorted.cluster==clustId) & (df_sorted.n_nodes>self.minNodes)
                if mask.sum()<n: # not enough clusters to make minimum node cutoff
                    self.logger.write('Warning: not enough cells in cluster %s to make minimum node requirement. Taking largest clusters instead.' % clustId)
                    #minNodesTmp = df_sorted[df_sorted.cluster==clustId].sort_values(ascending=False)[n-1]
                    closestMembers[clustId] = (df_sorted.loc[df_sorted.cluster==clustId,'n_nodes'].sort_values(ascending=False).iloc[:n].index.values).tolist()
                else:
                    closestMembers[clustId] = (df_sorted.loc[mask].iloc[:n].index.values).tolist()
        
        self.closestMembers = closestMembers
        return self.closestMembers
        
    def radarChart(self, fontsize=10, figFn=None):
        if self.centroids is None: raise Exception('Must run defineClusters() first')
        self.logger.write('Producing radar chart')

        clusterSizes = self.df.groupby('cluster').size()
        spoke_labels = [self.metricNames[cc] for cc in self.columns]
        data = self.centroids.copy()
        data[data<-3] = -3   # minimum value that can be shown on a radar chart
        data = data.tolist()

        theta = radar_factory(len(spoke_labels), frame='polygon')

        fig, ax = plt.subplots(figsize=(9, 9), subplot_kw=dict(projection='radar'))
        fig.subplots_adjust(wspace=0.25, hspace=0.20, top=0.85, bottom=0.05, right=0.85)

        # ax.set_rgrids([0.2, 0.4, 0.6, 0.8])  # placeholder to add labels
        ax.set_title('Cluster analysis ('+self.idName.replace('_','-')+', k='+str(self.nClusters)+')', weight='bold', fontsize=fontsize+2, position=(0.5, 1.1),
                     horizontalalignment='center', verticalalignment='center')
        ax.set_ylim((-3,3))
        for ii, (d, color) in enumerate(zip(data, self.clustColors)):
            if ii in self.dropClusters: continue
            ax.plot(theta, d, color=color)
            ax.fill(theta, d, facecolor=color, alpha=0.25)
        ax.set_varlabels(spoke_labels, fontsize=fontsize)
        ax.set_yticks([-2,-1,0,1,2])
        ax.set_yticklabels(ax.get_yticks(), fontsize=fontsize)
        
        # label cluster lines as well as in legend
        for ii, jj in enumerate(self.clusterLabelMap):
            if jj in self.dropClusters: continue
            xy=ax.lines[ii].get_xydata()[ii%len(self.columns)]
            ax.text(xy[0],xy[1],self.clusterLabelMap[jj],ha='center',va='center',fontsize=fontsize)
        
        labels = ['Cluster '+self.clusterLabelMap[ii]+' (N='+str(clusterSizes[ii])+')' for ii in self.clusterLabelMap]
        legend = ax.legend(labels, loc=(0.85, .95), labelspacing=0.1, fontsize=fontsize)
            
        if figFn is None: figFn = paths['graphics']+'clusterRadar'+self.colsToUseId+'_cr'+self.clusterradius+'_k'+str(self.nClusters)+'_'+self.idName+'_urban'*self.urban+self.densityDecileSuffix+'.pdf'
        plt.savefig(figFn)
        self.logger.write('Saved radar chart as %s' % figFn)
        
        return

    def mapClusters(self, bycontinent=False, cmap='Blues', forceUpdate=False):
        """Colorizes the svgs by clusters. 
        Bycontinent produces separate svgs for each continent. If false, does the whole world.
        forceUpdate updates the blank svg
        This is mostly based on mapAggregatedResults"""
        if self.df is None: self.defineClusters()
        self.logger.write('Mapping clusters')
           
        db = psqlt.dbConnection(verbose=True, schema = defaults['osm']['analysisSchema'],curType='default',logger=self.logger)
     
        contList = self.continents if bycontinent else [None] # just a list to loop over
        for cont in contList: 
            if cont is None:
                contName = 'planet'
                isos = str(tuple(cc[0] for cc in self.db.execfetch('SELECT DISTINCT iso FROM %s_iso' % defaults['gadm']['TABLE'])))
            else:
                contName = cont
                isos = np.unique(list(osm_lookups.gadm2continentLookup(0,self.clusterradius,continent=cont).keys()))
                isos = '(\''+isos[0]+'\')' if len(isos)==1 else str(tuple(isos)) 
            whereClauseISO = 'WHERE iso in %s' % isos
            
            # Make blank svg first
            svg = svgt.pg2SVG(paths['scratch']+'blankSVG-'+contName+'-'+str(self.gadmLevel)+'.svg')
            if self.gadmLevel>0: svg.prepareOutline(defaults['gadm']['TABLE']+'_iso',where=whereClauseISO)
            outlines = True if self.gadmLevel>0 else False
            svg.createBlankSVG(defaults['gadm']['TABLE'],self.ids,geomCol='the_geom',groupby=self.ids,where=whereClauseISO,outlines=outlines,simplify=5000,dropSmall=20000**2,forceUpdate=forceUpdate)
            figName = paths['graphics']+'clusterMap_cr'+self.clusterradius+'_k'+str(self.nClusters)+'_'+self.idName+'_'+contName+'_urban'*self.urban+self.densityDecileSuffix

            # get mapping df
            mapDf = self.df.reset_index()
            mapDf['id'] = mapDf.iso
            if self.gadmLevel>0:
                mapDf['id'] += mapDf[self.ids[1:]].astype(int).astype(str).apply(lambda x: '-'+'-'.join(x), axis=1)
            mapDf = mapDf.set_index('id')['cluster'].dropna()
    
            lw = 0.5 if self.gadmLevel==0 else 0  # don't have separate outlines for ISO level
            svg.colorizeSVG(mapDf,figName+'.svg',data2color=linearColormapLookup(cmap,mapDf.values),cbylabel='cluster',lw=lw,outlineLw=0.5,makePNG=True)

    def chartClosestMembers(self,specifyExamples=None, xaxType='ghsl',includeDensity=True, figFn=None,
                            pgf=False):
        """PLOT FOR MAIN PAPER, as well as for analysis
        Now revised to drop trends over time and replace with SNDi
        If figFn is not None, this is assumed to be for the main paper
        Produces plot of the closest member to each cluster, with trends over time

        specifyExamples is an optional dict of the gridcell to use for a specific cluster.
            It can be specified as an integer (the Nth closest gridcell to the centroid) or a (row,col) tuple identifying the gridcell globally
            Warning: the clusters are numbered WITHOUT dropping any of the clusters. Numbering is zero based        
       
        xaxType indicates whether the time series plot (ghsl) or sndi distribution (sndi) is on the additional axes
       
        """
        import matplotlib.pyplot as plt # Needed since plt is designated as a local variable by section below
        if pgf:
            import matplotlib
            from matplotlib.backends.backend_pgf import FigureCanvasPgf
            matplotlib.backend_bases.register_backend('pdf', FigureCanvasPgf)
            import matplotlib.pyplot as plt
            pgf_with_latex = {
                    "text.usetex": True,            # use LaTeX to write all text
                    "pgf.rcfonts": False,           # Ignore Matplotlibrc
                    "pgf.preamble": [
                                r'\usepackage{color}'] + [
                                    r'\definecolor{{color{name}}}{{rgb}}{{ {rgb} }}'.format(name=self.clusterLabelMap[ii], rgb=str(self.clustColors[ii][:3])[1:-1]) for ii in range(len(self.clustColors)) if specifyExamples is None or ii in specifyExamples]
                                }
            matplotlib.rcParams.update(pgf_with_latex)
        
        assert self.byCell  # only works with gridded data. gadm polygons are too large
        assert xaxType in ['ghsl','sndi']
        self.logger.write('Producing chart of closest members and trends')
        if specifyExamples is None: specifyExamples = {}
        closestMembers = self.getClosestMember(forceUpdate=True, n = 12) # Get more than one, to make later syntax work for case when specifyExamples is given as integer. ### if specifyExamples is not None else 2)  # take n=10 in case specifyExamples is not None
 
        nCols = 4 if len(self.clusterLabelMap) in [4,8] else 5
        nRows = int(math.ceil(len(self.clusterLabelMap)*1./nCols))

        if xaxType=='ghsl':  # version of plot that has time series on x axis. 2 rows for each cluster (time series and map) plus one for spacing
            cfreqs_nw, cfreqs = self.clusterFrequencies(by_ghsl=True)
            afreqs_nw, afreqs = self.clusterFrequencies(by_ghsl=False)
            if self.sortColMeans is not None:
                scMin, scMax = self.sortColMeans.min()-0.3, self.sortColMeans.max()+0.1
            fig, axes = plt.subplots(nRows*3-1,nCols, figsize=(2*nCols,2.5*nRows),gridspec_kw = {'hspace':0,'height_ratios':([3,1,1]*nRows)[:-1]})
        else: # plot sndi on x axis
            assert xaxType=='sndi' and nRows==2
            #fig, axes = plt.subplots(nRows,nCols, figsize=(2*nCols,2*nRows))
            #fig, ax = plt.subplots(figsize=(2*nCols,2*nRows))
            fig = plt.figure(figsize=(2*nCols,2*nRows))
            gs = gridspec.GridSpec(nRows*2+1, nCols)
            axes=np.array([[fig.add_subplot(gs[0:2,jj]) for jj in range(nCols)],
                  [fig.add_subplot(gs[3:5,jj]) for jj in range(nCols)]])
            midAx = fig.add_subplot(gs[2,0:nCols])
            kdelims = [-2,12]
            # move midAx up a bit - see https://stackoverflow.com/questions/42357765/adjust-padding-figure-margins-in-gridspec
            pos1 = midAx.get_position()
            pos2 = [pos1.x0 + 0.04, pos1.y0 + 0.05,  pos1.width-0.04, pos1.height-0.06] 
            midAx.set_position(pos2) 
            midAx.set_xlim(kdelims)
            midAx.set_ylim(0.0001,0.5) # slightly above zero to cut off zeros from being plotted
            midAx.set_xticks(list(range(0,12,2)))
            midAx.set_yticklabels([])
            midAx.text(5,-0.25,'SNDi',ha='center')  # label padding not working great, so kludge
            midAx.set_ylabel('Kernel\ndensity', fontsize=8)
            midAx.set_yticks([])
            #midAx.grid(color='0.5', axis='x')
            midAx.spines['right'].set_visible(False)
            midAx.spines['top'].set_visible(False)

        for ii in range(nRows):
            #axes[ii*3+1,0].set_ylabel('frc',fontsize=6)
            rowNum = ii*3 if xaxType=='ghsl' else ii
            for jj in range(nCols):
                if (ii*nCols+jj) < len(self.clusterLabelMap):
                    clusterNum = list(self.clusterLabelMap.keys())[ii*nCols+jj] # implicitly, determines which clusters to skip
                    if clusterNum in specifyExamples:
                        if isinstance(specifyExamples[clusterNum],int):
                            row,col = self.closestMembers[clusterNum][specifyExamples[clusterNum]]
                        else:
                            row, col = specifyExamples[clusterNum]
                        if self.df.loc[(row,col)].cluster!=clusterNum:
                            raise Exception('You specified example (%d,%d) for cluster %s. But that example is in a different cluster.' %(row,col,clusterNum))
                    else:
                        row, col = closestMembers[clusterNum][0]
                    clusterLabel= self.clusterLabelMap[clusterNum]
                    if pgf:
                        clusterLabel = r'{\textcolor{color'+clusterLabel+r'} {\bfseries '+clusterLabel+'}}'
                    if figFn is None:  # regular plot
                        titleStyle='analysis'
                    else: # plot for main paper
                        clusterLabel+=': '+clusterNames[self.clusterLabelMap[clusterNum]]
                        titleStyle='mainFig' if xaxType=='ghsl' or rowNum==0 else 'mainFig_below'  # second row, put title under
                    axes[rowNum,jj] = mapCell(row,col, self.clusterradius, titleStyle=titleStyle, clusterLabel=clusterLabel, ax=axes[rowNum,jj], logger=self.logger)
                    if xaxType == 'ghsl':
                        cfreqs[clusterNum][1975:].plot(ax=axes[rowNum+1,jj],lw=0.75,c='b',label='fraction gridcells')
                        cfreqs_nw[clusterNum][1975:].plot(ax=axes[rowNum+1,jj],lw=0.75,c='r', label='fraction nodes')
                        axes[rowNum+1,jj].axhline(afreqs[clusterNum],lw=0.5,ls='--',c='b', label='_nolegend_')
                        axes[rowNum+1,jj].axhline(afreqs_nw[clusterNum],lw=0.5,ls='--',c='r', label='_nolegend_')
                    
                        # plot PCA or other sortcol value
                        if self.sortColMeans is not None:
                            ax2 = axes[rowNum+1,jj].twinx()
                            ax2.bar(2007, self.sortColMeans[clusterNum]-scMin, 5, scMin, color='b',alpha=0.3)
                            ax2.set_ylim(scMin, scMax-scMin+1.)
                            ax2.set_yticks([])
                            ax2.text(2007, self.sortColMeans[clusterNum]+0.2,'%.2f'%self.sortColMeans[clusterNum],va='bottom',ha='center',fontsize=6)
                    
                        axes[rowNum+1,jj].set_ylim(-0.02,0.45)
                        axes[rowNum+1,jj].set_yticks([0,0.4])
                        axes[rowNum+1,jj].tick_params(axis='y', which='major', pad=1)  # reduce y label padding
                        if ii==nRows-1: axes[rowNum+1,jj].set_xticks([1975,1990,2000,2014])
                        axes[rowNum+1,jj].set_xlabel('')
                        axes[rowNum+1,jj].tick_params(labelsize=6)
                    else:   # sndi on middle axis
                        if self.sortCol is not None:
                            kde = gaussian_kde(self.sortColValues.loc[self.sortColValues.cluster==clusterNum,'pca1'].dropna()).evaluate(np.arange(kdelims[0],kdelims[1],0.1))
                            midAx.plot(np.arange(kdelims[0],kdelims[1],0.1), kde, color=self.clustColors[clusterNum], lw=0.75)
                            # add dot on middle
                            dotX = self.sortColValues.loc[(row,col),self.sortCol]
                            dotY = kde[int((dotX-kdelims[0])*10)]
                            midAx.plot(dotX, dotY, 'o', color=self.clustColors[clusterNum])    
                            
                            # labels
                            labOffsets  = {0:(0.14,0.05), 1:(0.14,0.05), 2:(-0.3,0.1), 4:(-0.2,0.14), 5:(0.12, 0.1), 8:(0.12,0.1)}
                            labPos = (dotX+labOffsets[clusterNum][0], dotY+labOffsets[clusterNum][1]) if clusterNum in labOffsets else (dotX-0.2, dotY+0.12)
                            midAx.annotate(self.clusterLabelMap[clusterNum], labPos, color = self.clustColors[clusterNum])
                            # connecting line
                            #ypos = [dotY, 4] if ii==0 else [-4,dotY]
                            #if clusterNum in [1,2]: # need diagonal line
                            #    x1 = -0.5 if clusterNum==1 else 3.5
                            #    midAx.plot([x1, dotX], ypos, lw=2, color=self.clustColors[clusterNum], zorder=3)
                            #else:
                            #    midAx.vlines(dotX, ypos[0], ypos[1], lw=2, color=self.clustColors[clusterNum], zorder=3)
                        
                else: # no clusters left, just suppress axes
                    axes[rowNum,jj].axis('off')
                    if xaxType=='ghsl': axes[rowNum+1,jj].axis('off')
                if xaxType=='ghsl' and ii<nRows-1: axes[ii*3+2,jj].axis('off')  # padding between plots
        if xaxType=='ghsl': axes[1,0].legend(loc=2,frameon=False,fontsize=6)
        
        #fig.tight_layout()
        if figFn is None: figFn = paths['graphics']+'clusterExamples_bycell'+self.colsToUseId+'_cr'+self.clusterradius+'_k'+str(self.nClusters)+'_urban'*self.urban+self.densityDecileSuffix+'.pdf'
        fig.savefig(figFn)            
        self.logger.write('Saved closest member chart as %s' % figFn)
                    
    def chartClosestMembers_multi(self, n=60, ctypes=None, figFn=None):
        """Same as chartClosestMembers, but does a page of n members for each cluster
        If ctypes is specified (as a list), it only does that type
        For example, to get the list of Type E examples, run:
          self.chartClosestMembers_multi(n=60, ctypes=[4], figFn=paths['scratch']+'clusterE_examples'
        """
        assert self.byCell  # only works with gridded data. gadm polygons are too large
        assert ctypes is None or isinstance(ctypes, list)
        self.logger.write('Producing chart of %d closest members' % n)
        # make sure that the closest members are calculated and accessible recursively
        # force update=True is because we previously ran this with n=1
        _ = self.getClosestMember(n=n,forceUpdate=True)
        
        pagesPerCluster = int(math.ceil(n/12.)) # 12 images max per page
        assert  pagesPerCluster <=26
        pageIds = ['abcdefghijklmnopqrstuvwxyz'[ii] for ii in range(pagesPerCluster)]

        if figFn is None:
            figStemName = paths['graphics']+'clusterMultiExamples_bycell'+self.colsToUseId+'_cr'+self.clusterradius+'_k'+str(self.nClusters)+'_urban'*self.urban+self.densityDecileSuffix
            titleStyle = 'analysis'
        else:
            figStemName=figFn
            titleStyle = 'SI'  # this is the only time we specific a filename
        outFigList = [figStemName+'_'+str(clusterNum)+'_'+pageId+'.pdf' for clusterNum in range(self.nClusters) for pageId in pageIds]

        plt.rcParams['text.usetex'] = False # tex causes problems in parallel

        def _mapOnePage(cmap,clusterNum,outFn):  # allows for parallelization
            n = len(cmap)
            nCols = 3
            nRows = int(math.ceil(n*1./nCols))
            fig, axes = plt.subplots(nRows,nCols,figsize=(7,8.75))
            for ii in range(nRows):
                for jj in range(nCols):
                    exampleNum = ii*nCols+jj
                    if exampleNum<n: 
                        clusterLabel = self.clusterLabelMap[clusterNum] if clusterNum in self.clusterLabelMap else str(clusterNum)
                        axes[ii,jj] = mapCell(cmap[exampleNum][0], cmap[exampleNum][1], self.clusterradius, titleStyle=titleStyle, clusterLabel=clusterLabel, ax=axes[ii,jj], usetex=False, logger=None)
                    else: # no clusters left, just suppress axes
                        axes[ii,jj].axis('off')
            attempts=0
            while attempts<10:
                try: # I/O problem. See #256
                    fig.savefig(outFn)
                    plt.close(fig)
                    break
                except Exception as e: 
                    attempts+=1
                    print(('Failed to save %s on attempt %d' % (outFn, attempts)))
                    print(e)
                    time.sleep(5)
            else: # only if we reach max attempts
                raise Exception('Could not save %s' % outFn)
            return

        funcs, names = [], []
        for pageIdN, pageId in enumerate(pageIds):
            startC = pageIdN * 12
            endC = min(n, pageIdN * 12 + 12)
            for clusterNum in range(self.nClusters):
                if ctypes is not None and clusterNum not in ctypes: 
                    print(('Skipping type {}'.format(clusterNum))) 
                    continue
                funcs+=[[_mapOnePage,[self.closestMembers[clusterNum][startC:endC],clusterNum,figStemName+'_'+str(clusterNum)+'_'+pageId+'.pdf']]]
                names+=['_mapOnePage_'+str(clusterNum)+'_'+pageId]
        # temporary running in serial to avoid latex problem. See issue #454
        runFunctionsInParallel(funcs,names=names,maxAtOnce=None, parallel=defaults['server']['parallel'])
        
        mergePDFs(outFigList, figStemName+'.pdf')  
        self.logger.write('Saved closest member multi chart as %s' % figStemName+'.pdf')

    def listClosestMembers_multi(self, n=60,  outFn=None):
        """Same as chartClosestMembers_multi, but this is intended simply to export a list of the lat/lon etc for each cluster, useful for the "flyto" feature in our public web site.
        201912: Also includes ISO and country name. But this writes to the same file as chartClosest...? but with different output. That's not good, but well documented.

        Returns and/or saves a dataframe listing the cells.

        See above for syntax. e.g.
          self.listClosestMembers_multi(n=60, outFn=paths['scratch']+'clusterE_examples'
                """
        assert self.byCell  # only works with gridded data. gadm polygons are too large
        self.logger.write('Producing list of %d closest members' % n)
        # make sure that the closest members are calculated and accessible recursively
        _ = self.getClosestMember(n=n,forceUpdate=True)

        #kmDefaultDrop = [6,9] # clusters to drop
        #clusterNames = {'H':'Disconnected','G':'Dendritic','F':'Dead ends','E':'Circuitous','D':'Broken grid',
        #       'C':'Irregular grid','B':'Degree-3','A':'Grid','I':'Cluster I','J':'Cluster J'}

        clusterLabelMap = OrderedDict([(jj,clusterNames[string.ascii_uppercase[ii]]) for ii, jj in enumerate([kk for kk in range(defaultK) if kk not in kmDefaultDrop])])
        dfClusters = pd.DataFrame(list(clusterLabelMap.items()), columns=['cnum','Name'])
        
        if outFn is None:
            outFn = paths['graphics']+'clusterMultiExamples_bycell'+self.colsToUseId+'_cr'+self.clusterradius+'_k'+str(self.nClusters)+'_urban'*self.urban+self.densityDecileSuffix+'.pandas'

        dfs = []
        for clusterNum,clusterName in list(clusterLabelMap.items()):
            x=self.closestMembers[clusterNum]
            df = pd.DataFrame(x, columns=['row','col'])
            df['Cluster'] = clusterName
            df['proximityOrder'] = df.index+1
            dfs += [df]
        df=  rasterTools.latlon_from_rowcol(pd.concat(dfs), loc='center', prefix='')
        cc = rasterTools.get_country_from_row_col(df.row, df.col, self.clusterradius)

        df['ISO'] = [a for a,b in cc]
        df['Country'] = [b for a,b in cc]
        df.to_pickle(outFn)
        print(('Wrote '+outFn))
        
    def chartClustersbyWBR(self,figFn=None, mainFig=False):
        """Chart for Fig 4 (TS paper) of how clusters change over time.
        Also does more extensive versions for SI
        """
        params = { 'text.usetex': True,
                   'text.latex.unicode': True,
                   'text.latex.preamble':r'\usepackage{amsmath} \usepackage{amssymb} \usepackage{hyperref} ',}
        plt.rcParams.update(params)

        if figFn is None:
            figFn = paths['graphics']+'clusterFreqs_wbr'+self.colsToUseId+'_cr'+self.clusterradius+'_k'+str(self.nClusters)+'_urban'*self.urban+self.densityDecileSuffix
        wbrNodes, wbrCells = self.clusterFrequencies(True, True)
        wbrNodes.index.names=['cluster','ghslyr']  # to avoid latex problems in printing ghsl_yr
        wbrCells.index.names=['cluster','ghslyr']  
        from region_definitions import  continentsWBR, developmentWBR
        
        continentsWBR.update(developmentWBR)
        wbrsToPlot = continentsWBR.copy()
        if 'ARB' in wbrsToPlot: del wbrsToPlot['ARB'] # skip ARB and make room for legend
        
        # special treatment for main paper fig
        if mainFig:
            # clean up names
            wbrsToPlot.update({'WLD':'World','LCN':'Latin America','MEA':'M. East \\& N. Africa','SAS':'South Asia'})
            wbrsToPlot = OrderedDict((k, wbrsToPlot[k]) for k in wbrsToPlot.fromkeys(['WLD','AFR','EUU','EAS','SAS','NAC','LCN','MEA'])) # reorder
            nRows,fs, figsize = 3, 8, (osmt.figSizes['1.5col'][0], osmt.figSizes['1.5col'][1]*1.8)
        else:
            nRows,fs, figsize = 4, 12, osmt.figSizes['portrait']
            
        assert len(wbrsToPlot)<=11 # otherwise, we need to make a bigger figure below
        xtickLabs = [r'$<$1975','1975-89','1990-99','2000-14','','all']
        xidxs = [cc for cc in wbrNodes.index.get_level_values(1).unique() if cc!=-1]
        barColors = [self.clustColors[ii] for ii in list(self.clusterLabelMap.keys())] # this is robust to dropping some clusters


        for df,colName,figSuffix in [(wbrNodes,'frc_nodes','-nodes'),(wbrCells,'frc_cells','-cells')]:
            if mainFig and colName=='frc_cells': continue # skip this for main paper fig
            fig, axes = plt.subplots(nRows,3,figsize=figsize)
            if mainFig: 
                plt.subplots_adjust(hspace=0.35,left = 0.13,bottom=0.12, top=0.95)
            else:
                plt.subplots_adjust(hspace=0.25,left = 0.1,top=0.95)
                
            for ii, wbr in enumerate(wbrsToPlot):
                row = int(ii/3.) if ii<2 else int((ii+1)/3.) # save room for legend in its own plot
                col = ii%3  if ii<2 else (ii+1)%3


                plotDf = df[df.wbr==wbr].reset_index(level=0).pivot(columns='cluster',values=colName).loc[xidxs]
                plotDf = plotDf[list(self.clusterLabelMap.keys())]  # this drops clusters that are to be dropped
                plotDf.columns = [self.clusterLabelMap[cc] for cc in plotDf.columns] # rename clusters to letters
                plotDf.loc[2999] = -1000  # dummy column to space things out
    
                plotDf.sort_index().plot.bar(stacked=True, width = .8, ax=axes[row,col],color=barColors)
            
                axes[row,col].set_title(wbrsToPlot[wbr],fontsize=fs)
                axes[row,col].set_ylim([0,1])
                axes[row,col].set_yticks([0,0.5,1])
                axes[row,col].legend().set_visible(False)
                axes[row,col].xaxis.set_tick_params(width=0, labelsize=fs-1)
                if col==0:
                    axes[row,col].yaxis.set_tick_params(labelsize=fs-1)
                    if colName=='frc_nodes':
                        axes[row,col].set_ylabel('fraction nodes',fontsize=fs)
                    else:
                        axes[row,col].set_ylabel('fraction grid cells',fontsize=fs)
                else:
                    axes[row,col].set_yticklabels([])
                
                if row==nRows-1:
                    axes[row,col].set_xticklabels(xtickLabs)
                else:
                    axes[row,col].set_xticklabels([])
                axes[row,col].set_xlabel('')
                
            # add legend in (0,2)
            plotDf = df[df.wbr=='WLD'].reset_index(level=0).pivot(columns='cluster',values=colName).loc[xidxs] 
            plotDf = plotDf[list(self.clusterLabelMap.keys())]  # this drops clusters that are to be dropped
            # reverse order so legend lines up with order of bars
            plotDf.columns = reversed([self.clusterLabelMap[cc]+': '+clusterNames[self.clusterLabelMap[cc]] for cc in plotDf.columns]) # rename clusters to letters
            plotDf.plot.bar(stacked=True, ax=axes[0,2],color=reversed(barColors))
            if mainFig:
                axes[0,2].legend(loc=9, ncol=1,fontsize=fs-1,bbox_to_anchor=(0.6, 1.2), frameon=False)
            else:         
                axes[0,2].legend(loc=9, ncol=1,fontsize=fs,bbox_to_anchor=(0.6, 1.15), frameon=False)
            axes[0,2].set_ylim([-10,-9])  # hide the actual lines
            axes[0,2].axis('off')

            fig.savefig(figFn+figSuffix+'.pdf')
            print(('Saved '+figFn+figSuffix+'.pdf'))
    
    def chartClustersbyCountryorCity(self,n=None,by='country',figFn=None):
        """Chart for SI of cross-sectional distribution of clusters by country or Atlas City
        n can be an integer or a range, or None to do the first 200 cities"""
        
        assert by in ['country','city','both']
        if figFn is None: figFn = paths['graphics']+'figSI-kmeans-clusters-by'+by+'.pdf'
        
        if n is None: # do all through a recursive call
            assert by=='city' # we want to do all cities, but not implemented for all countries
            self.chartClustersbyCountryorCity(n=[0,100],  by='city', figFn=figFn.replace('.pdf','-p1.pdf'))
            self.chartClustersbyCountryorCity(n=[100,200],by='city', figFn=figFn.replace('.pdf','-p2.pdf'))
            return
        nRange = [0,n] if isinstance(n,int) else n

        assert isinstance(nRange, list)
        
        def _getClusterDf(nRange, by):
            if by=='country':
                df = pd.DataFrame(self.df.cluster).join(self.iso_df)
            else:
                cityDf = self.db.db2df('city_cell_lookup_'+self.clusterradius, schema='cities').set_index(['row','col'])
                df =  pd.DataFrame(cityDf).join(self.df.cluster, how='inner') # do it this way round to drop cells not in a city
            
            # drop clusters 
            df = df[np.logical_not(df.cluster.isin(self.dropClusters))].rename(columns={'iso':'country'})
            # calculate fractions of each cluster by country or city    
            cdf = df.groupby([by,'cluster']).size()/df.groupby([by]).size()
        
            cdf = cdf.reset_index().pivot(index=by,columns='cluster')
            cdf.columns= cdf.columns.droplevel()
            #cdf.drop([cc for cc in cdf.columns if cc not in self.clusterLabelMap], axis=1, inplace=True)
            cdf.rename(columns=self.clusterLabelMap, inplace=True)
            cdf.fillna(0,inplace=True)
        
            # restrict to top n countries or cities
            nLower = None if nRange[0]==0 else -1*nRange[0]
            if by=='country':
                popDf = osmt.loadOtherCountryData().sort_values('pop2012',ascending=True).dropna(subset=['pop2012']).set_index('ISOalpha3')
                clookup = osmt.country2ISOLookup()['ISOalpha2shortName']
                cdf = cdf.reindex(popDf.index[-1*nRange[1]:nLower]).reset_index()
                cdf['cname'] = cdf.ISOalpha3.apply(lambda x: clookup[x])
                cdf = cdf.drop('ISOalpha3',axis=1).set_index('cname')
            else:
                cityDf = self.db.db2df('cities_disconnectivity_'+self.clusterradius, schema='cities')[['city','iyear','stockorchange','length_m']]
                cityDf = cityDf.loc[(cityDf.stockorchange=='cityedges') & (cityDf.iyear==3),['city','length_m']].set_index('city').sort_values('length_m', ascending=True)
                if n<100: # take top 10 but exclude ones in same country
                    cityDf['iso'] = osm_cities.city_atlas().atlascities.ISO
                    cityDf.drop_duplicates(subset='iso',keep='last',inplace=True) # keep last one, which is largest
                cdf = cdf.reindex(cityDf.index[-1*nRange[1]:nLower]).reset_index()
                clookup = dict(osm_cities.city_atlas().atlascities.City)
                cdf['cname'] = cdf.city.apply(lambda x: clookup[x])
                cdf = cdf.drop('city',axis=1).set_index('cname')
            return cdf
        
        print('Getting cluster data frames')
        if by=='both':
            cdf = pd.concat([_getClusterDf(nRange=nRange,by=byItem) for byItem in ['city','country']])
        else:
            cdf = _getClusterDf(nRange=nRange,by=by) # put this as a function in case we want both

        barColors = [self.clustColors[ii] for ii in range(self.nClusters) if ii not in self.dropClusters] # this is robust to dropping some clusters

        n = nRange[1]-nRange[0]
        if n>=100: # plot 2 columns full page
            fig = plt.figure(figsize = osmt.figSizes['portrait'])
            gs = gridspec.GridSpec(16, 2, hspace=0) # have a fake subplot for legend
            axes=np.array([fig.add_subplot(gs[0:-2,jj]) for jj in range(2)])
            cdf.iloc[:int(len(cdf)/2)].plot(kind='barh', stacked=True, color=barColors, ax=axes[1], legend=None)
            cdf.iloc[int(len(cdf)/2):].plot(kind='barh', stacked=True, color=barColors, ax=axes[0], legend=None)
        elif by=='both':
            fig = plt.figure(figsize = osmt.figSizes['2.5col'])
            gs = gridspec.GridSpec(5, 2, hspace=0) # have a fake subplot for legend
            axes=np.array([fig.add_subplot(gs[0:-2,jj]) for jj in range(2)])
            cdf.iloc[:int(len(cdf)/2)].plot(kind='barh', stacked=True, color=barColors, ax=axes[1], legend=None)
            cdf.iloc[int(len(cdf)/2):].plot(kind='barh', stacked=True, color=barColors, ax=axes[0], legend=None)
        else:
            raise Exception('small plot only implemented for both country and city (not one)')
            
        for ii, ax in enumerate(axes): # hbar does bottom to top, so our df is in reverse order
            ax.set_xlim(0,1)
            xlabSuff = ['country','city'][ii] if by=='both' else by
            ax.set_xlabel('Grid cell fraction by '+xlabSuff)
            ax.set_ylabel('')
            ax.yaxis.set_tick_params(labelsize=7)
            if by=='both': ax.xaxis.set_tick_params(labelsize=7)

        legAx = fig.add_subplot(gs[-1,:])
        fakeDf = pd.DataFrame([1./8]*8).T
        barWidth = 2 if n>=100 else 1.25
        fakeDf.plot(kind='barh', stacked=True, color=barColors, ax=legAx, width=barWidth, legend=None)
        for ii, label in enumerate(self.clusterLabelMap.values()):
            legAx.text(ii/8.+1./16,0,label,fontweight='bold', va='center', ha='center')
        legAx.set_xticks([])
        legAx.set_yticks([])
        #legAx.set_xticklabels(self.clusterLabelMap.values())
        legAx.set_xlabel('Empirical street-network archetype',fontweight='bold')
        if n>=100: legAx.xaxis.set_label_position('top')
        if n<100:  legAx.set_ylim(-0.75,1.75) # kludge for spacing
        legAx.patch.set_alpha(0) # transparent background
        #legAx.axis('off')
        for spine in legAx.spines: legAx.spines[spine].set_visible(False)
        legAx.yaxis.set_visible('off')
        #legAx.text(0.5, 1, 'Cluster',ha='center')
        
        fig.tight_layout()
        fig.savefig(figFn)
        self.logger.write('Saved country cluster distribution as %s' % figFn)

    def chartTypeE(self, n=96):
        """development. Do pages of type E examples, i.e. gated communities"""
    
        typeE_egs = self.getClosestMember(forceUpdate=True, n = n)[4] # 4 is cluster E

    def analyzeCityTrends(self):
        """For text, look at a few cities and see where trends are going
        these are notes only"""
        cityDf = self.db.db2df('city_cell_lookup_'+self.clusterradius, schema='cities').set_index(['row','col'])
        df =  pd.DataFrame(cityDf).join(self.df.cluster, how='inner') # do it this way round to drop cells not in a city

        ghslDf = self.db.execfetch('SELECT c.row, c.col, ghsl_yr FROM cities.city_cell_lookup_{} c, aggregated_by_cell_{} a WHERE c.row=a.row AND c.col=a.col;'.format(self.clusterradius, self.clusterradius))
        ghslDf = pd.DataFrame([list(rr) for rr in ghslDf], columns = ['row','col','ghsl_yr']).set_index(['row','col'])
        df = df.join(ghslDf)

        clusterTrends = df.groupby(['cluster','city','ghsl_yr']).size()
        clusterTrends.name = 'n_cells'

        frcTypeE = clusterTrends.loc[4] / clusterTrends.groupby(level=[1,2]).sum()
        print((frcTypeE.loc['Bangkok']))

    def clusterFrequencies(self, by_ghsl=True, by_wbr=False):
        """Returns dfs with frequencies of clusters (optionally by ghsl_yr)
        First df is node weighted, second is the cell frequencies"""
        if by_ghsl: assert self.ghsl_yr_df is not None
 
        if by_ghsl and not by_wbr:
            grouper = (pd.DataFrame(self.df.cluster).join(self.ghsl_yr_df).join(self.n_nodes)).groupby(['cluster','ghsl_yr'])
            nw = grouper.n_nodes.sum() / (grouper.n_nodes.sum().groupby('ghsl_yr').sum())
            cw = grouper.size() / (grouper.size().groupby('ghsl_yr').sum())
        elif by_ghsl and by_wbr:
            # trick here is that WBRs are not unique
            wbrs = osmt.country2WBregionLookup()
            nw, cw = [], []
            
            joinDf = pd.DataFrame(self.df.cluster).join(self.ghsl_yr_df).join(self.n_nodes).join(self.iso_df)
            nnodes = joinDf.groupby(['cluster','ghsl_yr','iso']).n_nodes.sum()
            ncells = joinDf.groupby(['cluster','ghsl_yr','iso']).size()

            for wbr in wbrs.index.unique():
                tmpDf  = pd.DataFrame(nnodes.loc[:,:,wbrs.ISOalpha3[wbr]].groupby(level=[0,1]).sum()/(nnodes.loc[:,:,wbrs.ISOalpha3[wbr]].groupby(level=1).sum()))
                tmpDf2 = pd.DataFrame(nnodes.loc[:,:,wbrs.ISOalpha3[wbr]].groupby(level=[0]).sum()/(nnodes.loc[:,:,wbrs.ISOalpha3[wbr]].sum()))
                tmpDf2['ghsl_yr'] = 'ALL'
                tmpDf = pd.concat([tmpDf,tmpDf2.set_index('ghsl_yr',append=True)])
                tmpDf['wbr'] = wbr
                nw.append(tmpDf.copy())
                tmpDf  = pd.DataFrame(ncells.loc[:,:,wbrs.ISOalpha3[wbr]].groupby(level=[0,1]).sum()/(ncells.loc[:,:,wbrs.ISOalpha3[wbr]].groupby(level=1).sum()))
                tmpDf2 = pd.DataFrame(ncells.loc[:,:,wbrs.ISOalpha3[wbr]].groupby(level=[0]).sum()/(ncells.loc[:,:,wbrs.ISOalpha3[wbr]].sum()))
                tmpDf2['ghsl_yr'] = 'ALL'
                tmpDf = pd.concat([tmpDf,tmpDf2.set_index('ghsl_yr',append=True)])
                tmpDf['wbr'] = wbr
                cw.append(tmpDf.copy())
            nw = pd.concat(nw)
            cw = pd.concat(cw)
            nw.columns=['frc_nodes','wbr']
            cw.columns=['frc_cells','wbr']
        elif not by_ghsl and by_wbr:
            raise Exception('not implemented') 
        else:
            grouper = (pd.DataFrame(self.df.cluster).join(self.n_nodes)).groupby(['cluster'])
            nw =  grouper.n_nodes.sum() / (grouper.n_nodes.sum()).sum()
            cw = grouper.size() / (grouper.size()).sum()
        return nw, cw
        
    def clusters2postgres(self):
        """Writes a table of cluster ids and centroid distances to postgres
        This allows a faster load for plotting purposes"""
        assert self.byCell
        self.logger.write('Writing table of cluster ids to postgres')
        tablename = 'kmeans_k'+str(self.nClusters)+'_'+self.clusterradius+'_urban'*self.urban

        # write gridcell clusters to table, and also distance to cluster centroid
        self.db.df2db(self.df[['cluster','dist2centroid']],tablename, if_exists='replace', index=True, schema=self.schema)
                
    def postgres2clusters(self):
        """Reads table of cluster ids and centroid distances"""
        assert self.byCell
        self.logger.write('Reading table of cluster ids from postgres')
        tablename = 'kmeans_k'+str(self.nClusters)+'_'+self.clusterradius+'_urban'*self.urban
        
        self.df = self.db.db2df(tablename,schema=self.schema).set_index(['row','col'])
        
        # get supporting columns - n_nodes, iso and ghsl year
        colsToUse = ['iso','ghsl_yr','n_nodes']+[self.sortCol]*(self.sortCol is not None)
        dfAll = pd.read_sql_table('disconnectivity_by_cell_'+self.clusterradius,self.engine, schema=self.schema, index_col=self.idColumns, columns=colsToUse)
        dfAll = dfAll.reindex(self.df.index)
        self.iso_df = dfAll.iso
        self.ghsl_yr_df = dfAll.ghsl_yr
        self.n_nodes = dfAll.n_nodes
        if self.sortCol is not None: 
            self.sortColValues = self.df[['cluster']].join(dfAll[[self.sortCol]])
            self.sortColMeans = self.sortColValues.groupby('cluster')[self.sortCol].mean()

    def reportCell(self,row=None, col=None):
        """Reports which cluster a cell is in, and its normalized distance and percentile from the cluster centroid
        This is for development, i.e. to see where our previous cluster exemplars end up"""
        
        if row is None or col is None:
            # call recursively with PNAS v1 definitions
            for row, col in [(4214, 21418), #UK   H
                             (5751, 13055), #USA  G
                             (5866, 25096), # Turkey F
                             (7033, 25809), # Palestine E
                             (9231, 31225), # India D
                             (14194, 25093), # South AFrica C
                             (4406, 25729), # Russia B
                             (14804, 14816)]:
                self.reportCell(row, col)
            return
            
        clust = self.df.loc[(row, col),'cluster']
        clustLabel = self.clusterLabelMap[clust]
        dist  = self.df.loc[(row, col),'dist2centroid']
        pctile = 100 - percentileofscore(self.df.loc[self.df.cluster==clust,'dist2centroid'],dist,'mean')
        
        self.logger.write('Cell {row}, {col} assigned to Cluster {clustLabel}.'.format(row=row, col=col, clustLabel=clustLabel))
        self.logger.write('Normalized distance from centroid: {dist:.2f}. {pctile:.1f}th percentile of cells in that cluster (higher is closer)'.format(dist=dist, pctile=pctile))
        
        
        
def radar_factory(num_vars, frame='circle'):
    """Create a radar chart with `num_vars` axes.
    from http://matplotlib.org/examples/api/radar_chart.html

    This function creates a RadarAxes projection and registers it.

    Parameters
    ----------
    num_vars : int
        Number of variables for radar chart.
    frame : {'circle' | 'polygon'}
        Shape of frame surrounding axes.

    """
    # calculate evenly-spaced axis angles
    theta = np.linspace(0, 2*np.pi, num_vars, endpoint=False)
    # rotate theta such that the first axis is at the top
    theta += np.pi/2

    def draw_poly_patch(self):
        verts = unit_poly_verts(theta)
        return plt.Polygon(verts, closed=True, edgecolor='k')

    def draw_circle_patch(self):
        # unit circle centered on (0.5, 0.5)
        return plt.Circle((0.5, 0.5), 0.5)

    patch_dict = {'polygon': draw_poly_patch, 'circle': draw_circle_patch}
    if frame not in patch_dict:
        raise ValueError('unknown value for `frame`: %s' % frame)

    class RadarAxes(PolarAxes):

        name = 'radar'
        # use 1 line segment to connect specified points
        RESOLUTION = 1
        # define draw_frame method
        draw_patch = patch_dict[frame]

        def fill(self, *args, **kwargs):
            """Override fill so that line is closed by default"""
            closed = kwargs.pop('closed', True)
            return super(RadarAxes, self).fill(closed=closed, *args, **kwargs)

        def plot(self, *args, **kwargs):
            """Override plot so that line is closed by default"""
            lines = super(RadarAxes, self).plot(*args, **kwargs)
            for line in lines:
                self._close_line(line)

        def _close_line(self, line):
            x, y = line.get_data()
            # FIXME: markers at x[0], y[0] get doubled-up
            if x[0] != x[-1]:
                x = np.concatenate((x, [x[0]]))
                y = np.concatenate((y, [y[0]]))
                line.set_data(x, y)

        def set_varlabels(self, labels, fontsize=None):
            if fontsize is None:
                self.set_thetagrids(np.degrees(theta), labels)
            else:
                self.set_thetagrids(np.degrees(theta), labels, fontsize=fontsize)
                
        def _gen_axes_patch(self):
            return self.draw_patch()

        def _gen_axes_spines(self):
            if frame == 'circle':
                return PolarAxes._gen_axes_spines(self)
            # The following is a hack to get the spines (i.e. the axes frame)
            # to draw correctly for a polygon frame.

            # spine_type must be 'left', 'right', 'top', 'bottom', or `circle`.
            spine_type = 'circle'
            verts = unit_poly_verts(theta)
            # close off polygon by repeating first vertex
            verts.append(verts[0])
            path = Path(verts)

            spine = Spine(self, spine_type, path)
            spine.set_transform(self.transAxes)
            return {'polar': spine}

    register_projection(RadarAxes)
    return theta

def unit_poly_verts(theta):
    """Return vertices of polygon for subplot axes.

    This polygon is circumscribed by a unit circle centered at (0.5, 0.5)
    """
    x0, y0, r = [0.5] * 3
    verts = [(r*np.cos(t) + x0, r*np.sin(t) + y0) for t in theta]
    return verts
       
def paradigms2clusters():
    """Uses saved centroids to assign our 3 paradigms to clusters"""
    cr = defaults['osm']['clusterRadii'][0]
    logger=osmt.osm_logger('paradigms2clusters')
    clusterLabelMap = OrderedDict([(jj,clusterNames[string.ascii_uppercase[ii]]) for ii, jj in enumerate([kk for kk in range(defaultK) if kk not in kmDefaultDrop])])

    centroids = pd.read_csv(paths['output']+'kmeans_centroids.tsv',sep='\t',index_col=0)
    #paradigmDb = defaults['server']['postgres_db']+'continents'  # this is now the 'normal' db
    db = psqlt.dbConnection(logger=logger )
    closestClusters = {}
    for paradigm in ['grid','medieval','hell']:
        df = db.db2df('disconnectivity_iso_'+paradigm+'_'+cr+'_urban')[centroids.columns]
        normArray = np.array((df-centroids.loc['mean'])/centroids.loc['stddev']).flatten()
        dists = np.linalg.norm(centroids.drop(['mean','stddev'])-normArray,axis=1,keepdims=True).flatten()
        logger.write('Distance from paradigm %s to centroids: ' % paradigm)
        logger.write(', '.join([clusterLabelMap[ii]+': '+str('%.2f' % dd) for ii, dd in enumerate(dists) if ii not in kmDefaultDrop]))
        logger.write('Closest cluster: ' + clusterLabelMap[dists.flatten().argmin()])
        logger.write('\n')
        
    

                
                
def runall(colsToUse=kmDefaultCols, doGadm=True):
    if sorted(defaults['osm']['continents']) not in [['fakeplanet'],['planet'], ['africa','asia','australiaoceania','centralamerica','europe','northamerica','southamerica']]:
        print ('Skipping cluster analysis. This is appropriate for planet only.')
        return

    for cr in defaults['osm']['clusterRadii']:
        for k in [10, 9, 8]: # [5,
            km = kmeans_analyzer(nClusters=k, columns=[cc for cc in colsToUse], byCell=True,clusterradius=cr,sortCol='pca1')
            km.defineClusters(sortOrder='asc')
           
            if k==defaultK:
                km.clusters2postgres()
            
            km.reportCell()  # devel
            km.radarChart()
            km.chartClosestMembers()
            km.chartClosestMembers_multi()
            km.chartClustersbyWBR()
            if doGadm:
                for gadmLevel in range(len(defaults['gadm']['ID_LEVELS'])):
                    km = kmeans_analyzer(nClusters=k,gadmLevel=gadmLevel, columns=colsToUse,clusterradius=cr,sortCol='pca1')
                    try:
                        km.defineClusters(sortOrder='desc')
                    except Exception as e:
                        if 'Analysis area is too small' in e.message:
                            km.logger.write(e.message)
                            km.logger.write('Quitting cluster analysis.')
                            return
                        else:
                            raise e
                    km.radarChart()
                    if gadmLevel<4: km.mapClusters()


                    
if __name__ == '__main__':
    import inquirer
    runmode = inquirer.prompt([inquirer.List('A', message='Run which mode?', choices=[
        'run all',
        'export_centroids',
        'Figures for default case for PNAS', # This should all be removed, most likely; this is done in clean_figures
        'list_examples_for_website',
        'dev',
            ])])['A']
    if runmode in ['dev']:
        pass
    if runmode in ['list_examples_for_website']:
        km = kmeans_analyzer(nClusters= defaultK,
                             columns= kmDefaultCols,
                             byCell=True,
                             clusterradius= defaults['osm']['clusterRadius'], 
                             sortCol='pca1')
        km.postgres2clusters()
        km.listClosestMembers_multi()
        
    if runmode in ['export_centroids']:
        km = kmeans_analyzer(nClusters= 10,
                             columns= kmDefaultCols,
                             byCell=True,
                             clusterradius=  defaults['osm']['clusterRadius'], 
                             sortCol='pca1')
        km.defineClusters(sortOrder='asc', saveToDisk=True)

    if runmode in ['run all']:
        runall()
    elif runmode in ['Figures for default case for PNAS',]:
        km = kmeans_analyzer(nClusters= 10,
                             columns= kmDefaultCols,
                             byCell=True,
                             clusterradius= defaults['osm']['clusterRadius'], 
                             sortCol='pca1')
        #km.defineClusters(sortOrder='asc')
        km.postgres2clusters()
        km.chartClosestMembers(figFn=paths['graphics']+'figXS-kmeans-examples.pdf')
        thatisenough
        km.chartClustersbyWBR()
        km.radarChart()
        km.chartClosestMembers_multi()
        
        

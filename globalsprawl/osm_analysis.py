#!/usr/bin/python
# coding=utf-8
"""
Analysis of final dataset
It produces many of the tables and figures for PLOS ONE 2019 and PNAS 2020
Many functions are for exploratory analysis, and are not fully documented,
but are included here for completeness and future work

Usage:
  osm_analysis dev    [options]
  osm_analysis test    [options]
  osm_analysis export   [options]
  osm_analysis copy_to_local   [options]
  osm_analysis stata   [options]
  osm_analysis cities  [options]
  osm_analysis countries  [options]
  osm_analysis scatterplots  [options]
  osm_analysis plot_trends_by_region  [options]
  osm_analysis plot_trends_by_country [options]
  osm_analysis TSscatterplots [options]
  osm_analysis xs_correlations [options]
  osm_analysis manuscript  [options]
  osm_analysis tssi [options]
  osm_analysis xssi [options]
  osm_analysis TSfig2 [options]
  osm_analysis TSfig2variants [--tablesuffix=<tablesuffix>] [options]
  osm_analysis TSfig2-other-metrics [options]
  osm_analysis TSmaterial  [options]
  osm_analysis XSmaterial  [options]
  osm_analysis mergepdfs  [options]
  osm_analysis citymaps [options]
  osm_analysis outlier-cities  [options]
  osm_analysis cleanfigures [options]
  osm_analysis methods_figure [options]
  osm_analysis keystats [options]
  osm_analysis pca-sensitivity  [options]
  osm_analysis robustness-allways-cell [options]
  osm_analysis robustness-allways-id5 [options]
  osm_analysis Bangkok_clusters [options]
  osm_analysis tiled_unit_graph_paradigms
  osm_analysis latexmk  [options]
  osm_analysis gen_wide_saved_file [options]
  osm_analysis html     [options]
  osm_analysis deploy
  osm_analysis debug-ghslstock [options]
  osm_analysis   [options]
  osm_analysis -h | --help

Options:
  -h --help      Show this screen.
  --continent=<continent>   Process only a single continent
  --cluster-radius=<radius>   Specify a single cluster radius to override config file setting
  --db=<database>      Specify database to use (e.g. osm, osmdev, etc)
  -f --force-update    Overwrite/recalculate database tables and outputs
  -s --serial       Turn off parallel computation; useful for debugging 
  --system-parallel     Run each continent as a separate system process, to avoid Pythin GIL. Caution: running 3 smallest real continents at once is (nearly) too much.
  --skip-conflict-checks  Include this flag to override default check that you're not interfering with another user's changes to the database.
  -l --local     Use local Pandas files rather than the database or a tunnel to the database.
  -v --verbose        Verbose output for debugging
Modes: 

If no mode is given, a menu of choices is provided.

 export: create pandas and tsv files of postGIS output.  
Also, copy over the final/summary database tables if there's a local server present
 country: do analyis at country level
 copy_to_local: copy the ISO and id1 and city and WBR disconnectivity tables from a server at port 9432 (ie an ssh tunnel to sprawl) to the localhost postGIS server. Also, copy the pandas files in working and scratch folders. Also, copy most of the contents of output/graphics and output/tables.
 cities: Use the Atlas data in a separate module to do city-level analysis

 citymaps: The citymaps.py module does not take command line flags, so use this as its interface
 keystats: Generate a .tex file containing some stats.
 deploy: Copy osm10, osm10cars, osm7 figures to Dropbox / manuscripts
 XSmaterial: Make figures/tables for XS manuscript
 TSmaterial: Make figures/tables for TS manuscript
 manuscript:  older mode, still needed, for both XS and TS stuff, ie not yet split up
 latexmk:  It seems not all .tex files in output/tables folders get compiled properly. This (re)does it
  gen_wide_saved_file: this is for internal use, ie it's called automatically. Do not use.

The main feature is a class called osm_ghsl_analysis.

See osm_analysis_triage.py for other functions not yet re-incorporated here (but not exons)

"""
__author__ = 'cpbl,amb'
import docopt
# For robustness-allways mode, this module needs to run in an environment that has not pre-loaded matplotlib, e.g. plain ipython --pdb
# Run mpl in PGF mode so as to be able to use hyperlinks, etc:
import matplotlib
PGF = False # Turn PGF on for robustness-allways-cell mode ONLY, and launch with "ipython --pdb" rather than "pylab", to make the SImode summaries.  By contrast, if producing layouts using LaTeX rather than matplotlib, you don't need PGF.
if PGF:
    matplotlib.use('pgf') # This throws a warning under ipython --pylab if PGF is mistakenly True.
import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
#plt.rc('font', family='serif')
plt.rc('text.latex', unicode=True)
params = {'text.usetex': True,
          'text.latex.unicode': True,
          'text.latex.preamble': r'\usepackage{amsmath} \usepackage{amssymb} \usepackage{hyperref} ', }
plt.rcParams.update(params)
plt.rc('pgf', texsystem='pdflatex')

matplotlib.rcParams['pgf.preamble'] = [r'\usepackage{hyperref}', ]
# if PGF: Unfortunately, you should run this module from python or ipython, not "ipython -pylab"


from osm_config import paths, defaults,  update_paths_to_be_specific_to_database
import os
import pandas as pd
import numpy as np
from collections import OrderedDict
import textwrap
import inquirer
import postgres_tools as psqlt
import cpblUtilities
cpblUtilities.defaults.update(defaults) # See
from cpblUtilities.parallel import runFunctionsInParallel
#print('Updated cpblUtilities paths:\n{}'.format(cpblUtilities.defaults['paths']))
from cpblUtilities.mathgraph import savefigall, figureFontSetup, remove_underscores_from_figure, ensure_non_graphical_interface, human_format
from cpblUtilities.utilities import mergePDFs, multipage_plot_iterator, str2pathname, shelfSave, shelfLoad, dgetget, dsetset
from cpblUtilities.matplotlib_utils import plot_diagonal, vtext
from cpblUtilities.cpblunicode import str2latex
from cpblUtilities.matplotlib_utils import bottomlefttext, bottomrighttext, toplefttext
from cpblUtilities.color import getIndexedColormap
from cpblUtilities.textables import formatDFforLaTeX, dataframeWithLaTeXToTable
from cpblUtilities.configtools import get_docopt_mode
from cpblUtilities.country_tools import country_tools
from cpblUtilities.mathgraph import  dfOverplotLinFit, overplotLinFit
from cpblUtilities.matplotlib_utils import toprighttext,toplefttext

from pointmetrics_calculations import get_analysis_radii, get_analysis_radii_suffixes
import matplotlib.pyplot as plt
from aggregate_connectivity_metrics import str_reps, disconnectivity_analysis
from metric_settings import metricNames, order_variables
import metric_settings as smetrics
from osm_config import defaults
from scipy.stats import gaussian_kde, pearsonr
from osmTools import loadOtherCountryData, country2WBregionLookup, pgisConnection, osm_logger, GHSL_YEARS, discrete_colors
import osmTools as osmt
import osm_cities as cities
import kmeans_analysis as kmeans
try: 
    import pystata as stata
    from pystata.latexRegressions import latexRegressionFile
except ImportError as e:
    print(" Failed to import cpbl's pystata module, so some stuff in osm_analysis will not work.")

from unitgraphs import stylized_unit_graphs
from rasterTools import make_and_save_gridcell_network_image

mapsScratch = paths['scratch']+'bgmaps/'

os.system('mkdir -p {}tables'.format(paths['output']))


from region_definitions import  continentsWBR, developmentWBR

from pandas.plotting import scatter_matrix


######################################################################################
######################################################################################
#    DATA RETRIEVAL METHODS
######################################################################################
######################################################################################

from metric_settings import add_standard_derived_metrics

def metrics_deltas(adf):
    """ This finds changes in columns of a dataframe, after grouping on the index. Use 

    Example use:
        delta0= df0.groupby(df0.index.name)[[vv for vv in df0.columns if vv in metricNames.keys()]+['ghsl_yr']].apply(metrics_deltas).sort_values('pca1').dropna(subset=['pca1'])
        delta0 = delta0.query('ghsl_yr>0') # Had more than one year

    Here are the one-liners: https://stackoverflow.com/questions/40183800/pandas-difference-between-largest-and-smallest-value-within-group/40183864?noredirect=1#comment79497853_40183864
    """
    timevar = 'ghsl_yr' if 'ghsl_yr' in adf else 'iyear'
    sadf = adf.sort_values(timevar)  # Ensure they're climbing.
    first, last = sadf.iloc[0], sadf.iloc[-1]
    return last-first  # Even if not all years are present


def test_add_country_data_to_df_by_iso():
    dft = pd.DataFrame(dict(iso=['USA','MEX'], tval= [1,2]))
    out = add_country_data_to_df_by_iso(dft, duplicate_for_world_regions=False)
    assert len(dft) == len (out)
    dft['continent'] = 'northamerica'
    out = add_country_data_to_df_by_iso(dft, duplicate_for_world_regions=True)

def add_country_data_to_df_by_iso(df, duplicate_for_world_regions=True):
    """
    This adds columns for world regions and country-level statistics to a DataFrame.
    Warning:
       duplicate_for_world_regions:    this duplicates rows heavily, because it adds one line per "GroupCode" (world region) that each country fits into.
    Use duplicate_for_world_regions=False to exclude world regions.

    The only requirement is that df have reset_index() done and contain a 'iso' column.
    If there is also a 'continent' column, it will be used to screen out test continent data.
    There should normally also be  'ghsl_yr' and 'n_nodes' columns. If so, the latest new nodes count (ie in 2000 or 2014) will be added by iso.

    """
    n_check = len(df) # Make sure we losing or gaining length intentionally
    #
    #  Add world region lookup by country (thus duplicating rows)
    #
    this = add_country_data_to_df_by_iso
    if "_country2WBregionLookup" not in this.__dict__:
        this._country2WBregionLookup = country2WBregionLookup()
    
    c2wb = this._country2WBregionLookup.reset_index().rename(
        columns={'ISOalpha3': 'iso'})
    # WBcode is just for subsequent merge, below
    c2wb = c2wb[['CountryName', 'WBcode', 'iso',
                 'GroupCode', 'GroupName']].drop_duplicates()
    wdf2 = df.set_index('iso').join(c2wb.set_index(
        'iso'), how='left').reset_index()  # , 'iso','left')
    needchecking = wdf2[wdf2.iso != wdf2.WBcode]
    if 0 and not needchecking.empty:
        print('  The following rows are being dropped because while we could match on ISO between DB data and c2wb, the WBcode did not match the ISO')
        print((needchecking[['iso', 'WBcode']].drop_duplicates()))
    wdf2 = wdf2[['GroupCode', 'GroupName']+ [cc for cc in wdf2.columns if cc not in ['GroupCode', 'GroupName']]]

    if not duplicate_for_world_regions:
        wdf2 = wdf2.query('GroupCode=="WLD"')
        if not len(wdf2) == n_check:
            print(' We have lost many countries when trying to add country-level information.')

    # Also add 10-region world regions?
    ct = country_tools.get_Gallup_country_lookups()[['ISO','Region']].set_index('ISO').dropna()
    wdf2.set_index('iso', inplace=True)
    wdf2 = wdf2.join(ct).rename(columns={'Region':'gRegion'})
    wdf2.index.name='iso'
    wdf2= wdf2.reset_index().dropna(subset=['iso'])

    # Also add Inglehard world regions? (not done)
            
    #
    #  Add country-level statistics
    #
    ocd = loadOtherCountryData(forceUpdate=False, imputation=True)
    df = wdf2.set_index('WBcode').join(ocd.set_index('WBcode'))
    
    df_withNulls = wdf2.set_index('WBcode').join(ocd.set_index('WBcode'))
    df = df_withNulls[pd.notnull(df_withNulls.index)]

    if 'continent' in df:
        df = df[df.continent.isin(['planet', 'fakeplanet',
                                   'centralamerica', 'southamerica', 'europe', 'asia', 'africa',     'australiaoceania', 'northamerica'])]
    needchecking = wdf2[wdf2.iso != wdf2.WBcode]
    if not needchecking.empty:
        print('  The following rows: while we could match on ISO between DB data and c2wb, the WBcode did not match the ISO')
        print((needchecking[['iso', 'WBcode']
                           ].drop_duplicates().set_index('iso').to_dict()))
        #wdf2 = wdf2[wdf2.iso == wdf2.WBcode]

    if 'ghsl_yr' in df and 'n_nodes' in df:
        # Grab latest n_nodes for each city:
        df.reset_index(inplace=True)
        latest_new_nodes = df[df.ghsl_yr >= 1999].sort_values(
            'ghsl_yr').groupby('iso').tail(1).set_index('iso')[['n_nodes']]
        latest_new_nodes.columns = ['latest_ghsl_nodes']
        df = df.set_index('iso').join(latest_new_nodes)
    return df.reset_index()


def known_psql_tables():
    """ Make a module-permanent list of database table names and corresponding disk export filenames.
    """
    this = known_psql_tables
    if "kptdf" not in this.__dict__:
        # Compile a list/lookup of known tables:
        known_tables = []
        for clusterradius in defaults['osm']['clusterRadii']:
            for continent in defaults['osm']['continents']:
                for resolution in (defaults['gadm']['ID_LEVELS']+['wbr']*(continent in ['fakeplanet', 'planet']))[::-1]:
                    for suffix in ['', 'density_ghsl', 'ghsl_urban', 'urban', 'ghsl']:
                        psqltable = 'disconnectivity_' + \
                            '_'.join(
                                [resolution, continent, clusterradius])+'_'*(not not suffix) + suffix
                        # N.B.: The following assumes that the current value of urban_pctile in the config file represents the value actually in effect during the last from-pointmetrics aggregation!
                        pdfile = '{}{}_{}.pandas'.format(
                            paths['working'], psqltable, defaults['osm']['urban_pctile'])
                        known_tables += [[resolution, continent, clusterradius,
                                          suffix, psqltable, pdfile, defaults['osm']['urban_pctile']]]
        # Here  "wbr" is treated as a GADMlevel
        kptdf = pd.DataFrame(known_tables, columns=[
                                              'GADMlevel', 'continent', 'clusterradius', 'suffix', 'psqltable', 'pdfile', 'urban_pctile'])
        this.kptdf = kptdf
    return this.kptdf.copy()

def test_get_country_stocks_and_TS():
    # Test that from_db gives the same exactly as from local Pandas files:
    dfdb= get_country_stocks_and_TS(gadmlevel='iso', suffix=None, forceUpdate=False, from_db=True)
    df2=  get_country_stocks_and_TS(gadmlevel='iso', suffix=None, forceUpdate=False, from_db=False)
    # Check that these are identical:
    df =df2.merge(dfdb)
    assert len(df) == len(df2)
    assert len(df.columns) == len(df2.columns)

    
def get_country_stocks_and_TS(gadmlevel='iso', urban = False, density = False, forceUpdate=False, from_db=None):
    """ Add stocks as another ghsl_yr in the time series.
    """
    assert density is False # That option is not written /tested yet
    table= known_psql_tables().query('GADMlevel=="{}" and suffix=="{}"'.format(gadmlevel, 'ghsl'+'_urban'*urban))
    assert len(table)==1
    if from_db is None: from_db = defaults['USE_DB']
    if from_db:
        db = osmt.pgisConnection(verbose=True)
        ts = db.db2df(table.iloc[0].psqltable )
    else:
        print((' WARNING: Loading from pandas file rather than database (from_db={})'.format(from_db)))
        ts = pd.read_pickle(table.pdfile.tolist()[0])
    ts['ghsl'] = True
    ts['Year'] = ts['ghsl_yr'].astype(int).astype(str)

    table= known_psql_tables().query('GADMlevel=="{}" and suffix=="{}"'.format(gadmlevel, 'urban'*urban))
    assert len(table)==1
    if from_db: 
        st  = db.db2df(table.iloc[0].psqltable )
    else:
        st = pd.read_pickle(table.pdfile.tolist()[0])
    st['ghsl'] = False
    st['Year'] = 'Stock'
    df = pd.concat([ts,st], sort=False)
    if gadmlevel=='wbr':
        df['wbr'] = df.wbregion
    df=df.sort_values([gadmlevel,'Year'])
    # Now the ghsl row n_nodes add up for each region to the stock n_nodes
    return add_standard_derived_metrics(df)

def get_long_TS_dataset_countries_and_cities_and_regions(urban=False, density=False, forceUpdate=False, from_db=None, formatted_year_names=True):
    """ This returns GHSL and stocks, for both countries and WBRs, with extra country columns added in.

    Using the result:
    dft = dft.dropna(subset=['iso']).sort_values('Year')[['CountryName', 'Year', 'N(nodes)', metric, metric2]].rename(
            columns=metricNames).reset_index().drop_duplicates().set_index('index').pivot('CountryName', 'Year')

    dft.dropna(subset=['wbr'])[
    """
    assert density is False # Not written yet
    if from_db is None: from_db = defaults['USE_DB']
    dft = pd.concat([
        get_country_stocks_and_TS(gadmlevel='iso', urban=urban, forceUpdate=forceUpdate, from_db=from_db),
        get_country_stocks_and_TS(gadmlevel='wbr', urban=urban, forceUpdate=forceUpdate, from_db=from_db)
        ], sort=False).sort_values('ghsl_yr')

    nullnodes= pd.isnull(dft.n_nodes)
    assert nullnodes.sum() <7 # SPM, CXR, XCA get dropped?
    print((' Dropping {}'.format(dft[nullnodes].iso.unique())))
    dft=dft[~nullnodes]
    #assert len(dft[metric].dropna() >5)
    #dft['N(nodes)'] = dft['N(nodes)'].map(human_format) 
    # dft['Year'] = dft['ghsl_yr'].astype(int)

    # Add country data only to iso rows, but keep non-iso rows (ie split dft, recombine):
    df_all_long = pd.concat([
        add_country_data_to_df_by_iso(dft.dropna(subset=['iso']),
                                      duplicate_for_world_regions=False),
        dft[pd.isnull(dft.iso)]
        ], sort=False)# N.B. iso is NaN for all WBRs

    # Check some properties: we have not duplicated country rows:
    assert (df_all_long.groupby('iso').degree.count()<=6).all()
    assert (df_all_long.groupby('wbr').degree.count()==6).all()

    df_all_long['city'] = False
    df_all_long['placename'] = df_all_long.CountryName

    # Add cities as well
    ca = cities.city_atlas()
    citydf = ca.load_connectivity(separate=False,  duplicate_for_world_regions=False, from_db=from_db)
    
    citydf['city'] = True
    city_year_names = ca.atlasyears_labels_shortest if formatted_year_names else [sss.replace('$<$','').replace('-','_').replace("'",'') for sss in ca.atlasyears_labels_shortest]
    citydf['Year'] = citydf.iyear.replace(dict(list(zip([1,2,3], city_year_names))))
    citydf.set_index(['iyear','stockorchange'],  inplace=True)
    citydf.loc[(3,'cityedges'),'Year'] = 'Stock'
    citydf.reset_index(inplace=True)
    if 0:
        citydf['Year'] = citydf.apply(lambda adf: 'Stock' if adf.stockorchange=='cityedges' and adf.iyear==3   else adf.iyear, #ca.atlasyears_labels_shortest[adf.iyear-1],
                               axis=1)
        citydf['shortyear'] = citydf.apply(lambda adf: 'Stock' if adf.stockorchange=='cityedges' and adf.iyear==3   else ca.atlasyears_labels_shortest[adf.iyear-1],
                               axis=1)
    citydf = citydf.assign(Year=citydf.Year.astype('category', categories= city_year_names +['Stock']))
    i2sn =   osmt.country_tools().country2ISOLookup()['ISOalpha2shortName']
    citydf['placename'] = citydf.City+r', {\footnotesize\sc '+citydf.ISO.map(lambda ii: i2sn.get(ii,ii))+'}'
    df = pd.concat([df_all_long, citydf], sort=False)
    df['N(nodes)'] = df['n_nodes'].astype(int)
    
    return df

def get_saved_wide_flattened_TS_dataframes(db=None, table_subset_suffix=None ):
    """ This is useful for combining data from multiple databases.
    """
    dfs={'city':{}, 'iso':{}}
    for db in ['osm10','osm10cars','osm7']:
        fnstem = paths['scratch'].replace(defaults['server']['postgres_db'], db)  +'saved_wide_flattened_TS_dataframes_{db}_{suffix}'.format(db =db , suffix = table_subset_suffix)
        
        #fn = paths['scratch']+'saved_wide_flattened_TS_dataframes_{db}_{suffix}.pandas'
        if not os.path.exists(fnstem+'city.pandas'):
            sh='./osm_analysis.py  gen_wide_saved_file --db={db} --cluster-radius={cr}  '.format(db = db, cr = '10' if '10' in db else '7' if '7' in db else 'Faile')
            print(sh)
            os.system(sh)
        dfn,dfc = pd.read_pickle(fnstem+'iso.pandas'), pd.read_pickle(fnstem+'city.pandas')
        dfs['city'][db] = dfc
        dfs['iso'][db] = dfn
    return dfs

def get_wide_flattened_TS_dataframes(table_subset_suffix=None ):

        """ This generates an often-useful combined long-format dataset of countries and cities, along with a flattened-column version for cities and for countries.
        This method saves its results, but does not use them. They are only used by get_Saved_wide... above.
"""
        table_subset_suffix = table_subset_suffix if table_subset_suffix is not None else 'ghsl_urban'

        dft = get_long_TS_dataset_countries_and_cities_and_regions(urban= 'urban' in table_subset_suffix,
                                                        density= 'density' in table_subset_suffix,
                                                                   formatted_year_names=False,                                                                   
                                                        forceUpdate=False)#, from_db=True)
        # delete the metrics (but not the counts) for low-count rows:
        metrics = ['pca1','degree','logcircuity_500_1000']
        dft.loc[dft.n_nodes<=100, metrics]= np.nan # AdD more metrics here!? TO DO

        from cpblUtilities.pandas_utils import flatten_multiindex_columns
        
        # Countries
        dfn = dft.query('city==False').dropna(subset=['iso']).query('Year!="-1"').sort_values('Year')[['placename', 'Year', ]+metrics+[ 'n_nodes']].reset_index().set_index('index').pivot('placename', 'Year')

        dfnf = flatten_multiindex_columns(dfn, sep='_')
        fnstem = paths['scratch']+'saved_wide_flattened_TS_dataframes_{db}_{suffix}'.format(db = defaults['server']['postgres_db'], suffix = table_subset_suffix)
        print(('   '+fnstem))
        
        dfnf.to_pickle(fnstem+'iso.pandas')

        
        # Cities
        dfc = dft.query('city==True').query('iyear==1 or stockorchange=="citychanges" or Year=="Stock"').sort_values('pca1', ascending=False).sort_values('Year')[['placename', 'Year', ]+metrics+[ 'N(nodes)']].reset_index().set_index('index').pivot('placename', 'Year')
        dfcf = flatten_multiindex_columns(dfc, sep='_')
        dfcf.to_pickle(fnstem+'city.pandas')

        return dfnf, dfcf


######################################################################################
######################################################################################
#    ANALYSIS CLASS
######################################################################################
######################################################################################

        
class osm_ghsl_analysis(object):
    def __init__(self, logger=None,    min_nodes=15, fig2mode=None):
        """
        """
        self.distanceRatios = [
            'distanceRatio_'+RRsuffix for RRsuffix in get_analysis_radii_suffixes()]
        self.nRRs = [
            'n_'+RRsuffix for RRsuffix in get_analysis_radii_suffixes()]
        self.nodeDensities = [
            'nodedensity_'+RRsuffix for RRsuffix in get_analysis_radii_suffixes()[:2]]
        self.dendricities = ['frc_length_noncycle',
                             'frc_length_bridge', 'frc_edges_noncycle', 'frc_edges_bridge']
        self.PCAs = ['PCA1', 'PCA2']
        self.ls_cols = ['ls_pop', 'ls_density', 'lognodalsparsity']
        self.location_cols = ['geom_centroid_lat', 'geom_centroid_lon']
        self.ghsl_year_colors = dict(  
            list(zip([1975, 1990, 2000, 2014], getIndexedColormap('winter', 4)))) # OVERWRITTEN BELOW
        self.ghsl_year_colors[-1] = 'k'
        self.ghsl_year_colors = {-1: 'k', 1975: 'mediumblue',
                                 1990: 'royalblue', 2000: 'blueviolet', 2014: 'magenta'} 
        #self.ghsl_year_colors = dict( zip([1975, 1990, 2000, 2014],   osmt.discrete_colors(4)))  # This is what we use in clean_figures, TS Fig 1.
        self.fig2geogcolors = {}
        # Use a file common to all database variants. Make sure to run primary database first.
        self.fig2geogcolors_filename = paths['basescratch'] + \
            'figure2_colors.pyshelf'
        # Choose between horizontal dashed lines or intersection-dots for figure2
        self.fig2_dots_for_stock = True
        self.fig2_mode = fig2mode  # 'pnasmain' # pnasmain, pnasSI, beamer
        if self.fig2_mode == 'pnasmain':
            self.fig2_xticklabel_fontsize = 4
            self.fig2_figfontsize = 7
            self.fig2_labelfontsize = 5
            self.fig2_ABCDfontsize = 6
            self.fig2_linewidth = 1
            self.fig2_markersize = 8
            self.fig2_worldline_width = 3
            self.fig2_graycountries_linewidth = .4
            self.fig2_ticklength = 2
        else:
            self.fig2_xticklabel_fontsize = 7
            self.fig2_figfontsize = 9
            self.fig2_labelfontsize = 5
            self.fig2_ABCDfontsize = 6
            self.fig2_linewidth = 3
            self.fig2_markersize = 40
            self.fig2_worldline_width = 4
            self.fig2_graycountries_linewidth = .8
            self.fig2_ticklength = None

        self.min_nodes = min_nodes
        self.known_psql_tables = known_psql_tables() # self.known_psql_tables is deprecated but still used a bunch.

    def export_final_connectivity_tables_to_pandas(self, continent=None, clusterradius=None, resolution=None,  columns=False, forceUpdate=False, skipConflictChecks=False, arow=None):
        """ See also website_export...py
        """

        if arow is None:
            cur = psqlt.dbConnection()
            runFunctionsInParallel([[self.export_final_connectivity_tables_to_pandas, [continent, clusterradius, resolution,  columns, forceUpdate, skipConflictChecks, arow]] for iii, arow in known_psql_tables().iterrows()], names=['-'.join(xx.map(str)) for ii, xx in known_psql_tables().iterrows()],
                                   parallel=defaults['server']['parallel'])
            return
        # Just do one table:
        cur = psqlt.dbConnection()
        availableTables = cur.list_tables()

        ARCHETYPES = stylized_unit_graphs().available_graphs()
        if arow['continent'] not in ['planet']+ARCHETYPES:
            print((' Skipping export of {} to Pandas since continent is not "planet".'.format(
                arow['psqltable'])))
            return
        if arow['psqltable'] not in availableTables:
            print((
                ' Skipping {t} --> pandas  because the table does not exist.'.format(t=arow['psqltable'])))
            return
        if forceUpdate or not os.path.exists(arow.pdfile):
            print((' Downloading {} from psql ...'.format(arow.psqltable)))
            df = cur.db2df(arow.psqltable)
            print(('    Restricting cells to {} nodes, and adding standard metrics...'.format(
                self.min_nodes)))
            # Add: make a histogram of node counts here: df.n_nodes.map(np.log).dropna().hist()
            df = df[df.n_nodes >= self.min_nodes]
            df = add_standard_derived_metrics(df)
            print(('   Saving {} ...'.format(arow.pdfile)))
            df.to_pickle(arow.pdfile)
        else:
            print((' NOT recreating {} ...'.format(arow.pdfile)))
        return


    def iso_kdes_of_key_variables_over_time(self, gadmlevel='iso', decimate=False, usesns=False):

        ltscc= get_long_TS_dataset_countries_and_cities_and_regions(urban=True, density=False, forceUpdate=False, from_db=None, formatted_year_names=True)
        df = ltscc.query('city==False').drop_duplicates(['iso','ghsl_yr']).query('ghsl_yr>0') #query('GroupName=="World"')
        df['yrcolor'] = df.ghsl_yr.map(lambda yr,cl=self.ghsl_year_colors: cl.get(yr, 'y'))

        spm_varlist = [vv for vv in smetrics.sensible_metric_order(long_names=False, include_variance=False) if vv in [
                       'negdegree', 'fraction_deadend', 'fraction_1_3',  # 'logcurviness',
                       'frc_edges_bridge', 'frc_edges_noncycle',
                       'logcircuity_500_1000', 'pca1', ]]
        if usesns:
            import seaborn as sns
        plt.close('all')
        fig = plt.figure(figsize=(6, 4))
        if usesns:
            sns.set_style(
                "ticks", {'ytick.direction': 'in', 'xtick.direction': 'in', })

        df2 = df.copy()
        pvars = [cc for cc in spm_varlist if cc not in ['year', 'ghsl_yr']]
        if decimate and len(df2) > decimate:
            olength = len(df2)
            df2 = df2.sample(decimate)
            print(('    Decimated from {} to {} points.'.format(olength, len(df2))))
        else:
            print(('    Found {} rows.'.format(len(df2))))

        print('    Setting up axes...')
        fig,axs = plt.subplots(2, 3, figsize = (6, 4))
        plt.subplots_adjust(wspace=.03, hspace=.03,
                            bottom=.01, top=.99, right=.99, left=.01)
        for iax, avar in enumerate(spm_varlist):
            ax = axs.flatten()[iax] 
            # Put a colour-year legend in the top left and bottom right, only.
            for ghsl_yr in [1975, 1990, 2000, 2014]:
                df2[df2.ghsl_yr==ghsl_yr][avar].plot(kind='density',ax=ax,color= self.ghsl_year_colors[ghsl_yr], label= GHSL_YEARS['years'].get(ghsl_yr) if iax == 5 else None)
                ax.set_xlabel('')

            vname = metricNames.get(avar, avar).replace('(', '\n(').replace(
                '3-', '\n3-').replace('Frc', 'Fraction').replace(' deadends', '\ndeadends').replace('Sprawl ', '')

            ax.set_xticks([])
            ax.set_yticks([])
            assert vname
            if iax == 5:
                ax.legend(title=vname)
            elif iax==1:
                toplefttext(ax, vname.strip().replace('(SNDi)','SNDi'))
            else:
                toprighttext(ax, vname.strip().replace('(SNDi)','SNDi'))

        plt.show()
        remove_underscores_from_figure()
        fn = paths['graphics'] + \
            ('KDE-over-time-{}-sixvars-{}.pdf'.format(gadmlevel, defaults['server']['postgres_db'])).replace('_', '-')
        plt.savefig(fn)
        print((' Wrote '+fn))
                        

    def xs_correlation_tables(self, ):#gadmlevel=None):
        """
        Show correlation matrix for all key variables, at a given GADM level
        """
        bigISOs= ['BGD','BRA','CHN','IDN','IND','JPN','MEX','NGA','PAK','RUS','USA']

        corrvars = ['pca1', 'degree', 'fraction_deadend', 'frc_length_noncycle', 'logcircuity_500_1000', 'frc_edges_noncycle', 'curviness', 'log10length_m', 'length_m_per_pop'] 
        subs = [(kk, vv, vv, stata.EXACT) for kk,vv in list(smetrics.metricNames.items())] 
        latex = latexRegressionFile('corrmatricesXS-{}'.format(defaults['server']['postgres_db']), modelVersion='2018', regressionVersion='0',
                                    substitutions = subs)

        outs=''
        for gadmlevel in defaults['gadm']['ID_LEVELS']: # not wbr
            df = pd.read_pickle(known_psql_tables().query('suffix=="{}" and GADMlevel=="{}"'.format( 'urban', gadmlevel)).pdfile.values[0])
            dtafile =paths['scratch']+'tmp-{}-corrvars'.format(gadmlevel)
            if not os.path.exists(dtafile+'.dta.gz'):
                stata.df2dta(df, dtafile, forceUpdate=False)
            outs += stata.stataLoad(dtafile)
            outs += latex.addCorrelationTable(gadmlevel.replace('_',''), ' '.join(corrvars),)

            if gadmlevel ==defaults['gadm']['ID_LEVELS'][-1]: # For smallest scale, also do some country-specific versions:
                for iso in bigISOs+['GBR', 'SWE', 'DEU', 'CAN', ]  +0*['UZB', 'NER', 'SJM', 'JEY', 'KAZ', 'LIE', 'HRV', 'IND', 'HKG',
       'BLZ', 'BGD', 'TZA', 'OMN', 'ITA', 'XNC', 'PAN', 'HUN', 'PRT',
       'COL', 'GRL', 'PRY', 'BEN', 'VEN', 'TJK', 'IRQ', 'MDA', 'CIV',
       'MNG', 'CHE', 'MKD', 'MMR', 'FRO', 'TTO', 'NOR', 'ESP', 'IRL',
       'BMU', 'MNE', 'AND', 'SMR', 'FLK', 'DJI', 'BOL', 'URY', 'AZE',
       'SOM', 'TUV', 'ZAF', 'SVK', 'SPM', 'NRU', 'PRK', 'KGZ', 'IRN',
       'MYT', 'LAO', 'VUT', 'KHM', 'SVN', 'PLW', 'BDI', 'DMA', 'GUF',
       'KWT', 'TCA', 'UGA', 'VNM', 'DEU', 'MOZ', 'CPV', 'ECU', 'GTM',
       'ZMB', 'SGP', 'NPL', 'KEN', 'REU', 'TWN', 'CAN', 'GNB', 'BGR',
       'DOM', 'LKA', 'LSO', 'AUT', 'TUN', 'BHS', 'JOR', 'CYP', 'GHA',
       'IMN', 'PSE', 'SRB', 'DZA', 'COM', 'TCD', 'LBY', 'USA', 'PYF',
       'DNK', 'FSM', 'MRT', 'ISL', 'SUR', 'CHL', 'SYC', 'GIN', 
       'MTQ', 'LUX', 'NLD', 'PHL', 'COD', 'SLE', 'WLF', 'SEN', 'BFA',
       'ATG', 'ETH', 'MNP', 'ABW', 'CUW', 'GAB', 'TKM', 'BRN', 'MLT',
       'CUB', 'XAD', 'PAK', 'KIR', 'BEL', 'BWA', 'GUM', 'VCT', 'ARG',
       'MAR', 'CAF', 'NZL', 'LBN', 'KNA', 'CZE', 'GNQ', 'KOR', 'ESH',
       'VGB', 'GMB', 'VAT', 'AFG', 'MDG', 'UKR', 'CYM', 'EST', 'GRC',
       'BRA', 'YEM', 'BLR', 'BES', 'PRI', 'BTN', 'MHL', 'CHN', 'MUS',
       'TLS', 'TGO', 'IDN', 'CMR', 'CRI', 'ZWE', 'GRD', 'EGY', 'POL',
       'MLI', 'MYS', 'LTU', 'ALA', 'MEX', 'AIA', 'ROU', 'SHN', 'PER',
       'FJI', 'AUS', 'GLP', 'TON', 'NIC', 'AGO', 'GEO', 'MWI', 'QAT',
       'RUS', 'THA', 'SWZ', 'SLB', 'MDV', 'BRB', 'SDN', 'SWE', 'ARE',
       'COK', 'ERI', 'TUR', 'FIN', 'HND', 'MCO', 'RWA', 'NAM', 'JPN',
       'GUY', 'SYR', 'BIH', 'GGY', 'JAM', 'FRA', 'LCA', 'ASM', 'ISR',
       'NFK', 'MAF', 'MSR', 'CXR', 'GIB', 'LVA', 'XKO', 'WSM', 'ARM',
       'SAU', 'NCL', 'ALB', 'BHR', 'SLV', 'VIR', 'SXM', 'BLM', 'MAC',
       'NGA', 'STP', 'COG', 'HTI', 'SSD', 'LBR', 'PNG']:
                    outs += latex.addCorrelationTable('{}-{}'.format(gadmlevel.replace('_',''),iso), ' '.join(corrvars),
                                                      ifClause= 'iso=="{}"'.format(iso),
                                                      comments = '{}-{}'.format(gadmlevel.replace('_',''),iso))
        
        stata.stataSystem(outs,filename= paths['working']+'tmp_statasystem-corrmatricesXS')
        # Run again to read results

        latex.closeAndCompile()

    def country_scatterplotmatrices_ghslyear(self, gadmlevel=None, suffix=None, decimate=False, usesns=False):
        """
        Goal is to look at scatter plots of connectivity aggregates, separating out GHSL time groups, and density deciles.
        """

        if gadmlevel is None and suffix is None:
            ensure_non_graphical_interface()
            torun = []
            gls = ['iso', 'id_1', 'id_2']
            for gadmlevel in gls:
                torun += [[self.country_scatterplotmatrices_ghslyear,
                           [gadmlevel, suffix, decimate]]]
            runFunctionsInParallel(
                torun, names=gls, parallel=defaults['server']['parallel'])
            return

        print(('    Beginning scatterplot for {} {} {}'.format(
            gadmlevel, suffix, decimate)))
        files = self.known_psql_tables[self.known_psql_tables.GADMlevel == gadmlevel]
        assert len(files) == 5
        if suffix is None:
            suffix = 'ghsl_urban'
        files = files[files.suffix == suffix]
        assert len(files) == 1

        # Quick kludge: focus for a moment only on ghsl/urban:
        planetdf = pd.read_pickle(files.pdfile.tolist()[0])
        assert 'ghsl' in suffix
        planetdf['ghsl'] = True

        print('    Adding country data...')
        # Bring in other country data:
        df = add_country_data_to_df_by_iso(planetdf)

        # 28,32)#14, 16)#(7, 8.75)  # for full-page figures
        figSizeScatterMatrix = (40, 50)
        if usesns:
            figureFontSetup(uniform=12, figsize='paper')
        else:
            figureFontSetup(uniform=7, figsize='paper')

        df['lnCO2_roadtpt_pc'] = np.log(df.CO2_roadtpt / df.pop2012)
        df['lngdpCapitaPPP_WB'] = np.log(df.gdpCapitaPPP_WB)
        plt.close('all')
        df['yrcolor'] = df.ghsl_yr.map(
            lambda yr: self.ghsl_year_colors.get(yr, 'y'))
        # Drop the still-undeveloped regions
        df = df.dropna(subset=['ghsl_yr'])
        spm_varlist = [vv for vv in smetrics.sensible_metric_order(long_names=False, include_variance=False) if vv in [
            'lognodalsparsity_1000',
                       #'n_0_500', 'n_500_1000',
                       'negdegree', 'fraction_deadend', 'fraction_1_3', 'curviness',
                       'frc_edges_bridge', 'frc_edges_noncycle', 'frc_length_bridge', 'frc_length_noncycle',
                       'distanceratio_0_500', 'distanceratio_500_1000', 'pca1', 'pca2']] + ['GDP2014PPP_bn2005USD', 'lnCO2_roadtpt_pc',  # 'gdpCapitaPPP_WB',
                                                                                            'lngdpCapitaPPP_WB',  'oilRents_pcofGDP', ]*(gadmlevel == 'iso')*0+['ls_pop', ]*0

        # Try again using Seaborn's fanciness
        import seaborn as sns
        plt.close('all')
        if usesns:
            # sns.set(style="white")
            sns.set_style(
                "ticks", {'ytick.direction': 'in', 'xtick.direction': 'in', })

        df['year'] = df.ghsl_yr.map(int).astype(str)
        df2 = df.copy() 
        pvars = [cc for cc in spm_varlist if cc not in ['year', 'ghsl_yr']]
        df2 = df2.dropna(subset=pvars)
        print((' Dropped ALL rows with any NaNs: {} --> {}'.format(len(df), len(df2))))
        if decimate and len(df2) > decimate:
            olength = len(df2)
            df2 = df2.sample(decimate)
            print(('    Decimated from {} to {} points.'.format(olength, len(df2))))
        else:
            print(('    Found {} rows.'.format(len(df2))))

        print('    Setting up axes...')
        if usesns:
            g = sns.PairGrid(df2, vars=pvars,  palette=[
                             "red"], dropna=True)  # dropna does not work!
            axs = g.axes
        else:
            fig, axs = plt.subplots(len(pvars), len(pvars),)
        plt.gcf().set_size_inches(figSizeScatterMatrix)

        print(('    Looping over {n}x{n} axes, plotting in each...'.format(
            n=len(pvars))))
        if usesns:
            g.map_upper(plt.scatter, s=10, )
        else:
            for i1, v1 in enumerate(pvars):
                for i2, v2 in enumerate(pvars):
                    ax = axs[i2, i1]
                    if i1 == i2:
                        if 0:
                            # alpha=.5) #kde=True,
                            sns.distplot(df2[v1], ax=ax, color='k', kde=False)
                        # Put a colour-year legend in the top left and bottom right, only.
                        for ghsl_yr in [1975, 1990, 2000, 2014]:
                            sns.distplot(df2[df2.ghsl_yr == ghsl_yr][v1], ax=ax, color=self.ghsl_year_colors[ghsl_yr],  hist=False, kde=True, label=(
                                (i1 == 0 and i2 == 0) or (i1 == len(pvars) and i2 == len(pvars)))*str(ghsl_yr))
                    elif i1 > i2:
                        ax.scatter(df2[v1], df2[v2], s=1)
                    else:
                        sns.kdeplot(df2[v1], df2[v2], lw=3,
                                    legend=False, ax=ax)
                        assert pd.notnull(df2[v1]).all()
                        r, _ = pearsonr(df2[v1], df2[v2])
                        ax.annotate("r = {:.2f}".format(
                            r), xy=(.1, .9), xycoords=ax.transAxes, size=9)
                    xt = metricNames.get(v1, v1).replace('_', '').replace(' (','\n(').replace('1- and 3','1- and\n3').replace('3-degree nodes','3-degree\nnodes')
                    yt = metricNames.get(v2, v2).replace('_', '').replace(' (','\n(').replace('1- and 3','1- and\n3').replace('3-degree nodes','3-degree\nnodes')
                    ax.set_ylabel(yt*(i1 == 0 or i1 == len(pvars)-1), fontsize=50)
                    ax.set_xlabel(xt * (i2 == len(pvars)-1), fontsize=50)
                    ax.set_title(xt * (i2 == 0), fontdict={'fontsize': 50}, fontsize=50)
                    if i1 == len(pvars)-1:
                        ax.yaxis.set_label_position("right")
                    ax.tick_params(direction='in')
                    # Show colours legend in top left cell
                    if (i1 == 0 and i2 == 0) or (i1 == len(pvars)-1 and i2 == len(pvars)-1):
                        cpblUtilities.mathgraph.transbg(
                            ax.legend(loc='upper right', fontsize=20))
                    ax.axis('tight')

        remove_underscores_from_figure()
        fn = paths['graphics'] + 'scatterplotmatrix-{}-{}-countrymeasures-{}-{}'.format(
            gadmlevel, suffix.replace('_',''), 'allyears', defaults['server']['postgres_db'])
        print(('   Saving {}...'.format(fn)))
        plt.savefig(fn, dpi=300)
        plt.savefig(fn+'-thumb', dpi=50)
        plt.gcf().suptitle('Metric correlations: GADM scale {}'.format(
            gadmlevel.upper().replace('_', ' '), fontsize=60))
        print((' Wrote '+fn))
        os.system('convert -trim {f} {f}'.format(f=fn+'.png'))
        os.system('convert -trim {f} {f}'.format(f=fn+'-thumb.png'))

    def country_distributions(self, df=None):
        if df is None:
            # Bring in other country data:
            df = self.get_ghsl_urban_with_integrated_country_data()
        figSizePage = (14, 16)  # (7, 8.75)  # for full-page figures
        figureFontSetup(uniform=12, figsize='paper')
        plt.figure(123, figsize=figSizePage), plt.clf()

        plotvar = 'negdegree'
        df['yrcolor'] = df.ghsl_yr.map(
            lambda yr: self.ghsl_year_colors.get(yr, 'y'))

        for iax, decile in enumerate([-1, 7, 8, 9, 10]):
            ax = plt.subplot(5, 1, iax+1)
            y = df[(df.density_decile == decile) & (df.ghsl_yr != -1)
                   ][[plotvar, 'ghsl_yr', 'yrcolor']].dropna()
            for ayear, ay in y.groupby('ghsl_yr'):
                yy = ay[plotvar]
                gkde = gaussian_kde(yy)
                ind = np.linspace(yy.min(), yy.max(), 1000)
                ax.plot(ind, gkde.evaluate(ind), c=ay.yrcolor.unique()
                        [0], lw=2,  label=str(int(ayear)))
                ax.set_ylabel(r'$\rho$ decile {}'.format(decile))
            if iax == 0:

                plt.title(
                    'Distribution across ISOs of {}, by density and year'.format(plotvar))
                ax.set_ylabel('All densities')
            ax.set_xlim([-3.75, -1.5])

        assert defaults['osm']['continents'] == ['planet']
        fn = paths['graphics'] + 'planet-iso-'+plotvar+'-ghsl-density-'+defaults['server']['postgres_db']
        plt.savefig(fn)
        plt.show()


    def html_country_and_city_ranking_tables(self, rankby='recent', metric='pca1', metric2=None, suffix2=None,
                                    table_subset_suffix=None ):
        """Adapted from LaTeX_country_and_city_ranking_tables below. May still contain vestigial code.
        """
        ccsvfn='/home/meuser/web/publications/2019-PNAS-sprawl/data/cities-ranked-{}.tsv'.format(metric)
        ncsvfn='/home/meuser/web/publications/2019-PNAS-sprawl/data/countries-ranked-{}.tsv'.format(metric)
        chfn='/home/meuser/web/publications/2019-PNAS-sprawl/cities-ranked-{}.html'.format(metric)
        nhfn='/home/meuser/web/publications/2019-PNAS-sprawl/countries-ranked-{}.html'.format(metric)
        
        table_subset_suffix = table_subset_suffix if table_subset_suffix is not None else 'ghsl_urban'
        assert rankby in ['recent', 'stock']
        if metric not in ['pca1', 'length_m_per_pop', 'length_m', 'log10length_m', 'fraction_deadend']:
            return

        if metric2 is None:
            metric2 = {'pca1': 'degree', 'length_m_per_pop': 'nodes_per_pop',
                       'length_m': 'pca1', 'fraction_deadend': 'fraction_1_3'}.get(metric,  'degree')

        dft = get_long_TS_dataset_countries_and_cities_and_regions(urban= 'urban' in table_subset_suffix,
                                                        density= 'density' in table_subset_suffix,
                                                        forceUpdate=False)#, from_db=True)

        _dfn, _dfc =  get_wide_flattened_TS_dataframes(table_subset_suffix= table_subset_suffix)

        
        # delete the metrics (but not the counts) for low-count rows:
        dft.loc[dft.n_nodes<=100, [metric,metric2]]= np.nan

        # Prepare to drop small countries: (214 rows --> 170 rows)
        dft.loc[(dft.Year=='Stock') & (dft.n_nodes<5000) & (dft.city==False), 'N(nodes)'] = '<5k'
        # Following two lines can be used to simplify syntax below; I wrote it after most of this section.
        isostocknodes = pd.DataFrame(dft.groupby('iso').apply(lambda adf: adf.query('Year=="Stock"').n_nodes.values[0]).rename('stock_n_nodes'))
        dft = dft.set_index('iso').join(isostocknodes).reset_index()
        # Whoops; that sets stock_n_nodes incorrectly for cities.
        citystocknodes = pd.DataFrame(dft.query('city==True').groupby('City').apply(lambda adf: adf.query('Year=="Stock"').n_nodes.values[0]).rename('stock_n_nodes'))
        dft.set_index('City', inplace=True)
        dft.update(citystocknodes)
        dft.reset_index(inplace=True)
        
        # Do above now so we can format the rest before doing pivot: [now not necessary since we have stock_n_nodes
        if 0: dft['N(nodes)'] = dft['N(nodes)'].map(human_format)         

        # Include any world regions in country lists? Just Earth for now:
        worldName = r'$\rightarrow${\sc Earth}$\leftarrow$'
        dft.loc[dft.wbr=='WLD', 'placename'] = worldName

        # Bold-ify names of big countries:
        bigc = (dft.pop2012>20e6) & (dft.city==False)
        if 0: dft.loc[bigc, 'placename'] = r'<b> '+  dft.loc[bigc, 'placename'] +' </b>'


    
        ####################################        
        # Countries: Make a recent-oriented table for TSSI.
        ####################################        

        dfw = dft.query('city==False').query('Year!="-1"').dropna(subset=['iso']).sort_values('Year')[['placename', 'Year', metric, metric2, 'N(nodes)']].rename(
            columns=metricNames).reset_index().set_index('index').pivot('placename', 'Year')

        # Drop small countries: (214 rows --> 170 rows)
        dfw = dfw[dfw[('N(nodes)','Stock')] != '<5k']
        dfw.sort_values(('Sprawl (SNDi)','2014'), inplace=True, ascending=False)
        dfw.dropna(subset=[('Sprawl (SNDi)','2014')], inplace=True)

        from cpblUtilities2.textables import chooseSFormat
        chooseSFormat3 = lambda x: chooseSFormat(x, threeSigDigs=True)
        dfw['N(nodes)'] = dfw['N(nodes)'].applymap(chooseSFormat3)

        dfw.index.name=None # Remove "placename" row from html
        formatDFforLaTeX(dfw, row=None,  precision=1, colour=None, leadingZeros=False)#, sigdigs=3)

        # Shorten columns names (second level of multiindex)
        gy = OrderedDict([(str(a),b) for a,b in list(GHSL_YEARS['years'].items())])
        dfw.columns = pd.MultiIndex.from_tuples([(a, gy[b].replace('--','-').replace('$<$','<')) for a, b in dfw.columns.values])
        dfw2=dfw.copy()
        dfw2.index.name='Use of these data requires citation of the source journal article (Barrington-Leigh and Millard-Ball, PNAS, 2019/2020).'
        dfw2.to_csv(ncsvfn, sep='\t')

        html = dfw.to_html().replace('<th>Year</th>', '<th class="js-sort-string">Country</th>'.replace('$','')) 
        for th in dfw.columns.levels[1].values:
            html = html.replace('<th>{}</th>'.format(th), '<th class="js-sort-number">{}</th>'.format(th))


        HTMLHEAD="""
<html>
  <head>
    <!-- See here for options: https://www.cssscript.com/demo/html-table-sortable/ -->
<script src="sort-table/sort-table.js"></script>
<link rel="stylesheet" href="sort-table/sort-table.css">
<style>
      tr:nth-of-type(odd) {
      background-color:#ccc;
    }
      tr:nth-of-type(even) {
      background-color:#eee;
    }
</style>
  </head>
  <body>
  """
        with open(nhfn, 'wt') as fout:
            fout.write(HTMLHEAD+"""
<center>
<h2> Countries ranked by street-network sprawl<p>Barrington-Leigh and Millard-Ball, PNAS 2020</h2>
<h3> Click on a column to sort the table.  You can also download the <a href="data/{csv}">raw data</a>.</h3>

<table class="js-sort-table">
""".format(csv=os.path.split(ncsvfn)[1])+ '\n'.join( html.split('\n')[1:]) +"""
</center>

""")

            #os.system(' chromium-browser {}'.format(fn))


        # Cities:
        vrformatString='l||rrr|r||rrr|r||rrr|r|'
        dfc = dft.query('city==True').query('iyear==1 or stockorchange=="citychanges" or Year=="Stock"').sort_values('pca1', ascending=False)[['placename','Year', metric, metric2, 'N(nodes)']].rename(  columns=metricNames).reset_index().set_index('index').pivot('placename', 'Year').dropna(subset = [('Sprawl (SNDi)',"'00-13")]).sort_values([('Sprawl (SNDi)',"'00-13")], ascending=False)
        # Reorder columns:
        dfc['N(nodes)'] = dfc['N(nodes)'].applymap(chooseSFormat3)
        dfc['City'] = dfc.index.str.split(',').str[0]
        dfc[('','Country')] = dfc.index.str.split(',').str[1].str.replace(' {\\\\footnotesize\\\\sc ','').str.replace('}','')
        #dfc=dfc.set_index(['City','Country'])
        #dfc.index.name=None # Remove "placename" row from html
        formatDFforLaTeX(dfc, row=None,  precision=1, colour=None, leadingZeros=False)#, sigdigs=3)
        #cols = dfc.columns.levels[0]
        colsT = 'Sprawl (SNDi)', 'Nodal degree', 'N(nodes)'
        colsB = ["$<$1990", "90-99", "'00-13", 'Stock']
        wcols = [('','Country')]+             [(a,b) for a in colsT for b in colsB]

        formatDFforLaTeX(dfc, row=None,  precision=1, #sigdigs=3,
                         colour=None, leadingZeros=False)
        dfc=dfc.reset_index()
        dfc.index.name=None
        dfc2=dfc.copy()
        dfc2[dfc2.columns[0]]=dfc2[dfc2.columns[0]].str.replace(r', {\\footnotesize\\sc ',', ').str.replace('}','')
        dfc2.index.name='Use of these data requires citation of the source journal article (Barrington-Leigh and Millard-Ball, PNAS, 2019/2020).'
        dfc2.to_csv(ccsvfn, sep='\t')
        html = dfc.set_index('City')[wcols].to_html().replace('$<$1990', '<1990').replace(r'{\footnotesize\sc ','').replace('}','').replace('<th>Year</th>', '<th class="js-sort-string">City</th>').replace('$','').replace("""<tr>
      <th>City</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>""", '')
        # First two columns are alpha-sort,
        for th in colsB: #dfc.columns.levels[1].values:
            html = html.replace('<th>{}</th>'.format(th), '<th class="js-sort-number">{}</th>'.format(th))


        with open(chfn, 'wt') as fout:
            fout.write(HTMLHEAD+"""
<center>
<h2> Cities ranked by street-network sprawl<p>Barrington-Leigh and Millard-Ball, PNAS 2020</h2>
<h3> Click on a column to sort the table.  You can also download the <a href="data/{csv}">raw data</a>.</h3>

<table class="js-sort-table">
""".format(csv=os.path.split(ccsvfn)[1]) + '\n'.join( html.split('\n')[1:]) +"""
</center>

""")
        #os.system(' chromium-browser {}'.format(fn))
            
        
    def LaTeX_country_and_city_ranking_tables(self, rankby='recent', metric='pca1', metric2=None, suffix2=None,
                                    table_subset_suffix=None ):
        """ Produce some tables for the SI, ranking countries or cities.
        For stock-ranked version, list the per-year measures, but order them by stock.

        Alas, plenty of code is in common with the city_ranking_table method.

        suffix2: TBD: use to specify e.g. ghsl, when main suffix is ghsl_urban? Not yet implemented

        This could be integrated with get_wide_flattened_TS_dataframes(table_subset_suffix=None ):---> TO DO 
        """
        table_subset_suffix = table_subset_suffix if table_subset_suffix is not None else 'ghsl_urban'
        assert rankby in ['recent', 'stock']
        if metric not in ['pca1', 'length_m_per_pop', 'length_m', 'log10length_m', 'fraction_deadend']:
            return

        if metric2 is None:
            metric2 = {'pca1': 'degree', 'length_m_per_pop': 'nodes_per_pop',
                       'length_m': 'pca1', 'fraction_deadend': 'fraction_1_3'}.get(metric,  'degree')
        def topbottom10(adf, N=10):
            adf = adf.copy()
            # Insert row of dots:
            adf.iloc[N,:] = r'$\vdots$'
            adf = adf.iloc[:(N+1)].append(adf.iloc[-N:]).reset_index(drop=True)
            assert len(adf)==2*N+1
            return adf

        dft = get_long_TS_dataset_countries_and_cities_and_regions(urban= 'urban' in table_subset_suffix,
                                                        density= 'density' in table_subset_suffix,
                                                        forceUpdate=False)#, from_db=True)

        _dfn, _dfc =  get_wide_flattened_TS_dataframes(table_subset_suffix= table_subset_suffix)

        
        # delete the metrics (but not the counts) for low-count rows:
        dft.loc[dft.n_nodes<=100, [metric,metric2]]= np.nan

        # Prepare to drop small countries: (214 rows --> 170 rows)
        dft.loc[(dft.Year=='Stock') & (dft.n_nodes<5000) & (dft.city==False), 'N(nodes)'] = '<5k'
        # Following two lines can be used to simplify syntax below; I wrote it after most of this section.
        isostocknodes = pd.DataFrame(dft.groupby('iso').apply(lambda adf: adf.query('Year=="Stock"').n_nodes.values[0]).rename('stock_n_nodes'))
        dft = dft.set_index('iso').join(isostocknodes).reset_index()
        # Whoops; that sets stock_n_nodes incorrectly for cities.
        citystocknodes = pd.DataFrame(dft.query('city==True').groupby('City').apply(lambda adf: adf.query('Year=="Stock"').n_nodes.values[0]).rename('stock_n_nodes'))
        dft.set_index('City', inplace=True)
        dft.update(citystocknodes)
        dft.reset_index(inplace=True)
        
        # Do above now so we can format the rest before doing pivot: [now not necessary since we have stock_n_nodes
        dft['N(nodes)'] = dft['N(nodes)'].map(human_format)         

        # Include any world regions in country lists? Just Earth for now:
        worldName = r'$\rightarrow${\sc Earth}$\leftarrow$'
        dft.loc[dft.wbr=='WLD', 'placename'] = worldName

        # Bold-ify names of big countries:
        bigc = (dft.pop2012>20e6) & (dft.city==False)
        dft.loc[bigc, 'placename'] = r'{\bf '+  dft.loc[bigc, 'placename'] +' }'



        ########################################################################
        #              XS
        ########################################################################

        ####################################
        # Make a stock-oriented table for XSSI
        ####################################
        showv = ['placename','pca1','degree','frc_length_noncycle', 'logcircuity_500_1000','N(nodes)']

        # Countries:
        isostock = dft.query('city==False').query('Year=="Stock"').dropna(subset=['iso']).sort_values('pca1', ascending=False)
        longt = isostock.query('n_nodes>=5000')[showv].rename( columns=metricNames)
        formatDFforLaTeX(longt, row=None, precision = 1, #sigdigs=3,
 colour=None, leadingZeros=False)
        print((longt.head()))
        dataframeWithLaTeXToTable(longt.rename(columns={'placename':''}),
                                  paths['output']+'tables/table-stock-countries-{}.tex'.format(defaults['server']['postgres_db']),
                                  formatString= 'l'+'r'*(len(longt.columns)-1))
                                  
        # Cities:
        cstock = dft.query('city==True').query('Year=="Stock"').sort_values('pca1', ascending=False) # Caution! stock_n_nodes is for countries
        #cstock['placename'] = cstock.City+' ('+cstock.ISO+')'
        clongt = cstock.query('n_nodes>=5000')[showv].rename( columns=metricNames).dropna(how='all',axis=1) # No population data.
        formatDFforLaTeX(clongt, row=None, precision = 1, #sigdigs=3,
 colour=None, leadingZeros=False)
        dataframeWithLaTeXToTable(clongt.rename(columns={'placename':''}),
                                  paths['output']+'tables/table-stock-cities-{}.tex'.format(defaults['server']['postgres_db']),
                                  formatString= 'l'+'r'*(len(clongt.columns)-1))

        
        ####################################        
        # Make a topbottom SHORT stock-oriented table for XS.
        ####################################        
        # Countries:
        shortt = isostock.query('n_nodes>=100000').query('pop2012>=10e6')[showv].rename( columns=metricNames)
        formatDFforLaTeX(shortt, row=None, precision = 1, #sigdigs=3,
 colour=None, leadingZeros=False)
        print((shortt.head()))
        dataframeWithLaTeXToTable(topbottom10(shortt).rename(columns={'placename':''}),
                                  paths['output']+'tables/table-stock-topbottom-countries-{}.tex'.format(defaults['server']['postgres_db']),
                                  formatString= 'l'+'r'*(len(shortt.columns)-1))
                                  

        # Cities:
        shortt = cstock.query('n_nodes>=3000')[showv].rename( columns=metricNames).dropna(how='all', axis=1)
        formatDFforLaTeX(shortt, row=None, precision = 1, #sigdigs=3,
 colour=None, leadingZeros=False)
        dataframeWithLaTeXToTable(topbottom10(shortt).rename(columns={'placename':''}),
                                  paths['output']+'tables/table-stock-topbottom-cities-{}.tex'.format(defaults['server']['postgres_db']),
                                  formatString= 'l'+'r'*(len(shortt.columns)-1))

        ########################################################################
        #              TS
        ########################################################################

        ####################################        
        # Countries: Make a recent-oriented table for TSSI.
        ####################################        
        crformatString='l||rrrr|r||rrrr|r||rrrr|r|'
        dfw = dft.query('city==False').query('Year!="-1"').dropna(subset=['iso']).sort_values('Year')[['placename', 'Year', metric, metric2, 'N(nodes)']].rename(
            columns=metricNames).reset_index().set_index('index').pivot('placename', 'Year')

        # Drop small countries: (214 rows --> 170 rows)
        dfw = dfw[dfw[('N(nodes)','Stock')] != '<5k']
        dfw.sort_values(('Sprawl (SNDi)','2014'), inplace=True, ascending=False)
        dfw.dropna(subset=[('Sprawl (SNDi)','2014')], inplace=True)
        formatDFforLaTeX(dfw, row=None,  precision=1, #sigdigs=3,
                         colour=None, leadingZeros=False)

        # Shorten columns names (second level of multiindex)
        gy = OrderedDict([(str(a),b) for a,b in list(GHSL_YEARS['years_short'].items())])
        dfw.columns = pd.MultiIndex.from_tuples([(a, gy[b]) for a, b in dfw.columns.values])

        dataframeWithLaTeXToTable(dfw.reset_index().rename(columns={'placename':''}),
                                  paths['output']+'tables/table-recent-countries-{}.tex'.format(defaults['server']['postgres_db']),
                                          formatString= crformatString)

        # Cities:
        vrformatString='l||rrr|r||rrr|r||rrr|r|'
        dfc = dft.query('city==True').query('iyear==1 or stockorchange=="citychanges" or Year=="Stock"').sort_values('pca1', ascending=False)[['placename','Year', metric, metric2, 'N(nodes)']].rename(  columns=metricNames).reset_index().set_index('index').pivot('placename', 'Year').dropna(subset = [('Sprawl (SNDi)',"'00-13")]).sort_values([('Sprawl (SNDi)',"'00-13")], ascending=False)
        # Reorder columns:
        dfc = dfc[[(a,b) for a in dfc.columns.levels[0] for b in ["$<$1990", "90-99", "'00-13", 'Stock']]]
        formatDFforLaTeX(dfc, row=None,  precision=1, #sigdigs=3,
                         colour=None, leadingZeros=False)
        dataframeWithLaTeXToTable(dfc.reset_index().rename(columns={'placename':''}),
                                  paths['output']+'tables/table-recent-cities-{}.tex'.format(defaults['server']['postgres_db']),
                                          formatString= vrformatString)

        
        ####################################        
        # Countries: Make a topbottom SHORT recent-oriented table for TSSI.
        ####################################

        #shortt = dfw.query('n_nodes>=100000').query('pop2012>=10e6')[showv].rename( columns=metricNames)
        
        dfws = dft.query('city==False').query('stock_n_nodes>=100000').query('Year!="-1"').dropna(subset=['iso']).sort_values('Year')[['placename', 'Year',  metric, 'N(nodes)']].rename(
            columns=metricNames).reset_index().set_index('index').pivot('placename', 'Year').reset_index()
        dfws.sort_values(('Sprawl (SNDi)','2014'), inplace=True, ascending=False)
        dfws.dropna(subset=[('Sprawl (SNDi)','2014')], inplace=True)
        formatDFforLaTeX(dfws, row=None, precision=1,#sigdigs=3,
                         colour=None, leadingZeros=False)

        # Drop nodeal degree:
        #dfws.drop(columns='Nodal degree', inplace=True)
        
        # How to get the columns i the right order? If I translate to strings above, pivot re-sorts the columns.
        # No. Just re-choose the columns as I've done below/later for cities: cleaner?   TO DO --> [ ]
        colnames = dfws.columns.values
        gy = OrderedDict([('','')]+[(str(a),b) for a,b in list(GHSL_YEARS['years_short'].items())])
        colnames = [(a, gy[b]) for a, b in colnames]
        dfws.columns = pd.MultiIndex.from_tuples(colnames)

        dfws = dfws.reset_index(drop=True).rename(columns={'placename':''})
        dataframeWithLaTeXToTable(topbottom10(dfws), 
                                  paths['output']+'tables/table-recent-topbottom-countries-{}.tex'.format(defaults['server']['postgres_db']),
                                  formatString= crformatString)

        # Cities
        dfc = dft.query('city==True').query('iyear==1 or stockorchange=="citychanges" or Year=="Stock"').sort_values('pca1', ascending=False)[['placename','Year', metric, 'N(nodes)']].rename(  columns=metricNames).reset_index().set_index('index').pivot('placename', 'Year').dropna(subset = [('Sprawl (SNDi)',"'00-13")]) # .query('stock_n_nodes>3000')

        dfc = dfc.loc[_dfc[_dfc['N(nodes)_00_13']>3000].index].sort_values([('Sprawl (SNDi)',"'00-13")], ascending=False)
        
        # Reorder columns:
        dfc = dfc[[(a,b) for a in dfc.columns.levels[0] for b in ["$<$1990", "90-99", "'00-13", 'Stock']]]
        formatDFforLaTeX(dfc, row=None,  precision=1, #sigdigs=3,
                         colour=None, leadingZeros=False)
        dataframeWithLaTeXToTable(topbottom10(dfc.reset_index().rename(columns={'placename':''})),
                                  paths['output']+'tables/table-recent-topbottom-cities-{}.tex'.format(defaults['server']['postgres_db']),
                                          formatString= vrformatString)


        
        return

    @staticmethod
    def _find_y_intercept_multi_line(x, y, y0):
        """ Given x, y vectors specifying a plot line, find any crossings of y=y0
        Plan: 
        """
        assert len(x) == len(y)
        x1, x2, y1, y2 = np.array(
            x[:-1]), np.array(x[1:]), np.array(y[:-1]), np.array(y[1:])
        xi = (y0-y1)*(x2-x1)/(y2-y1) + x1
        intersects = (xi >= x1) & (xi <= x2)
        yi = y1 + (xi-x1)*(y2-y1)/(x2-x1)
        xi, yi = xi[intersects], yi[intersects]
        if len(xi) > 0:
            return(xi[0], yi[0])
        return([], [])


    def _pnas_figure2_trends_plot_one_panel(self, regionNameDict, dfghsl, dfstock, colourdict, timevar='ghsl_yr', paddingFactor=.7, metric='pca1',  ax=None, fylim=None, subplots=True):
        """ Helper function for method that follows.
        wbrdict: lookup of WBR or ISO to text name/label, 
        dfghsl, dfstock: dataframes for the time series and stock
        colourdict: dict from WBR or ISO to colours
        matchon: 'wbregion' or 'ISO'. hm, not needed. It should be index of the dfs.
        fylim is useful as follows: call them all once without it, check the ylim, and then call them again with the ylim. that way each panel knows what final ylim will be.
    New plan: save the two ylims (left pair, right pair) when finished with figure. If they're different than what they're overwriting, need to run it all again.
        """
        if ax is None:
            ax = plt.gca()

        if colourdict in ['jet']:
            colourdict = cpblUtilities.color.getIndexedColormap(
                colourdict, len(regionNameDict))
        elif colourdict in ['auto']:
            # http://colorbrewer2.org/#type=qualitative&scheme=Dark2&n=5
            colourdict = discrete_colors().get(len(regionNameDict),
                                               cpblUtilities.color.getIndexedColormap('jet', len(regionNameDict)))

        if not isinstance(colourdict, dict):  # Choose order based on metric:
            colourdict = dict(list(zip(dfstock.loc[list(regionNameDict.keys())].sort_values(
                metric, ascending=False).index.values, colourdict)))
        th = []
        for wbcode, desc in list(regionNameDict.items()):
            desc = {'United States': 'USA',
                    'Russian Federation': 'Russia'}.get(desc, desc)
            if self.fig2_mode in ['pnasmain']:
                desc = desc.replace(r'\&', '\n'+r'\&').replace('European Union', 'E.U.').replace('North America',
                                                                                                 'North\nAmerica').replace('Low income', 'Low\nincome').replace('High income', 'High\nincome').replace('Middle income','Middle\nincome')
            df = dfghsl.loc[wbcode]
            ax.plot(df[timevar], df[metric], color=colourdict[wbcode], label=desc,
                    lw=self.fig2_linewidth, alpha=.7)
            dfs = dfstock.loc[wbcode]
            if subplots and not self.fig2_dots_for_stock:
                ax.plot((df[timevar].min(), df[timevar].max()), (dfs[metric], dfs[metric]), '--', color=colourdict[wbcode], label='_nolabel',   alpha=.5,
                        lw=1.5,
                        zorder=-8)
            if self.fig2_dots_for_stock:
                xi, yi = self._find_y_intercept_multi_line(
                    df[timevar], df[metric], dfs[metric])
                if xi:
                    ax.scatter(xi, yi, self.fig2_markersize,
                               colourdict[wbcode])

            tx, ty = (df[timevar].min()+df[timevar].max()) / \
                2, sorted(df[metric])[-2]  
            """  Try to do better than this:  For highest traces, I want the top point of the trace. For the lowest, I want the lowest point of the trace.  However, in principle one trace could be the highest and the lowest.

            """

            labelsbyhand = self.fig2_mode in ['pnasmain'] and (
                'EUU' in regionNameDict or 'LIC' in regionNameDict)
            if self.fig2_mode in ['pnasmain']:
                tx, ty = {'AFR': (2005, 3.3), 'EUU': (2010, 3.7), 'EAS': (2004, 4.6), 'SAS': (1985, 3.6), 'NAC': (1985, 5.3), 'LCN': (1990, 1.5), 'MEA': (2003, 2.2),
                          'HIC': (2005, 4.75), 'LIC': (2006, 3.2), 'MIC': (1982, 3.3), 'HPC': (2005, 2.7)}.get(wbcode, (tx, ty))
            print((wbcode, tx, ty))
            th += [ax.text(tx, ty,
                           desc, color=colourdict[wbcode], ha='center', va='center',
                           zorder=100 if labelsbyhand else 10,
                           size=self.fig2_labelfontsize if subplots else None,
                           bbox=dict(  # ec='None',
                               pad=0,
                               edgecolor="None",
                               alpha=.3 if labelsbyhand else .7,
                               facecolor="w") if subplots else None  # ec="0.8"
                           )]
        if fylim is not None and subplots:
            ax.set_ylim(fylim)  # Use limits generated by non-subplots version

        if not labelsbyhand:
            cpblUtilities.resolve_overlaps(
                th, ax=ax, shiftResolution=None, paddingFactor=paddingFactor, animate=False, verbose=False)
        xtv = df[timevar].unique().tolist()
        ghslyt = GHSL_YEARS['years_shortest'] if subplots else GHSL_YEARS['years']
        ax.set_xticks(xtv)
        ax.set_xlim([df[timevar].min(), df[timevar].max()])
        # How to label xticks?
        if timevar == 'ghsl_yr':
            plt.setp(ax.set_xticklabels([ghslyt[xx] for xx in df[timevar].unique()]),
                     'fontsize', self.fig2_xticklabel_fontsize if subplots else None)

        # Tuck in the left and right tick labels:
        xtl = ax.get_xticklabels()
        plt.setp(xtl[0], 'ha', 'left')
        plt.setp(xtl[-1], 'ha', 'right')
        if self.fig2_ticklength is not None:
            ax.tick_params(length=self.fig2_ticklength)


        return

    def _get_fig2_color_lookups(self, category, geognames):
        """ Provide object-level permanence and even disk-file permanence. However, will need to copy/link files across database subfolders in scratch. """
        assert isinstance(category, str) and hasattr(
            geognames, "__iter__")

        if not self.fig2geogcolors and os.path.exists(self.fig2geogcolors_filename):
            # Can only happen once per instantiation of this class
            self.fig2geogcolors = shelfLoad(self.fig2geogcolors_filename)
        thisd = dgetget(self.fig2geogcolors, [category], {})
        if thisd is None:
            thisd = {}
        # Reassign all colours for the passed geographics (though keep colors for old/dropped ones)
        if any([gn not in thisd for gn in geognames]):
            thisd.update(
                dict(list(zip(geognames, discrete_colors(n_categories=len(geognames))))))
            self.fig2geogcolors.update({category: thisd})
            shelfSave(self.fig2geogcolors_filename, self.fig2geogcolors)
        assert self.fig2geogcolors[category]
        return self.fig2geogcolors[category]

    @staticmethod
    def pnas_figure2_choose_ylims(metric, ylims):
        """ These figures have two pairs of linked yaxes. It's a bit complicated how to scale them, since it's okay for the thin grey lines in countries plot to be off scale.
        ylims is a dict, coming either from the observed figure or from saved values. The saved values are the ylims of the non-linked (subplots=False) version.
        """
        if 0:  # metric in ['degree','pca1','negdegree']:
            leftylim, rightylim = ylims['continentsWBR'], ylims['countries']
        else:
            leftylim = [min(ylims['continentsWBR'][0], ylims['developmentWBR'][0]),  max(
                ylims['continentsWBR'][1], ylims['developmentWBR'][1])]
            if 'cities' in ylims:
                rightylim = [min(ylims['countries'][0], ylims['cities'][0]),  max(
                    ylims['countries'][1], ylims['cities'][1])]
            else:
                rightylim = ylims['countries']
        return leftylim, rightylim

    def pnas_figure2_trends(self, metric='pca1', biggestN=10, steepN=0, subplots=True, table_subset_suffix=None):
        """
        Show trends of selected WBR regions (GHSL time), countries (GHSL time), and cities (Atlas time).
        We want to have colours assigned to each place be persistent across successive calls to this method, and ideally even successive instantiations of this class.

        When subplots=True, a four-panel figure is made (for the main text).

        yaxes are linked across subplots. Information from the previous run is saved in order to get the yaxis limits correct.
        The left two subplots have linked yaxes, and the right two have their own linked yaxes
        Alternatively, when self.fig2_mode is "pnasmain", we hardcode below the axis ylimits

        if subplots=False, then make a separate figure for each plot, and de-link yaxes, instead of putting four axes (subplots) in one figure. This is for presentations.

        TO DO: Use gallup world regions for first subplot, not World Bank weird ones. [never done]

        save left-pair ylim, right-pair ylim. Return True (thus, need to rerun this function in subplots=True mode) if new values are different than old(done?)
              Update: fylim is overwritten for countries, cities if metric=pca1 / fig2_mode=='pnasmain'

        """
        PCA1cityYLIM = [0,8.1]  # 201909: Use this for all axes if allfourlinked
        table_subset_suffix = 'ghsl_urban' if table_subset_suffix is None else table_subset_suffix
        figureFontSetup(uniform=self.fig2_figfontsize)
        filenamecore = 'trends-plot-{}-top{}-steep{}{}{}'.format(metric, biggestN, steepN,
                                                                 {'ghsl_urban': '', 'ghsl': '-includingNonUrban'}.get(
                                                                     table_subset_suffix, table_subset_suffix.replace('_', '')),
                                                                 '-fig2'*(self.fig2_mode == 'pnasmain'))

        outfn = paths['output']+'graphics/{}-{}.pdf'.format(filenamecore, defaults['server']['postgres_db'])
        # Load most recent fylims:
        # 'fig1_ylims_trends-plot-{}-top{}-steep{}.pyshelf'.format(metric, biggestN, steepN)
        fylims_file = paths['scratch']+filenamecore+'.pyshelf'
        if os.path.exists(fylims_file):
            ylimleft, ylimright = self.pnas_figure2_choose_ylims(
                metric, cpblUtilities.shelfLoad(fylims_file))
        else:
            ylimleft, ylimright = None, None
        if self.fig2_mode == 'pnasmain':
            assert metric in ['pca1']
            ylimleft, ylimright = [
                1.5835440000000001, 6.4729559999999999], [-0.42851473000000001, 7.1588921299999999]
            # TO DO: line above should use PCA1cityYLIM; maybe something below would become redundant.
            ylimleft, ylimright = [0, 8.05], [0, 8.05]

        figSize = (5.2, 3.35)  # aspect ratio same as below
        figSizeBig = (7, 4.5)  # for top-10 countries full column.
        figSizePage = (7, 8.75)  # for full-page figures
        if subplots:
            fig, axs = plt.subplots(
                1, 4,  figsize=osmt.figSizes['1.5col'] if self.fig2_mode == "pnasmain" else figSizeBig)  # figSizeBig)
            sharey = True,
            # How do I share pairwise? See https://stackoverflow.com/questions/42973223/how-share-x-axis-of-two-subplots-after-they-are-created
        else:
            figs, axs = list(zip(*[plt.subplots(1, figsize=(3.25, 2.5))
                              for ii in range(4)]))
        axs = OrderedDict(
            list(zip(['continentsWBR', 'developmentWBR', 'countries', 'cities'], axs)))

        # Geographic regions
        ax = axs['continentsWBR']
        dfwbr = pd.read_pickle(self.known_psql_tables[(self.known_psql_tables.GADMlevel == 'wbr') & (
            self.known_psql_tables.suffix == table_subset_suffix)].pdfile.values[0]).dropna(subset=['ghsl_yr']).sort_values('ghsl_yr').query('ghsl_yr>0')
        dfwbr_stock = pd.read_pickle(self.known_psql_tables[(self.known_psql_tables.GADMlevel == 'wbr') & (
            self.known_psql_tables.suffix == 'urban')].pdfile.values[0])
        # GroupCode is the WBgroup. WBcode is the ISO code.
        c2wb = country2WBregionLookup()
        wbrn = c2wb[['GroupName']].drop_duplicates()

        self._pnas_figure2_trends_plot_one_panel(continentsWBR, dfwbr.set_index('wbregion'), dfwbr_stock.set_index('wbregion'),
                                                    self._get_fig2_color_lookups(
                                                        'continentsWBR', list(continentsWBR.keys())),
                                                    ax=ax, metric=metric, fylim=ylimleft, subplots=subplots)
        if 0:
            ax.legend(bbox_to_anchor=(1, 0.5))
        # We want to hide the left-side y-axis labels for PNAS TS SI "variants" figure, with 3 robustness versions to compare with figure 2:
        if not (metric=='pca1' and (table_subset_suffix in 'ghsl' or 'cars' in defaults['server']['postgres_db'])): 
            ax.set_ylabel(smetrics.metricNames[metric])
        #toplefttext(ax,'A', dx=1.0/15)
        # Country (income) groups
        ax = axs['developmentWBR']
        # ax=plt.subplot(142)
        self._pnas_figure2_trends_plot_one_panel(developmentWBR, dfwbr.set_index('wbregion'), dfwbr_stock.set_index('wbregion'),
                                                    self._get_fig2_color_lookups(
                                                        'developmentWBR', list(developmentWBR.keys())),
                                                    ax=ax, metric=metric, fylim=ylimleft, subplots=subplots)
        ax.plot(dfwbr.query('wbregion=="WLD"').ghsl_yr,  dfwbr.query('wbregion=="WLD"')[
                metric], 'k', alpha=.7, lw=self.fig2_worldline_width, label='World')
        earthx, earthy = dfwbr.query('wbregion=="WLD"').ghsl_yr.max(
        ),  dfwbr.query('wbregion=="WLD"')[metric].max()
        if self.fig2_mode == "pnasmain":
            earthx, earthy = (2011, 3.9)
        ax.text(earthx, earthy, 'Earth', color='k',
                size=self.fig2_labelfontsize, ha='right')
        if 0:
            ax.legend(bbox_to_anchor=(0, 0.8))
        if not subplots:
            ax.set_ylabel(smetrics.metricNames[metric])
        #toplefttext(ax,'B', dx=1.0/15)

        
        ################# Large individual countries ##############################
        ax = axs['countries']  # ax=plt.subplot(143)
        df0 = self.has_sufficient_counts(self.get_ghsl_urban_with_integrated_country_data('iso')).sort_values(
            ['ghsl_yr']).sort_values('pop2014_millions_IEA', ascending=False).dropna(subset=['ghsl_yr']).query('ghsl_yr>0').set_index('iso')
        if metric not in df0:
            df0 = add_standard_derived_metrics(df0)
            assert metric in df0
            
        df0stock = pd.read_pickle(self.known_psql_tables[(self.known_psql_tables.GADMlevel == 'iso') & (
            self.known_psql_tables.suffix == 'urban')].pdfile.values[0]).set_index('iso')

        df0big = df0.query('pop2014_millions_IEA>1')
        # Choose biggest countries by population
        topc = df0big.index.unique()[:biggestN]
        hardcoded_topc = ['China', 'India', 'United States', 'Indonesia', 'Brazil', 'Pakistan', 'Nigeria', 'Bangladesh', 'Russian Federation', 'Japan', 'Mexico', 'Philippines', 'Ethiopia', 'Vietnam',
                          'Egypt, Arab Rep.', 'Germany', 'Iran, Islamic Rep.', 'Turkey', 'Congo, Dem. Rep.', 'Thailand', 'France', 'United Kingdom', 'Italy', 'South Africa', 'Myanmar', ]  

        if steepN:
            # Choose most extreme countries by trends:
            delta0 = df0big.groupby(df0big.index.name)[[vv for vv in df0big.columns if vv in list(metricNames.keys(
            ))]+['ghsl_yr']].apply(metrics_deltas).sort_values(metric).dropna(subset=[metric])
            delta0 = delta0.query('ghsl_yr>0')  # Had more than one year
            topc = np.unique(np.concatenate(
                [topc, delta0.index[:steepN], delta0.index[-steepN:]]))
        self._pnas_figure2_trends_plot_one_panel(df0big.loc[topc, 'CountryName'].to_dict(), df0big.sort_values('ghsl_yr'), df0stock,
                                                    self._get_fig2_color_lookups(
                                                        'iso', topc.values),
                                                    ax=ax, metric=metric, fylim=PCA1cityYLIM if metric=='pca1' else ylimright, subplots=subplots)
        if subplots:
            if self.fig2_mode !='pnasmain':
                ax.yaxis.set_label_position("right")
                ax.yaxis.set_ticks_position("right")
        else:
            ax.set_ylabel(smetrics.metricNames[metric])

        # Later, plot the grey other countries in the background. Don't do it now, as it affects the ylim.


        ######################################### Cities ###########################################################
        # Note: we don't have ls_pop-based metrics available for cities.
        ca = cities.city_atlas()
        stockdf, absdf = ca.load_connectivity(separate=True, duplicate_for_world_regions=False)
        if metric not in stockdf:  # We skip this panel for ls_pop-based metrics
            plt.setp(axs.pop('cities'), 'visible', False)
        else:
            ax = axs['cities']  # plt.subplot(144)

            # Again, choose largest and extreme trends:
            # Choose biggest cities by edge length, but only one per country
            aggdf = ca.load_aggregated()
            """
    osm10m=> select city,length_m,nnodes from cities_aggregated_10 where iyear=3 and stockorchange='cityedges' order by nnodes DESC LIMIT 20;
         city     | length_m | nnodes 
    --------------+----------+--------
     LosAngeles   | 64727463 | 284575
     Chicago      | 55237165 | 210323
     London       | 28033699 | 185830
     SaoPaulo     | 32243309 | 178997
     MexicoCity   | 31184687 | 175437
     Philadelphia | 41421535 | 162659
     Houston      | 40059310 | 161288
     BuenosAires  | 31894354 | 159126
     Paris        | 28993190 | 146423
     Istanbul     | 22193887 | 143366
     Milan        | 26152599 | 137474
     Hyderabad    | 15064681 | 136107
     Osaka        | 20862480 | 135582
     Bangkok      | 24739040 | 126041
     Seoul        | 25567550 | 117077
     Manila       | 16911821 | 111861
     Khartoum     | 14265062 | 105156
     Johannesburg | 24384275 |  97691
     Santiago     | 13280957 |  91619
     Riyadh       | 19275198 |  87550
    (20 rows)

    osm10m=> select city,length_m,nnodes from cities_aggregated_10 where iyear=3 and stockorchange='cityedges' order by length_m DESC limit 20;
         city     | length_m | nnodes 
    --------------+----------+--------
     LosAngeles   | 64727463 | 284575
     Chicago      | 55237165 | 210323
     Philadelphia | 41421535 | 162659
     Houston      | 40059310 | 161288
     SaoPaulo     | 32243309 | 178997
     BuenosAires  | 31894354 | 159126
     MexicoCity   | 31184687 | 175437
     Paris        | 28993190 | 146423
     Guangzhou    | 28327411 |  57206
     London       | 28033699 | 185830
     Milan        | 26152599 | 137474
     Seoul        | 25567550 | 117077
     Bangkok      | 24739040 | 126041
     Johannesburg | 24384275 |  97691
     Minneapolis  | 23709235 |  87320
     Istanbul     | 22193887 | 143366
     Beijing      | 21317779 |  36037
     Osaka        | 20862480 | 135582
     Shanghai     | 20439240 |  34395
     Moscow       | 19536518 |  52616
    (20 rows)

    """
            topc = stockdf.sort_values('n_nodes')[-biggestN:].City.values
            # No, by length (need to use aggregated table to get length):
            topc_df = aggdf[(aggdf.iyear == 3) & (aggdf.stockorchange == "cityedges")].sort_values(
                'length_m', ascending=False).groupby('iso').first().sort_values('length_m', ascending=False)[:10]
            topc = topc_df.City.values
            if PRIMARY_DB: 
                assert all(topc == [ 'Tokyo', 'New York', 'Guangzhou', 'Mexico City', 'Sao Paulo',
                                  'Buenos Aires', 'Paris', 'London', 'Bangkok', 'Seoul'])
                #['Tokyo', 'New York', 'Moscow', 'Paris', 'London', 'Seoul', 'Milan',  'Sao Paulo', 'Guangzhou', 'Mexico City'])
            else:
                topc = [ 'Tokyo', 'New York', 'Guangzhou', 'Mexico City', 'Sao Paulo',
                                  'Buenos Aires', 'Paris', 'London', 'Bangkok', 'Seoul']
                #['Tokyo', 'New York', 'Moscow', 'Paris', 'London', 'Seoul', 'Milan',   'Sao Paulo', 'Guangzhou', 'Mexico City']


            
            # Choose most extreme cities by trends:
            # Require considerable growth, e.g. 100 nodes per period:
            dfc = absdf[absdf.n_nodes > 100]
            if 1:
                print(' Largest cities:')
                print((stockdf[(stockdf.iyear == 3) & (
                    stockdf.stockorchange == "cityedges")].loc[topc][['pca1', 'degree']].reset_index()))

            # Could simply use deltadf, above, here?!
            if steepN:
                deltac = dfc.groupby(dfc.index.name)[[vv for vv in dfc.columns if vv in list(metricNames.keys(
                ))]+['iyear']].apply(metrics_deltas).sort_values(metric).dropna(subset=[metric])
                deltac = deltac.query('iyear>0')  # Had more than one year
                topc = np.unique(np.concatenate(
                    [topc, deltac.index[:steepN], deltac.index[-steepN:]]))
            dfc['cityname'] = dfc.index.values
            if 0:
                # 'Vancouver', ' Tehran',
                topc = ['Accra', 'Beijing', 'London', 'Dhaka']
            regionNameDict = dfc.loc[topc, 'cityname'].to_dict()
            self._pnas_figure2_trends_plot_one_panel(regionNameDict,
                                                     dfc.sort_values('iyear'), stockdf,
                                                        self._get_fig2_color_lookups(
                                                            'atlas', topc),
                                                        timevar='iyear',
                                                        paddingFactor=None, ax=ax, metric=metric, fylim= PCA1cityYLIM if metric=='pca1' else ylimright, subplots=subplots)
            if subplots and not self.fig2_mode =='pnasmain':
                ax.yaxis.set_label_position("right")
                ax.yaxis.set_ticks_position("right")
            if 0:
                # Not sure Pnas wants this label repeated
                ax.set_ylabel(smetrics.metricNames[metric])

            # plt.axis('tight')
            ax.set_xlim([1, 3])
            ca.set_iyear_xticklabels(
                ax, version='abs', data_are_real_years=False, fontsize=self.fig2_xticklabel_fontsize)
            #toplefttext(ax,'D', dx=1.0/15)

        # Now deal with axis limits

        # ############## Now: deal with yaxis reversal; plotting all countries in gray; A/B/C/D labels, and choosing ylim:
        verbose = defaults['verbose']
        if verbose:
            plt.show(), plt.draw(), input('Set direction:')
        if smetrics.metricSettings[metric]['direction'] == -1:
            # Caution below; if axes are shared an even number of times, this could undo the effect.
            [anax.invert_yaxis() for anax in list(axs.values())]
        if verbose:
            plt.show(), plt.draw(), input('Show grey')
        # Plot ALL >1M population countries in the background:
        # yl=ax.get_ylim()# Assumes cities has largest range
        bggrey = []
        ylc = axs['countries'].get_ylim()
        if 'cities' in axs:        
            ylc = axs['cities'].get_ylim()
        for ii, arow in df0big.sort_values(['ghsl_yr'])[['ghsl_yr', metric]].reset_index().groupby('iso'):
            bggrey += [axs['countries'].plot(arow.ghsl_yr, arow[metric], color='k', label='_nolabel_',
                                             lw=self.fig2_graycountries_linewidth, alpha=.05, zorder=-10, visible=False)]  # Start invisible, to not affect ylim
        plt.setp(bggrey, 'visible', True)
        if self.fig2_mode == "pnasmain":
            axs['countries'].set_ylim(ylc)
            if 'cities' in axs:
                axs['cities'].set_ylim(ylc)
        if verbose:
            plt.show(), plt.draw(), input('set ylim')
        if verbose:
            plt.show(), plt.draw(), input('add abd')
        for ii, aax in enumerate(axs.values()):
            if subplots:
                toplefttext(aax, 'ABCDEF'[
                            ii], dx=1.0/15, dy=1.0/40, size=self.fig2_ABCDfontsize, edgecolor='None')
        if subplots:
            fig.subplots_adjust(wspace=.09)
        if verbose:
            plt.show(), plt.draw()

        # Did the ylims change? ie should we rerun this function?
        newylims = dict([[kk, sorted(axs[kk].get_ylim())] for kk in (
            'continentsWBR', 'developmentWBR', 'countries', 'cities') if kk in axs])
        newleftylim, newrightylim = self.pnas_figure2_choose_ylims(
            metric, newylims)

        if subplots:
            ylimsChanged = newleftylim != ylimleft or newrightylim != ylimright
            axs['developmentWBR'].set_yticklabels([])
            axs['countries'].set_yticklabels([])
            axs['cities'].set_yticklabels([])
        if not subplots and 'cities' in axs:
            cpblUtilities.shelfSave(fylims_file, newylims)
            print(('Saved fylims for ', fylims_file, self.fig2_mode, metric, biggestN, steepN, subplots, table_subset_suffix))
            print(('     '+str(newylims)))

        if subplots:
            cpblUtilities.savefigall(
                outfn, pdf=True, png=False, dpi=fig.dpi, rv=False)
            plt.savefig(outfn.replace('.pdf', '.svg'), dpi=fig.dpi)
            print((' Saved '+outfn))
        else:
            for ii in range(4):
                plt.figure(figs[ii].number)
                cpblUtilities.savefigall(
                    outfn[:-4]+'-{}.pdf'.format(ii+1), dpi=figs[ii].dpi, pdf=True, png=False, rv=False)
        return False if not subplots else ylimsChanged

    def plot_trends_by_region(self, onlyRegions=None):
        """
        This can plot trends and stock for each region, but it also shows (faintly, behind) the trends of all countries in each region.
        Thus, we must load the country urban-GHSL data, the WBregion urban-GHSL data, and the WBregion urban stock data.

        Recall that each ISO is a member of several/many regions. So we don't want to incorporate regions ("GroupName") into  the ISO-means df.
        """
        assert onlyRegions is None  # Not implemented
        # Following should/could still be replaced by a method that can use either pandas OR psql depending on -f / availability...
        # Load countries time series, WBregions time series, and WBregions stock
        df0 = self.has_sufficient_counts(
            self.get_ghsl_urban_with_integrated_country_data('iso')).sort_values('ghsl_yr').dropna(subset=['ghsl_yr'])
        df0 = df0[df0.ghsl_yr > 0]
        dfwbr = pd.read_pickle(self.known_psql_tables[(self.known_psql_tables.GADMlevel == 'wbr') & (
            self.known_psql_tables.suffix == 'ghsl_urban')].pdfile.values[0]).dropna(subset=['ghsl_yr'])
        dfwbr = dfwbr[dfwbr.ghsl_yr > 0]
        dfwbr_stock = pd.read_pickle(self.known_psql_tables[(self.known_psql_tables.GADMlevel == 'wbr') & (
            self.known_psql_tables.suffix == 'urban')].pdfile.values[0])

        # GroupCode is the WBgroup. WBcode is the ISO code.
        c2wb = country2WBregionLookup()
        # Do not try to merge c2wb in now, since regions-to-ISOs is a many-to-one match in each direction

        ghsl_years = [yy for yy in sorted(
            df0.ghsl_yr.astype(int).unique()) if yy > 0]
        for adf in [df0, dfwbr, dfwbr_stock]:
            add_standard_derived_metrics(adf)

        for univar in [vv for vv in smetrics.sensible_metric_order(long_names=False, include_variance=False) if vv in df0]:
            print(('   Starting {}'.format(univar)))
            dfAll = df0[smetrics.get_keepmask(univar, df0[univar])]
            ylims = [dfAll[univar].min(), dfAll[univar].max()]

            for adict in multipage_plot_iterator([[a, b] for a, b in c2wb.groupby('GroupName')], filename=paths['graphics']+'world-regions-GHSLtrends-{}-{}'.format(str2pathname(univar), defaults['server']['postgres_db'])):
                grouptuple, ax, fig = adict['data'], adict['ax'], adict['fig']
                region, wblist = grouptuple
                GroupCode = wblist.index.unique()
                assert len(GroupCode) == 1
                GroupCode = GroupCode[0]

                print(('     Ready for {} ({})'.format(GroupCode, region)))

                dfr0 = df0[df0.WBcode.isin(wblist.WBcode.values)].sort_values([
                    'latest_ghsl_nodes', 'ghsl_yr'])
                dfr = dfwbr[dfwbr.wbregion == GroupCode].sort_values(
                    ['ghsl_yr']).set_index('ghsl_yr')
                dfrstock = dfwbr_stock[dfwbr_stock.wbregion == GroupCode]
                assert len(dfr) == len(ghsl_years)
                assert len(dfrstock) == 1

                if smetrics.metricSettings[univar]['direction'] == -1:
                    ax.invert_yaxis()
                groups = dfr0.dropna(subset=[univar]).groupby('ghsl_yr')[univar].aggregate(list)
                ax.boxplot(groups.values, positions = groups.index.values, 
                           widths=7, zorder=5)
                # Plot these ISOs another way, as individual lines:
                isoColours = cpblUtilities.color.getIndexedColormap(
                    'jet', dfr0.sort_values('latest_ghsl_nodes').iso.unique())
                nISOs = len(isoColours)
                ccounter = 0
                # The sort=False means leave the order
                for iso, adf in dfr0.sort_values('latest_ghsl_nodes').groupby('iso', sort=False):
                    tadf = adf.dropna(subset=['ghsl_yr', univar]).sort_values('ghsl_yr')
                    if tadf.empty: continue
                    hh = ax.plot(tadf['ghsl_yr'], tadf[univar], '-'+'-'*(
                        ccounter % 2 and nISOs > 7), color=isoColours[iso], label=iso, alpha=.1, zorder=1)
                    ax.text(tadf.iloc[-1].ghsl_yr, tadf.iloc[-1][univar], iso, size=2, color = isoColours[iso], ha='left', va='center')
                    if len(tadf)>1:
                        ax.text(tadf.iloc[0].ghsl_yr, tadf.iloc[0][univar], iso, size=2, color = isoColours[iso], ha='right', va='center')
                    ccounter += 1

                # And plot the 2017 stock:
                ax.plot([min(ghsl_years), max(ghsl_years)],
                        dfrstock[univar].values*[1, 1], 'k:', zorder=3, alpha=.5)
                ax.plot(dfr.index, dfr[univar], 'k.-', lw=3)
                ax.set_ylim(smetrics.get_plotlim(univar, ylims))
                ax.set_xticks(ghsl_years)
                if adict['bottom']:
                    labs = ax.set_xticklabels(
                        [str(int(yr)) for yr in ghsl_years])
                    labs = ax.set_xticklabels(
                        [GHSL_YEARS['years_shortest'][yr] for yr in ghsl_years])
                else:
                    ax.set_xticklabels([])
                # For labels that are too long, either split them at the first space after a critical length, or shrink the font size. Which? [There are now also shortnames possible in metrics_settings.py]
                txtsplit = '\n'.join(textwrap.wrap(
                    region.replace('_', ' ').replace('(', '\n('), 23))
                bottomlefttext(ax, str2latex(txtsplit),
                               size=7, alpha=0.5, color='c')
                if adict['left']:
                    ax.set_ylabel(metricNames[univar].replace('_', '-'))
                else:
                    ax.set_yticklabels([])
                ax.set_xlim([min(ghsl_years)-3.95, max(ghsl_years)+3.95])
                fig.subplots_adjust(wspace=.06, hspace=0.06)

            # Following no longer needed, but instead there's a kludge in the generator. Not pretty.
            #   multipage_plot_iterator.next()
            
    def plot_ts_scatterplots(self, mode='city', annotateall=True, showmarkers=False):
        """
        New figure: 1970 (cities) or 1975 (countries)  stock vs deltas for cities, maybe for countries too (filtered by new development)

        Something about growth rate in roads vs SNDi (scatterplot) by country or id_1
        This belongs in TSSI and may not be finished (2019-03)
        """
        from cpblUtilities.color import assignColormapEvenly, addColorbarNonImage

        import matplotlib
        matplotlib.rcParams.update({'font.size': 8})
        ca = cities.city_atlas()

        # Start over using long-form TS data:
        dft = get_long_TS_dataset_countries_and_cities_and_regions(urban= True,  density= False)

        # Cities:
        dfc = dft.query('city==True').query('iyear==1 or stockorchange=="citychanges" or Year=="Stock"').sort_values('pca1', ascending=False)[['placename','length_m', 'Year', 'pca1', 'N(nodes)']].reset_index().set_index('index').pivot('placename', 'Year').dropna(subset = [('pca1',"'00-13")])

        # Flatten columns:
        dfc['recentLength'] = dfc[('length_m',"'00-13")]
        dfc['recentPCA1']   = dfc[('pca1',"'00-13")]
        dfc['stockLength'] = dfc[('length_m','Stock')]
        dfc['stockPCA1']   = dfc[('pca1','Stock')]
        dfc['Growth rate'] = dfc.recentLength / ( dfc.stockLength - dfc.recentLength)
        dfc['oldPCA1'] = dfc[('pca1','$<$1990')]
        keepcols = ['placename', 'recentLength','stockLength','Growth rate', 'recentPCA1','stockPCA1','oldPCA1']
        dfc = pd.DataFrame(dfc.reset_index()[keepcols].values, columns=keepcols) # Reduce multiindex to scalar index




        dfw = dft.query('city==False').query('Year!="-1"').dropna(subset=['iso']).sort_values('Year')[['placename', 'length_m', 'Year', 'pca1', 'N(nodes)']].reset_index().set_index('index').pivot('placename', 'Year')


        # Flatten columns:
        dfw['recentLength'] = dfw[('length_m',"2014")]
        dfw['recentPCA1']   = dfw[('pca1',"2014")]
        dfw['stockLength'] = dfw[('length_m','Stock')]
        dfw['stockPCA1']   = dfw[('pca1','Stock')]
        dfw['Growth rate'] = dfw.recentLength / ( dfw.stockLength - dfw.recentLength)
        dfw['oldPCA1'] = dfw[('pca1','1975')]
        keepcols = ['placename', 'recentLength','stockLength','Growth rate', 'recentPCA1','stockPCA1','oldPCA1']
        dfw = pd.DataFrame(dfw.reset_index()[keepcols].values, columns=keepcols) # Reduce multiindex to scalar index

        dfw = dfw.merge(   dft[['iso','placename']].drop_duplicates() )#.set_index('iso', drop=False) # bring back iso through kludge

        
        if mode in ['city']:
            oldyear='1990'
            recentyear= ca.atlasyears_deltalabels[-1]
            sRecentYears = recentyear
            df=dfc
        else:
            oldyear=   str(list(GHSL_YEARS['years'].keys())[0])
            recentyear= GHSL_YEARS['years'][2014].replace('--','-') # pylab not TeX-rendering the -- 
            sRecentYears = recentyear
            df=dfw
            df=df.query('recentLength>100000') # Must have built 100 km recently
        def _msize(m):
            wth=2+ m*1.0/float(df.stockLength.min())
            if isinstance(wth, float):
                return np.log10(wth)*3 +.5
            return np.log10(list(wth))*3 +.5
            
        df['lkmr'] = (df.recentLength*1.0/1e3).map(np.log10)

        df[keepcols[2:]] = df[keepcols[2:]].astype(float) # Not sure why this is necessary

        xx=assignColormapEvenly('viridis', df.lkmr, asDict=False,missing=[1,1,1]) # For symbols
        xxbrg=assignColormapEvenly('brg', df.lkmr, asDict=False,missing=[1,1,1]) # For text
        df['color'] = df.lkmr.map(   xxbrg  )

        df['textlabel'] =  df.placename 

        df['recentLength_km'] = df.recentLength*1.0/1000
         
        #
        # First, SNDi vs growth 
        #
        dfwns = df.copy()
        fig,ax = plt.subplots(1, figsize = osmt.figSizes['2x2col'] ) 
        fig.subplots_adjust(right=.57, bottom=.47)
        hmarkers = ax.scatter(dfwns.recentLength*1.0/1000, dfwns.recentPCA1,  c = dfwns.color.values, visible=showmarkers, label='__nolabel__')# s= _msize(dfwns.stockPCA1.values))
        ax.set_xscale('log')
        ax.set_xlabel('Recent road stock growth (m, {})'.format(sRecentYears))
        ax.set_ylabel('SNDi ({})'.format(sRecentYears))

        res = dfOverplotLinFit(dfwns,'recentLength', 'recentPCA1',    ax = ax,
         label = '$R^2$={r2:.2g} $b$={b:.2g}$\pm${b2se:.2g}') 
        vtext(dfwns.recentLength, dfwns.recentPCA1,  dfwns.textlabel.str.split(',').str[0],  ax=ax, ha='center', va='center', size=3.5, color=dfwns.color.values)
        ax.set_xlim({'city':[1.5e4, 4e7], 'iso':[4e4, 1.1e9]}[mode])
        ax.legend(fontsize=5)
        fn=paths['graphics']+'TS-scatter-roadgrowth-SNDi-{mode}-{dbname}.pdf'.format(mode=mode, dbname=defaults['server']['postgres_db'])
        plt.savefig(fn)
        os.system('pdfcrop {fn} {fn}'.format(fn=fn))
        
        #         # Report fit results:
        fitdf = res['fitdf'].assign(region=mode).assign(model='Log recentLength')
        print(('\n\n   Here are the fits for {} level, for growth (not growth rate):'.format( mode)))
        print((fitdf.sort_index()))
        print('\n\n')
        fitdfs=[fitdf]

        #
        # And  SNDi vs growth rate
        #

        fig,ax = plt.subplots(1, figsize = osmt.figSizes['2x2col'] ) 
        fig.subplots_adjust(right=.57, bottom=.47)
        ax.scatter(dfwns['Growth rate'], dfwns.recentPCA1, s= _msize(dfwns.stockLength), c = dfwns.color.values, zorder = +30,
                   visible = showmarkers, label="__nolabel__")
        ax.set_xscale('log')
        ax.set_xlabel('Recent road stock growth rate ({})'.format(sRecentYears))
        ax.set_ylabel('SNDi ({})'.format(sRecentYears))
        res = overplotLinFit(        dfwns['Growth rate'].astype(float), dfwns.recentPCA1.astype(float),    ax = ax,
                                              label = '$R^2$={r2:.2g} $b$={b:.2g}$\pm${b2se:.2g}')
        vtext(dfwns['Growth rate'], dfwns.recentPCA1,   dfwns.textlabel.str.split(',').str[0],  ax=ax, ha='center', va='center', size=3.5, color=dfwns.color.values)

        ax.set_xlim({'city':[.016, 16], 'iso':[.011, 2.8]}[mode])
        
        #         # Report fit results:
        fitdf = res['fitdf'].assign(region=mode).assign(model='Log Growth rate')
        print(('\n\n   Here are the fits for {} level, for growth rate:'.format( mode)))
        print((fitdf.sort_index()))
        print('\n\n')
        fitdfs=[fitdf]

        gr = country_tools.get_Gallup_country_lookups()[['ISO','Region']].dropna(how='any').set_index('ISO')

        if mode=='iso': # Still need to merge in iso to cities, above?
            for greg,isos in gr.groupby('Region'):
                for RHS in ['Growth rate', 'recentLength']:
                    tmp = dfwns.set_index('iso').loc[isos.index].dropna()
                    res=overplotLinFit(tmp[RHS].astype(float), tmp['recentPCA1'].astype(float),    ax = ax, label='__nolabel__',)
                    # Hide these for now:
                    plt.setp([res['hmci'], res['hci'], res['hf']], 'visible', False)
                    fitdfs+= [res['fitdf'].assign(region=greg).assign(model=RHS)]
            print('   Here are the fits for ISO level, for growth rate:')
            print((pd.concat(fitdfs).sort_index()))
        

            ax.set_ylim([.5,10])
        ax.legend(fontsize=5)
        fn=paths['graphics']+'TS-scatter-roadgrowthrate-SNDi-{mode}-{dbname}.pdf'.format(mode=mode, dbname=defaults['server']['postgres_db'])
        plt.savefig(fn)
        os.system('pdfcrop {fn} {fn}'.format(fn=fn))



        
        #
        # Second, do persistence plot
        #
        df['color'] = df.lkmr.map(   xx  ) # Overwrite colormap used above
        
        fig,ax = plt.subplots(1, figsize = osmt.figSizes['2x2col'] ) # ergh: make it double size. Then use subplots_adjust to shrink from the right. 
        ax.scatter(df.oldPCA1, df.recentPCA1, s= _msize(df.stockLength), c=df.color,
                   alpha = .8, label='_nolabel_',
                   linewidth=0)
        fitd = dfOverplotLinFit(df, 'oldPCA1', 'recentPCA1', ax=ax, label='')
        print(('Here is a fit for persistence at {}'.format(mode)))
        print((fitd['fitdf']))
        print(('len(df)=={}'.format(len(df))))
        # Add some (hidden) sample sizes for legend:
        xl,yl = ax.get_xlim(), ax.get_ylim()
        h_l1= [ ax.scatter(-11,-10,  _msize(km*1000), c= xx(2), label = lab) for km,lab in [(100,'10$^2$ km'),]]
        h_l2= [ ax.scatter(-10,-10,  _msize(km*1000), c= xx(2), label = lab) for km,lab in [(10000,'10$^4$ km')]]
        if mode=='iso':
            # Only label a subset:
            toLabel = [#'URY','MRT', 'TCD', 'SOM','BTN','AFG', 'KEN', 'IRL',  'VIR']+[
                'CHN', 'IND', 'BGD', 'EGY', 'ETH', 'IDN', 'JPN', 'MEX', 'NGA', 'PAK', 'PHL', 'RUS', 'USA', 'VNM', 'BRA']
            
            itl = dfwns.iso.isin(toLabel)
            if 0:
                vtext(dfwns.loc[itl].oldPCA1, dfwns.loc[itl].recentPCA1-.07,  dfwns.loc[itl].textlabel,  ax=ax, ha='center', va='top', size=3.5 , zorder= -10, color=[.2,.2,.2])
            if annotateall: # Microfont labels for SI
                vtext(dfwns.loc[~itl].oldPCA1, dfwns.loc[~itl].recentPCA1-.07,  dfwns.loc[~itl].iso,  ax=ax, ha='center', va='top', size=.2, zorder= -10, color=[.2,.2,.2])
        if mode=='city':
            toLabel = ['New York', 'Sao Paulo', 'London', 'Mexico City', 'Moscow', 'Milan', 'Paris', 'Seoul', 'Guangzhou', 'Tokyo']
            toLabel = [ 'Tokyo', 'New York', 'Guangzhou', 'Mexico City', 'Sao Paulo', 'Buenos Aires', 'Paris', 'London', 'Bangkok', 'Seoul']
            
            df['textlabel' ] =  df.placename.str.split(r',').str[0]
            itl = df.textlabel.isin(toLabel)
            if 0: 
                vtext(df.loc[itl].oldPCA1, df.loc[itl].recentPCA1-.07,  df.loc[itl].textlabel,  ax=ax, ha='center', va='top', size=3.5, zorder= -10, color=[.2,.2,.2])
            if annotateall: # Microfont labels for SI            
                vtext(df.loc[~itl].oldPCA1, df.loc[~itl].recentPCA1-.07,  df.loc[~itl].placename.str.replace(r'\\footnotesize',''),  ax=ax, ha='center', va='top', size=.8, zorder= -10, color=[.2,.2,.2])

        ax.set_xlim(xl), ax.set_ylim(yl)
        if 0 and mode=='iso':
            ax.set_xlim([-12,8])
            ax.set_ylim([-12,12])
            plt.show()
            hohoho
        ax.legend(title = oldyear+' road stock', loc='lower right')
        # Add colorbar:
        fig.subplots_adjust(right=.57, bottom=.47)        
        cbh = addColorbarNonImage(xx, df.lkmr.dropna().values)#, aspect = 10)
        cbax = plt.gca()
        # This next trick is sad, but because the colorbar yticklabels are already custom, I don't have an exponentiate option, apparently : 
        yt = [float(h.get_text().strip('$')) for h in cbax.get_yticklabels()]
        ytl = ['' if not int(t)==t else '10$^{}$'.format(int(t))   for t in yt]
        cbax.set_yticklabels(ytl)
        if mode=='city': 
            cbax.set_ylabel('Recently-built km')
        # Add text:
        ax.set_xlabel('SNDi ({} stock)'.format(oldyear))
        if mode=='iso':
            ax.set_ylabel('SNDi ({})'.format('2000--2013/14'))
        ax.set_ylim( [0, ax.get_ylim()[1]])
        plot_diagonal(df.oldPCA1, df.recentPCA1, ax=ax, color = '0.5', zorder =-10)
        toplefttext(ax, {'city':'B (cities)', 'iso':'A (countries)'}[mode], frameon=False)
        fn=paths['graphics']+'TS-scatter-persistence-{mode}-{annotated}{dbname}.pdf'.format(mode=mode,
                                                                                            annotated= annotateall*'microlabels-',
                                                                                            dbname=defaults['server']['postgres_db'])
        plt.savefig(fn)
        os.system('pdfcrop {fn} {fn}'.format(fn=fn))

        
    def plot_trends_by_country(self):

        df0 = self.has_sufficient_counts(
            self.get_ghsl_urban_with_integrated_country_data('iso')).sort_values('ghsl_yr')
        df1 = self.has_sufficient_counts(
            self.get_ghsl_urban_with_integrated_country_data('id_1')).sort_values('ghsl_yr')
        df0stock = pd.read_pickle(self.known_psql_tables[(self.known_psql_tables.GADMlevel == 'iso') & (
            self.known_psql_tables.suffix == 'urban')].pdfile.values[0])

        df0 = df0.dropna(subset=['ghsl_yr'])
        df1 = df1.dropna(subset=['ghsl_yr'])

        c2wb = country2WBregionLookup()
        for dd in [df0, df1, df0stock]:
            add_standard_derived_metrics(dd)

        # ['Nodal degree']:#
        for univar in [vv for vv in smetrics.sensible_metric_order(include_variance=False) if vv in df0]:
            dfAll = df0[(df0.ghsl_yr > 0) & (
                smetrics.get_keepmask(univar, df0[univar]))]
            ylims = [dfAll[univar].min(), dfAll[univar].max()]
            ghsl_years = [yy for yy in sorted(
                df0.ghsl_yr.astype(int).unique()) if not yy == -1]
            # Prepare a list of the subsets in advance, so that we screen out those which are too small etc, before laying out the axes. Sort countries by number of nodes
            items = [[iso,
                      df0[(df0.iso == iso) & (df0.ghsl_yr > 0)][[
                          'CountryName', 'ghsl_yr', univar, ]],  # This will be adf0
                      df1[(df1.iso == iso) & (df1.ghsl_yr > 0)][['CountryName', 'ghsl_yr', univar, 'id_1']]]  # This will be adf1
                     for iso in df0.sort_values('n_nodes', ascending=False).iso.unique()]

            items = [[a, b, c] for a, b, c in items if not b.empty]
            allmin, allmax = np.inf, -np.inf
            for adict in multipage_plot_iterator(items, filename=paths['graphics']+'countries-GHSLtrends-{}-{}'.format(str2pathname(univar), defaults['server']['postgres_db'])):
                isotuple, ax, fig = adict['data'], adict['ax'], adict['fig']
                iso, adf0, adf1 = isotuple
                if adf1.empty:
                    print((' No id1 for '+iso))
                else:
                    # Various compact code here fails for cases when some or *all* values in a year are nan. So here is a more pedantic coding:
                    yrs = adf1.ghsl_yr.unique()
                    mins = [np.nanmin(adf1[adf1.ghsl_yr == ayr]
                                      [univar].values) for ayr in yrs]
                    maxs = [np.nanmax(adf1[adf1.ghsl_yr == ayr]
                                      [univar].values) for ayr in yrs]
                    allmin = min([allmin, min(mins)])
                    allmax = max([allmax, max(maxs)])
                    if pd.isnull(mins).any():
                        print((' Found NaN values for {}'.format(iso)))
                        print((dict(list(zip(yrs, mins)))))
                    ax.fill_between(yrs, mins, maxs,
                                    color='c',
                                    zorder=-8, alpha=.1)

                ax.plot(adf0.ghsl_yr, adf0[univar], 'k.-', lw=3)

                groups = adf1.dropna(subset=[univar]).groupby('ghsl_yr')[univar].aggregate(list)
                ax.boxplot(groups.values, positions = groups.index.values,
                           widths=7)
                # And plot the 2017 stock:
                ax.plot([min(ghsl_years), max(ghsl_years)], df0stock[df0stock.iso ==
                                                                     iso][univar].values*[1, 1], 'k:', zorder=3, alpha=.5)

                ax.set_xlim([min(ghsl_years)-1.95, max(ghsl_years)+1.95])
                ax.set_xticks(ghsl_years)
                if adict['bottom']:
                    labs = ax.set_xticklabels(
                        [GHSL_YEARS['years_shortest'][yr] for yr in ghsl_years])
                else:
                    ax.set_xticklabels([])
                if adict['left']:
                    ax.set_ylabel(metricNames[univar].replace('_', '-'))
                else:
                    ax.set_yticklabels([])
                ax.set_ylim(smetrics.get_plotlim(univar, ylims))
                if smetrics.metricSettings[univar]['direction'] == -1:
                    ax.invert_yaxis()
                fig.subplots_adjust(wspace=.06, hspace=0.06)
                bottomlefttext(
                    ax, adf0.CountryName.iloc[0], size=8, alpha=.5, color='m')
            print((
                ' Min/max overall for plots of {} are: {} / {}'.format(univar, allmin, allmax)))

    def has_sufficient_counts(self, df):
        return(df[df.n_nodes > self.min_nodes])



    def get_ghsl_urban_variant(self, gadmlevel='iso', suffix=None, forceUpdate=False, from_db=None):
        """
        This should replace function that follows. Forget mergedplanet for now, but we still want to merge the variants into one df, so as to have simpler data files/structure 
        """
        if suffix is None:
            suffix = 'ghsl_urban'
        if from_db is None: from_db = defaults['USE_DB']

        if from_db:
            db = osmt.pgisConnection(verbose=True)
            table= known_psql_tables().query('GADMlevel=="{}" and suffix=="{}"'.format(gadmlevel, suffix))
            assert len(table)==1
            df  = db.db2df(table.iloc[0].psqltable )
            df['ghsl'] = 'ghsl' in suffix
            print(('\nUsed DATABASE for {}'.format(table.iloc[0].psqltable )))

        else:

            files = self.known_psql_tables[self.known_psql_tables.GADMlevel == gadmlevel]
            # This is really for debugging only; in principal there could be more or less than four, depending on settings
            assert len(files) in [4,5]

            # Quick kludge: focus for a moment only on ghsl/urban:
            files = files[files.suffix == suffix]
            planetdf = pd.read_pickle(files.pdfile.tolist()[0])
            planetdf['ghsl'] = 'ghsl' in suffix


            if 0:  # Reinstate this stuff when we're concat'ing:!
                # So this contains both those with ghsl and density categories, and the pooled ones without
                planetdf = pd.concat(dfs)

                # Now mark the all-years and all-densities as such:
                planetdf.density_decile = planetdf.density_decile.fillna(-1)
                planetdf.ghsl_yr = planetdf.ghsl_yr.fillna(-1).astype(int)
                planetdf['ghsl'] = planetdf.ghsl_yr != -1
                # Check for consistency. .to_string() makes it wide display.
                print((pd.crosstab(planetdf.ghsl_yr,
                                  planetdf.density_decile).to_string()))
            df=planetdf
            
        if 'degree' not in df.columns:
            df['degree'] = -df.negdegree
        return df


    def get_ghsl_urban_with_integrated_country_data(self, gadmlevel='iso', suffix=None, from_db=None):
        """
        Recall that each ISO is a member of several/many regions. So we don't want to incorporate regions ("GroupName") into  the ISO-means df.
        Instead, use the country data and the c2wb lookup when you want to get a subset df for a particular region.
        Here, c2wb is only used to get the country name.
        """
        if from_db is None: from_db = defaults['USE_DB']
        # Bring in other country data:
        wdf = self.get_ghsl_urban_variant(gadmlevel, suffix=suffix, from_db=from_db)
        df = add_country_data_to_df_by_iso(wdf)

        return(df)

    def reportKeyStats(self):
        """
        Reports some key pieces of data that we cite in the text
        Prints the output and creates the input file for Lyx

        """
        # Let's replace all get_ghsl_urban .... calls with get_long_TS_dataset_countries_and_cities_and_regions TOD O ---> [ ]
        df0 =  self.has_sufficient_counts(self.get_ghsl_urban_with_integrated_country_data('iso', 'ghsl_urban')).sort_values(
            ['ghsl_yr']).sort_values('pop2014_millions_IEA', ascending=False).dropna(subset=['ghsl_yr']).query('ghsl_yr>0').set_index('iso')

        df0big = df0.query('pop2014_millions_IEA>1')
        # Choose biggest countries by population

        import datetime

        outFn = '{opath}keystats{dbname}.tex'.format(
            opath=paths['output'], dbname=defaults['server']['postgres_db'])
        outF = open(outFn, 'w')

        def outtxt(txt):
            print (txt)
            outF.writelines(str(txt)+'\n')

        def commentstr(txt):
            return('% '+str(txt).replace('\n', '\n% '))

        outtxt('%% Report generated at %s\n' % str(datetime.datetime.now()))
        # Following makes mistakes when there are NaNs present for some years.
        pcabyyr = pd.pivot_table(df0big.reset_index()[
                                 ['iso', 'pca1', 'ghsl_yr']],    values='pca1', index='iso', columns='ghsl_yr')
        pca_change = (pcabyyr[2014] - pcabyyr[1975]).dropna()
        fraction_worse = (pca_change > 0).sum()*1.0/len(pca_change)
        fraction_signif_worse = (pca_change > .2).sum()*1.0/len(pca_change)
        print(fraction_signif_worse)
        fraction_recently_improving = (
            pcabyyr[2014] < pcabyyr[2000]).dropna().sum()*1.0/len(pca_change)
        outtxt(r"\newcommand\fractionPopulousCountriesUrbanIncreasingSprawl{%.0f\%%\xspace}" % (
            fraction_worse*100))
        outtxt(r"\newcommand\fractionPopulousCountriesRecentlyImproving{%.0f\%%\xspace}" % (
            fraction_recently_improving*100))
        outtxt(r"\newcommand\numberPopulousCountries{%d\xspace}" % (
            len(pca_change)))

        # Also get the "world" values:
        dfwbr = self.get_ghsl_urban_variant('wbr', 'ghsl_urban').dropna(subset=['ghsl_yr']).sort_values('ghsl_yr').query('ghsl_yr>0')
        dfwbr_stock = self.get_ghsl_urban_variant('wbr', 'urban')

        dfw = dfwbr[dfwbr.wbregion == 'WLD'].sort_values(
            'ghsl_yr').set_index('ghsl_yr')

        dfw[['fraction_deadend', 'fraction_1_3', 'degree', 'pca1', 'pca2']]

        """

The last four decades have seen a striking global trend towards increased street-network
sprawl, as reflected in the rightward shift of the distributions of SNDi (Figure 1 inset). More-
over, new development in almost all geographic regions shows a marked rise in SNDi, at least
until the turn of the century (Figure 2A). Overall (Figure 2B grey line), street-network sprawl
rose during the first two time periods and flattened out in the most recent one. This corresponds
to a 20% decrease in the global fraction of urban intersections with degree-4+, and a 26% in-
crease in the fraction of dead-ends (see SI).
        """
        df = dfwbr[dfwbr.wbregion == 'WLD'][['ghsl_yr', 'fraction_deadend',
                                             'fraction_1_3', 'pca1', 'degree']].set_index('ghsl_yr')
        growth_frc_dead = (df.loc[2014, 'fraction_deadend']-df.loc[1975,
                                                                   'fraction_deadend'])/df.loc[1975, 'fraction_deadend']
        growth_4plus = -(df.loc[2014, 'fraction_1_3']-df.loc[1975,
                                                             'fraction_1_3'])/(1-df.loc[1975, 'fraction_1_3'])
        outtxt(r"\newcommand\fractionIncreaseFrcDeadends{%.0f\%%\xspace}" % (
            growth_frc_dead*100))
        outtxt(
            r"\newcommand\fractionDecreaseFrcFourplus{%.0f\%%\xspace}" % (-growth_4plus*100))
        outtxt(r"\newcommand\frcDeadendsGlobalInitial{%.1f\%%\xspace}" % (
            df.loc[1975, 'fraction_deadend']*100))
        outtxt(r"\newcommand\frcDeadendsGlobalFinal{%.1f\%%\xspace}" % (
            df.loc[2014, 'fraction_deadend']*100))
        outtxt(r"\newcommand\frcFourPlusGlobalInitial{%.1f\%%\xspace}" % (
            100-df.loc[1975, 'fraction_1_3']*100))
        outtxt(r"\newcommand\frcFourPlusGlobalFinal{%.1f\%%\xspace}" % (
            100-df.loc[2014, 'fraction_1_3']*100))


        corrdf = compare_nodal_degree_by_world_region()
        outtxt(r"\newcommand\USADegreeCorrSNDI{{{:.2f}\xspace}}".format( corrdf.query('scale=="id_5"').loc['Northern America']['R']))
        outtxt(r"\newcommand\NAmericaDegreeCorrSNDI{{{:.2f}\xspace}}".format( corrdf.query('scale=="id_5"').loc['USA']['R']))
        outtxt(r"\newcommand\WEuropeDegreeCorrSNDI{{{:.2f}\xspace}}".format( corrdf.query('scale=="id_5"').loc['Western Europe']['R']))
        outtxt(r"\newcommand\GermanyDegreeCorrSNDI{{{:.2f}\xspace}}".format( corrdf.query('scale=="id_5"').loc['DEU']['R']))



        # Fit OLS to persistence data: countries and cities, PCA1 and nodes:
        dft = get_long_TS_dataset_countries_and_cities_and_regions(urban= True,  density= False)
        _dfn, _dfc =  get_wide_flattened_TS_dataframes(table_subset_suffix= 'ghsl_urban')#table_subset_suffix)
        from cpblUtilities.mathgraph import dfOLS
        # Countries:        
        pcaISO = dfOLS(_dfn,  'pca1_2014', 'pca1_1975',)
        degreeISO = dfOLS(_dfn, 'degree_2014', 'degree_1975', )
        circuityISO = dfOLS(_dfn,  'logcircuity_500_1000_2014', 'logcircuity_500_1000_1975',)


        # Cities:
        dfwc = dft.query('city==True').query('stockorchange!="citychanges" or Year!="90-99"').sort_values('Year')[['placename', 'Year', 'pca1', 'degree',  'logcircuity_500_1000', 'N(nodes)', 'pop2012',]].reset_index().set_index('index').pivot('placename', 'Year')

        # Calculate also mean change for each country / variable:
        ct1,ct2 = ('1990', "00_13")
        nt1,nt2 = ('1975', "2014")
        for nn in ['pca1', 'degree', 'logcircuity_500_1000']:
            _dfc['d_'+nn] =  _dfc[nn+'_'+ct2] - _dfc[nn+'_'+ct1]
            _dfn['d_'+nn] =  _dfn[nn+'_'+nt2] - _dfn[nn+'_'+nt1]
        deltaC = _dfc[[c for c in _dfc.columns if c.startswith('d_')]]
        deltaN = _dfn[[n for n in _dfn.columns if n.startswith('d_')]]
        from cpblUtilities.pandas_utils import weightedMeanSE_pandas,         flatten_multiindex_columns
        muc= weightedMeanSE_pandas(deltaC, weightVar=None)
        mun= weightedMeanSE_pandas(deltaN, weightVar=None)
        # Collect into DF:
        mudf=pd.DataFrame([(metric, scope, xdf['d_'+metric], xdf['se_d_'+metric],)  for metric in ['pca1', 'degree', 'logcircuity_500_1000'] for scope,xdf in [('Cities', muc), ('Countries',mun)]],
                          columns = ('Metric', 'Scale','mub','muse'))
        formatDFforLaTeX(mudf, row=None, sigdigs=3, colour=None, leadingZeros=False)
        mudf['Metric'] =mudf['Metric'].apply(lambda ss: metricNames[ss]).replace({'Sprawl (SNDi)':'SNDi', 'Log Circuity (0.5--1km)': 'Log circuity (0.5--1km)'})
        
        
        dfc = dfwc.copy()
        remap_cols = ['c{}'.format(i) for i in range(len(dfc.columns))] # Flatten multiindex
        dfc.columns = remap_cols
        xx=weightedMeanSE_pandas(dfc, weightVar=None, as_df=True).loc[remap_cols]
        xx.index=dfwc.columns
        xx = xx.reset_index().set_index(['level_0', 'Year'])



        pcaCity = dfOLS(dfwc, ('pca1', "'00-13"), ('pca1','$<$1990'), )
        degreeCity = dfOLS(dfwc, ('degree',"'00-13"), ('degree', '$<$1990'), )
        circuityCity = dfOLS(dfwc, ( 'logcircuity_500_1000', "'00-13"), ( 'logcircuity_500_1000',  '$<$1990'), )
        # In fact, make a little table of these coefs:
        L = [pcaISO, degreeISO, circuityISO,  pcaCity, degreeCity, circuityCity]
        print([len(a) for a in L])
        olsdf = pd.DataFrame(dict(
                                  b0 = [r['b'][0] for r in L],
                                  b = [r['b'][1] for r in L],
                                  se0 =[r['se'][0]*1.96 for r in L],
                                  se = [r['se'][1]*1.96 for r in L],),
                                  index = pd.MultiIndex.from_tuples([('SNDi','Countries'), ('Nodal degree','Countries'), ('Log circuity (0.5--1km)', 'Countries'), ('SNDi','Cities'), ('Nodal degree', 'Cities'), ('Log circuity (0.5--1km)', 'Cities')]))
        # Format this table:
        olsdf.index.names=('Metric','Scale')
        formatDFforLaTeX(olsdf, row=None, sigdigs=3, colour=None, leadingZeros=False)

        olsdf = olsdf.join(mudf.set_index(['Metric','Scale']))
        
        #olsdf['Intercept'] = olsdf['b0']+'$\pm$'+olsdf['se0']
        olsdf[r'Shift'] = olsdf['mub']+'$\pm$'+olsdf['muse']
        olsdf['Persistence'] = olsdf['b']+'$\pm$'+olsdf['se']
        olsdf = olsdf.reset_index().sort_values(['Metric', 'Scale'])[['Metric', 'Scale',r'Shift','Persistence']].reset_index(drop=True)

        for ii,adf in olsdf.reset_index().iterrows():
            outtxt(r"\newcommand\persistenceCoefficient"+adf.Scale+adf.Metric.split('(')[0].strip().replace(' ','')+'{'+adf.Persistence+r'\xspace}')
            outtxt(r"\newcommand\persistenceMeanShift"+adf.Scale+adf.Metric.split('(')[0].strip().replace(' ','').replace('(','').replace(')','').replace('','')+'{'+adf[r'Shift']+r'\xspace}')
        outtxt(   analyse_street_network_paradigms( keystats=True) )

        olsdf.loc[(1,3,5), 'Metric'] = ''
        dataframeWithLaTeXToTable(olsdf,
                                  paths['output']+'tables/persistence-coefficients-{}.tex'.format(defaults['server']['postgres_db']),
                                  formatString= 'rrrr')
        
        # From DB: total length of all roads (including non-urban)
        assert  len( defaults['osm']['clusterRadii'])==1
        db = osmt.pgisConnection(verbose=True)
        outs = db.execfetch("""
        select sum(length_m)/1e9 from aggregated_iso_planet_{radius};  
        """.format(radius = defaults['osm']['clusterRadii'][0]))
        outtxt(r"\newcommand\planetRoadLengthGmExclnocars{{{:.0f}\xspace}}".format(float(outs[0][0])))
        
        outs = db.execfetch("""
        select sum(length_m)/1e9 from edges_planet_{radius};  
        """.format(radius = defaults['osm']['clusterRadii'][0]))
        outtxt(r"\newcommand\planetRoadLengthGmInclnocars{{{:.0f}\xspace}}".format(float(outs[0][0])))
        
        outs = db.execfetch("""
        SELECT COUNT(*)::real/1e6 FROM nodes_planet_{radius};  
        """.format(radius = defaults['osm']['clusterRadii'][0]))
        outtxt(r"\newcommand\nNodesMinclnocars{{{:.0f}\xspace}}".format(float(outs[0][0])))
                
        outs = db.execfetch("""
        SELECT COUNT(*)::real/1e6 FROM edges_planet_{radius};  
        """.format(radius = defaults['osm']['clusterRadii'][0]))
        outtxt(r"\newcommand\nEdgesMinclnocars{{{:.0f}\xspace}}".format(float(outs[0][0])))
        
        # Moved from clean_figures.py: reports key stats on kmeans
        nUrban = db.execfetch('SELECT COUNT(*)::real/1e6 FROM aggregated_by_cell_{} WHERE urban'.format(defaults['osm']['clusterRadii'][0]))[0][0]
        outtxt(r"\newcommand\nUrbanGridCellsM{{{:.1f}\xspace}}".format(float(nUrban)))

        # Report dropped clusters
        colsToUse = kmeans.kmDefaultCols
        km = kmeans.kmeans_analyzer(nClusters=kmeans.defaultK, columns=[cc for cc in colsToUse], byCell=True,clusterradius= defaults['osm']['clusterRadius'], dropClusters=kmeans.kmDefaultDrop,sortCol='pca1')
        km.postgres2clusters()
        pcDrop = km.n_nodes[km.df.cluster.isin(kmeans.kmDefaultDrop)].sum()*100./km.n_nodes.sum()
        outtxt(r"\newcommand\pcNodesDroppedClusters{{{:.1f}\xspace}}".format(float(pcDrop)))


        # Report PCA1 for street-network paradigms
        # See ./run_archetypes_using_planet_PCA.sh  for generating these values.


        
        outF.close()
        print(('Wrote {}'.format(outFn)))
        return


                

def copy_pandas_and_psql_and_output_to_local_server():
    # Get pandas files in scratch and working (assuming folders are the same across machines!)
    for spec, path in [['*.pandas', paths['working']],
                       ['*.pandas', paths['scratch']],
                       ['beyond_gadm.tsv', paths['scratch']],                       
                       ['*.pyshelf', paths['working']],
                       ['for_pca', paths['scratch']],
                       ['*pdf', paths['graphics']],
                       ['*png', paths['graphics']],
                       ['*.tex', paths['output']+'tables/']]:
        print((
            ' ------------- Copying  remote {path}{spec}  --> local {path} ------------'.format(path=path, spec=spec)))
        oscommand = 'rsync  --perms --safe-links -vzr -e ssh sprawl.research.mcgill.ca:{path}{spec} {path}/'.format(
            path=path, spec=spec)
        print(('       '+oscommand))
        os.system(oscommand)

    # Next, copy postgres tables too! AFTER DELETING THE LOCAL VERSIONS!!
    tablelist = []
    dbname = defaults['server']['postgres_db']
    for radius in defaults['osm']['clusterRadii']:
        tablelist += ['cities_disconnectivity_{} '.format(radius)]
        for continent in defaults['osm']['continents']:
            for gadmlevel in ['wbr']+ defaults['gadm']['ID_LEVELS'][:2]:
                for _suffix in ['', '_urban', '_density_ghsl', '_ghsl_urban']:
                    tablelist += ['disconnectivity_{}_{}_{}{} '.format(
                        gadmlevel, continent, radius, _suffix)]
    print(('   About to copy the following tables in {} from one server to another. \n{}\n Confirm servers:'.format(
        dbname,  tablelist)))
    assert input('   FROM server: port 9432\n    TO server: port 5432\n  You should initiate the ssh tunnel now.  You should also have already created locally (using server_setup add_db) the {} database, which is to receive the tables.   Okay?'.format(
        dbname)).lower() in ['y', 'yes']
    print('   Testing localhost (press ctrl-D)...')
    os.system('psql  -p 5432 ' +
              dbname)  # 'cd ~postgres && sudo su postgres  -c "psql -d postgres -c ';' "  """)
    print(
        '   Testing remote host [ensure ssh tunnel is set up!] (press ctrl-D)...')
    os.system('psql -h localhost  -p 9432 ' +
              dbname)  # 'cd ~postgres && sudo su postgres  -c "psql -d postgres -c ';' "  """)
    assert input('   Did they both connect?').lower() in ['y', 'yes']
    cur = psqlt.dbConnection(verbose=True,  host='local', db=dbname)
    for tl in tablelist:
        if 0:
            tablelist += [' -t disconnectivity_{}_{}_{}{} '.format(
                gadmlevel, continent, radius, suffix)]
        print(('  About to DELETE local {} ...'.format(tl)))
        ##oscommand = " psql {} -h localhost -p 5432 -c 'DROP TABLE IF EXISTS {} CASCADE; ' ".format(dbname, tl)
        cur.execute('DROP TABLE IF EXISTS {} CASCADE; '.format(tl))
        # os.system(oscommand)
        print(('  About to COPY {} from remote ...'.format(tl)))
        oscommand = 'pg_dump -h localhost -p 9432 -t {}   {} |psql  -p 5432 {} '.format(
            tl, dbname, dbname)
        print (oscommand)
        os.system(oscommand)



def pca_sensitivity_tests():
    """
    N.B. If use the "covariance" method on Stata,  get PCA vectors that are mutually orthogonal, 
    and get correlations between PCA vectors and my original variables which match the "corr" column that Stata 
    gives in its "pcacoefsave" output.
    This is even true using weights.  When using the "correlation" method (default), 
    neither of these is true. I believe this is as it should be, since the PCA is still optimized for a normalized basis.
    """
    from pca import pca_result, estimatePCA

    assert len(defaults['osm']['clusterRadii']) == 1
    fn = 'disconnectivity_id_5_planet_' + \
        defaults['osm']['clusterRadii'][0]+'_urban_2'
    df = pd.read_pickle(paths['working']+fn+'.pandas')
    df2 = df.dropna().set_index(defaults['gadm']['ID_LEVELS'])
    df2 = df2[df2.n_nodes > 10]
    nologs = ['negdegree', 'fraction_deadend', 'frc_length_bridge', 'frc_length_noncycle',
              'frc_edges_noncycle', 'frc_edges_bridge', 'curviness']+[dd for dd in df2.columns if 'distanceratio' in dd]
    wlogs = ['negdegree', 'fraction_deadend', 'frc_length_bridge', 'frc_length_noncycle', 'frc_edges_noncycle', 'frc_edges_bridge', 'logcurviness',
             'logcircuity_0_500', 'logcircuity_500_1000', 'logcircuity_1000_1500', 'logcircuity_1500_2000', 'logcircuity_2000_2500', 'logcircuity_2500_3000']

    # Vary sample, and compare using large pop to using weights:
    allS = {'ls_pop': {}, 'n_nodes': {}}
    # We calculate unweighted PCA on the top N observations according to the weight-var. "-1" means use them all, but weight by weightvar
    topNs = [-1, 1000, 5000, 10000, 100000, len(df2)]
    xtopNs = {'ls_pop': [0, 1, 2, 3, 4, 5],
              'n_nodes':              [6, 7, 8, 9, 10, 11]}
    texwvar = {'ls_pop': 'pop', 'n_nodes': 'nodes'}  # 'N$_{nodes}$'}
    for showC in ['loadings', 'correlations']:
        # Three PCA components -- three axes
        fig, axs = plt.subplots(3, figsize=(12, 15))
        for package in ['stata', 'jakevdp']:
            xticks, xticklabels = [], []
            pcvarlist = wlogs
            # figureFontSetup(uniform=12,figsize='paper') # Can't use this; it
            colors = dict(
                list(zip(pcvarlist, discrete_colors(n_categories=len(pcvarlist)))))
            for wvar in ['ls_pop', 'n_nodes']:
                xticks += xtopNs[wvar]
                for topN in topNs:  # Estimate PCAs
                    if topN == -1:
                        allS[wvar][topN] = estimatePCA(
                            df2[wlogs+[wvar]], wvar, fn+'-wlogs-'+wvar+'-weighted', paths['scratch'], package=package, fix_signs=False)
                    else:
                        dfB = df2.sort_values(wvar, ascending=False)[:topN]
                        allS[wvar][topN] = estimatePCA(
                            dfB[wlogs], None, fn+'-wlogs-{}-top{}'.format(wvar, topN), paths['scratch'], fix_signs=False)
                        allS[wvar][topN].minweight = int(dfB[wvar].min())
                    cor = allS[wvar][topN].correlations if showC == 'correlations' else allS[wvar][topN]
                    if cor.loc['negdegree', 'PCA1'] < 0:
                        if 1:
                            allS[wvar][topN].flip_sign('PCA1')
                        print('Did not Flipped!')
                    plt.close(allS[wvar][topN].fig)
                # Plot half the results
                xticklabels += ['weighted\nby '+texwvar[wvar]] + ['Top {topN}\nmin({wvar})=\n{themin}'.format(
                    topN=topN, wvar=texwvar[wvar],  themin=human_format(allS[wvar][topN].minweight)) for topN in topNs[1:]]
                xtopN = xtopNs[wvar]
                for ipc in [1, 2, 3]:
                    ax = axs[ipc-1]
                    for vv in wlogs:
                        ax.plot(xtopN, [(allS[wvar][itn].correlations if showC == 'correlations' else allS[wvar][itn]).loc[vv, 'PCA'+str(ipc)] for itn in topNs], {
                                'stata': '-', 'jakevdp': '--'}[package], label='__nolabel__' if wvar == 'ls_pop' else metricNames[vv],    color=colors[vv])
                        ax.text(xtopN[-1],
                                (allS[wvar][topNs[-1]].correlations if showC == 'correlations' else allS[wvar][topNs[-1]]).loc[vv, 'PCA'+str(ipc)], '  '+metricNames[vv], size=5, ha='left', color=colors[vv])
                    ax.set_ylabel('Correlation with'*(showC == 'correlations') +
                                  'Loading of'*(showC == 'loadings')+' PCA$_{'+str(ipc)+'}$')
                    ax.set_xticks(xticks)
                    plt.setp(ax.set_xticklabels(xticklabels), 'fontsize', 8)
                    ax.grid()

            cpblUtilities.mathgraph.transbg(plt.legend())
            # fig.suptitle(package)
        fig.savefig(paths['output']+'PCA-sensitivity-{suffix}-{showC}-{dbname}.pdf'.format(
            suffix=fn, package=package, showC=showC, dbname=defaults['server']['postgres_db']))
        plt.close(fig)
    return



def merge_pdfs_for_SI():
    SI_vars = ['pca1', 'degree', 'fraction_deadend', 'frc_length_noncycle', 'logcircuity_500_1000', 'frc_edges_noncycle',
               'curviness', 'log10length_m', 'length_m_per_pop']  # 'log10n_nodes','pca2','length_m_per_pop',
    # SI_trendsMetrics = ['pca1','degree','fraction_deadend','frc_length_noncycle','logcircuity_500_1000', 'frc_edges_noncycle','curviness', 'log10length_m','length_m_per_pop',    ]#'log10n_nodes','pca2',

    # SI_vars = ['pca1','degree','log10length_m'           ]#'log10n_nodes','pca2','length_m_per_pop',
    cpblUtilities.mergePDFs(['{graphics}trends-plot-{metric}-top10-steep0-{dbname}.pdf'.format(dbname=defaults['server']['postgres_db'], graphics=paths['graphics'], metric=metr.replace('_', '-')) for metr in SI_vars],
                            '{graphics}trends-plot-ALL-top10-steep0-{dbname}.pdf'.format(graphics=paths['graphics'], dbname=defaults['server']['postgres_db']), delete_originals=False)

    
    cpblUtilities.mergePDFs(['{graphics}trends-plot-{metric}-top10-steep0-includingNonUrban-{dbname}.pdf'.format(dbname=defaults['server']['postgres_db'], graphics=paths['graphics'], metric=metr.replace('_', '-')) for metr in SI_vars],
                            '{graphics}trends-plot-ALL-top10-steep0-includingNonUrban-{dbname}.pdf'.format(graphics=paths['graphics'], dbname=defaults['server']['postgres_db']), delete_originals=False)


    cpblUtilities.mergePDFs(['{graphics}countries-GHSLtrends-{mname}-{dbname}ALL.pdf'.format(dbname=defaults['server']['postgres_db'], graphics=paths['graphics'], mname=str2pathname(univar))
                             for univar in SI_vars], '{graphics}countries-GHSLtrends-MOST-{dbname}.pdf'.format(dbname=defaults['server']['postgres_db'], graphics=paths['graphics']), delete_originals=False)


    cpblUtilities.mergePDFs(['{graphics}world-regions-GHSLtrends-{mname}-{dbname}ALL.pdf'.format(dbname=defaults['server']['postgres_db'], graphics=paths['graphics'], mname=str2pathname(univar))
                             for univar in SI_vars], '{graphics}world-regions-GHSLtrends-MOST-{dbname}.pdf'.format(dbname=defaults['server']['postgres_db'], graphics=paths['graphics']), delete_originals=False)


    cpblUtilities.mergePDFs(['{scratch}citytrends/cities-by-country-trends-abs-{mname}-{dbname}ALL.pdf'.format(dbname=defaults['server']['postgres_db'], scratch=paths['scratch'], mname=str2pathname(univar))
                             for univar in SI_vars], '{graphics}cities-by-country-MOST-{dbname}.pdf'.format(dbname=defaults['server']['postgres_db'], graphics=paths['graphics']), delete_originals=False)


    cpblUtilities.mergePDFs(['{scratch}citytrends/cities-by-world-region-trends--abs-{mname}-{dbname}ALL.pdf'.format(dbname=defaults['server']['postgres_db'], scratch=paths['scratch'], mname=str2pathname(univar))
                             for univar in SI_vars], '{graphics}cities-by-world-region-MOST-{dbname}.pdf'.format(dbname=defaults['server']['postgres_db'], graphics=paths['graphics']), delete_originals=False)


def display_street_network_paradigms():
    """
   (
    """
    from rasterTools import latLonToCell
    PARADIGMS = stylized_unit_graphs().available_graphs()
    row, col = latLonToCell(41.000598, 28.926487)
    row, col = latLonToCell(48, 1.02)
    #row,col = 5879, 25071
    #row,col = 5880, 25071
    row, col = (5040, 21722)

    assert len(defaults['osm']['clusterRadii'])==1
    
    for paradigm in PARADIGMS:
        fn = '{}network-{}-{}-{}.pdf'.format(paths['scratch']+'cellimages/',
                                             paradigm, row, col)
        defaults['osm']['continents'] = [paradigm]
        print(' You may need to force update to re-make the cell images.')
        make_and_save_gridcell_network_image(
            row,
            col,
            fn,
            nbyn=1.7 if paradigm in ['hell'] else 1,
            clusterradius= defaults['osm']['clusterRadii'][0], forceUpdate=forceUpdate)  # forceUpdate is module-global

def analyse_street_network_paradigms( keystats=False):
    """ If keystats is True, just return text for keystats LaTeX file """
    PARADIGMS = stylized_unit_graphs().available_graphs()
    dfs = []
    assert len(defaults['osm']['clusterRadii'])==1
    
    for paradigm in PARADIGMS:
        for gl in defaults['gadm']['ID_LEVELS']:
            fn = paths['working'] + \
                'disconnectivity_{gl}_{ar}_{rad}_2.pandas'.format(gl=gl, ar=paradigm, rad =defaults['osm']['clusterRadii'][0])
            if os.path.exists(fn):
                adf = pd.read_pickle(fn)
                adf['gadm'] = gl
                adf['paradigm'] = paradigm
                dfs += [adf]
            else:
                print(("Can't find {}.".format(fn)))

    df = pd.concat(dfs, sort=True).set_index(['paradigm', 'gadm'])

    # One problem is to get rid of all the ones around the edges of the entire rectangle of the network.
    # The other problem is to that there is anyway natural variation due to edge effects in each GADM shape at each scale smaller than iso.   Below I produce a table for each GADM level and we find that after filtering against edges of the network, we find nearly the same means at each scale. Thus the problems are that some nodes must be merging into clusters.
    # An important point to note is how much variation there is just due to edge effects. We should really be using this as one way to constrain confidence in our estimates at relatively small scales.
    
    print(( df.groupby(level=[0,1]).mean()))

    # Eliminate edges by only taking top 50% of n_nodes for each cell:
    df['med_nodes'] = df.groupby(level=[0,1]).n_nodes.median()
    dfnonedge = df.query('n_nodes>=med_nodes')
    df = dfnonedge
    print(( df.groupby(level=[0,1]).mean() )) # Much nicer! after eliminating low-count regions
    for idlevel in defaults['gadm']['ID_LEVELS']:
        oga = osm_ghsl_analysis()
        vcats = \
            [('Scalar index', 'pca1')] +\
            [('Nodes', d) for d in ['degree', 'fraction_deadend', 'fraction_1_3', ]] +\
            [('Dendricity', d) for d in oga.dendricities] + \
            [('Circuity', d.lower()) for d in oga.distanceRatios] + \
            [('Sinuosity', 'curviness')]
        dcat = OrderedDict([(b, a) for a, b in vcats])
        df3 = df.query('gadm=="{}"'.format(idlevel.lower())).groupby(level=[0,1]).mean().reset_index()
        if keystats and idlevel=='iso': # Could pick any GADM level , really.
            txt='\n'.join([r'\newcommand\SNDI{}{{${:.2f}$\xspace}}'.format(row['paradigm'], row['pca1']) for ii,row in df3.iterrows()])
            print(txt)
            return(txt)
        df4 = df3[['paradigm']+[c for c in list(dcat.keys()) if c if c in df3.columns]
                  ].set_index('paradigm').T[['grid', 'medieval', 'hell']]
        df4['cat'] = df4.index.map(lambda vv: dcat.get(vv, vv))
        df5 = df4.reset_index().set_index(['cat', 'index'])
        df5[df5.abs() < 1e-6] = 0


        # Now actually move rows so that each category label is a line of its own:
        # Split df up by cat:
        df5 = df5.reset_index().set_index('cat')
        dfs = [pd.DataFrame(df5.loc[[ii]]) for ii in df5.index.unique()]
        newdfs = []
        for adf in dfs:
            blank = pd.DataFrame(
                pd.Series([adf.index[0], '', '', '', ''], index=adf.reset_index().columns)).T
            adf.index = ['']*len(adf)
            newdfs += [blank.set_index('cat')]+[adf]
        df6 = pd.concat(newdfs).reset_index()

        from cpblUtilities.textables import formatDFforLaTeX, latexTable, cpblTable_to_PDF, dataframeWithLaTeXToTable
        print (df6)
        formatDFforLaTeX(df6,
                         row=None,
                         sigdigs=None,
                         precision=2,
                         colour=None,
                         leadingZeros=False,
                         noTeX=False)
        print (df6)
        if 0:
            df6 = df6.reset_index()
            x = df6['cat'].values
            toblank = list(np.where([False]+list(x[1:] == x[:-1])))
            x = np.array(x)
            x[toblank] = ''
            df6['cat'] = x

        from metric_settings import define_metric_settings
        dms = define_metric_settings()
        df6['index'] = df6['index'].map(lambda m: dgetget(dms, [m, 'name'], ''))
        df6.columns = ['', '', 'Grid', 'Medieval', 'Culs-de-sac']

        lt = latexTable(df6)
        outy = lt.toCPBLtable(paths['tex']+'paradigms-'+idlevel+'-aggregates-table-'+defaults['server']['postgres_db'],
                              tableTitle=None,
                              footer=None,
                              boldHeaders=False,
                              boldFirstColumn=False,
                              columnWidths=None,
                              formatCodes='lrrrr',
                              hlines=False,
                              vlines=None,
                              masterLatexFile=None,
                              landscape=None,
                              cformat='lrrrr',
                              tableName=None)

        cpblTable_to_PDF(paths['tex']+'paradigms-'+idlevel.replace('_','-')+'-aggregates-table-'+defaults['server']['postgres_db'])

    fig, axs = plt.subplots(3, 6)
    for iaa, aa in enumerate(['hell', 'medieval', 'grid']):
        for igg, gg in enumerate(defaults['gadm']['ID_LEVELS'][::-1]):
            ax = axs[iaa, igg]
            ax.hist(df.loc[aa, gg].pca1)
            if igg == 0:
                ax.set_title(gg.replace('_', ' '))
            if iaa == 0:
                ax.set_ylabel('foo')

    # Rerun with rasters, and do hists of gridcells instead.?

    plt.show()

    # To find where the edge effects are, look at all variables in PCA:
    # This shows distribution / outliers due to edge effects in our paradigms aggregates to the id_5 level.
    df = df.reset_index().query('gadm == "id_5"')
    wlogs = ['negdegree', 'fraction_deadend', 'frc_length_bridge', 'frc_length_noncycle', 'frc_edges_noncycle', 'frc_edges_bridge', 'logcurviness',
             'logcircuity_0_500', 'logcircuity_500_1000', 'logcircuity_1000_1500', 'logcircuity_1500_2000', 'logcircuity_2000_2500', 'logcircuity_2500_3000']
    for arch in df.paradigm.unique():
        fig, axs = plt.subplots(4, 4, figsize=(18, 10))
        plt.suptitle(arch)
        for ii, vv in enumerate(['pca1']+wlogs):
            ax = axs.flatten()[ii]
            ax.set_ylabel(dms[vv]['name'])
            ax.hist(df.query('paradigm == "{}"'.format(arch))
                    [vv].values, bins=25)
            ax.set_yscale('log')
        plt.savefig(paths['graphics'] +
                    'paradigm-id5-distributions-{}-{}.pdf'.format(arch, defaults['server']['postgres_db']))

    return


def compare_nodal_degree_by_world_region():  # Well, world regions, not WBR
    """ This is to address whether nodal degree does a good job inside the USA, while the more expansive PCA1 is needed globally.
 We simply look at degree-PCA1 correlations by WBR.

p-values are not very useful; they are all near zero for any n>few 
    """
    from scipy.stats.stats import pearsonr
    from statsmodels.stats.weightstats import DescrStatsW
    assert len(defaults['osm']['clusterRadii'])==1

    allout = []
    rn = country_tools().get_country_list()
    c2wb = country2WBregionLookup().loc[continentsWBR]
    for scale in ['city', 'id_1', 'id_5', ]:
        if scale in ['id_1', 'id_5']:
            df = pd.read_pickle(
                paths['working']+'disconnectivity_{}_planet_{}_urban_2.pandas'.format(scale, defaults['osm']['clusterRadii'][0]))
            df = add_country_data_to_df_by_iso(df)
            # Use alternative region assignments:
            if 0:
                rn['region'] = rn['intermediate-region'].replace(
                    '', np.nan).fillna(rn.region5)
            df2 = df.set_index('iso').join(
                rn[['ISOalpha3', 'region22']].set_index('ISOalpha3'))

        elif scale in ['city']:
            ca = cities.city_atlas()
            df2 = ca.load_connectivity().query('stockorchange=="cityedges"').query('iyear==3')
            df2['city'] = df2['City']
            if 1:
                dfc = pd.read_pickle(paths['working']+'cities_disconnectivity_{}.pandas'.format(defaults['osm']['clusterRadii'][0])).query(
                'stockorchange=="cityedges"').query('iyear==3')

                ac = cities.city_atlas().get_list_of_cities()
                dfc2 = dfc.set_index('city').join(ac.set_index('name')).reset_index().set_index('ISO').join(rn[['ISOalpha3', 'region22']].set_index('ISOalpha3'))
                dfc2['degree'] = -dfc2['negdegree']

            df2 = df2.set_index('ISO').set_index('iso').join(rn[['ISOalpha3', 'region22']].set_index('ISOalpha3'))

        else:
            mising

        df2['region'] = df2['region22']
        df2.index.name = 'iso'
        df2['country_weight'] = 1.0 / df2.groupby('iso')['pca1'].transform(len) # Country weight
        df2.reset_index(inplace=True)
        df2.dropna(subset=['iso'], inplace=True)
        # Construct a dict, in case some countries are in more than one region, and because we want some explicitly overlapping groups:
        kvars = ['degree', 'pca1', 'region', 'country_weight']
        regions = dict([
            (reg, df2[df2.iso.isin(rn.query('region22=="{}"'.format(reg)).ISOalpha3)][kvars
                ].dropna())
            for reg in rn.region22.unique() if reg])
        for extraiso in ['USA','GBR','FRA','DEU','ESP','ITA']:
            regions[extraiso] = df2.query('iso=="'+extraiso+'"')[ kvars ].dropna()
        regions['outside Americas'] = df2[df2.iso.isin(
            rn[-rn.region22.isin(['Latin America and the Caribbean', 'Northern America'])].ISOalpha3)][ kvars ].dropna()

        if 0:  # One-line version without weights, with p-value
            byr = [pd.DataFrame(pd.Series(dict(list(zip(['R', 'p', 'Nobs', 'region'], list(pearsonr(
            adf.degree, adf.pca1))+[len(adf), reg]))))).T.set_index('region') for reg, adf in list(regions.items())]


        if 1: # p-value not useful. Use country weights:
            byr = [pd.DataFrame(pd.Series(dict(
                    R = DescrStatsW(adf[['pca1','degree']],   adf.country_weight).corrcoef[0][1]*-1,
                    Nobs = len(adf),
                    Ncountries = adf.country_weight.sum(),
                    weighting = 'country',
                    region = reg))).T.set_index('region') for reg, adf in list(regions.items())]

        byr = pd.concat(byr).dropna().query('Nobs>2').sort_values('R')
        byr['scale'] = scale
        allout += [byr]
    allout = pd.concat(allout)
    print([adf for n, adf in allout.groupby('scale')])
    return(allout)

def robustness_variants_list_id5_examples():
    xn,yn = ('10m', '10m car-ways')
    df = collect_ISO_data_from_multiple_databases('id_5')[['pca1', 'iso', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5','version','length_m', ]].dropna()
    df = df.query('version!="10m"')
    df = df.query('length_m>1000')
    # Only keep id5s which exist in all versions:
    df = df.set_index(['iso','id_1', 'id_2', 'id_3', 'id_4', 'id_5', 'version']).unstack().dropna().stack().reset_index()

    iso2cname = country_tools.country2ISOLookup()['ISO2cname']
    df = df.set_index(['iso','id_1', 'id_2', 'id_3', 'id_4', 'id_5', 'version']).unstack().dropna()
    df['dpca1'] = (df[('pca1', xn)]- df[('pca1', yn)]).abs()
    df['dL'] = (df[('length_m', xn)]- df[('length_m', yn)])

    # Pick countries of interest
    df.sort_values(['iso','dpca1'], ascending=False, inplace=True)
    print((df[df.index.get_level_values(0).isin(['SLE', 'CAF', 'HKG'])].groupby('iso').head(10)))

    # Pick negative length changes:
    df.sort_values(['dL','iso'], ascending=False, inplace=True)
    print((df[[cc for cc in df.columns if cc not in [('dpca1','')]]].groupby('iso').head(10)))


def compare_two_robustness_variants(scale='all'):
    """ Depends on collect_ISO_data_from_multiple_databases() which defines the available runs.
     For ISO, do scatter plot to see off-diagonals
     For cells, find extreme-off-diagonal examples
    """
    from cpblUtilities.color import unordered_discrete_colors
    from rasterTools import latlon_from_rowcol
    from raster_anecdotes import add_url_columns_from_latlon
    plt.close('all')

    if scale in ['id_5', 'all']:

        xn,yn = ('7m', '7m all-ways')
        xn,yn = ('10m', '10m car-ways')
        df = collect_ISO_data_from_multiple_databases('id_5')[['pca1', 'iso', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5','version','length_m', ]].dropna()
        df = df.query('length_m>1000')

        # Only keep id5s which exist in all versions:
        df = df.set_index(['iso','id_1', 'id_2', 'id_3', 'id_4', 'id_5', 'version']).unstack().dropna().stack().reset_index()
        iso2cname = country_tools.country2ISOLookup()['ISO2cname']

        for iso in df.iso.unique():
            plt.close('all')
            dfc = df.query('iso=="{}"'.format(iso))
            if len(dfc)<30: continue
            fig, axs = plt.subplots(1,3, figsize = (12,4)) 
            ax1,ax2,ax3=axs
            x,y = dfc.query('version=="'+xn+'"'), dfc.query('version=="'+yn+'"'),  # re-split-up the df, now that we did a joint dropna()

            # SNDI: plot one version vs the other  (by country)                
            ax1.scatter(x.pca1, y.pca1, s=1)#, c = x.regioncolour)
            plot_diagonal(x.pca1, y.pca1, ax=ax1, color='.8', zorder=-10)
            ax1.set_xlabel('SNDI ({})'.format(x.version.unique()[0]))
            ax1.set_ylabel('SNDI ({})'.format(y.version.unique()[0]))
            plt.suptitle('{} ({})'.format(iso2cname.get(iso, ''), iso), )

            # length: plot one version vs the other  (by country)                
            ax2.scatter(x.length_m, y.length_m, s=1,)# c = x.regioncolour)
            plot_diagonal(x.length_m, y.length_m, ax=ax2, color='.8', zorder=-10)
            ax2.set_xscale('log'),      ax2.set_yscale('log')
            ax2.set_xlabel('Length ({})'.format(x.version.unique()[0]))
            ax2.set_ylabel('Length ({})'.format(y.version.unique()[0]))

            # Plot distributions for each version (by country)
            hh=dfc.groupby('version')['pca1'].plot.kde(ax=ax3)
            ax3.legend(title= '{} ({})'.format(iso2cname.get(iso, ''), iso))
            ax3.set_xlim([-3,12.7])
            ax3.set_xlabel('SNDi')

            plt.savefig(paths['baseoutput']+'robustness-{}-id5-{}-v-{}-{}.pdf'.format(iso, xn.replace(' ' ,''), yn.replace(' ',''), defaults['server']['postgres_db']))

    if scale in ['iso', 'all']:

        for xn,yn in [('10m', '10m car-ways')
                      #('7m', '7m all-ways'),
                      ('10m', '7m'),
                           ]:

            df = collect_ISO_data_from_multiple_databases('iso')[['pca1', 'iso', 'gRegion','version','length_m']].dropna()
            clu = dict(list(zip(df.gRegion.unique(), unordered_discrete_colors(n_categories= len(df.gRegion.unique())))))
            df['regioncolour'] =  df.gRegion.map(lambda rr: clu[rr])

            fig, ax = plt.subplots(1)
            x,y = df.query('version=="'+xn+'"'), df.query('version=="'+yn+'"'),  # re-split-up the df, now that we did a joint dropna()


            ax.scatter(x.pca1, y.pca1, s=20, c = x.regioncolour)
            ax.set_xlabel('SNDI ({})'.format(x.version.unique()[0]))
            ax.set_ylabel('SNDI ({})'.format(y.version.unique()[0]))
            for iyc, cc in enumerate(clu.items()):
                n, c = cc
                ax.text(0.1, 7.7-.4*iyc, n, color=c)
            plt.savefig(paths['baseoutput']+'robustness-pca1-iso-{}-v-{}-{}.pdf'.format(xn.replace(' ' ,''), yn.replace(' ',''), defaults['server']['postgres_db']))

            fig, ax = plt.subplots(1)
            x=x.query('length_m>1000')
            y=y.query('length_m>1000')
            ax.scatter(x.length_m, y.length_m, s=20, c = x.regioncolour)
            ax.set_xscale('log')
            ax.set_yscale('log')
            ax.set_xlabel('Length ({})'.format(x.version.unique()[0]))
            ax.set_ylabel('Length ({})'.format(y.version.unique()[0]))
            for iyc, cc in enumerate(clu.items()):
                n, c = cc
                ax.text(6e5, 3e9/1.5**iyc, n, color=c)
            plt.savefig(paths['baseoutput']+'robustness-length-iso-{}-v-{}-{}.pdf'.format(xn.replace(' ' ,''), yn.replace(' ',''), defaults['server']['postgres_db']))
        
    if scale in ['cell', 'all']: # Don't bother comparing 10m and 7m. Just compare allways with not-allways:
        dbc1,dbc2 = '10m', '10m car-ways'
        cells_l = collect_ISO_data_from_multiple_databases('cell').query('version=="{}" or version=="{}"'.format(dbc1,dbc2))
        cells_l = cells_l.query('length_m>2000')
        versions = cells_l.version.unique()
        # When LIMIT is being used, only a subset will be available for both versions.
        cells = cells_l.set_index(['row','col','version',]).unstack().dropna(subset=[('pca1',dbc1), ('pca1', dbc2)])
        cells['dpca1'] = (cells[('pca1', dbc1)]- cells[('pca1', dbc2)]).abs()
        cells.sort_values('dpca1', ascending=False, inplace=True)

        df_od = cells[:300][[cc for cc in cells if cc not in [('dpca1','')]]].stack().reset_index() # Off diagonal examples to use
        df_od = latlon_from_rowcol(df_od, loc='center')
        df_od = add_url_columns_from_latlon(df_od).set_index(['row','col','version'])
        dbs = df_od.dbn.unique()

        from rasterTools import mapCell
        for adict in cpblUtilities.multipage_plot_iterator([(a,b) for a,b in df_od.iterrows()], # Convert back to one version per row!
                                                           nrows=3, ncols=2, filename= paths['basescratch']+'robustness_cells_{}_{}-{}'.format(dbs[0], dbs[1], defaults['server']['postgres_db']), wh_inches = None):
            a, series = adict['data']
            row,col,version  = a
            print((' Plotting row, col {}, {} for {}'.format(row,col, version)))
            mapCell(row, col, series['cr'], ax= adict['ax'],
                    # Cannot do the titlestyle below. See raster_anecdotes, which requires PGF mode etc in matplotlib to do URLS
                    titleStyle=  '{countryname} ({iso}) {latlon} \n',  #+r' \href{{{streeturl}}}{{Street}} \href{{{osmurl}}}{{OSM}} \href{{{saturl}}}{{Sat}}',
                    #titleStyle= '{countryname} {latlon} \n SNDi = '+str(series['pca1']),
                    clusterLabel=None, logger=None, nbyn= None, db = series['dbn'])
            
            assert not adict['last']

def plot_PCA_coefficient_versions():
    """ 
    (1)Ensure PCA coefficients have been estimated for various databases.
    (2)Collect PCA coefficients from various databases (version).
    (3)Plot coefficients.
    """
    #(2)
    from pca import pca_result, estimatePCA
    o10 = pd.DataFrame(pca_result('/home/meuser/osm/workingData/osm10/master_id5_PCA_coefficients_planet_10_urban.pyshelf'))
    o10cars = pd.DataFrame( pca_result('/home/meuser/osm/workingData/osm10cars/master_id5_PCA_coefficients_planet_10_urban.pyshelf'))
    o7 =  pd.DataFrame(pca_result('/home/meuser/osm/workingData/osm7/master_id5_PCA_coefficients_planet_7_urban.pyshelf'))
    y = np.arange(len(o10.index.values))
    plt.close('all')
    fig,ax = plt.subplots(1)
    alpha=.2
    from cpblUtilities.color import unordered_discrete_colors
    colours = unordered_discrete_colors(3)

    ax.barh(y,   o10.PCA1, height=.3 , alpha=alpha, label='Baseline', color=colours[0])
    ax.barh(y-.3,   o7.PCA1, height=.3 , alpha=alpha+.2, label='7 m', color=colours[1])
    ax.barh(y-.3-.3,   o10cars.PCA1, height=.3 , alpha=alpha+.4, label='no paths', color=colours[2])
    labels = [metricNames[vv] for vv in o10.index]
    for yy, ss in zip(y, labels):
        ax.text(.01+0*yy, yy-.3, ss, va='center', )
    ax.set_yticks([])
    ax.set_xlabel('PCA$_1$ coefficient')
    ax.legend()
    plt.show()
    fn=paths['graphics']+'PCA-coefficients-three-variations.pdf'
    plt.savefig(fn)
    os.system('pdfcrop {} {}'.format(fn,fn))

    
def collect_ISO_data_from_multiple_databases(scale='iso'):
    """ Grab ISO-level data from multiple databases. Collect them into a DataFrame. Then can do some plots. """
    TC  = OrderedDict([['7m', dict(dbn='osm7', cr='7')],
                       ['10m car-ways', dict(dbn='osm10cars', cr='10')],
                      ['10m', dict(dbn='osm10', cr='10')]])
    for kk,vv in list(TC.items()): 
        vv['db'] = osmt.pgisConnection(db= vv['dbn'])
        db = vv['db']
        get_columns = ['pca1','length_m']
        if scale in ['id_5']:
            get_columns += ['iso', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5']
            vv['df'] = pd.DataFrame([list(x) for x in
                                          db.execfetch("""
                                          select {cols} FROM disconnectivity_{sc}_planet_{cr}_urban;
                                          """.format(cols=','.join(get_columns), cr=vv['cr'], sc= 'id_5'))],
                                          columns = get_columns)
        if scale in ['iso']:
            get_columns += [scale]
            vv['df'] = add_country_data_to_df_by_iso(
                            pd.DataFrame([list(x) for x in
                                          db.execfetch("""
                                          select {cols} FROM disconnectivity_{sc}_planet_{cr}_urban;
                                          """.format(cols=','.join(get_columns), cr=vv['cr'], sc= scale))],
                                          columns = get_columns),
                    duplicate_for_world_regions=False)
        elif scale=='cell':
            get_columns += ['row','col','urban']
            vv['df'] =   pd.DataFrame([list(x) for x in
                                          db.execfetch("""
                                          select {cols} FROM disconnectivity_by_cell_{cr} where urban='t' and pca1 != double precision 'NaN'
                                            LIMIT 100000;
                                          """.format(cols=','.join(get_columns), cr=vv['cr'], ))],
                                          columns = get_columns)
        vv['df']['version'] = kk
        vv['df']['dbn'] = vv['dbn']
        vv['df']['cr'] = vv['cr']
    return pd.concat([a['df'] for a in list(TC.values())])


def compare_dbversions_by_scatterplot():
    dfs = get_saved_wide_flattened_TS_dataframes(db='osm10', table_subset_suffix='ghsl_urban')
    dfc = pd.concat([dfs['city'][x][['pca1_Stock']].rename(columns = {'pca1_Stock':x}) for x in ['osm10','osm7','osm10cars']], axis=1)
    dfn = pd.concat([dfs['iso'][x][['pca1_Stock']].rename(columns = {'pca1_Stock':x}) for x in ['osm10','osm7','osm10cars']], axis=1)
    dfn['outlier'] = (dfn['osm10cars']-dfn['osm10']).abs()>2
    dfc['outlier'] = (dfc['osm10cars']-dfc['osm10']).abs()>2
    dfn['impossible'] = dfn['osm10cars']<dfn['osm10']
    dfc['impossible'] = dfc['osm10cars']<dfc['osm10']
    dfc.index = dfc.index.map(lambda ss: ss.split(',')[0])
    assert (dfc.index.str.strip().str.len()>0).all()
    plt.close('all')
    figc,axc = plt.subplots(1)
    fign,axn = plt.subplots(1)
    for title, df,ax, fig in [['Countries', dfn,axn, fign],['Cities', dfc,axc, figc]]:
        ax.scatter(df.osm10, df.osm7, alpha=.5, label='7 m radius')
        ax.scatter(df.osm10, df.osm10cars, alpha=.5, label='No cycle/footpath routes')
        plot_diagonal(df.osm10, df.osm7, ax, color='k', alpha=.5, zorder=-80)
        ax.legend(title=title)
        ax.set_xlabel('SNDi (baseline)')
        ax.set_ylabel('SNDi (alternate calculations)')
        if title== 'Cities':
            ax.set_ylabel('')
        ax.axis('image')
        # Label outliers?
        dfo=df[df.outlier]
        for placename,row in dfo.iterrows():
            ax.text(row.osm10, row.osm10cars, placename, size=7)
        dfo=df[df.impossible]
        for placename,row in dfo.iterrows():
            ax.text(row.osm10, row.osm10cars, placename, size=4)
        fn=paths['graphics']+'{}-by-db-scatter.pdf'.format(title)
        fig.savefig(fn, )
        os.system('pdfcrop {} {}'.format(fn,fn))
    plt.show()
    # Show problem cases:
    print((dfc[dfc.impossible]))
    """
                 osm10     osm7  osm10cars  outlier  impossible
    placename                                                  
    Gaoyou     1.80450  1.85414    1.79415    False        True
    Oyo        3.25991  3.27385    3.25765    False        True
    Pingxiang  2.23922  2.34144    2.23836    False        True
    Vinh Long  1.37444  1.43438    1.31475    False        True
    Yulin      2.65292  2.40684    2.62310    False        True
        """
    print((dfn[dfn.impossible]))
    """                             osm10      osm7  osm10cars  outlier  impossible
    placename                                                                   
    Kiribati                  5.056016  5.043052   4.530048    False        True
    Northern Mariana Islands  4.702508  4.692390   4.602574    False        True
    Palau                     7.317090  7.056635   7.294712    False        True
    """

def citylab_stats():
    """Some stats for citylab - Laura Bliss article 2/5/20"""
    assert defaults['server']['postgres_db'] == 'osm10'
    db = psqlt.dbConnection()
    cmd = """SELECT degree, COUNT(*) FROM nodes_planet_10
                WHERE nocars=False AND urban_2
                GROUP BY degree ORDER BY degree;"""
    result = db.execfetch(cmd)
    n_deadend = dict(result)[1]
    n_nodes = sum([rr[1] for rr in result])
    print(('{} urban deadends out of {} urban nodes'.format(n_deadend, n_nodes)))

    """manual check
    select sum(n_culdesac) AS n_culdesac, sum(n_nodes) AS nnodes,  sum(n_culdesac)/  sum(n_nodes) AS frc_culdesac
    from (SELECT fraction_deadend*n_nodes AS n_culdesac, n_nodes
    FROM disconnectivity_iso_planet_10_urban) t1"""

    cmd = """SELECT classification, count(*) AS n, sum(length_m)::float/1000/1000000 AS million_km
                FROM edges_planet_10
                WHERE nocars=False AND urban_2
                GROUP BY classification"""
    result = db.execfetch(cmd)
    n_bridges = [rr[1] for rr in result if rr[0]=='B'][0]
    n_edges = sum([rr[1] for rr in result])

    """manual check gives slightly greater number, due to double counting of edges where they cross id_5 boundaries
    SELECT sum(length_m)::float/1000/1000000 as m_km, sum(n_edges) AS n_edges,  sum(n_bridges)/  sum(n_edges) AS frc_bridges, sum(n_bridges) AS n_bridges
                from (SELECT frc_edges_bridge*n_edges AS n_bridges, n_edges, length_m
                        FROM disconnectivity_iso_planet_10_urban) t1;;"""
    print(('{} urban bridges out of {} urban edges'.format(n_bridges, n_edges)))

if __name__ == '__main__':
    # Docopt is a library for parsing command line arguments
    from osm_config import parse_common_docopt_options
    from cpblUtilities.configtools import get_docopt_mode

    # Parse arguments, use file docstring as a parameter definition
    arguments = docopt.docopt(__doc__)
    forceUpdate = parse_common_docopt_options(defaults, arguments)
    defaults['USE_DB'] = not arguments['--local']
    runmode = get_docopt_mode(arguments)
    # Need to update paths for db-specificity in allied modules:
    stata.update_paths(defaults)
    cpblUtilities.update_paths(defaults)
    assert 'osm' in cpblUtilities.paths['tex']

    skipConflictChecks = arguments['--skip-conflict-checks']
    PRIMARY_DB = defaults['server']['postgres_db'] == 'osm10'

    if runmode == 'export':
        assert defaults['USE_DB']  # otherwise That would not make sense
        analyzer = osm_ghsl_analysis()
        analyzer.export_final_connectivity_tables_to_pandas(
            skipConflictChecks=skipConflictChecks, forceUpdate=forceUpdate)
    if runmode == 'copy_to_local':
        assert defaults['USE_DB']  # otherwise That would not make sense
        copy_pandas_and_psql_and_output_to_local_server()
    if runmode == 'stata':
        stfn = paths['working']+'iso_means_q10'
        if not os.path.exists(stfn):
            analyzer = osm_ghsl_analysis()
            df = analyzer.high_density_only(analyzer.has_sufficient_counts(
                analyzer.get_ghsl_urban_with_integrated_country_data('iso')))

            df['CO2_roadtpt_pc'] = df.CO2_roadtpt / df.pop2012
            df['lngdpCapitaPPP_WB'] = np.log(df.gdpCapitaPPP_WB)

            stata.df2dta(df[['density_decile', 'ghsl_yr', 'CO2_roadtpt_pc', 'iso', 'continent', 'curviness', 'negdegree', 'distanceratio_0_500', 'distanceratio_1000_1500', 'distanceratio_1500_2000', 'distanceratio_2000_2500', 'distanceratio_2500_3000', 'distanceratio_500_1000', 'fraction_deadend', 'fraction_1_3', 'fraction_length_included', 'gdpCapitaPPP_WB', 'roads_km', 'roads_pc_paved', 'rugged_popw', 'urbanPop2012', 'urbanPopPc2012', 'urbanPopPcGrowth2012', 'gdpCapita_GDP2014PPP_bn2005USD', 'oilRents_pcofGDP', ]].dropna(), 
                         stfn)

        from pystata import stataSystem, tsv2dta, stataLoad,  df2dta, WPdta
        from cpblUtilities import str2pathname
        from pystata.latexRegressions import latexRegressionFile
        mVersion, rVersion = '2017', '1'
        latex = latexRegressionFile(
            'osm-country-regressions', modelVersion=mVersion, regressionVersion=rVersion, substitutions={})
        latex.skipStataForCompletedTables = True
        """
        smodels=latex.str2models('reg '+' '.join(yxvars))
        stataout=stataLoad(WPdta(dtaname))+latex.regTable('tmp2_'+name,smodels,returnModels=True)
        print('   RUNNING STATA only to check for collinear explanatory variables (likely called for a pysal model?)...')
        stataSystem(stataout,filename=WP+'tmp_statasystem'+name)
        # Run again to read results
        smodels=latex.str2models('reg '+' '.join(yxvars))
        latex.regTable('tmp2_'+name,smodels,returnModels=True,skipStata=True,extraTexFileSuffix='no')
        assert 'estcoefs' in smodels[0]
        kused=[kk for kk in smodels[0]['estcoefs'].keys() if kk not in ['_cons']]
        if len(kused)<len(yxvars[1:]):
            print(' * * *  Dropped some variables in Stata due to collinearity!')
            print('   Dropped variables: ',set(yxvars[1:])-set(kused))
            # And now check which variables were actually used:
            #smodels[
        print('  --- Used: ',kused)
        latex.closeAndCompile()
        return(kused)
        """

    if runmode == 'pca-sensitivity':
        plot_PCA_coefficient_versions()
        pca_sensitivity_tests()
        pca_various_software_tests()
    if runmode == 'mergepdfs':
        merge_pdfs_for_SI()

    if runmode == 'TSfig2variants': # This is tricky; it requires messing with paths and defaults
        if arguments['--db'] is None:
            os.system("""

./osm_analysis.py TSfig2variants --db=osm7 --cluster-radius=7 
./osm_analysis.py TSfig2variants --db=osm10cars --cluster-radius=10
./osm_analysis.py TSfig2variants --db=osm10 --cluster-radius=10
./osm_analysis.py TSfig2variants --db=osm10 --cluster-radius=10 --tablesuffix='ghsl'

            """)
        else:
            table_subset_suffix = arguments['--tablesuffix']
            analyzerm = osm_ghsl_analysis(fig2mode='pnasmain')
            analyzerm.pnas_figure2_trends('pca1', subplots=True,  table_subset_suffix= table_subset_suffix ) 

        
    if runmode in ['manuscript']:
        merge_pdfs_for_SI()
        from aggregate_connectivity_metrics import principal_component_analysis
        
    if runmode in ['manuscript', 'TSfig2', 'TSmaterial']:
        # First, do the actual Figure 2 for main text:
        analyzerm = osm_ghsl_analysis(fig2mode='pnasmain')
        analyzerm.pnas_figure2_trends('pca1', subplots=True)

    if runmode in ['manuscript']:
        
        if PRIMARY_DB:# See #480
            pcAnalyzer = principal_component_analysis() # No longer takes: forceUpdate=False)
            pcAnalyzer.format_LaTeX_table_of_PCA_coefficients()

        # All eight city and country ranking tables: stock and recent; full list and top/bottom 10; city and country
        analyzer = osm_ghsl_analysis()
        analyzer.LaTeX_country_and_city_ranking_tables(rankby='recent', metric='pca1', metric2='degree', 
                                             table_subset_suffix= 'ghsl_urban')

            
    if runmode in ['manuscript', 'TSfig2-other-metrics']:
        # Next, do all the figure-2-like figures for appendix
        def dometric(rankby, metric, metric2=None, table_subset_suffix=None):
            analyzer = osm_ghsl_analysis(fig2mode = 'pnasSI')
            if rankby in ['recent']:
                if 1:
                    # Once with separate plots. This learns ylims. In subplots=False mode, fylims are saved.
                    analyzer.pnas_figure2_trends(
                        metric, subplots=False, table_subset_suffix=table_subset_suffix)
                # Apply ylims to subplot version.
                analyzer.pnas_figure2_trends(
                    metric, table_subset_suffix=table_subset_suffix)
        funcs, names = [], []
        trendsMetrics = order_variables([ 'log10n_nodes', 'pca2', 'pca1', 'degree', 'fraction_deadend', 'frc_length_noncycle',
                                         'logcircuity_500_1000',  'frc_edges_noncycle', 'curviness',  'length_m_per_pop', 'log10length_m', 'lognodalsparsity_1000'])

        for table_subset_suffix in ['ghsl', 'ghsl_urban', ]:
            for metric in trendsMetrics:
                for rankby in ['recent', 'stock']:
                    funcs += [[dometric, [rankby, metric],
                               dict(table_subset_suffix=table_subset_suffix)]]
                    names += ['-'.join([metric, rankby,
                                        table_subset_suffix])]
        runFunctionsInParallel(
            funcs, names=names, parallel=defaults['server']['parallel'])


        merge_pdfs_for_SI()
        analyzer = osm_ghsl_analysis()
        if PRIMARY_DB:
            kmeans.paradigms2clusters()
            
    if runmode in ['TSscatterplots', 'TSSI','TSmaterial']:
        analyzer = osm_ghsl_analysis()
        analyzer.plot_ts_scatterplots('city')
        analyzer.plot_ts_scatterplots('iso')
        analyzer.plot_ts_scatterplots('city', annotateall=False)
        analyzer.plot_ts_scatterplots('iso', annotateall=False)

    if runmode in ['plot_trends_by_region', 'TSmaterial']:
        analyzer = osm_ghsl_analysis()
        analyzer.plot_trends_by_region()
    if runmode in  ['xs_correlations', 'XSmaterial']:
        analyzer = osm_ghsl_analysis()
        analyzer.xs_correlation_tables()
        analyzer.xs_correlation_tables()
    if runmode in ['xssi']:
        analyzer = osm_ghsl_analysis()
        compare_dbversions_by_scatterplot()
    if runmode == 'outlier-cities':
        db = osmt.pgisConnection(verbose=True)
        ts = db.execfetch(   """
        select c.city, d.iso, c.pca1 as citypca1, d.pca1 as countrypca1, -c.negdegree as citydegree, -d.negdegree as countrydegree, ur.pca1 as countryallpca1, c.pca1-d.pca1 AS diff, c.n_nodes
        FROM cities.cities_disconnectivity_10 c, disconnectivity_iso_planet_10_ghsl_urban d,  disconnectivity_iso_planet_10_ghsl ur, cities.citylookup l
WHERE stockorchange = 'citychanges' and iyear=3 and d.ghsl_yr=2014 and ur.ghsl_yr=2014 and l.iso=d.iso and l.iso= ur.iso and l.city=c.city order by diff desc;
        """)
        print(('\n'.join([str(x) for x in ts])))
        df =pd.DataFrame(ts, columns = ['city','iso','citypca1', 'countrypca1', 'citydegree', 'countrydegree', 'countryallpca', 'cdiffpca1', 'n_nodes'])
        df['absdiff'] = df.cdiffpca1.abs()
        df['urbandiff'] = df.countrypca1
        df = df.sort_values('absdiff', ascending=False)
        #List regions for some countries?
        print((df.query('iso=="BRA"')))
        print((df.query('iso=="MEX"')))
        print((df.query('iso=="THA"')))
        #print( df[df.n_nodes>1000])
        dfb = df[df.n_nodes>3000]
        print(dfb)

        
    if runmode == 'Bangkok_clusters':
        db = osmt.pgisConnection(verbose=True)
        dfd = db.execfetch("select cluster, dist2centroid from kmeans_k10_10_urban as k, city_cell_lookup_10 as l where l.city='Bangkok' and l.row=k.row and l.col= k.col;")
        df = pd.DataFrame([list(r) for r in dfd], columns = ['cluster','dist'])
        print((df.cluster.value_counts()))
        
    if runmode == 'robustness-allways-cell':
        compare_two_robustness_variants('cell')            
    if runmode == 'robustness-allways-id5':
        robustness_variants_list_id5_examples()            
        compare_two_robustness_variants('id_5')            
    if runmode == 'test':
        test_get_country_stocks_and_TS()
        test_add_country_data_to_df_by_iso()

        pass
    if runmode in ['countries', 'XSmaterial']:
        corrdf = compare_nodal_degree_by_world_region()#'all')

    if runmode in ['countries', 'TSmaterial', 'plot_trends_by_country']:
        analyzer = osm_ghsl_analysis()
        if 0:
            analyzer.kludge_make_fakeplanet()
        analyzer.plot_trends_by_country()

    if runmode in ['scatterplots', 'XSmaterial']:
        analyzer = osm_ghsl_analysis()
        analyzer.country_scatterplotmatrices_ghslyear('iso')  # (None, decimate=False)
        analyzer.country_scatterplotmatrices_ghslyear()  # (None, decimate=False)
        
    if runmode in ['TSmaterial']:
        analyzer = osm_ghsl_analysis()
        analyzer.iso_kdes_of_key_variables_over_time()
    if runmode == 'citymaps':
        from citymaps import *
        make_small_pngs()
    if runmode == 'cleanfigures':
        from clean_figures import *
        fig1_ts()
        fig5_xs()
        fig_SI_ts_ghslvsatlas()
        fig_SI_xs_kmeans_radar()
        fig_SI_xs_sndi_distributions()
        fig6_xs_kmeans_distributions()
        fig3_ts_citymap()
        fig3_xs_kmeans_archetypes(pgf=True)
        fig4_ts_kmeans_archetypes_and_trends()
        fig5_ts_wbr_trends()
        fig4_xs_automobility()


    if runmode == 'methods_figure':  # Show diagrams explaining metrics
        from method_diagrams import circuityMaps
        circuityMaps().edgeclasses_sinuosity()
        circuityMaps().circuity_sequence_for_presentations('Hyderabad')
    if runmode == 'cities':  # Just to make clear the file organization:
        forward_arguments = [
            '--serial', '--db', '--force-update', '--cluster-radius', '--continent']
        allarguments = ' '.join([aa if vv in [True] else '' if vv in [False] else aa+'='+str(
            vv) for aa, vv in list(arguments.items()) if aa.startswith('--') and aa in forward_arguments])
        assert all([vv in [False, None] for aa, vv in list(arguments.items(
        )) if aa.startswith('--') and aa not in forward_arguments])
        print (allarguments)
        this_just_plots_not_runall_etc
        oscmd = 'python osm_cities.py plot '+allarguments
        print(oscmd)
        os.system(oscmd)
    if runmode in ['keystats', 'manuscript']:
        analyzer = osm_ghsl_analysis()
        if PRIMARY_DB:
            analyzer.reportKeyStats()
    if runmode in ['tiled_unit_graph_paradigms']:
        if 0:
            defaults['server']['postgres_db'] = 'osm10'
            update_paths_to_be_specific_to_database(defaults)
        analyse_street_network_paradigms()
        display_street_network_paradigms()
    if runmode in ['latexmk', 'XSmaterial', 'TSmaterial']:
        for ff in [L.strip().replace('osm10cars',defaults['server']['postgres_db']) for L in """
                cities-table-by-recent-pca1-osm10cars-standalone.tex
                cities-table-by-stock-pca1-osm10cars-standalone.tex
                countries-table-by-recent-fraction-deadend-osm10cars-standalone.tex
                countries-table-by-recent-length-m-per-pop-osm10cars-standalone.tex
                countries-table-by-recent-log10length-m-osm10cars-standalone.tex
                countries-table-by-recent-pca1-osm10cars-standalone.tex
                countries-table-by-stock-fraction-deadend-osm10cars-standalone.tex
                countries-table-by-stock-length-m-per-pop-osm10cars-standalone.tex
                countries-table-by-stock-log10length-m-osm10cars-standalone.tex
                countries-table-by-stock-pca1-osm10cars-standalone.tex
                PCA-coefficients-standalone.tex
                """.split('\n') if L.strip()]:
            os.system('cd {tables} && latexmk {ff}'.format(tables=paths['output']+'tables', ff=ff))
    if runmode in ['html']:
        analyzer = osm_ghsl_analysis()
        analyzer.html_country_and_city_ranking_tables(rankby='recent', metric='pca1', metric2='degree',  table_subset_suffix= 'ghsl_urban')

    if runmode in ['gen_wide_saved_file']:
        # This is just to create saved files for use by get_saved_wide_flattened_TS_dataframes:
        get_wide_flattened_TS_dataframes(table_subset_suffix='ghsl_urban')
        get_wide_flattened_TS_dataframes(table_subset_suffix='urban')


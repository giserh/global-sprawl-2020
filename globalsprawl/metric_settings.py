#!/usr/bin/python
"""
Here is where we hardcode various quantitative properties (ranges) and LaTeX-friendly display names for our disconnectivity metrics.

The master lookup (metricSettings) uses the psql names as keys, but it is also available with the long (LaTeX) names as keys (metricSettingsL).

The normal interface to some fields (plotlim, keeprange) is through function calls to methods, below.

Here is a suggested use for metricNames to rename variables from the psql keys. It tries lower case:
  df.index =df.index.map(lambda ss: metricNames.get(ss.lower(),  metricNames.get(ss,ss.replace('_','-'))))

Issues:

Right now, "plotlim" is proably only good for ISO and WBR. It could have values specified by GADM level, too, or probably just two sizes, a macro and microer scale.
"""
from pointmetrics_calculations import get_analysis_radii_suffixes
from cpblUtilities import orderListByRule
import numpy as np

def define_metric_settings():
    # define display names
    names = {'curviness': 'Sinuosity', 'logcurviness':'Log(sinuosity)', 'negdegree':'Nodal degree (negative)', 
          'fraction_1_3': 'Frc 1- and 3-degree nodes', 'fraction_deadend': 'Frc deadends', 
          'frc_edges_bridge': 'Frc bridges (N edges)', 'frc_edges_noncycle': 'Frc non-cycle (N edges)',
          'frc_length_bridge': 'Frc bridges (length)', 'frc_length_noncycle': 'Frc non-cycle (length)',
          'lognodalsparsity': 'Log nodal sparsity',
                   'pca1': 'Sprawl (SNDi)', #'Street-network disconnectedness index (SNDi)', #Sprawl PCA$_1$',
             'pca2': 'Sprawl PCA$_2$',
                   'degree':'Nodal degree',
             'log10length_m':'log(length)',
        'length_m_per_pop':'length/person (m)',
             'nodes_per_pop':'Nodes/person',
        'length_m':'length (m)',
             'log10length_m':'log$_{10}$(length/m)',
             'n_nodes':r'$N_{\rm nodes}$',
             'log10n_nodes':r'$\log_{10}\left(N_{\rm nodes}\right)$',
    }
    drNames = [(ii, str(int(ii.split('_')[0])/1000.).replace('.0','')+'--'+str(int(ii.split('_')[1])/1000.).replace('.0','')) for ii in get_analysis_radii_suffixes()]
    names.update([('distanceratio_'+dr[0],'Circuity ('+dr[1]+'km)') for dr in drNames])
    names.update([('logcircuity_'+dr[0],'Log Circuity ('+dr[1]+'km)') for dr in drNames])
    names.update([('weirdlogdistanceratio_'+dr[0],'Mean Weird Log Circuity ('+dr[1]+'km)') for dr in drNames])
    names.update([('nodedensity_'+dr[0],'Nodal density ('+dr[1]+'km)') for dr in drNames])
    names.update([('lognodalsparsity_'+dr[0].split('_')[1],'Log nodal sparsity ('+dr[1].split('-')[1]+'km)') for dr in drNames])
    # [issue 326]: suggest axis limits, axis direction, and outlier dropping criteria for each variable 
    ms =dict([(kk,dict(psqlname=kk, name=nn, longname=nn, shortname=nn, plotlim=None, keeprange=(-np.inf,np.inf),direction=+1)) for kk,nn in list(names.items())])

    ms['degree']['direction']= -1
    #ms['degree']['plotlim']= [1.66667 , 4.0] # Not sure where we want this scale, but not for figure 2. So comment it out.
    ms['negdegree']['plotlim']= [-4, -1.66667]
    #ms['distanceratio_0_500']['plotlim'] = [0.995296, 2.63854] # Argh. Does this mean distance ratios are still less than 1?
    #ms['frc_length_noncycle']['keeprange']= [0.0,1.0]
    ms['curviness']['keeprange'] = (1,1.5)
    #ms['curviness']['plotlim'] = [None, 1.25]
    #ms['pca1']['plotlim'] =[-.05,1.45] #OLD!!
    ms['pca2']['direction']= 1 # new pca2 does not need inversion, it seems..

        
    return ms

metricSettings = define_metric_settings()
metricSettingsL = dict([[ams['longname'],ams] for kk,ams in list(metricSettings.items())])
metricNames = dict([(kk,vv['name']) for kk,vv in list(metricSettings.items())])

def sensible_metric_order(include_density=False, include_variance=True, includeNegatives=False, includePositives=True, include_nonlog=False, include_log=True, long_names=False, include_everything=False):
    """ 
    Here's a short list (with include_density=False, include_variance=False) of variables to plot/show.
    If you're trying to order an existing list, use order_variables() instead.

    By default, these names are the psql names, all lower case.  Use long_names to get the metricsNames names
    """
    if include_everything:
        include_density=True
        include_variance=True
        includeNegatives=True
        includePositives=True
        include_nonlog=True
        include_log=True
        
    outl = [
     'pca1',
     'pca2',

     ]+ includePositives*['degree',]+[
     ]+ includeNegatives*['negdegree',]+[
     'fraction_1_3',
     'fraction_deadend',
         
         ]+ include_nonlog*[
     'distanceratio_0_500',
     'distanceratio_500_1000',
     'distanceratio_1000_1500',
     'distanceratio_1500_2000',
     'distanceratio_2000_2500',
     'distanceratio_2500_3000',
         ]+ include_log*[
     'logcircuity_0_500',
     'logcircuity_500_1000',
     'logcircuity_1000_1500',
     'logcircuity_1500_2000',
     'logcircuity_2000_2500',
     'logcircuity_2500_3000',
             ]+[
     'frc_length_bridge',
     'frc_length_noncycle',

     'frc_edges_noncycle',
     'frc_edges_bridge',

     'curviness',
         ]+ include_log*[
     'logcurviness',]+[
        ]+ include_density*[
     'nodedensity_0_500',
     'nodedensity_500_1000',
     'nodedensity_1000_1500',
     'nodedensity_1500_2000',
     'nodedensity_2000_2500',
     'nodedensity_2500_3000',

     'lognodalsparsity',
     'lognodalsparsity_2000',
     'lognodalsparsity_1000',
     'lognodalsparsity_3000',
     'lognodalsparsity_1500',
     'lognodalsparsity_2500',
     'lognodalsparsity_500',
        ]+ include_variance*[
     'v1_explained_variance_ratio',
        'v2_explained_variance_ratio',
    
    ]
    if long_names:
        return [metricNames.get(vv,vv) for vv in outl]
    return outl


def get_keepmask(var, values, verbose=True):
    """ Pass a variable name and a vector of values get back a vector of True/False for whether they fit in the keeprange for that variable.
    """
    lookup = metricSettings if var in metricSettings else metricSettingsL #Use psqlname or longname lookup
    newlims = lookup[var]['keeprange']
    keepers = (values >= newlims[0]) & (values <= newlims[1])
    if not verbose:
        return keepers
    nNo = len(values) - sum(keepers)
    if nNo>0:
        print(('\n\n  ** N.B. Lost {}/{} ({}%) of {} values due to keeprange enforcement (e.g.: {})\n'.format(nNo,len(keepers)-nNo,nNo*1.0/len(keepers),var, values[-keepers].values[:(min(nNo,3))])))
    return keepers

def get_plotlim(var,deflim=None):
    """ Pass a variable name and the existing xlim() or ylim() and get back a possibly-(partly) restricted new xlim/ylim.
    """
    lookup = metricSettings if var in metricSettings else metricSettingsL #Use psqlname or longname lookup
    newlims = lookup[var]['plotlim']
    if newlims is None: return deflim
    for ii in (0,1):
        if newlims[ii] is None: newlims[ii] = deflim[ii]
    return newlims

def order_variables(varlist, **argv):
    """ Given a set of variables (lowercase by default; see sensible_metric_order for other options), put them in our suggested order.
    """
    return orderListByRule(varlist, sensible_metric_order(**argv))
    
 
def add_standard_derived_metrics(df):
    """ Endow a dataframe with some extra columns based on what we keep in psql

    Caution: some of the following are coded in two places. See also fill_in_svg in aggregate...
    Caution: This acts inplace, but also returns the df.
    """
    df['degree'] = -df.negdegree
    df['log10n_nodes'] = df.n_nodes.map(np.log10)
    df['logn_nodes'] = df.n_nodes.map(np.log10)  # Deprecated
    if 'length_m' in df:
        df['log10length_m'] = np.log10(df['length_m'].astype(float))
        if 'ls_pop' in df:
            df['length_m_per_pop'] = (df['length_m'].astype(
                float)/df['ls_pop'].astype(float)).replace(np.inf, np.nan)
    if 'ls_pop' in df:
        df['nodes_per_pop'] = (df['n_nodes'].astype(
            float)/df['ls_pop'].astype(float)).replace(np.inf, np.nan)
    df['logNodalDensity_1000'] = -df.lognodalsparsity_1000
    return(df)

#!/usr/bin/python
# -*- coding: utf-8 -*
""" 
Generates clean figures for main text and SI
Figures with *xs* are for PLOS ONE 2019
Figures with *ts* are for PNAS 2019
"""
from osm_config import paths, defaults
import aggregate_connectivity_metrics as aggmetrics
import os, time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
params = { 'text.usetex': True,
           'text.latex.unicode': True,
           'text.latex.preamble':r'\usepackage{amsmath},\usepackage{amssymb}',}
plt.rcParams.update(params)
from collections import OrderedDict
import svgutils.transform as sg

import osm_cities, citymaps
import osmTools as osmt
import postgres_tools as psqlt
import kmeans_analysis as kmeans
from metric_settings import metricNames
from beyond_gadm import automobility_analysis

worldMapMax = 6 # max value for colorbar, should be consistent across maps
tsColorBarDict = {'min':1.5, 'median':4, 'max':worldMapMax} # for ts maps, ensure that sensitivity maps (e.g. osm7) are on the same scale

def fig1_ts():
    """Country-level map of SNDi for recent development"""

    oldFontSize = plt.rcParams['font.size']
    plt.rcParams['font.size'] = 12 # for colorbar label
    if not(os.path.exists(paths['scratch']+'worldmaps')): os.mkdir(paths['scratch']+'worldmaps')


    for urban in [True,False]: # create both the main plot, and one for the SI that includes non urban
        aggmetrics.fillInSvg('planet',0, defaults['osm']['clusterRadius'], 'pca1',False,2014,urban,ticks=[0,2,4,6,8,10], colorRangeDict=tsColorBarDict)
        if urban and defaults['server']['postgres_db']=='osm10': # main figure
            pdfFn = paths['graphics']+'fig1-ts-pca1-2014-urban'
        elif defaults['server']['postgres_db']=='osm10':
            pdfFn = paths['graphics']+'figSI-ts-pca1-2014'
        else:  # osm10cars or osm7
            pdfFn = paths['graphics']+'figSI-ts-pca1-2014'+'-urban'*urban+'-'+defaults['server']['postgres_db']

        # create inset
        _ ,svg2Fn  = fig1_ts_inset(urban=urban)  # if we used the returned fig rather than loading from the file, we get a weird border
        
        # merge the two
        svg1Fn = paths['scratch']+'worldmaps/planet-pca1-iso-radius{}-ghsl2014{}.svg'.format(defaults['osm']['clusterRadius'],'-urban'*urban)
        svg1=sg.fromfile(svg1Fn)
        svg2=sg.fromfile(svg2Fn).getroot()
        svg2.moveto(104,226, scale=0.78)
        svg1.append([svg2])

        svgFn = paths['scratch']+'fig1-pca1-2014{}.svg'.format('-urban'*urban)
        svg1.save(svgFn)
        print(('Saved %s' % svgFn))
        
        # to crop : --export-area=x0:y0:x1:y1, only works for PNGs. So need to crop the PDF manually
        cmd = 'inkscape '+svgFn+' --export-ignore-filters --export-pdf='+pdfFn+'-tmp.pdf'
        os.system(cmd)
        cmd = "pdfcrop --margins '-80 0 -15 0'  %s %s" % (pdfFn+'-tmp.pdf', pdfFn+'.pdf')
        os.system(cmd)
        os.remove(pdfFn+'-tmp.pdf')
        os.remove(svg2Fn)
        plt.rcParams['font.size'] = oldFontSize # change back to old detail
        print(('Saved %s.pdf' %pdfFn))
    
def fig1_ts_inset(cr=None,gadmLevel=0, urban=True):
    """kdensity plot of PCA1 over time."""
    if cr is None: cr =defaults['osm']['clusterRadius']
    db =  osmt.pgisConnection(curType='default') # default makes it easier to read to a dataframe
    
    cols = [defaults['gadm']['ID_LEVELS'][gadmLevel]]+['ghsl_yr','pca1']
    tName = 'disconnectivity_'+defaults['gadm']['ID_LEVELS'][gadmLevel]+'_planet_'+cr+'_ghsl'+urban*'_urban'
    df = pd.DataFrame(db.execfetch('SELECT '+','.join(cols)+' FROM '+tName+';'),columns=cols)
    df = df[pd.notnull(df.pca1) & (df.ghsl_yr>0)]
    yrs = [1975,1990,2000,2014]
    yrNames = [r'$<$1975','1975-89','1990-99','2000-14']
    
    fig, ax = plt.subplots(figsize=osmt.figSizes['1col'])
    colors = osmt.discrete_colors(4)
    for ii, yr in enumerate(yrs):
        df[df.ghsl_yr==yr].pca1.plot(kind='density',ax=ax,color=colors[ii])
        ax.set_xlim(-3,15)
        ax.set_xticks([0,6,12])
        ax.set_xticklabels(['0',metricNames['pca1'],'12'],fontsize=12)
        ax.set_xticks(np.arange(-2,15,2),minor=True)
        ax.set_yticks([])
        ax.set_ylabel('Kernel density',fontsize=12)
        #ax.set_xlabel(r'$PCA_1$')
    for ii, yrLabel in enumerate(yrNames):
        ax.text(14,ax.get_ylim()[1]*(0.9-ii*0.08),yrLabel,ha='right',color=colors[ii],size=12)
    figFn = paths['scratch']+'fig1-ts-inset.svg'
    #fig.tight_layout()
    #fig.set_facecolor('none')
    #fig.set_edgecolor('none')
    fig.frameon=False
    
    fig.savefig(figFn,bbox_inches='tight', pad_inches=0.1,frameon=False)
    print(('Saved %s' % figFn))
    return fig, figFn
    
def fig4_xs_automobility():
    """Plots of car ownership and frc walking by empirical type, USA"""
    automobility_analysis().gridCellPlot()

def fig5_xs():
    """Country-level map of SNDi for stock of development development"""
    aggmetrics.fillInSvg('planet',0,defaults['osm']['clusterRadius'] ,'pca1',False,False,True,ticks=[0,2,4,6,8,10], colorRangeDict={'max':worldMapMax})
    pdfFn = paths['graphics']+'fig5-xs-pca1-stock-urban'
    
    # create inset
    _ ,svg2Fn  = fig5_xs_inset()  # if we used the returned fig rather than loading from the file, we get a weird border
    
    # create a white rectangle to obscure some small islands (kludge)
    rect = sg.fromstring("""<svg xmlns="http://www.w3.org/2000/svg" id="svg2">
        <g
            id="rectlayer">
        <rect
            style="fill:#ffffff;fill-opacity:1;stroke:#000000;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
            id="whiterect"
            width="205" height="150" x="000" y="210"/>
        </g>
        </svg>""").getroot()
        
    # merge the two
    svg1Fn = paths['scratch']+'worldmaps/planet-pca1-iso-radius{}-urban.svg'.format(defaults['osm']['clusterRadius'])
    svg1=sg.fromfile(svg1Fn)
    svg2=sg.fromfile(svg2Fn).getroot()
    svg2.moveto(98,208, scale=0.95)  # 105,230, scale=0.8


    svg1.append(rect)
    svg1.append([svg2])

    svgFn = paths['scratch']+'fig5-xs-pca1-urban.svg'
    svg1.save(svgFn)
    print(('Saved %s' % svgFn))
    
    # to crop : --export-area=x0:y0:x1:y1, only works for PNGs. So need to crop the PDF manually
    cmd = 'inkscape '+svgFn+' --export-ignore-filters --export-pdf='+pdfFn+'-tmp.pdf'
    os.system(cmd)
    cmd = "pdfcrop --margins '-75 0 -8 0'  %s %s" % (pdfFn+'-tmp.pdf', pdfFn+'.pdf')
    os.system(cmd)
    os.remove(pdfFn+'-tmp.pdf')
    print(('Saved %s.pdf' %pdfFn))
    
def fig5_xs_inset(cr=None):
    """kdensity plot of PCA1 for top 5 countries"""
    if cr is None: cr =defaults['osm']['clusterRadius']

    sampDf, isos, isoNames = get_sndi_distribution(cr, n=5, includePlanet=True, weightVar='n_nodes')
    
    fig, ax = plt.subplots(figsize=osmt.figSizes['1col'])
    ax.set_alpha(0) # transparent background

    colors = osmt.discrete_colors(5) + ['#000000']
    for ii, iso in enumerate(isos):
        sampDf[sampDf.iso==iso].pca1.plot(kind='density',ax=ax,color=colors[ii])
        ax.set_xlim(-3,15)
        ax.set_xticks([0,6,12])
        ax.set_xticklabels(['0',metricNames['pca1'],'12'],fontsize=10)
        ax.set_xticks(np.arange(-2,15,2),minor=True)
        ax.set_yticks([])
        ax.set_ylabel('Kernel density (node weighted)',fontsize=10)
        #ax.set_xlabel(r'$PCA_1$')
    for ii, isoLabel in enumerate(isoNames):
        ax.text(7.25,ax.get_ylim()[1]*(0.9-ii*0.08),isoLabel,ha='left',color=colors[ii],size=10)
    figFn = paths['scratch']+'fig5-xs-inset.svg'
    #fig.tight_layout()
    ax.set_facecolor('none')
    fig.frameon=False

    # hack to remove right and top axis - this should be done with spines, but they reemerge when saving as svg
    ax.set_frame_on(False)
    ax.axvline(ax.get_xlim()[0], lw=1.5, color='k')
    ax.axhline(ax.get_ylim()[0], lw=1.5, color='k')

    fig.savefig(figFn,bbox_inches='tight', pad_inches=0.01,frameon=False)
    print(('Saved %s' % figFn))
    return fig, figFn
        
def fig_SI_xs_sndi_distributions(cr=None):
    """Plot for SI of distributions of SNDI for each country
    Includes unweighted and weighted
    Top 49 countries (leave one panel for legend)"""
    
    if cr is None: cr =defaults['osm']['clusterRadius']

    sampDf_wt, isos, isoNames = get_sndi_distribution(cr, n=49, weightVar='n_nodes')
    sampDf_unwt, isos, isoNames = get_sndi_distribution(cr, n=49, weightVar=None)

    fig, axes = plt.subplots(10,5,figsize=osmt.figSizes['portrait'])
    fig.subplots_adjust(hspace=0.35)
    colors = osmt.discrete_colors(2)
    for ii, (ax,iso) in enumerate(zip(axes.flatten(),isos+[''])):
        if ii==49: # don't plot in last panel - just legend
            ax.plot(0,0,label='Node\nweighted',color=colors[0])
            ax.plot(0,0,label='Unweighted',color=colors[1])
            ax.set_axis_off()
            ax.legend(fontsize=8)
            continue
        sampDf_wt[sampDf_wt.iso==iso].pca1.plot(kind='density',ax=ax,color=colors[0])
        sampDf_unwt[sampDf_unwt.iso==iso].pca1.plot(kind='density',ax=ax,color=colors[1])
        ax.set_xlim(-3,15)
        ax.set_ylim(0,0.32)
        ax.set_xticks([0,6,12])
        ax.set_xticks(np.arange(-2,15,2),minor=True)
        if ii>=45: 
            #ax.set_xticklabels(['0','6','12'],fontsize=8)
            ax.set_xlabel(metricNames['pca1'])
        else:
            ax.set_xticklabels(['','',''])
        ax.set_yticks([])
        if ii%5==0:
            ax.set_ylabel('Kernel density',fontsize=6)
        else:
            ax.set_ylabel('')
        ax.set_title(isoNames[ii],x=0.5,y=0.65,fontsize=7,bbox={'alpha':0.5,'edgecolor':None,'lw':0, 'facecolor':'white'})

    figFn = paths['graphics']+'fig-SI-xs-sndi-distributions.png'   
    fig.savefig(figFn,bbox_inches='tight', pad_inches=0.1)
    print(('Saved %s' % figFn))

def get_sndi_distribution(cr, n, includePlanet=False, weightVar=None):
    """returns dataframe to plot for SI figure and inset to world map
    Returns top n countries, with grid cells weighted if weightVar is specified
    Also returns isos in order of pca1
    If includePlanet is True, then add the world as a "country" too """
    
    db =  osmt.pgisConnection(curType='default') # default makes it easier to read to a dataframe
    
    # get disconnectivity by cell for top 5 most populous countries
    isos = osmt.loadOtherCountryData().set_index('ISOalpha3').pop2012.sort_values(ascending=False).head(n).index.values
    cols = ['iso','pca1','ls_pop']
    if weightVar is not None: cols.append(weightVar)
    tName = 'disconnectivity_by_cell_'+cr
    df = pd.DataFrame(db.execfetch('SELECT '+','.join(cols)+' FROM '+tName+' WHERE urban AND iso IN '+str(tuple(isos))+';'),columns=cols)

    # get national level data, for sort order for column labels
    isoDf = pd.DataFrame(db.execfetch('SELECT iso, pca1 FROM disconnectivity_iso_planet_'+cr+'_urban WHERE iso in '+str(tuple(isos))+';'), columns=['iso','pca1'])
    isos = isoDf.sort_values('pca1').iso.values.tolist()    
        
    isoLookup = osmt.country2ISOLookup()['ISOalpha2shortName']
    isoNames = ['USA' if ii=='USA' else isoLookup[ii] for ii in isos] # make USA shorter
    
    if includePlanet:
        isos.append('WLD')
        isoNames.append('Earth')
        dfp = pd.DataFrame(db.execfetch('SELECT '+','.join(cols)+' FROM '+tName+' WHERE urban;'),columns=cols)
        dfp['iso'] = 'WLD'
        df =pd.concat([df, dfp])
    
    # create node-weighted version
    df.dropna(inplace=True)
    sampSize = 500000
    if weightVar is None:
        sampDf = df
    else:
        sampDf = pd.concat([df[df.iso==iso].sample(n=sampSize, replace=True, weights=weightVar, axis=0) for iso in isos])
    
    return sampDf, isos, isoNames
    
def fig3_ts_citymap(city='Seoul',cr=None):
    if cr is None:
        cr = defaults['osm']['clusterRadius']
        assert cr
    cm =citymaps.cityMaps(cities=city,cr=cr)
    cm.mapOneCity(city,mainFig=True)
    fn = paths['scratch']+'citymaps/'+city+'_'+cr
    outFn = paths['graphics']+'fig3-ts-citymap-'+city+'.png'
    os.system('optipng -o3 %s.tiff' % fn)
    os.system('mv %s.png %s' % (fn, outFn))
    print(('Saved %s' % outFn))
    
def fig3_xs_kmeans_archetypes(pgf=True, # Set to False to get Multi to work (for the moment)
):
    colsToUse = kmeans.kmDefaultCols
    km = kmeans.kmeans_analyzer(nClusters=kmeans.defaultK, columns=[cc for cc in colsToUse], byCell=True,clusterradius= defaults['osm']['clusterRadius'], dropClusters=kmeans.kmDefaultDrop,sortCol='pca1')
    km.postgres2clusters()
    print(('Per cent of nodes in dropped clusters:\n%.1f' % ((km.n_nodes[km.df.cluster.isin(kmeans.kmDefaultDrop)].sum()*100./km.n_nodes.sum()))))
    
    km.chartClosestMembers_multi(n=12, figFn = paths['graphics']+'fig-SI-xs-kmeans-archetypes-multi')
    
    figFn = paths['graphics']+'fig3-xs-kmeans-archetypes'
    km.chartClosestMembers(figFn = figFn+'-tmp.pdf', xaxType='sndi', includeDensity=False,
                           pgf= pgf, # Color instead of bold for letter names.
                            specifyExamples= kmeans.defaultExamples)
    cmd = "pdfcrop --margins '3 3 3 3'  %s %s" % (figFn+'-tmp.pdf', figFn+'.pdf')
    os.system(cmd)
    os.remove(figFn+'-tmp.pdf')
    print(('Saved cropped version as %s.pdf' % figFn))
        

def fig4_ts_kmeans_archetypes_and_trends(pgf=True):
    """Repeat of XS figure, except shows distribution over time in center panel"""
    colsToUse = kmeans.kmDefaultCols
    km = kmeans.kmeans_analyzer(nClusters=kmeans.defaultK, columns=[cc for cc in colsToUse], byCell=True,clusterradius= defaults['osm']['clusterRadius'], dropClusters=kmeans.kmDefaultDrop,sortCol='pca1')
    km.postgres2clusters()
    print(('Per cent of nodes in dropped clusters:\n%.1f' % ((km.n_nodes[km.df.cluster.isin(kmeans.kmDefaultDrop)].sum()*100./km.n_nodes.sum()))))
       
    figFn = paths['graphics']+'fig4-ts-kmeans-archetypes'
    km.chartClosestMembers(figFn = figFn+'-tmp.pdf', xaxType='ghsl', includeDensity=False,
                           pgf= pgf, # Color instead of bold for letter names.
                           specifyExamples= kmeans.defaultExamples)
    cmd = "pdfcrop --margins '3 3 3 3'  %s %s" % (figFn+'-tmp.pdf', figFn+'.pdf')
    os.system(cmd)
    os.remove(figFn+'-tmp.pdf')
    print(('Saved cropped version as %s.pdf' % figFn))
        

def fig5_ts_wbr_trends():
    colsToUse = kmeans.kmDefaultCols
    km = kmeans.kmeans_analyzer(nClusters=kmeans.defaultK, columns=[cc for cc in colsToUse], byCell=True,clusterradius=defaults['osm']['clusterRadius'], dropClusters=kmeans.kmDefaultDrop,sortCol='pca1')
    km.postgres2clusters()

    km.chartClustersbyWBR(figFn = paths['graphics']+'fig5-ts-kmeans-freqs-by-WBR', mainFig=True)
    km.chartClustersbyWBR(figFn = paths['graphics']+'figSI-ts-kmeans-freqs-by-WBR')


def fig6_xs_kmeans_distributions():
    """Plots distribution of kmeans by country and city (for main text)
       and also larger versions by country and city for SI"""
    colsToUse = kmeans.kmDefaultCols
    km = kmeans.kmeans_analyzer(nClusters=kmeans.defaultK, columns=[cc for cc in colsToUse], byCell=True,clusterradius=defaults['osm']['clusterRadius'],
                             dropClusters=kmeans.kmDefaultDrop,sortCol='pca1')
    km.postgres2clusters()
    
    # main paper version
    km.chartClustersbyCountryorCity(n=10, by='both', figFn=paths['graphics']+'fig6-xs-kmeans-distributions.pdf')
    
    # SI versions
    km.chartClustersbyCountryorCity(n=100, by='country', figFn=paths['graphics']+'figSI-xs-kmeans-distributions-country.pdf')
    km.chartClustersbyCountryorCity(n=None, by='city', figFn=paths['graphics']+'figSI-xs-kmeans-distributions-city.pdf')
       
def fig_SI_ts_ghslvsatlas(cr=None,forceUpdate=False):
    """Plots year built CDF for ghsl vs atlas.  """
    if cr is None: cr =defaults['osm']['clusterRadius']
    # To turn on Tex, call following
    if 0:
        figureFontSetup(uniform=12,figsize='paper')
    else:
        # or if you're trying to avoid cpblUtils...
        params = {
           'text.usetex': True,
           'text.latex.unicode': True,
            'text.latex.preamble':r'\usepackage{amsmath},\usepackage{amssymb}',
           }
        plt.rcParams.update(params)
    
    pandasFn = paths['scratch']+'ghsl_atlas_comparison_'+cr+'.pandas'
    atlas = osm_cities.city_atlas()
    if not(os.path.exists(pandasFn)) or forceUpdate:
        db =  osmt.pgisConnection()
        results = db.execfetch('''select count(*), city, ghsl_yr, iyear 
            FROM nodes_planet_{} n, citychanges c 
            WHERE ST_Intersects(n.geog::geometry, c.geom) 
            GROUP BY ghsl_yr, iyear, city order by city, ghsl_yr, iyear'''.format(cr))
        df = pd.DataFrame([list(rr) for rr in results],columns=['n','city','ghsl_yr','atlas_yr'])
        df.atlas_yr = df.atlas_yr.apply(lambda x: atlas.atlasyears_xticks[x-1])
        df = df.merge(atlas.get_list_of_cities()[['name','ISO']], left_on='city',right_on='name').drop('name',axis=1)
        
        df.to_pickle(pandasFn)
    else:
        df = pd.read_pickle(pandasFn)

    df = df[df.ghsl_yr>0]
    colors = osmt.discrete_colors()[5]
    continentsWBR = OrderedDict([
          ['WLD',       'World'],
          ['AFR',       'Africa'],
          ['EUU',       'European Union'],
          ['SAS',       'S. Asia'],
          ['EAS',       r'E. Asia \& Pacific'],
          ['NAC',       'North America'],
          ['LCN',       r'Latin America \& Carib'],#bean'],
          ['MEA',       'Mid-East \& N. Africa'],
    ])
    wbrLookup = osmt.country2WBregionLookup().ISOalpha3

    fig, axes = plt.subplots(2,4,figsize = (osmt.figSizes['full'][0],osmt.figSizes['full'][1]*1.5))
    for ii, (wbr,wbrname) in enumerate(continentsWBR.items()):
        row = int(ii/4.)
        col = ii%4
        mask = df.ISO.isin(wbrLookup.loc[wbr])
        
        (df[mask].groupby('ghsl_yr').n.sum().cumsum()*1./10e3).plot(color=colors[0],ax=axes[row,col],label='GHSL')
        (df[mask].groupby('atlas_yr').n.sum().cumsum()*1./10e3).plot(color=colors[1],ax=axes[row,col],label='Atlas')
        axes[row,col].set_title(wbrname,fontsize=8)
        axes[row,col].yaxis.set_major_locator(MaxNLocator(3))
        axes[row,col].set_yticklabels(axes[row,col].get_yticks().astype(int), fontsize=7)
        axes[row,col].set_xlabel('')
        
        if col==0: axes[row,col].set_ylabel('Cumulative nodes 10$^3$',size=7)
        axes[row,col].set_xticks([1975,1990,2000,2014])
        if row==1: 
            axes[row,col].set_xticklabels(axes[row,col].get_xticks(), fontsize=7)
        else:
            axes[row,col].set_xticklabels([])
        
        if wbr=='WLD':
            y1 = (df[mask].groupby('ghsl_yr').n.sum().cumsum()*1./10e3).loc[1990]+20
            y2 = (df[mask].groupby('atlas_yr').n.sum().cumsum()*1./10e3).loc[1999]-50
            #axes[row,col].legend(loc=4,fontsize=7)
            axes[row,col].text(1990,y1,'GHSL',color=colors[0],fontsize=7,ha='right')
            axes[row,col].text(1999,y2,'Atlas',color=colors[1],fontsize=7)
            
    #fig.tight_layout()
    fig.subplots_adjust(wspace=0.28,hspace=0.25)
    figFn = paths['graphics']+'figSI-xs-ghsl-vs-atlas-CDF.pdf'
    figFn = paths['graphics']+'Fig-SI-ghsl-vs-atlas-CDF.pdf'
    fig.savefig(figFn)
    print(('Saved %s' % figFn))
    plt.close()

def fig_SI_xs_kmeans_radar():
    colsToUse = kmeans.kmDefaultCols
    km = kmeans.kmeans_analyzer(nClusters=kmeans.defaultK, columns=[cc for cc in colsToUse], byCell=True, clusterradius= defaults['osm']['clusterRadius'], dropClusters=kmeans.kmDefaultDrop,sortCol='pca1')
    km.defineClusters(sortOrder='asc')
    
    km.radarChart(figFn = paths['graphics']+'figSI-xs-kmeans-radar.pdf')

if __name__ == '__main__':
    import inquirer
    runmode = inquirer.prompt([inquirer.List('A', message='Run which mode?', choices=[
        'run all',
        'fig3_xs_kmeans_archetypes with color text',
        'GHSL_vs_Atlas',
        'dev',
            ])])['A']
    if runmode in ['run all', 'fig3_xs_kmeans_archetypes with color text']:
        fig3_xs_kmeans_archetypes(pgf=True)
    if runmode in ['GHSL_vs_Atlas']:
        fig_SI_ts_ghslvsatlas()        
    if runmode in ['run all']:
        fig1_ts()
        fig5_xs()
        fig_SI_xs_ghslvsatlas()
        fig_SI_xs_kmeans_radar()
        fig_SI_xs_sndi_distributions()
        fig6_xs_kmeans_distributions()
        fig5_ts_wbr_trends()
        fig3_ts_citymap()
        
    if runmode in ['dev']:
        pass
        

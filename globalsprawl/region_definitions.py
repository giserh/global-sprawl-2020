from collections import OrderedDict



continentsWBR = OrderedDict([
    #['WLD',       'World'],
    ['AFR',       'Africa'],
    ['EUU',       'European Union'],
    ['EAS',       r'E. Asia \& Pacific'],
    ['SAS',       'S. Asia'],
    ['NAC',       'North America'],
    ['LCN',       r'Latin America \& Carib'],  # bean'],
    ['MEA',       'Mid-East \& N. Africa'],
])
developmentWBR = OrderedDict([
    ['HIC',       'High income'],
    ['MIC',       'Middle income'],
    ['LIC',       'Low income'],
    #          ['ARB',       'Arab World'],
    ['HPC',       'HIPC'],  # Heavily indebted poor countries (
])
